-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2022 at 01:00 PM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `otpsend`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts_master`
--

CREATE TABLE IF NOT EXISTS `contacts_master` (
  `cId` int(11) NOT NULL,
  `contact_type` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `contact_otp` varchar(10) NOT NULL,
  `contact_verify` varchar(10) DEFAULT NULL,
  `otp_gen_time` varchar(50) DEFAULT NULL,
  `otp_veri_time` varchar(50) DEFAULT NULL,
  `counter` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts_master`
--

INSERT INTO `contacts_master` (`cId`, `contact_type`, `contact`, `contact_otp`, `contact_verify`, `otp_gen_time`, `otp_veri_time`, `counter`) VALUES
(26, 'mobile', '8368348821', '6103', 'YES', '01/04/2022 17:23:15', '01/04/2022 17:23:25', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts_master`
--
ALTER TABLE `contacts_master`
  ADD PRIMARY KEY (`cId`),
  ADD UNIQUE KEY `contact_type` (`contact_type`,`contact`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts_master`
--
ALTER TABLE `contacts_master`
  MODIFY `cId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
