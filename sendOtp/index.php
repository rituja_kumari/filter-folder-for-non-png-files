<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Send OTP</title>
    <script type="text/javascript" src="jquery-1.9.1.js"></script>
    <script>
            function contact_verify_get_code(type,value){
            var data = {} ;
            url = 'ajax_send_otp.php?contact_type='+type;
            data[type] = value;
            $("#err_"+type).html('');
            $.ajax({
                url:url,
                data:data,
                method:'POST',
                success:function(resp){

                jdata = JSON.parse(resp);

                $.each(jdata.message,function(index,value){
                    $("#err_"+index).html(value);
                });
                }

            });
        }


        function contact_verify(type,value){
            var data = {} ;
            url = 'ajax_verify_otp.php?contact_type='+type;
            data[type] = value;
            data['contact_otp'] = $("#"+type+'_veri_code').val();
            $("#err_"+type).html('');
            $.ajax({
                url:url,
                data:data,
                method:'POST',
                success:function(resp){

                jdata = JSON.parse(resp);

                $.each(jdata.message,function(index,value){
                    $("#err_"+index).html(value);
                });
                }

            });
        }
    </script>
</head>
<body>
<form name="form" id="form" enctype="multipart/form-data" method="post" >   
    <div>
    <label>Mobile No.</label>
        <input name="mobile" id="mobile" class="input" type="text" size="10" maxlength="10" placeholder="Mobile no. of the Candidate" require title="Mobile no must be only 10 digits." />  
        <input type="button" onclick="contact_verify_get_code('mobile',document.getElementById('mobile').value);" value="Send OTP">
        <input name="mobile_veri_code" id="mobile_veri_code" class="input" type="password" size="4" maxlength="4" placeholder="Code" require title="" value=""/>  
        <input href="javascript:void(0);" type="button" onclick="contact_verify('mobile',document.getElementById('mobile').value);" value="Verify" />
    </div>
    <div class="error" id="err_mobile">
    </div>
    </form>   
</body>
</html>