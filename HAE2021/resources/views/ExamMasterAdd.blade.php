<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	   @include('include.head')
     <script src="{{ url('/') }}/js/jquery.min.js"></script>
      <script src="{{ url('/') }}/js/owl.carousel.min.js"></script>
      <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      <script src="{{ url('/') }}/js/custom.js"></script>
</head>
<body class="antialiased">
  <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-plus
                                          "></i>
                                       </span> Add Exam Master
                                    </h3>
                                 </div>
                                 <div class="table-div">
                                    <div class=" t-left">
                                       <a href="em-list" class="button-web"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
                                    </div>
                                    <div>
                                       <form method="post" action="em-submit">
                                          @csrf
                                          <table class="table-records">
                                            <tr>
                                                 <td style="text-align: left;"><label  class="input-label">Faculty <span style="color:red">*</span></label></td>
                                                 
                                                 <td style="text-align: left;"><label  class="input-label">Course <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Branch <span style="color:red">*</span></label></td>

                                                   <td style="text-align: left;"><label  class="input-label">Serial No <span style="color:red">*</span></label></td>
                                               </tr>
                                                
                                               <tr>
                                              <td>
                                                 <select class="form-control1" name="FcId" id="FcId" required>
                                                      <option value="">Select</option>
                                                      @foreach($faculty as $key => $value)
                                                      <option value="{{$key}}">{{$value}}</option>
                                                      @endforeach
                                                   </select>
                                              </td>
                                              <td>
                                                  <select class="form-control1" name="CfCoId" id="CfCoId" required>
                                                        <option value="">Select</option>
                                                            
                                                 </select>
                                              </td>
                                              <td>
                                                 <select class="form-control1" name="CfBrId" id="CfBrId" required>
                                                      <option value="">Select</option>
                                                          
                                               </select>
                                              </td>
                                                 <td><input type="number" name="EmSrNo" required placeholder="Enter Serial No" class="form-control1"></td>
                                              </tr>
                                              <!-- 1st -->
                                              <tr>
                                                <td style="text-align: left;"><label  class="input-label">Exam Code  <span style="color:red">*</span></label></td>
                                                
                                                <td style="text-align: left;" colspan="2"><label  class="input-label" >Exam Name <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Total Subjects <span style="color:red">*</span></label></td>
                                                
                                              </tr>

                                              <tr>
                                                <td><input type="text" name="EmCode" required placeholder="Enter Exam Code" class="form-control1"></td>

                                                <td colspan="2"><input type="text" name="EmName" required placeholder="Enter Exam Name" class="form-control1"></td>

                                                <td><input type="number" name="EmTotSub" required placeholder="Enter Total Subjects" class="form-control1"></td>
                                              </tr>
                                              <!-- 2nd row -->
                                              <tr>
                                                
                                                <td style="text-align: left;"><label  class="input-label">Total Marks <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Minimum Credits <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Maximum Credits <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Passing Percentage <span style="color:red">*</span></label></td>
                                              </tr>
                                              <tr>
                                                  <td><input type="number" name="EmMaxTot" required placeholder="Enter Total Marks" class="form-control1"></td>

                                                  <td><input type="number" name="EmMinCr" required placeholder="Enter Minimum Credits" class="form-control1"></td>

                                                  <td><input type="number" name="EmMaxCr" required placeholder="Enter Maximum Credits" class="form-control1"></td>

                                                  <td><input type="number" name="EmPassPC" required placeholder="Enter Passing Percentage" class="form-control1"></td>
                                              </tr>
                                              <!-- 3rd row -->

                                              
                                              <tr>
                                                 <td style="text-align: left;"><label  class="input-label">Course Year  <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Evaluation Criteria <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Evaluation Deviation (%) <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">College Level Exam <span style="color:red">*</span></label></td>
                                              </tr>

                                              <tr>
                                                <td></td>

                                                <td><!-- evaluation criteria combo --></td>

                                                <td><input type="number" name="EmValDev" required placeholder="Enter Evaluation Deviation" class="form-control1"></td>

                                                <td>
                                                  <select class="form-control1" name="EmColg" id="EmColg" required>
                                                  <option value="">Select</option>
                                                  @foreach($YesNo as $key1 => $value1)
                                                  <option value="{{$key1}}">{{$value1}}</option>
                                                   @endforeach
                                                </select>
                                                </td>
                                              </tr>

                                              <tr>
                                                <td style="text-align: left;"><label  class="input-label">Allow Lateral Entry <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Professional Internship? <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Revaluation Available? <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Revaluation Subjects Count <span style="color:red">*</span></label></td>
                                              </tr>

                                              <tr>
                                                 <td>
                                                  <select class="form-control1" name="EmLatEntry" id="EmLatEntry" required>
                                                  <option value="">Select</option>
                                                  @foreach($YesNo as $key2 => $value2)
                                                  <option value="{{$key2}}">{{$value2}}</option>
                                                   @endforeach
                                                </select>
                                                </td>

                                                 <td>
                                                  <select class="form-control1" name="CfIntrnShpExam" id="CfIntrnShpExam" required>
                                                  <option value="">Select</option>
                                                  @foreach($YesNo as $key3 => $value3)
                                                  <option value="{{$key3}}">{{$value3}}</option>
                                                   @endforeach
                                                </select>
                                                </td>

                                                 <td>
                                                  <select class="form-control1" name="CfIsReval" id="CfIsReval" >
                                                  <option value="">Select</option>
                                                  @foreach($YesNo as $key4 => $value4)
                                                  <option value="{{$key4}}">{{$value4}}</option>
                                                   @endforeach
                                                </select>
                                                </td>

                                                <td><input type="number" name="RevalEsCount"   class="form-control1"></td>
                                              </tr>

                                              <tr>
                                                <td style="text-align: left;"><label  class="input-label">Revaluation Threshold (%) <span style="color:red">*</span></label></td>
                                               
                                                <td style="text-align: left;"><label  class="input-label">Class Improvement <span style="color:red">*</span></label></td>
                                               
                                                <td style="text-align: left;"><label  class="input-label">Grand Total Exam Sr. No. <span style="color:red">*</span></label></td>
                                                
                                                <td style="text-align: left;"><label  class="input-label">Counter For PRN <span style="color:red">*</span></label></td>
                                                
                                                <!--  -->

                                              </tr>

                                              <tr>
                                                 <td><input type="number" name="RevalThreshold"  class="form-control1"></td>

                                                 <td>
                                                  <select class="form-control1" name="CfIsImprov" id="CfIsImprov" >
                                                  <option value="">Select</option>
                                                  @foreach($CfIsImprov as $key5 => $value5)
                                                  <option value="{{$key5}}">{{$value5}}</option>
                                                   @endforeach
                                                  </select>
                                                 </td>

                                                  <td><input type="text" name="CfTotLvl"  class="form-control1" placeholder="Comma separated format 2,3,4"></td>

                                                  <td><input type="number" name="EmRgNoCnt"  class="form-control1" ></td>

                                              </tr>

                                              <tr>
                                                <td style="text-align: left;"><label  class="input-label">No. Of Groups <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Compulsory Groups <span style="color:red">*</span></label></td>

                                                <td><input type="checkbox" value="Y"  id="EmConv" name="EmConv">
                                                <label >Convocation applicable?</label></td>

                                                <td><input type="checkbox" value="Y"  id="EmSpFlag" name="EmSpFlag">
                                                <label>Appear all subject if failed</label></td>

                                              </tr>

                                              <tr>
                                                <td><input type="number" name="CfOptGrpCnt"  class="form-control1"></td>

                                                <td><input type="number" name="CfCmpGrpCnt"  class="form-control1"></td>

                                                <td><input type="checkbox" value="1"  id="EmGrpClg" name="EmGrpClg">
                                                <label>Group selection at college</label></td>

                                                 <td><input type="submit" name="submit" class="button-web"></td>
                                              </tr>

                                          </table>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')

  <!--  -->
	<!-- <a href="em-list">Back</a>
	<form method="post" action="em-submit">
		@csrf
    	<table>
            <tr>
                <td>Faculty* :</td>
                <td>
                    <select class="" name="FcId" id="FcId" required>
                     <option value="">Select</option>
                     @foreach($faculty as $key => $value)
                     <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                    </select>
                </td>
            </tr>
             <tr>
                <td>Course* :</td>
                <td>
                 <select class="" name="EmCoId" id="EmCoId" required>
                        <option value="">Select</option>
                            
                 </select>
                </td>
            </tr>
            <tr>
                <td>Branch* :</td>
                <td>
                 <select class="" name="EmBrId" id="EmBrId" required>
                        <option value="">Select</option>
                            
                 </select>
                </td>
            </tr>
             <tr>
                <td>Exam Code :</td>
                <td><input type="name" name="EmCode" required></td>
            </tr>
            <tr>
                <td>Exam Name :</td>
                <td><input type="name" name="EmName" required></td>
            </tr>
            <tr>
                <td>Exam Short Name :</td>
                <td><input type="name" name="EmShName" required></td>
            </tr>
            <tr>
                <td>Sequence :</td>
                <td><input type="number" name="EmSrNo" required></td>
            </tr>
            <tr>
                <td></td>
    			<td><input type="submit" name="submit"></td>
    		</tr>
        </table>
	</form>
 -->
<script type="text/javascript">
    //on chng faculty #FcId
    $(document).ready(function() {
       $('#FcId').on('change', function() {
        //console.log("its working..");
          var menu_id = $(this).val();
          if(menu_id) {
                $.ajax({
                   url: 'findcourse/'+menu_id,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data) {
                      console.log(data);
                      if(data){
                      $('#CfCoId').empty();
                      $('#CfBrId').empty();
                      $('#CfCoId').focus;
                      $('#CfCoId').append('<option value="">Select Course</option>');
                      $.each(data, function(key, value){
                      $('select[name="CfCoId"]').append('<option value="'+ value.CfCoId +'">' + value.CoName+ '</option>');
                   });
                }else{
                   $('#CfCoId').empty();
                   $('#CfBrId').empty();
                }
                }
                });
          }else{
             $('#CfCoId').empty();
             $('#CfBrId').empty();
          }
       });

   });

     //on chng course #EmCoId
    $(document).ready(function() {
       $('#CfCoId').on('change', function() {
        //console.log("its working..");
          var menu_id = $(this).val();
          if(menu_id) {
                $.ajax({
                   url: 'findbranch/'+menu_id,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data) {
                      console.log(data);
                      if(data){
                      $('#CfBrId').empty();
                      $('#CfBrId').focus;
                      $('#CfBrId').append('<option value="">Select Branch</option>');
                      $.each(data, function(key, value){
                      $('select[name="CfBrId"]').append('<option value="'+ value.CfBrId +'">' + value.branch_name+ '</option>');
                   });
                }else{
                   $('#CfBrId').empty();
                }
                }
                });
          }else{
             $('#CfBrId').empty();
          }
       });

   });


</script>
</body>
</html>