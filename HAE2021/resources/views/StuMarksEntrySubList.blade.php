
@php 
    $EsNameOfEndOfHierachy=json_decode($EsNameOfEndOfHierachy,true);
    $ArrOfChildEsId=json_decode($ArrOfChildEsId,true);
@endphp
<style>
table tbody tr td 
{
    padding-top:5px !important;
    padding-bottom:5px !important;
    font-size:12px!important;
}
table thead tr th
{
    padding-top:5px !important;
    padding-bottom:5px !important;
    font-size:12px!important;
}table  tr td
{
    padding-top:10px !important;
    padding-bottom:10px !important;
    font-size:15px!important;
}
.displaynoneclass
{
    
}
.tableinput{width: 60%!important;}
.table-records tr td
{padding: 4px;}
</style>
@if($mode=='modal' or $mode=='Edit')
    
    @foreach($ExamForms as $Form) 
        @php
        
             $EfRollNo=$Form->EfRollNo;
            $EfRgId=$Form->EfRgId;
             $SessionalData=json_decode(Session('data'),true);
             $Countstrlen=strlen($SessionalData['0']['CgCode'].'/');
             $EfRollNo=substr($EfRollNo,$Countstrlen);
             $FullName=$Form->FullName;
             $EfObtMarks=$Form->EfObtMarks;
             $EfResult=$Form->EfResult;
             $EfGrdPts=$Form->EfGrdPts;
             $EfPC=$Form->EfPC;
             $EfCate=$Form->EfCate;
             if($EfCate=='R')
             {
                $cate='Regular';
             }
             elseif($EfCate=='X')
             {
                $cate='Repeater';
             }elseif($EfCate=='ATKT')
             {
                $cate='ATKT';
             }
        @endphp
    @endforeach
    @foreach($CODES as $codeval)
             @php
                $CodesArrayoFetch[$codeval->CdSeq]=$codeval->CdDesc;
                $CodesArrayoFetch[$codeval->CdCode]=$codeval->CdDesc;
             @endphp
    @endforeach
    @if($mode=='modal')
        <h1>Student Information </h1>
        <table  border="0">
            <tr > <td style="text-align:left">Roll Number :  @php echo $EfRollNo;@endphp</td> </tr>
            <tr> <td style="text-align:left">Student Name : @php echo $FullName;@endphp</td> </tr>
            <tr> <td style="text-align:left">Student Category : @php echo $cate;@endphp</td>  </tr>
        </table>
        <script>   $('#AddUpdateBtn').val('Add');
        $('#AddUpdateBtn').attr('onclick',"return ajax_combo3('AddStudentInfo/','','#res','MarksEntry')");</script>
    @elseif($mode=='Edit')  
            <script>
                $('#EfRollNo').val('@php echo $EfRollNo;@endphp');
                $('#FullName').val('@php echo $FullName;@endphp');
                $('#EfCate').val('@php echo $EfCate;@endphp');
               
                $('#EfCate').css('pointer-events','none');
                $('#AddUpdateBtn').val('Update');
                $('#AddUpdateBtn').attr('onclick',"return ajax_combo3('EditStudentInfo/','','#res','MarksEntry','{{$Form->EfRgId}}')");
                $('#EfCate').attr('onchange',"return ajax_combo3('ShowRollName/','','#subjectInfo','MarksEntry')");
            </script>
    @endif
@else
    <span style="color:red;" class="displaynoneclass">( <b>**Note:</b> Please Enter <b>AB</b> To Mark As Absent )</span>
    @php  $EfCate=$request->EfCate;@endphp

@endif

<div style="overflow: auto;">
<table class="table-records mt-0 displaynoneclass" border="1" >
<tbody>
               <tr>
                  <th style="text-align:center;width:5%;">Sr No.</th>
                  <th style="text-align:left;" >Subject Name</th>
                  <th style="width:8%;">Max Marks</th>
                  <th style="width:8%;">Min Marks</th>
                  <th style="width: 12%;">Obt Marks</th>
                  <th style="width:10%">Status</th>
                  @if($EfCate!='R' or $EfCate=='' ) 
                    <th style="width:15%;">Pass/Exam Year</th> 
                    <th style="width:10%;">Center Code</th> 
                    <th style="width:10%;">Roll No </th> 
                  @endif
               </tr>
          
           
                @php $a=1; @endphp
                @foreach($ArrForEndOfSubjectId as $EndSubjectId)
                    <tr>
                    <td >@php echo $a++; @endphp </td>
                        <td style="text-align:left">
                            @php 
                                $SubjectNAme='';
                                if($EndSubjectId['EsPSubId']!=$EndSubjectId['EsId'])
                                {
                                    $SubjectNAme=$EsNameOfEndOfHierachy[$EndSubjectId['EsPSubId']]." - ";
                                }  
                                 echo $SubjectNAme=$SubjectNAme.' '.$EsNameOfEndOfHierachy[$EndSubjectId['EsId']];
                            @endphp</td>
                        <td>
                            @php echo $MaximumMarks=(int)$EndSubjectId['EsMaxMarks']; @endphp
                            <input type="hidden" name="ArrForMaxMarks[{{$EndSubjectId['EsId']}}]"  value="{{$MaximumMarks}}">
                            <input type="hidden" name="ArrEsPSubId[{{$EndSubjectId['EsId']}}]"  value="{{$EndSubjectId['EsPSubId']}}">
                            <input type="hidden" name="ArrForEsName[{{$EndSubjectId['EsId']}}]" value="@php echo $SubjectNAme;  @endphp">
                            <input type="hidden" name="ArrForChildEsId[{{$EndSubjectId['EsId']}}]" value="@php echo $ArrOfChildEsId[$EndSubjectId['EsId']];  @endphp">
                        </td>
                        <td>
                            @php  $MinimumMarks=(int)$EndSubjectId['EsPassMarks']; if($MinimumMarks>0){ echo $MinimumMarks; } @endphp
                            <input type="hidden" name="ArrForPassMarks[{{$EndSubjectId['EsId']}}]" value="{{$MinimumMarks}}">
                        </td>
                        @if($mode=='0' or $mode=='Edit')
                            <td >
                                <input type="text" name="ArrForObtMarks[{{$EndSubjectId['EsId']}}]" id="ArrForObtMarks_{{$EndSubjectId['EsId']}}" max="{{$EndSubjectId['EsMaxMarks']}}" onchange="ajax_combo3('AutoCalcMarks/','','#ShowAutoObt','MarksEntry')" onkeyup="this.value = this.value.toUpperCase();" value="NA" class="tableinput">
                            </td>
                            <td >
                                @if($EfCate!='R' or $EfCate=='' )  
                                 <select name="ArrForPassFailsStatus[{{$EndSubjectId['EsId']}}]" id="ArrForPassFailsStatus_{{$EndSubjectId['EsId']}}">
                                    <option value="">Select</option>
                                    @foreach($CODES as $codeval)
                                       
                                        @if($codeval->CdSeq!='-11' and $codeval->CdSeq!='4')
                                            <option value="{{$codeval->CdSeq}}">{{$codeval->CdDesc}}</option>
                                        @endif
                                        
                                    @endforeach
                                    <option value="-11">NA</option>
                                </select> 
                                <!-- <input type="hidden" name="ArrForPassFailsStatus[{{$EndSubjectId['EsId']}}]" id="ArrForPassFailsStatus_{{$EndSubjectId['EsId']}}" value="0" > -->
                                @else
                                    <span id="SpanPassFailsStatus_{{$EndSubjectId['EsId']}}"></span> 
                                    <input type="hidden" name="ArrForPassFailsStatus[{{$EndSubjectId['EsId']}}]" id="ArrForPassFailsStatus_{{$EndSubjectId['EsId']}}"  value="0">
                                @endif
                            </td>
                            @if($EfCate!='R' or $EfCate=='' )
                                 <td><input type="text" name="PassedYear[{{$EndSubjectId['EsId']}}]" id="PassedYear_{{$EndSubjectId['EsId']}}" style="width: 132px;" class="datepicker tableinput" ></td> 
                                 <td><input type="text" name="PrevCenter[{{$EndSubjectId['EsId']}}]" id="PrevCenter{{$EndSubjectId['EsId']}}" style="width: 132px;"></td> 
                                 <td><input type="text" name="PrevRoll[{{$EndSubjectId['EsId']}}]" id="PrevRoll{{$EndSubjectId['EsId']}}" style="width: 132px;"></td> 
                               
                            @else 
                                <input type="hidden" name="PassedYear[{{$EndSubjectId['EsId']}}]" style="width: 132px;" >
                                <input type="hidden" name="PrevCenter[{{$EndSubjectId['EsId']}}]" id="PrevCenter{{$EndSubjectId['EsId']}}">
                                <input type="hidden" name="PrevRoll[{{$EndSubjectId['EsId']}}]" id="PrevRoll{{$EndSubjectId['EsId']}}">
                            @endif
                        @else 
                            @foreach($StudentMArksInfo as $MarksInfo)
                                @if($MarksInfo->ChildEsId==$ArrOfChildEsId[$EndSubjectId['EsId']])
                                    <td> 
                                        @if($MarksInfo->ObtMarks=='-11.10')
                                            AB
                                        @elseif($MarksInfo->ObtMarks=='-12.00')
                                            NA
                                        @else
                                            {{$MarksInfo->ObtMarks}}
                                        @endif </td>
                                    <td> {{$CodesArrayoFetch[$MarksInfo->PassFailStat] }}</td>
                                    @if($EfCate!='R' or $EfCate=='' )
                                        <td> {{$MarksInfo->GradeDesc}}</td>
                                    @endif
                                @endif
                            @endforeach
                          
                        @endif
                        @if($mode=='Edit')
                            @foreach($StudentMArksInfo as $MarksInfo)
                                    @if($MarksInfo->ChildEsId==$ArrOfChildEsId[$EndSubjectId['EsId']])
                                    
                                        @if($MarksInfo->ObtMarks=='-11.10')
                                            <script>$("#ArrForObtMarks_{{$EndSubjectId['EsId']}}").val('AB');</script>
                                        @elseif($MarksInfo->ObtMarks=='-12.00')
                                            <script>$("#ArrForObtMarks_{{$EndSubjectId['EsId']}}").val('NA');</script>
                                        @else
                                            <script>$("#ArrForObtMarks_{{$EndSubjectId['EsId']}}").val('{{$MarksInfo->ObtMarks}}');</script>
                                        @endif 
                                        @php 
                                            if($MarksInfo->PassFailStat=='1'){   $PassFailVal='Pass';}elseif($MarksInfo->PassFailStat=='0'){$PassFailVal='Fail';}else{ $PassFailVal='';}   
                                            $ExtraInfo=json_decode($MarksInfo->Extra,true);
                                            $PrevCenter=$PrevRollNo='';
                                            $PrevCenter=$ExtraInfo["PrevCenter"];
                                            $PrevRollNo=$ExtraInfo["PrevRollNo"];
                                        @endphp
                                        <script>
                                            $("#ArrForPassFailsStatus_{{$EndSubjectId['EsId']}}").val('{{$MarksInfo->PassFailStat}}');
                                            $("#SpanPassFailsStatus_{{$EndSubjectId['EsId']}}").html('{{$PassFailVal}}');
                                            $("#PrevCenter{{$EndSubjectId['EsId']}}").val('{{$PrevCenter}}');
                                            $("#PrevRoll{{$EndSubjectId['EsId']}}").val('{{$PrevRollNo}}');
                                           
                                        </script>   
                                          
                                        @if($EfCate!='R' or $EfCate=='' )
                                            <script>$("#PassedYear_{{$EndSubjectId['EsId']}}").val('{{$MarksInfo->GradeDesc}}');</script> 
                                        @endif
                                    @endif
                                @endforeach
                        @endif
                    </tr>
                @endforeach
            <tr>
                <td colspan="2" align="right"> Grand Total</td>
               
                <td> @php echo intval($EmMaxTot); @endphp <input type="hidden" name="TotalMaxMarks" value="{{$EmMaxTot}}"</td>
                <td >  @php echo intval($EmPassPC); @endphp</td>
                <td id="ShowAutoObt">@if($mode=='modal' or $mode=='Edit') @php echo $EfObtMarks; @endphp  @endif</td>
                <td></td>
                @if($EfCate!='R' or $EfCate=='' ) <td></td> <td></td> <td></td> @endif
            </tr>
            <tr style="display: none;">
                <td colspan="2"> 
                    @if($mode=='modal')
                        Final Status:   {{$CodesArrayoFetch[$EfResult] }}
                    @else
                        <select name="FinalResultStatus" class="form-control1">
                            <option value="">Final Status</option>
                            @foreach($CODES as $codeval)
                                <option value="{{$codeval->CdCode}}">{{$codeval->CdDesc}}</option>
                            @endforeach
                        </select>
                    @endif
                </td>
                        
                <td colspan="3">
                    @if($mode=='modal')
                        @foreach($CLASSCODES as $codeval)
                            @if($codeval->CdCode==$EfGrdPts)
                               Class:  {{$codeval->CdDesc}}  
                            @endif
                        @endforeach
                    @else
                        <select name="FinalResultClass" class="form-control1">
                            <option value="">Class</option>
                            @foreach($CLASSCODES as $codeval)
                                <option value="{{$codeval->CdCode}}">{{$codeval->CdDesc}}</option>
                            @endforeach
                        </select>
                    @endif
                </td>
                    
                <td> @if($mode=='modal') Percentage: {{$EfPC}} @endif</td>
            </tr>
            </tbody>
         </table> 
         </div>
        <div class="col-lg-12 t-right col-md-12">
            <input type="button" class="btn-admin mt-30 IsSubmit_disabled " type="button" id="AddUpdateBtn" value="Add" onclick="ajax_combo3('AddStudentInfo/','','#res','MarksEntry')">
        </div><script>
   $(function() {
      $(".datepicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
});
</script>