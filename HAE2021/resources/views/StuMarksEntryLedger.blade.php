@php 
    $EsNameOfEndOfHierachy=json_decode($EsNameOfEndOfHierachy,true);
    $ArrOfChildEsId=json_decode($ArrOfChildEsId,true);
    if($SpCl_value!='LedgerInPdf')
    {
        $Selcourse=$request->input('Selcourse');
        $EncSelcourse=base64_encode(get_encrypt($Selcourse));
        $Selexam=$request->input('Selexam');
        $EncSelexam=base64_encode(get_encrypt($Selexam));
        $Selbranch=$request->input('Selbranch');
        $EncSelbranch=base64_encode(get_encrypt($Selbranch));
    }
    
@endphp
@foreach($CODES as $codeval)
    @php
    $CodesArrayoFetch[$codeval->CdSeq]=$codeval->CdDesc;
    $CodesArrayoFetch[$codeval->CdCode]=$codeval->CdDesc;
   
    @endphp
@endforeach
<style>
table tbody tr td 
{
    padding-top:5px !important;
    padding-bottom:5px !important;
    font-size:12px!important;
}
table thead tr th
{
    padding-top:5px !important;
    padding-bottom:5px !important;
    font-size:12px!important;
}table  tr td
{
    padding-top:10px !important;
    padding-bottom:10px !important;
    font-size:15px!important;
}
.br-0{
    border: 0;
}
.t-right{justify-content: right;}
</style><br>
<div style="overflow: auto;"> 


@php $CountOfSub=sizeof($ArrForEndOfSubjectId)+6; @endphp
<table   class="table-records" border="1" style="border: 0;border-collapse: collapse;" > <!-- style="word-wrap:break-word"-->
    <thead>
        @if($SpCl_value=='LedgerInPdf')
            <tr >
                <th class="br-0" colspan="{{$CountOfSub}}"><h2>DIRECTORATE OF ART , MAHARASHTRA STATE, MUMBAI<br>HIGHER ART EXAMINATION 2021<br>( Course : {{$CourseName}} ) <br>College : {{$CgName}}</h2></th>    
            </tr>
            
            <tr><th class="br-0" colspan="{{$CountOfSub}}" ></th></tr>
            <tr><th class="br-0" colspan="{{$CountOfSub}}" ></th></tr>
            <tr><th class="br-0" colspan="{{$CountOfSub}}" ></th></tr>
        @endif
        <tr>
            <th rowspan="3">Seat No</th>
            @if($SpCl_value!='LedgerInPdf' and session('data')['0']['role']!='U')<th rowspan="3" class="IsSubmit_disabled">Action</th>@endif
            <th style="text-align:left">Name Of The Students</th>
                @foreach($ArrForEndOfSubjectId as $EndSubjectId)
                    <th>@php 
                        $SubjectNAme='';
                        if($EndSubjectId['EsPSubId']!=$EndSubjectId['EsId'])
                        {
                            $SubjectNAme=$EsNameOfEndOfHierachy[$EndSubjectId['EsPSubId']]." - ";
                        }  
                            echo $SubjectNAme=$SubjectNAme.' '.$EsNameOfEndOfHierachy[$EndSubjectId['EsId']];
                        @endphp
                    </th>
                @endforeach
            <th>GRNDTOT</th>
             <!--<th rowspan="3">%AGE</th>
            <th rowspan="3">GRADE</th> -->
            <th rowspan="3">RESULT</th>
           
        </tr>
        <tr>
            <th>Max</th>
            @foreach($ArrForEndOfSubjectId as $EndSubjectId)
                        <th style="text-align: center;"> @php 
                        $MaximumMarks=(int)$EndSubjectId['EsMaxMarks'];
                        if($MaximumMarks=='0')
                        {

                        }
                        else
                        {
                            echo $MaximumMarks;
                        }
                        @endphp
                       </th>
            @endforeach
            
            <th style="text-align: center;">{{$EmMaxTot}}</th>
        </tr> 
        <tr>
        <th>Min</th>
            @foreach($ArrForEndOfSubjectId as $EndSubjectId)
                        <th style="text-align: center;">
                        @php 
                        $MinimumMarks=(int)$EndSubjectId['EsPassMarks'];
                        if($MinimumMarks=='0')
                        {

                        }
                        else
                        {
                            echo $MinimumMarks;
                        }
                        @endphp 
                      </th>
            @endforeach
            <th style="text-align: center;">@php echo (int)$EmPassPC; @endphp</th>
        </tr>
    </thead>
    <tbody>
        @php  $RegularCount=$RepCount=$ATKTCount=$VacCount=0; @endphp
        @foreach($AllExamFormsDetails as $Form)

            @php
               
                if($Form['EfCate']=='R'){
                    $RegularCount++;$CategoryLabel='';
                }
                elseif($Form['EfCate']=='X'){
                    $RepCount++; $CategoryLabel="**";
                }
                elseif($Form['EfCate']=='ATKT')
                {
                    $ATKTCount++;$CategoryLabel="*";
                } elseif($Form['EfCate']=='VAC')
                {
                    $VacCount++;$CategoryLabel="***";
                }
            @endphp
            <tr>
                <td width="6%">{{$Form['EfRollNo']}}</td>
                @if($SpCl_value!='LedgerInPdf' and session('data')['0']['role']!='U')
                    <td style="text-align: center;width:25%;" class="IsSubmit_disabled">   
                        <a  class="Action-btn" onclick="ajax_combo3('DeleteStudentDetails/{{$Form->EfId}}/{{$Form->RgNo}}/','','#res','MarksEntry');"><i class="fa fa-trash"></i></a>  |

                        <a  class="Action-btn" onclick="ajax_combo3('ShowEdit/','','#subjectInfo','MarksEntry','{{$Form->RgNo}}');"><i class="fa fa-pencil"></i></a> 
                    </td>
                @endif
                <td style="text-align:left">{{$Form['FullName'].$CategoryLabel}} </td>
              
                @foreach($ArrForEndOfSubjectId as $EndSubjectId)
                        <td style="text-align: center;">
                            @foreach($AllStudentMArksInfo as $MarksInfo)
                                @php  
                                $ObtMarks='';
                                $ObtMarks=(int)$MarksInfo->ObtMarks; @endphp
                             
                                @if($MarksInfo->ChildEsId==$ArrOfChildEsId[$EndSubjectId['EsId']] and $MarksInfo->RgNo==$Form['RgNo'])
                                    @if($ObtMarks=='-11')
                                        AB
                                    @elseif($ObtMarks=='-12')
                                        NA
                                    @else
                                        @if($ObtMarks=='0')  @else {{$ObtMarks}} @endif
                                    @endif 
                                @endif
                            @endforeach
                        </td>
                @endforeach
                <td style="text-align: center;"> @if($Form->EfObtMarks=='0')  @else  {{$Form->EfObtMarks}} @endif</td>
                <td style="text-align: center;">  {{$CodesArrayoFetch[$Form->EfResult] }} </td>
               
            </tr>
        @endforeach
    </tbody>
</table>


</div>
<div id="AllExamFormsDetails">
    {{ $AllExamFormsDetails->links('pagination::bootstrap-4') }}
</div>
@if($SpCl_value!='LedgerInPdf')
    @if(session('data')['0']['role']!='U')            
        <div class="col-12 col-lg-12 col-md-12 IsSubmit_disabled" style="padding-top: 25px;">
                <center>प्रमाणपत्र</center>
            <input type="checkbox" name="Declaration" id="Declaration" value="approved" > प्रमाणित करण्यात येते की, वरील निकाल पत्रकात नमुद केलेले विद्यार्थी हे शासकीय उच्चकला परीक्षा 2021 करिता पात्र आहेत. विद्यार्थ्यांच्या नावा समोर नमुद केलेले गुण त्रिसदस्यीय समितीने तपासले असून योग्य व अचूक आहेत.
        </div>
    @endif
    <div class="row mt-30" >
        @if(session('data')['0']['role']!='U')         
            <div class="col-12 col-lg-3 col-md-3 al-center">
                <input type="button" class="btn-admin  IsSubmit_disabled" type="button" id="FinalSubmit" value="Final Submit" onclick="ajax_combo3('FinalSubmission/','','#subjectInfo','MarksEntry')">
            </div> 
        @endif
        <div class="col-12 col-lg-3 col-md-3 al-center">
            <a href="{{ url('dynamic_Ledgerpdf/'.$EncSelcourse.'/'.$EncSelexam.'/'.$EncSelbranch) }}"  class="btn-admin " target="_BLANK">View Ledger</a>
        </div>
    </div>
    <!--<div class="col-12 col-lg-12 col-md-12" style="padding-top: 25px;" >
     <a href="{{ url('dynamic_pdf/'.$EncSelcourse.'/'.$EncSelexam.'/'.$EncSelbranch) }}"  class="btn-admin mt-30 IsSubmit_disabled" target="_BLANK">Convert into PDF</a> 
    </div>-->
@else
    <table   class="table-records" border="0"  width="100%" style="padding-top:25px"> 
        <tr>
            <td> Total Students : {{ count($AllExamFormsDetails) }}</td>
            <td> Regular Students : {{ ($RegularCount) }}</td>
            <td> Repeater Students : {{ ($RepCount) }}</td>
            <td> ATKT Students : {{ ($ATKTCount) }}</td>
            <td> Vacation Students : {{ ($VacCount) }}</td>
        </tr>
    </table>
    <div class="row t-right"></div> <div class="col-md-4 "></div></div>

@endif
<script>
   $(document).ready(function() 
   {
        var timestamp = Number(new Date()); // current time as number
        $('.pagination').attr('id', timestamp);
      
        $(document).on('click', '#'+timestamp+' a', function(event)
        {
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            fetch_data(page,timestamp);
        });
        function fetch_data(page,timestamp)
        {
            $.ajax({
                data: $('#MarksEntry').serialize(),
                url:"{{ url('/fetchUniversityTable?page=') }}"+page,
                beforeSend: function() {
                    $('.body_overlay').show();
                },
                success:function(data)
                {
                    $('#subjectInfo').html(data.html);
                    $('.pagination').attr('id', timestamp);
                    $('.body_overlay').hide();
                }
                
            });
        }  
});
   </script>