<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      @include('include.head')
      <!--  <script src="{{ url('/') }}/js/jquery.min.js"></script>
         <script src="{{ url('/') }}/js/owl.carousel.min.js"></script>
         <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
         <script src="{{ url('/') }}/js/custom.js"></script> -->
      <style type="text/css">
         .justify-content-right{justify-content: flex-end;
         text-align: right;
         }
         .fa-search{color: #d3862e;}
         .body_overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: fixed;left: 0;top: 0;width: 100%;height: 100%;width: 100%;height: 100%;display: none;}
        .body_overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
      </style>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <script src="{{ url('/') }}/js/jquery.min.js"></script>
      <script src="{{ url('/')}}/js/ajax_js.js" ></script>
      <script type="text/javascript">
      $(window).ready(function() {
        $("#CenterDetails").on("keypress", function (event) {
            console.log("aaya");
            var keyPressed = event.keyCode || event.which;
            if (keyPressed === 13) {
               // alert("You pressed the Enter key!!");
                event.preventDefault();
                return false;
            }
        });
        });
      </script>
   </head>
   <body class="antialiased">
      <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-plus
                                          "></i>
                                       </span> Center Details
                                    </h3>
                                 </div>
                                 <div class="table-div">
                                    <div>
                                       <form method="post" name="CenterDetails" id="CenterDetails" action="export-excel">
                                          @csrf
                                          <div class="row t-center justify-content-right">
                                             <div class="col-lg-4">
                                                <div class="form-control1">
                                                   <input id="serchclg" type="text" style="border: 0;" placeholder="Search Here.."> <i class="fa fa-search"></i>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row t-center justify-content-center">
                                             <div class="col-lg-4">
                                                <label  class="input-label">District <span style="color:red">*</span></label>
                                                <select class="form-control1" name="CfDsId" id="CfDsId" onchange="return ajax_combo3('ShowCollege/','','#DivCollege','CenterDetails')" >
                                                   <option value="111">All Districts</option>
                                                   @foreach($district as $key => $value)
                                                   <option value="{{$key}}">{{$value}}</option>
                                                   @endforeach
                                                </select>
                                             </div>
                                             <div class="col-lg-4" id="DivCollege">
                                                <label  class="input-label">Collage <span style="color:red">*</span></label>
                                                <select class="form-control1" name="center" id="center" >
                                                   <option value="">Select</option>
                                                </select>
                                             </div>
                                             <!-- <div class="col-lg-4">
                                                <button type="submit" class="button-web mt-20" name="download" id="download" onclick="ToExcel();">Download</button>
                                             </div> -->
                                          </div>
                                       </form>
                                       <div  id="Collage-list" >
                                          <table id="userTable" class="table-records" border="1">
                                             <thead>
                                                <tr>
                                                   <th>Sr. No</th>
                                                   <th>Collage Name and College Code</th>
                                                   
                                                   <th>Course Name and Student Count</th>
                                                   <!-- <th>Action</th> -->
                                                </tr>
                                             </thead>
                                             <tbody>
                                                @php
                                                $count=1;
                                                @endphp
                                                @foreach($collegelist as $value)
                                                <tr>
                                                   <td>{{$count++}}</td>
                                                   <td>{{$value->CgName}} - {{$value->CgCode}}</td>
                                                   
                                                   <td style="text-align: left;">
                                                    @php
                                                    $Exams = \App\ExamForms::select('EfEmId','EfStrId')->where('EfCgId',$value->CgId)->distinct()->orderBy('EfEmId')->get();
                                                    $CoNo=1;
                                                    $EncCgId=base64_encode(get_encrypt($value->CgId));
                                                    @endphp
                                                    @foreach($Exams as $Sem)
                                                    
                                                    @php
                                                    $CoId= \App\Exam::where('EmId',$Sem->EfEmId)->where('EntryStat',1)->first()->CfCoId;
                                                    @endphp
                                                    <p style="font-size: smaller;">
                                                    {{$CoId}} - {{ \App\Course::where('CfCoId',$CoId)->where('EntryStat',1)->first()->CoName }} - 
                                                    
                                                    @php
                                                    echo $StuCount = \App\ExamForms::where('EfCgId',$value->CgId)->where('EfEmId',$Sem->EfEmId)->where('ResProStat','1')->count();
                                                  
                                                    $EncSelcourse=base64_encode(get_encrypt($CoId));
                                                    $EncSelexam=base64_encode(get_encrypt($Sem->EfEmId));
                                                    $EncSelbranch=base64_encode(get_encrypt($Sem->EfStrId));
                                                    
                                                    @endphp
                                                    <?php
                                                    $path='ledger/'.$value->CgId.$CoId.'.pdf';
                                                    $path1 = public_path($path);
                                                    if (file_exists($path1)) 
                                                    {?>
                                                      &nbsp;&nbsp;&nbsp;<a target='_blank'  href='public/{{$path}}' style="color: green;font-weight: bold;"> Uploaded Ledger</a><?php
                                                    }
                                                     ?>
                                                    
                                                    &nbsp;&nbsp;&nbsp;<a href="{{ url('dynamic_Ledgerpdf/'.$EncSelcourse.'/'.$EncSelexam.'/'.$EncSelbranch.'/'.$EncCgId) }}" target="_blank" style="font-weight: bold;">View Ledger</i></a><br>
                                                    </p>
                                                   
                                                    @endforeach
                                                    
                                                    </td>
                                                   <!-- <td>
                                                      @if($value->CsAuthFile)
                                                      <a href="public/uploads/{{$value->CsAuthFile}}" target="_blank"><i class="fa fa-eye"></i></a>
                                                      @else
                                                      <p style="color: red">Not Uploaded</p>
                                                      @endif
                                                   </td> -->
                                                </tr>

                                                <!-- echo "<br>".$Course="select CoName from courses where CfCoId IN (select CfCoId from cointakes where CgId='$value->CgId')"; -->
                                                @endforeach
                                                
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')
      <script type="text/javascript">
         // viwe college list on change CfDsId district
         $(document).ready(function() {
                $('#CfDsId1').on('change', function() {
                // console.log("its working..");
         
                   var menu_id = $(this).val();
                   if(menu_id) {
                         $.ajax({
                            url: 'RegisterClg/'+menu_id,
                            type: "GET",
                            data : {"_token":"{{ csrf_token() }}"},
                            dataType: "json",
                            success:function(data) {
                              var len = 0;
                              $('#userTable tbody').empty();
                              console.log(data);
                              if(data)
                              {
                               len = data.length;
                              }
                               if(len > 0)
                               {
                                 
                                 for(var i=0; i<len; i++)
                                 {
                                 
                                   var id = data[i].CgId;
                                   var CgName = data[i].CgName;
                                   var CgCode = data[i].CgCode;
                                   var CsAuthFile = data[i].CsAuthFile;
                                  console.log(typeof CsAuthFile);
                               
                                  <?php $CgId="<script>document.write(id)</script>" ?>
                                
                                   if(CsAuthFile == null){
                                       var a="<span style='color: red'>Not Uploaded</span>";
                                    }
                                    else
                                    {
                                      var a="<a target='_blank' href='public/uploads/"+ CsAuthFile  +"'><i class='fa fa-eye'></i></a>";
                                    }
         
                                   var tr_str = "<tr>" +
                                   "<td align='center'>" + (i+1) + "</td>" +
                                   "<td align='center'>" + CgName + " - "+CgCode+"</td>" +
                                   "</tr>";
                                   console.log(tr_str);
                                 
                                    $("#userTable tbody").append(tr_str);
                                 }
                             
                               }
                               else{
                                     var tr_str = "<tr>" +
                                         "<td align='center' colspan='3'>No record found.</td>" +
                                     "</tr>";
         
                                     $("#userTable tbody").append(tr_str);
                                   }
                         }
         
                         });
         
                   }
                    $('#userTable tbody').empty();
                });
         
                /*On chng center*/
         
                $('#center12').on('change', function() {
         
                    console.log("its working..");
                    var menu_id = $(this).val();
                    if(menu_id) {
                                  $.ajax({
                            url: 'RegisterClgC/'+menu_id,
                            type: "GET",
                            data : {"_token":"{{ csrf_token() }}"},
                            dataType: "json",
                            success:function(data) {
                              var len = 0;
                              var Usertbl=$('#userTable tbody');
                              $('#userTable tbody').empty();
                              console.log(data);
                              if(data)
                              {
                               len = data.length;
                              }
                               if(len > 0)
                               {
                                 
                                 for(var i=0; i<len; i++)
                                 {
                                 
                                   var id = data[i].CgId;
                                   var CgName = data[i].CgName;
                                   var CgCode = data[i].CgCode;
                                   var CsAuthFile = data[i].CsAuthFile;
                                   console.log(CsAuthFile);
                                   console.log(typeof CsAuthFile);
                                   if(CsAuthFile == null){
                                       var a="<span style='color: red'>Not Uploaded</span>";
                                    }
                                    else
                                    {
                                      var a="<a target='_blank' href='public/uploads/"+ CsAuthFile  +"'><i class='fa fa-eye'></i></a>";
                                    }
                                   var tr_str = "<tr>" +
                                   "<td align='center'>" + (i+1) + "</td>" +
                                   "<td align='center'>" + CgName + " - "+ CgCode +"</td>" +
                                   "</tr>";
                                   console.log(tr_str);
                                 
                                    $("#userTable tbody").append(tr_str);
                                 }
                             
                               }
                               else{
                                     var tr_str = "<tr>" +
                                         "<td align='center' colspan='4'>No record found.</td>" +
                                     "</tr>";
         
                                     $("#userTable tbody").append(tr_str);
                                   }
                         }
         
                         });
         
         
                    }
                    $('#userTable tbody').empty();
                 });
             
            });
         
         
         
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
                $('#CfDsId12').on('change', function() {
                // console.log("its working..");
                   var menu_id = $(this).val();
                   if(menu_id) {
                         $.ajax({
                            url: 'findcolg/'+menu_id,
                            type: "GET",
                            data : {"_token":"{{ csrf_token() }}"},
                            dataType: "json",
                            success:function(data) {
                             
                             console.log(data);
                             if(data)
                             {
                            //   $('#Collage-list').show();
                                 $('#center').empty();
                                 $('#center').focus;
                                 //console.log("its working..");
                                 $('#center').append('<option value="">Select Collage</option>');
                                 $.each(data, function(key, value)
                                 {
                                 $('select[name="center"]').append('<option value="'+ value.CgId +'">' + value.CgName+ '</option>');
                                 
                                 });
                             }
                             else
                             {
                                $('#center').empty();
                             }
                           }
                         });
                   }
                   else
                   {
                     $('#center').empty();
                   }
                });
         
                 /*Search center */
                  $("#serchclg").on("keyup", function() {
                     var value = $(this).val().toLowerCase();
                     $("#userTable tr").filter(function() {
                       $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                     });
                   });
         
            });
         
      </script>
   </body>
</html>