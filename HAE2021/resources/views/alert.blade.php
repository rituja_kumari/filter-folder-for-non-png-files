@if (session('alert'))
<script type="text/javascript">$('#myModal').modal('show');</script>
		<div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        
		        <div class="modal-body">
		          	<div class="alert alert-success alert-dismissible">
                      {{ session('alert') }}
					</div>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
  		</div>
@endif