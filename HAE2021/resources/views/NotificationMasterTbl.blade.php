<style>
table tbody tr td {
   padding-top:5px !important;padding-bottom:5px !important;
}
table thead th  {
   padding-top:5px !important;padding-bottom:5px !important;
}
</style>
<table class="table-records" border="1">
   <thead>
      <th>Sr No.</th>
      <th style="text-align: left;">Notification Title</th>
      <th>Start Date</th>
      <th>End Date</th>
      <th>Document</th>
      <th>Action</th>
   </thead>
   <tbody>
         @php($count=0)
        
         @foreach($notification_master as $key => $value)
            <tr>
               <td > {{++$count}}</td>
               <td style="text-align: left;"> {{$value->NotiTitle}}</td>
               <td > {{$value->FromDate}}</td>
               <td > {{$value->ToDate}}</td>
               <td > <a href="{{$value->NotifyFilePath}}" target="_BLANK"> Doc</a></td>
               <td>
                  <a href="Notification-delete/{{$value->NotifyId}}" class="Action-btn" onclick="return confirm('Are You Sure? Want to Delete It.');"><i class="fa fa-trash"></i></a>
               </td>
               
            </tr>
         @endforeach
</tbody>
</table>

         