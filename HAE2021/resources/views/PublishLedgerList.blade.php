@php
        
        $EncSelcourse=base64_encode(get_encrypt($CfCoId));
       
        $EncSelexam=base64_encode(get_encrypt($Selexam));
       
        $EncSelbranch=base64_encode(get_encrypt($CfBrId));
        $PublishedCgId=$FinalSubmittedCgId=array();
      //  unset($FinalSubmittedCgId);
       // unset($PublishedCgId);
        foreach($ctchangelog as $LogCgId)
        {
           $FinalSubmittedCgId[]=$LogCgId->user_id;
        }
        foreach($PUBLISH_LEDGER_Details as $PublishCgId)
        {
           $PublishedCgId[]=$PublishCgId->user_id;
        }
       
@endphp

<style>
table tbody tr td {
   padding-top:5px !important;padding-bottom:5px !important;
}
table thead th  {
   padding-top:5px !important;padding-bottom:5px !important;
}
</style>
<table class="table-records" border="1" width="100%" style="border-collapse: collapse;">
    <thead>
        <tr>
            <td>Sr No.</td>
            <td>College Name</td>
            <td>Uploaded Ledger</td>
            <td>Ledger</td>
            <td><input type="button" style="width:100px;" id="toggle" value="Select All" class="btn btn-primary btn-xs" onClick="assn_prg_do_this()" /></td>
        </tr>
    </thead>    
    <tbody>
        @php $c=1;@endphp
    
        @foreach($CollegeModel as $CollegeInfo)
            @php
             $EncCgId=base64_encode(get_encrypt($CollegeInfo->CgId));
             $EncCgCode=base64_encode(get_encrypt($CollegeInfo->CgCode));
             
            $path='ledger/'.$CollegeInfo->CgId.$CfCoId.'.pdf';
            $path1 = public_path($path);
                                                  
            @endphp
            <tr>
                <td>{{$c++}}</td>
                <td style="text-align:left">{{$CollegeInfo->CgCode}} &nbsp;  &nbsp; {{$CollegeInfo->CgName}}</td>
                <td>
                    @if (file_exists($path1))                                
                    <a href="public/{{$path}}" target="_BLANK">View</a>
                    @endif
                </td>
                
                     @if($CfCoId!='3')
                        <td> <a href="{{ url('dynamic_Ledgerpdf/'.$EncSelcourse.'/'.$EncSelexam.'/'.$EncSelbranch.'/'.$EncCgId.'/Publish') }}" target="_BLANK">View</a> </td>
                        @else
                        <td> 
                            
                            <a href="{{ url('Publish_BULK_Ledger/'.$EncCgCode) }}" target="_BLANK">View</a>
                        </td>
                    @endif
               
              
              
                @if(in_array($CollegeInfo->CgId,$FinalSubmittedCgId) and !in_array($CollegeInfo->CgId,$PublishedCgId))
                     <td><input type="checkbox" name="ArrToPublish[]" id="ArrToPublish_{{$CollegeInfo->CgId}}" value="{{$CollegeInfo->CgId}}"></td>
                   
                @else
                   <td>
                        @if(in_array($CollegeInfo->CgId,$PublishedCgId))
                            
                            <span style="color:green">Published</span>
                        @elseif(!in_array($CollegeInfo->CgId,$FinalSubmittedCgId))
                            <span style="color:red">Pending</span>
                        @endif
                   </td>
                   
                @endif
            </tr>
            <input type="hidden" name="AllColleges[]" value="{{$CollegeInfo->CgId}}">
        @endforeach
    </tbody>
    
</table>