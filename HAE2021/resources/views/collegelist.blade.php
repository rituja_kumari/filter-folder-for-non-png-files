<table id="userTable" class="table-records" border="1">
     <thead>
        <tr>
           <th>Sr. No</th>
           <th>Collage Name and College Code</th>
           
           <th>Course Name and Student Count</th>
           <!-- <th>Action</th> -->
        </tr>
     </thead>
     <tbody>
        @php

        $count=1;
        @endphp
        @foreach($collegelist as $value)
        <tr>
           <td>{{$count++}}</td>
           <td>{{$value->CgName}} - {{$value->CgCode}}</td>
           
           <td style="text-align: left;">
            @php
            $Exams = \App\ExamForms::select('EfEmId','EfStrId')->where('EfCgId',$value->CgId)->distinct()->orderBy('EfEmId')->get();
            $CoNo=1;
            $EncCgId=base64_encode(get_encrypt($value->CgId));
            @endphp
            @foreach($Exams as $Sem)
            
            @php
            $CoId= \App\Exam::where('EmId',$Sem->EfEmId)->where('EntryStat',1)->first()->CfCoId;
            @endphp
            <p style="font-size: smaller;">
            {{$CoId}} - {{ \App\Course::where('CfCoId',$CoId)->where('EntryStat',1)->first()->CoName }} - 
            
            @php
            echo $StuCount = \App\ExamForms::where('EfCgId',$value->CgId)->where('EfEmId',$Sem->EfEmId)->where('ResProStat','1')->count();
          
            $EncSelcourse=base64_encode(get_encrypt($CoId));
            $EncSelexam=base64_encode(get_encrypt($Sem->EfEmId));
            $EncSelbranch=base64_encode(get_encrypt($Sem->EfStrId));
            
            @endphp
            <?php
            $path='ledger/'.$value->CgId.$CoId.'.pdf';
            $path1 = public_path($path);
            if (file_exists($path1)) 
            {?>
              &nbsp;&nbsp;&nbsp;<a target='_blank'  href='public/{{$path}}' style="color: green;font-weight: bold;">Uploaded Ledger</a><?php
            }
             ?>
            
            &nbsp;&nbsp;&nbsp;<a href="{{ url('dynamic_Ledgerpdf/'.$EncSelcourse.'/'.$EncSelexam.'/'.$EncSelbranch.'/'.$EncCgId) }}" target="_blank" style="font-weight: bold;">View Ledger</i></a><br>
            </p>
           
            @endforeach
            
            </td>
           <!-- <td>
              @if($value->CsAuthFile)
              <a href="public/uploads/{{$value->CsAuthFile}}" target="_blank"><i class="fa fa-eye"></i></a>
              @else
              <p style="color: red">Not Uploaded</p>
              @endif
           </td> -->
        </tr>

        <!-- echo "<br>".$Course="select CoName from courses where CfCoId IN (select CfCoId from cointakes where CgId='$value->CgId')"; -->
        @endforeach
        
  </tbody>
</table>