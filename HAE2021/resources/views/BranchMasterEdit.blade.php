<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('include.head')
    <script src="{{ url('/') }}/js/jquery.min.js"></script>
    <script src="{{ url('/') }}/js/owl.carousel.min.js"></script>
    <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/js/custom.js"></script>
</head>
<body class="antialiased">
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                         <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-lead-pencil
                                          "></i>
                                       </span> Edit Branch
                                    </h3>
                                 </div>


                                 <div class="table-div">
                                    <div class=" t-left">
                                       <a href="../br-list" class="button-web"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
                                        <?php
                                          if( session('msg') != ''){
                                          ?>
                                       <p class="alert alert-danger" style="color: red"> {{session('msg')}} </p>
                                       <?php
                                          }
                                          ?>
                                    </div>
                                    <div>
                                    @foreach ($ListArr as $w)
                                    <form method="POST" action="{{route('br.update',[$w->CfBrId])}}">
                                    @csrf

                                      <table class="table-records">
                                        <tr>            
                                            <td style="text-align: left;"><label  class="input-label">Faculty <span style="color:red">*</span></label></td>

                                            <td style="text-align: left;"><label  class="input-label">Course <span style="color:red">*</span></label></td>

                                            <td style="text-align: left;"><label  class="input-label">Branch Code <span style="color:red">*</span></label></td>

                                           <!--   -->
                                                
                                           
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control1" name="FcId" id="FcId" >
                                                 <option value="">Select</option>
                                                 @foreach($faculty as $key => $value)
                                                <option value="{{$key}}" {{ ( $key == $w['FcId']) ? 'selected' : '' }}>{{$value}}</option>
                                                @endforeach
                                                </select>
                                            </td>

                                            <td>
                                                <select class="form-control1" name="CfCoId" id="CfCoId" required>
                                                 <option value="">Select</option>
                                                 @foreach($course as $key1 => $value1)
                                                <option value="{{$key1}}" {{ ( $key1 == $w['CfCoId']) ? 'selected' : '' }}>{{$value1}}</option>
                                                @endforeach
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control1" name="branch_code" id="branch_code"  required value="{{$w->branch_code}}" ></td>


                                        </tr>

                                        <tr>
                                            <td style="text-align: left;"><label  class="input-label">Branch Name <span style="color:red">*</span></label></td>

                                             <td style="text-align: left;"><label  class="input-label">Admission Open? <span style="color:red">*</span></label></td>
                                        <td></td>

                                           <!--  -->
                                        </tr>

                                        <tr>
                                            <td><input type="text" name="branch_name" required value="{{$w->branch_name}}" class="form-control1"></td>
                                            

                                            <td>
                                                <select class="form-control1" name="IsActive" id="IsActive" required>
                                                <option value="">Select</option>
                                                @foreach($admopn as $key2 => $value2)
                                                <option value="{{$key2}}" {{ ( $key2 == $w['IsActive']) ? 'selected' : '' }}>{{$value2}}</option>
                                                @endforeach
                                                </select>
                                             </td>

                                             <td><input type="submit" name="submit" class="button-web" id="btnClear" value="update"></td>
                                        </tr>
                                    </table>
                                    </form>
                                    @endforeach
                                    </div>
                                </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')

      <script type="text/javascript">
    //on chng faculty #FcId
    $(document).ready(function() {
       $('#FcId').on('change', function() {
        //console.log("hmm its change");
          var menu_id = $(this).val();
          if(menu_id) {
                $.ajax({
                   url: '../findcoursebr/'+menu_id,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data) {
                      console.log(data);
                      if(data){
                      $('#CfCoId').empty();
                      $('#CfCoId').focus;
                      $('#CfCoId').append('<option value="">Select Course</option>');
                      $.each(data, function(key, value){
                      $('select[name="CfCoId"]').append('<option value="'+ value.CfCoId +'">' + value.CoName+ '</option>');
                   });
                }else{
                   $('#CfCoId').empty();
                }
                }
                });
          }else{
             $('#CfCoId').empty();
          }
       });

   });
/*
      $(document).ready(function(){
        $('#btnClear').click(function(){        
            $('#BrCode').val('');
                  
        });
      });*/
  

</script>
</body>
</html>


<!-- <h4>Edit Branch master</h4>

@foreach ($ListArr as $w)

      
     <a href="../br-list">Back</a>
      <form method="POST" action="{{route('br.update',[$w->CfBrId])}}">
        @csrf
        <table>
            <tr>
                <td>Faculty</td>
                <td>
                    <select class="form-control" name="FcId" id="FcId" required>
                     <option value="">Select</option>
                     @foreach($faculty as $key => $value)
                    <option value="{{$key}}" {{ ( $key == $w['FcId']) ? 'selected' : '' }}>{{$value}}</option>
                    @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>Course</td>
                <td>
                    <select class="form-control" name="CoId" id="CoId" required>
                     <option value="">Select</option>
                     @foreach($course as $key => $value)
                    <option value="{{$key}}" {{ ( $key == $w['CoId']) ? 'selected' : '' }}>{{$value}}</option>
                    @endforeach
                    </select>
                </td>
            </tr>


            <tr>
                <td>Code</td>
                <td><input type="name" name="BrCode" required value="{{$w->BrCode}}"></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><input type="name" name="BrName" required value="{{$w->BrName}}"></td>
            </tr>

            <tr>
                <td></td>
                <td><input type="submit" name="submit"></td>
            </tr>
        </table>
    </form>
@endforeach -->



