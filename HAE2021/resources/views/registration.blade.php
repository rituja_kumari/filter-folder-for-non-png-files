<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="_token" content="{{csrf_token()}}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/owl.carousel.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/app-2.css" type="text/css"/>
      <!-- <link rel="stylesheet" href="{{ url('/') }}/css/parsley.css" type="text/css"/>
      <link rel="stylesheet" href="{{ url('/') }}/css/docs.css" type="text/css"/> -->
      <!-- Styles -->
      <style type="text/css">
      	.mr-0{margin: 0;}
      	.pt-10{padding-top: 10px;}
         .group-head-text{color: #000;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 500;}
    .checkbox-register{    padding: 10px;
    width: 20px;
    height: 20px;
    line-height: 20px;
    vertical-align: top;}
    .course-list{display: block;
     overflow-x: hidden;
 }
 .table-responsive{width: 100%;border: 1px solid #ddd;}
      .table-responsive td, th {
    padding: 2px 4px;
    text-align: center;
}
.table-responsive th{font-size: 14px;text-transform: uppercase;}
       </style>
    
    
    
   </head>
   <body class="antialiased">
      <div class="body_overlay"><div><img src="{{ url('/') }}/img/loading11.gif" width="80px" height="80px"/></div></div>
   <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="">
                        <header>
                           <div class="container">
                              <div class="row">
                                 <div class="col-lg-1 col-xs-4">
                                    <div class="logo-img">
                                       <img src="{{ url('/') }}/img/logo11.png" alt="Logo">
                                    </div>
                                 </div>
                                 <div class="col-lg-5 text-header row ">
                                    <p>Directorate of Art ,Maharashtra State,Mumbai<br>
                                       कला संचालनालय, महाराष्ट्र राज्य, मुंबई
                                    </p>
                                 </div>
                                 <div class="col-lg-6">
                                    <ul class="nav-ul">
                                       <li><a href="#" data-toggle="modal" data-target="#myModal-2">Login</a></li>
                                       <li><a href="#" data-toggle="modal" data-target="#myModal">College Entry</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </header>
                        <div class="row">
                           <div class="col-lg-6 m-pd-0">
                              <div class="carousel-wrap">
                                 <div class="owl-carousel">
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img1.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img2.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img3.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img4.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img5.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img6.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img7.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img8.png"></div>
                                    <!-- <div class="item"><img src="{{ url('/') }}/img/sliders/img9.png"></div> -->
                                    <!-- <div class="item"><img src="{{ url('/') }}/img/sliders/img10.png"></div> -->
                                    <!-- <div class="item"><img src="{{ url('/') }}/img/sliders/img11.png"></div> -->
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img12.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img13.png"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/img14.png"></div>
                                 </div>
                              </div>
                           </div>
                            <div class="col-lg-6 col-md-12 col-pad-0 bg-color">
                              <div class="holder">
                                 <div class="holder-title">
                                    <h6 class=""><b>INSTRUCTIONS</b></h6>
                                    <!-- <p>Read the instructions/ Pre-requisite carefully before filling the form</p> -->
                                 </div>
                                 
                                 <style type="text/css">
                                    .latestupdate{
                                       color: black;
                                       text-decoration: none;
                                       font-size: 14px;
                                       font-weight: bold;
                                    }
                                 </style>
                                  <ul class="helpdesk" style="display: none;">
                                    <li class="latestupdate">
                                        <a href="{{url('public/doausermanual.pdf')}}" target="_blank" >Click Here For UserManual <img src="{{ url('/') }}/img/new.gif" alt="logo" style="height: 80px;width: 80px;" /></a>
                                    </li>
                                    <li class="latestupdate">
                                       <a href="{{url('public/Circular-HAE-2021.pdf')}}" target="_blank" >Click Here For Circular</a>
                                    </li>
                                    <li class="latestupdate">
                                        Latest Update
                                        <div class="alert alert-danger" style="border-radius: 10px 10px 0 0;" role="alert">
                                          गुण अद्यावत किंवा समाविष्ट करण्याची अंतिम तारीख  २२ जुलै, २०२१.
                                       </div>
                                        <div style="height:130px;width:auto;overflow:scroll;overflow-x:hidden;overflow-y:scroll;">
                                        <p style="color: black;font-size: 13px;line-height: inherit;height: 130px;">
                                           माननीय अधिष्ठाता / प्राचार्य ,<br>
                                           
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;कला संचालनालयाच्या दिनांक 07/07/2021 रोजीच्या परिपत्रकान्वये पदविका व प्रमाणपत्रअभ्यासक्रमातील शासकीय उच्चकला परीक्षा-2021 करीता प्रविष्ट विद्यार्थ्यांचे अंतर्गत मुल्यमापन करून परिपूर्ण निकालपत्रक परिपत्रकात नमूद करण्यात आलेल्या लिंकवर दिनांक 16 जूलै, 2021 पर्यंत सादर करण्याबाबत कळविण्यात होते. सदरची मुदत दिनांक 22 जूलै, 2021 पर्यंत वाढविण्यात येत आहे.<br>
                                          Thanks & Regards,<br>

Directorate of Art,<br>
Sir J J School Of Art Campus, Dr. D. N. Road, Fort, <br>
Mumbai, Maharashtra 400001<br>
Phone: 022 2262 0231<br>
                                          
                                        </p></div>
                                    </li>
                                 </ul>
                                 <ul id="ticker01" style="display: none;">
                                    <li>
                                       <a>Before proceeding registration, please ensure that you have the following.</a>
                                       <ul class="pl-18 inner-ul-ticker" style="list-style: none;">
                                          <li>
                                             <p>a.  You must have an email ID of (Principal  and Coordinator )</p>
                                          </li>
                                          <li>
                                             <p>b. You must give a mobile number of (Principal  and Coordinator )</p>
                                          </li>
                                          <li>
                                             <p>c. Bank account details of Principal where you would like to receive all monetary transaction from DOA.</p>
                                          </li>
                                       </ul>
                                       <h3 class="ticker-head">
                                          PROCESS FOR REGISTRATION - ALL FIELDS ARE COMPULSORY. 
                                       </h3>
                                       <ul class="pl-18 inner-ul-ticker">
                                          <li>
                                             <p>Click on the Register Link.</p>
                                          </li>
                                          <li>
                                             <p>Institute Registration Details will appear.</p>
                                          </li>
                                          <li>
                                             <p>Drop Down Select State</p>
                                          </li>
                                          <li>
                                             <p>Drop Down Select Respective District</p>
                                          </li>
                                          <li>
                                             <p>Drop Down Select Respective College Name <br>
                                                <span style="font-size: 10px;">(College name is not visible in the drop down list, Kindly contact DOA office immediately).</span>
                                             </p>
                                          </li>
                                       </ul>
                                       <h3 class="ticker-head">COLLEGE ADDRESS DETAILS </h3>
                                       <ul class="pl-18 inner-ul-ticker">
                                          <li>
                                             <p> Flat/ Room/Block/ House No.</p>
                                          </li>
                                          <li>
                                             <p> Name of the Premises / Building</p>
                                          </li>
                                          <li>
                                             <p>Area Locality</p>
                                          </li>
                                          <li>
                                             <p>Landmark</p>
                                          </li>
                                          <li>
                                             <p> Taluka</p>
                                          </li>
                                          <li>
                                             <p>Village / City</p>
                                          </li>
                                          <li>
                                             <p> Pincode</p>
                                          </li>
                                       </ul>
                                       <h3 class="ticker-head">PRINCIPAL DETAILS </h3>
                                       <ul class="pl-18 inner-ul-ticker">
                                          <li>
                                             <p>   Full Name </p>
                                          </li>
                                          <li>
                                             <p> Office Land Line No.(With STD Code) . If not available type NA.</p>
                                          </li>
                                          <li>
                                             <p> Registered Mobile Number ( Verify with OTP sent of mobile)</p>
                                          </li>
                                          <li>
                                             <p>Registered Email ID ( Verify with OTP sent on email ID)</p>
                                          </li>
                                       </ul>
                                       <h3 class="ticker-head">COORDINATOR DETAILS (AUTHORISED PERSON BY PRINCIPAL) </h3>
                                       <ul class="pl-18 inner-ul-ticker">
                                          <li>
                                             <p>   Full Name </p>
                                          </li>
                                          <li>
                                             <p> Designation</p>
                                          </li>
                                          <li>
                                             <p> Mobile Number ( Verify with OTP sent of mobile)</p>
                                          </li>
                                          <li>
                                             <p> Email ID ( Verify with OTP sent on email ID)</p>
                                          </li>
                                       </ul>
                                       <h3 class="ticker-head">COURSES AVAILABLE WITH THE COLLEGE. </h3>
                                       <ul class="pl-18 inner-ul-ticker">
                                          <li>
                                             <p> From the drop select the respective courses available in your college </p>
                                          </li>
                                          <li><p>Enter the Intake Capacity and admitted student of the respective course and save.</p></li>
                                          <li><p>Follow the same process and add all the courses and details available in your College.</p></li>
                                       </ul>
                                       <h3 class="ticker-head">PRINCIPLE ACCOUNT DETAILS </h3>
                                       <ul class="pl-18 inner-ul-ticker">
                                          <li>
                                             <p>  Account Holder Name ( Default Principal name will reflect)></p>
                                          </li>
                                          <li>
                                             <p>  Bank Name</p>
                                          </li>
                                          <li>
                                             <p>  Bank Branch Name</p>
                                          </li>
                                          <li>
                                             <p> Bank Account No.</p>
                                          </li>
                                          <li>
                                             <p>  Re-Enter Bank Account No</p>
                                          </li>
                                          <li>
                                             <p>  Bank IFSC No.</p>
                                          </li>
                                       </ul>
                                       <br>
                                       <a>CHECK BOX THE FINAL DECLARATION AND SUBMIT.</a>
                                       <p class="suc-msg-ticker">Upon successful submission you will receive user ID Password on principal mobile number</p>
                                       <p><i>(Recommended to change the password after you login from the password change tab.)</i></p>
                                    </li>
                                 </ul>
                              </div>
                              <div class="holder-2">
                                 <div class="holder-title">
                                    <h6 class=""><b>HELPDESK</b></h6>
                                 </div>
                                 <div class="helpdesk">
                                    <h5 class="">Technical Helpline No ‎:<a href="tel:1800 212 00 55 99"> 89292 07668</a></h5>
                                    <h5 class="">Email: ‎<a href="mailto:inform.doa@gmail.com"> ‎  inform.doa@gmail.com</a></h5>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      </div>
      <script src="{{ url('/') }}/js/jquery.min.js"></script>
      <script src="{{ url('/') }}/js/owl.carousel.min.js"></script>
      <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      <script src="{{ url('/') }}/js/custom.js"></script>
      <script src="{{ url('/')}}/js/institute.js" ></script>
      <!-- <script src="https://cdn.jsdelivr.net/gh/guillaumepotier/Parsley.js@2.9.2/dist/parsley.js"></script> -->

      <div id="myModal" class="modal fade" role="dialog">
         <div class="modal-dialog register-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div >
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                <div class="t-center">
                     <button class="tab-btns tab-btns-active" id="ce-register">College Entry</button> 
                     <!-- <button class="tab-btns" id="st-register">Student Registration</button> -->
                  </div>
                  <div id="centernecontent">
                     @include('newcenterdetails')

                  </div>
                  <div id="contentcenter" style="display: none;">
                  <style>
                     .body_overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999999;position: fixed;left: 0;top: 0;width: 100%;height: 100%;width: 100%;height: 100%;display: none;}
                     .body_overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}

                     </style>
                  

                  <form class="row" id="institute" method="POST" action="{{ route('update') }}">
                  {{ csrf_field() }}
                     <div class="col-lg-12">
                        <h6 class="register-text">Institute Registration</h6>
                     </div>
                     <div class="form-group col-lg-12">
                        <div class="row group-shadow">
                           <div class="col-lg-3">
                              <label for="email" class="input-label">State / राज्य*</label>
                              <select class="form-control1" name="state" id="state" required="" >
                              <option value="">Select</option>
                              @foreach($states as $key => $value)
                                 <option value="{{ $key }}">{{ $value}}</option>
                              @endforeach
                              </select>
                              <span class="text-danger">
                              <strong id="state-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <label  class="input-label">District / जिल्हा*</label>
                              <select class="form-control1" name="District" id="District" required="">
                                 <option value="">Select</option>
                              </select>
                              <span class="text-danger">
                              <strong id="District-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <label class="input-label">College Name / महाविद्यालयाचे नाव*</label>
                              <select class="form-control1" name="College" id="College" required="">
                                 <option value="">Select</option>
                                 
                              </select>
                              <span class="text-danger">
                              <strong id="College-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <label class="input-label">Center Number / केंद्र क्रमांक*</label>
                              <input type="text" name="CenterNo" id="CenterNo" class="form-control1" disabled="" required="">
                              <input type="hidden" name="CgCode" id="CgCode" />
                           </div>
                        </div>
                     </div>

                     <div class="form-group col-lg-12 ">
                        <div class="row group-shadow">
                            <div class="col-md-12">
                                  <h6 class="group-head-text">College Address Details</h6>
                              </div>
                              <div class="col-lg-3">
                                 <input type="text" name="CfAdd1" id="CfAdd1"  class="form-control1" placeholder=" Flat/Room/Block/House No*" required="">
                                 <span class="text-danger">
                              <strong id="flatroom-error"></strong>
                              </span>
                              </div>
                           <div class="col-lg-3">
                              <input type="text" name="CfAdd2" id="CfAdd2"  class="form-control1" placeholder="Name of Premises/ Building*" required="">
                              <span class="text-danger">
                              <strong id="buildingname-error"></strong>
                              </span>
                           </div>
                            <div class="col-lg-3">
                              <input type="text"  name="CfAdd3" id="CfAdd3" class="form-control1" placeholder=" Road/ Street/Post Office*" required="">
                              <span class="text-danger">
                              <strong id="roadno-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" name="CfAdd4" id="CfAdd4" class="form-control1 " placeholder=" Area Locality*"  required="">
                              <span class="text-danger">
                              <strong id="area-error"></strong>
                              </span>
                           </div>
                            <div class="col-lg-3">
                              <input type="text" name="CfAdd5" id="CfAdd5" class="form-control1 " placeholder="Landmark*" required="">
                              <span class="text-danger">
                              <strong id="landmark-error"></strong>
                              </span>
                             
                           </div>
                           <div class="col-lg-3">
                              <input type="text" name="Taluka" id="Taluka" class="form-control1 " placeholder=" Taluka*" required="">
                              <span class="text-danger">
                              <strong id="taluka-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" name="TnName" id="TnName" class="form-control1 " placeholder=" Village/City*" required="">
                              <span class="text-danger">
                              <strong id="village-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" name="CfPinCod" id="CfPinCod" class="form-control1 " placeholder=" Pincode*" required="">
                              <span class="text-danger">
                              <strong id="pincode-error"></strong>
                              </span>
                           </div>
                          
                        </div>
                     </div>

                     <div class="form-group col-lg-6 ">
                        <div class="row group-shadow">
                           <div class="col-lg-12">
                              <input type="text" name="CfCgPrinName" id="CfCgPrinName" class="form-control1" placeholder="Principal Full Name / प्राचार्याचें संपूर्ण नाव *" onchange="return PutValue();" required="">
                              <span class="text-danger">
                              <strong id="CfCgPrinName-error"></strong>
                              </span>
                           </div>
                          
                           <div class="col-lg-4">
                              <input type="text"  name="CfCgPrinContact" id="CfCgPrinContact" class="form-control1 " placeholder="Mobile No / भ्रमणध्वनी क्रमांक*" required="">
                              <span class="text-danger">
                              <strong id="CfCgPrinContact-error"></strong>
                              </span>
                              <span class="text-danger">
                              <strong id="MobileVerify-error"></strong>
                              </span>
                              <span class="text-danger">
                              <strong id="MobileOtp-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-2" style="padding:0">
                              <button type="button" id="PrcOtp" class="fxt-btn-fill-2  b-info">Send OTP</button>
                              <span class="text-danger">
                              <strong id="MobileOtp-send"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" name="PrcMoOtp" id="PrcMoOtp" class="form-control1" placeholder="Enter OTP:">
                              <span class="text-danger">
                              <strong id="PrcMoOtp-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <button type="button" id="PrcMoVerify" class="fxt-btn-fill-2 v-success">Verify OTP</button>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-lg-6 ">
                        <div class="row group-shadow">
                           <div class="col-lg-12">
                              <input type="text" name="CgLandLineNo" id="CgLandLineNo"  class="form-control1" placeholder="Office LandLine No(With S.T.D. Code)*" required="">
                              <span class="text-danger">
                              <strong id="CgLandLineNo-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-4">
                              <input type="email" name="CfCgPrinEmail" id="CfCgPrinEmail"  class="form-control1" style="text-transform:lowercase" placeholder="Email Id*" required="">
                              <span class="text-danger">
                              <strong id="CfCgPrinEmail-error"></strong>
                              </span>
                              <span class="text-danger">
                              <strong id="PrcEmailVerify-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-2" style="padding:0">
                              <button type="button" id="PrcEmailbtn" class="fxt-btn-fill-2 b-info">Send OTP</button>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" name="PrcEmailotp" id="PrcEmailotp" class="form-control1" placeholder="Enter OTP:">
                              <span class="text-danger">
                              <strong id="PrcEmailotp-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <button type="button" id="PrcEmailVerifybtn" class="fxt-btn-fill-2 v-success">Verify OTP</button>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-lg-12 ">
                        <div class=" group-shadow">
                            
                        	<div class="row mr-0">
                              <div class="col-md-12">
                                  <h6 class="group-head-text">Coordinator Details</h6>
                              </div>
                           <div class="col-lg-3">
                              <input type="text" name="CoFName" id="CoFName" class="form-control1 " placeholder="Full Name*" required="">
                              <span class="text-danger">
                                 <strong id="CoFName-error"></strong>
                              </span>
                           </div>
                            <div class="col-lg-2">
                              <input type="text" name="CoDesigNation" id="CoDesigNation" class="form-control1 " placeholder=" Designation*" required="">
                              <span class="text-danger">
                                 <strong id="codesignation-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-2">
                              <input type="text" name="CoFMobile" id="CoFMobile"  class="form-control1 " placeholder="Mobile No*" required="" >
                              <span class="text-danger">
                                 <strong id="CoFMobile-error"></strong>
                              </span>
                              
                           </div>
                           <div class="col-lg-1" style="padding: 0;">
                              <button type="button" id="CofOtpBtn" class="fxt-btn-fill-2 b-info">Send OTP</button> 
                           </div>
                           <div class="col-lg-2">
                              <input type="text" name="CofMoOtp" id="CofMoOtp" class="form-control1 " placeholder="Enter OTP:">
                              <span class="text-danger">
                              <strong id="CoMobileVerify-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-2">
                              <button type="button" id="Cofverfybtn" class="fxt-btn-fill-2 v-success">Verify OTP</button>
                           </div> 
                           </div>
                           <div class="row mr-0 pt-10">               
                           <div class="col-lg-3">
                              <input type="email" name="CoFEmail" id="CoFEmail" style="text-transform:lowercase" class="form-control1 " placeholder="Email-Id*" required="">
                              <span class="text-danger">
                                 <strong id="CoFEmail-error"></strong>
                              </span>
                              <span class="text-danger">
                              <strong id="CoEmailVerify-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3" >
                              <button type="button" id="cofsendotpbtn" class="fxt-btn-fill-2 b-info">Send OTP</button>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" name="cofotp" id="cofotp" class="form-control1" placeholder="Enter OTP:">
                              <span class="text-danger">
                              <strong id="cofotp-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-3">
                              <button type="button" id="cofverfyemailbtn" class="fxt-btn-fill-2 v-success">Verify OTP</button>
                           </div>
                       </div>
                        </div>
                     </div>
                     <div class="form-group col-lg-12 ">
                        <div class="row group-shadow">
                           <div class="col-lg-12">
                         <h6 class="group-head-text">Select Courses</h6>
                         <select class="form-control1" name="CfCoId" id="CfCoId">
                              <option value="">Select</option>
                              @foreach($CoIds as $cokey => $course)

                                 <option value="{{ $cokey }}">{{$cokey}} - {{ $course}}</option>

                              @endforeach
                              </select>
                              <div class="row checkbox-section crs-type" id="course-selection" style="display: none;">
                                 <div class="col-lg-6 br-1px">
                                    <h5 class="checkbox-head"> Intake</h5>
                                    <div class="row">
                                       <div class="col-lg-4">
                                          <input type="text" maxlength="4" name="InGovt" id="InGovt" class="form-control1" placeholder="Govt. ">
                                          <span class="text-danger">
                                          <strong id="InGovt-error" style="top: 6px;"></strong>
                                          </span>
                                       </div>
                                       <div class="col-lg-4">
                                          <input type="text" maxlength="4" name="InAided" id="InAided" class="form-control1" placeholder="Aided ">
                                          <span class="text-danger">
                                          <strong id="InAided-error" style="top: 6px;"></strong>
                                          </span>
                                       </div>
                                       <div class="col-lg-4">
                                          <input type="text" maxlength="4" name="InUnAided" id="InUnAided" class="form-control1" placeholder="Unaided ">
                                          <span class="text-danger">
                                          <strong id="InUnAided-error" style="top: 6px;"></strong>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-6">
                                    <h5 class="checkbox-head"> Admitted Students</h5>
                                    <div class="row">
                                       <div class="col-lg-4">
                                          <input type="text" maxlength="4" name="AdGovt" id="AdGovt" class="form-control1" placeholder="Govt. ">
                                          <span class="text-danger">
                                          <strong id="AdGovt-error" style="top: 6px;"></strong>
                                          </span>
                                       </div>
                                       <div class="col-lg-4">
                                          <input type="text" maxlength="4" name="AdAided" id="AdAided" class="form-control1" placeholder="Aided ">
                                          <span class="text-danger">
                                          <strong id="AdAided-error" style="top: 6px;"></strong>
                                          </span>
                                       </div>
                                       <div class="col-lg-4">
                                          <input type="text" maxlength="4" name="AdUnAided" id="AdUnAided" class="form-control1" placeholder="Unaided ">
                                          <span class="text-danger">
                                          <strong id="AdUnAided-error" style="top: 6px;"></strong>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-12 t-right">
                                    <button type="button" id="AddCourse" class="fxt-btn-fill" >Save Course</button>
                                 </div>
                              </div>
                              <span class="text-danger">
                              <strong id="CfCoId-error" style="top: 6px;"></strong>
                              </span>
                              <div id="success-course" class="hide">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Data Saved Successfully.!!
                        </div>
                     </div>
                     <div id="coursefail-msg" class="hide">
                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">×</span>
                        </button>
                        <strong>Failed!</strong> Duplicate entry found.!
                        </div>
                     </div>
                           </div>                             
                        </div>
                     </div>
                     
                  <input type="hidden" name="buttondelcourse" id="buttondelcourse" vlaue="">
                     <div class="form-group col-lg-12 ">
                      <div class="row group-shadow">
                      <div id="delcourse-msg" class="hide">
                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">×</span>
                        </button>
                        <strong>DELETE!</strong> Record Deleted.!
                        </div>
                     </div>
                           <div class="col-lg-12">
                      	<table class="table-responsive" border="1">
                      		
                      			<tr>
                      					<th rowspan="2">Course No</th>
                      					<th colspan="3">Intake</th>
                      					<th colspan="3">Admitted Students</th>
                      					<th rowspan="2">Action</th>
                      				</tr>
                      			<tr>
                      					
                      					<th>Govt.</th>
                      					<th>Aided</th>
                      					<th>Unaided</th>
                      					<th>Govt.</th>
                      					<th>Aided</th>
                      					<th>Unaided</th>
                      					
                      				</tr>
                      			<tbody id="mytable">
                      				
                      				<!-- <tr>
                      					<td>Foundation</td>
                      					<td>Karuna</td>
                      					<td>Amit</td>
                      					<td>-</td>
                      					<td>Aniket.</td>
                      					<td>-</td>
                      					<td>Surabhi</td>
                      					<td><a href="#"> <i class="fa fa-trash"></i></a></td>
                      				</tr> -->
                      				
                      			</tbody>
                      		<script>
                           $(document).on("click", ".delete" , function() {
                           var delete_id = $(this).data('id');
                           var el = this;
                           //alert(delete_id);return false;
                           $.ajax({
                              url: 'CourseDel/'+delete_id,
                              type: 'get',
                              success: function(response){
                                 $(el).closest( "tr" ).remove();
                                 $('#delcourse-msg').removeClass('hide');
                                    setInterval(function(){ 
                                       $('#delcourse-msg').addClass('hide');
                                    }, 3000);
                              }
                           });
                           });
                            </script>
                      	</table>
                    </div>
                </div>
                      </div>
                     <div class="form-group col-lg-12">
                        <div class="row group-shadow">
                            <div class="col-md-12">
                                  <h6 class="group-head-text">Principle Account Details</h6>
                              </div>
                              <div class="col-lg-4">
                                 <input type="text" name="holdername" id="holdername"  class="form-control1" placeholder=" Account Holder Name*" disabled="" required="">
                                 <span class="text-danger">
                              <strong id="holdername-error"></strong>
                              </span>
                              </div>
                           <div class="col-lg-4">
                              <input type="text" name="BankName" id="BankName"  class="form-control1" placeholder=" Bank Name*" required="">
                              <span class="text-danger">
                              <strong id="BankName-error"></strong>
                              </span>
                           </div>
                            <div class="col-lg-4">
                              <input type="text"  name="BankBranchName" id="BankBranchName" class="form-control1" placeholder=" Bank Branch Name*" required="">
                              <span class="text-danger">
                              <strong id="BankBranchName-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-4">
                              <input type="text" name="BankAcNo" id="BankAcNo" class="form-control1 " placeholder=" Bank Account No*" onchange='return bankcheck();' required="">
                              <span class="text-danger">
                              <strong id="BankAcNo-error"></strong>
                              </span>
                           </div>
                            <div class="col-lg-4">
                              <input type="text" name="reBankAcNo" id="reBankAcNo" class="form-control1 " placeholder="Re-Enter Bank Account No*" required="">
                              <span class="text-danger">
                              <strong id="reBankAcNo-error"></strong>
                              </span>
                              <span class="text-danger">
                              <strong id="bankacc-error"></strong>
                              </span>
                           </div>
                           <div class="col-lg-4">
                              <input type="text" name="BankIFSCNo" id="BankIFSCNo" class="form-control1 " placeholder=" Bank IFSC Code*" required="">
                              <span class="text-danger">
                              <strong id="BankIFSCNo-error"></strong>
                              </span>
                           </div>
                          
                        </div>
                     </div>
                      <div class="form-group col-lg-12 " style="margin-bottom: 20px;">
                        <div class="row group-shadow">
                            <div class="col-md-12">
                           <input type="checkbox" name="agree-terms" id="agree-terms" onclick="return EnableDisable();" value="1" class="checkbox-register"> I <b><span id="PrincipleName">.................</span> </b> hereby declare that the information furnished above is true, complete and correct to the best of my knowledge and belief. 
                            </div>
                         </div>
                      </div>
                     <div class="form-group col-lg-6 " >
                        <div class="row">
                           <div class="col-lg-4 col-xs-6">
                              <button type="submit" id="save" disabled="" class="fxt-btn-fill">Submit</button>
                           </div>
                           <div class="col-lg-4 col-xs-6">
                              <button type="button" data-dismiss="modal" class="fxt-btn-fill">Cancel</button>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-lg-12">
                        <div id="success-msg-2" class="hide">
                           <div class="alert alert-info alert-dismissible fade in" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              </button>
                              <strong>Success!</strong> Data Updated Successfully!!
                           </div>
                        </div>
                     </div> <br>                   
                  </form>
                  <!--  -->

                </div>

                 <!-- content  Student Registration-->
                  <div id="contentstudent" style="display: none;">
                     <div id="success-msg-3" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                     <div id="StuRegSuccess" ></div>
                     <form class="row" id="register-form"  method="post" action="{{ route('registerss') }}" >
                        {{ csrf_field() }}
                        <div class="col-lg-12">
                           <h6 class="register-text">Student Registration</h6>
                        </div>
                        <div class="form-group col-lg-12">
                           <div class="row group-shadow">
                              <div class="col-lg-3">
                                 <label class="input-label" for="name">Email Id* </label>
                                 <div class="d-flex">
                                    <input type="text" class="form-control1" id="StuEmail" name="StuEmail" style="text-transform:initial" placeholder="Enter Email Id">
                                    <button  type="button" class="btn-otp" id="Emailotp">Send OTP</button>
                                 </div>
                                 <span class="text-danger"><strong id="StuEmail-error"></strong></span>
                              </div>
                              <div class="col-lg-3">
                              <label class="input-label">Enter OTP Received on Email*</label>
                              <div class="d-flex">
                              <input type="text" class="form-control1" id="sotp1" name="sotp1" placeholder="Enter OTP">
                              <!-- <button class="btn-otp v-success" id="StuEmVrfy">Verify OTP</button> -->
                              <button  type="button" class="btn-otp v-success" id="StuEmVrfy">Verify OTP</button>
                              </div>
                              <span class="text-danger"><strong id="sotp1-error"></strong></span>
                              </div>

                              <div class="col-lg-3">
                                 <label class="input-label" for="name">Mobile No.* </label>
                                 <div class=" d-flex">
                                    <input type="text" class="form-control1" id="StuMob" name="StuMob" placeholder="Enter Mobile No.">
                                    <button  type="button" class="btn-otp" id="Mblotp">Send OTP</button>
                                 </div>
                                 <span class="text-danger"><strong id="StuMob-error"></strong></span>
                              </div>
                              <div class="col-lg-3">
                           <label class="input-label">Enter OTP Received on Mobile*</label>
                           <div class="d-flex">
                           <input type="text" class="form-control1" id="sotp2" name="sotp2" placeholder="Enter OTP">
                          <!--  <button class="btn-otp v-success" id="StuMoVrfy">Verify OTP</button> -->
                          <button  type="button" class="btn-otp v-success" id="StuMoVrfy">Verify OTP</button>
                           </div>
                           <span class="text-danger"><strong id="sotp2-error"></strong></span>
                           </div>


                              
                           </div>
                           
                        </div>
                        <div class="form-group col-lg-12 mb-30">
                              <div class="row group-shadow">
                              <div class="col-md-3">
                                 <label class="input-label" for="name">Candidate Name (SSC Certificate)* </label>
                                 <input type="text" class="form-control1" id="StuName" name="StuName" placeholder="Enter Name">
                                 <span class="text-danger"><strong id="StuName-error"></strong></span>
                              </div> 

                              <div class="col-md-3">
                                 <label class="input-label" for="name">Password*  </label>
                                 <input type="password" class="form-control1" id="StuPass" name="StuPass" placeholder="Enter Password" style="text-transform:initial" onchange='return passcheck();'>
                                 <span class="text-danger"><strong id="StuPass-error"></strong></span>
                              </div>
                              <div class="col-md-3">
                                 <label class="input-label">Confirm Password*</label>
                                 <input type="password" class="form-control1" id="StuPassC" name="StuPassC" style="text-transform:initial" placeholder="Re-Enter Password">
                                 <span class="text-danger"><strong id="StuPassC-error"></strong></span>
                              </div>
                              <div class="col-md-3">
                                 <label class="input-label" for="name">Dat Of Birth*  </label>
                                 <input type="date" class="form-control1" name="StuDob" placeholder="Enter Password">
                                 <span class="text-danger"><strong id="StuDob-error"></strong></span>
                              </div>
                           </div>
                        </div>
                        <!--    -->
                        <div class="form-group col-lg-6 " >
                           <div class="row">
                              <div class="col-lg-4 col-xs-6">
                                 <button type="submit" class="fxt-btn-fill" id="s-register">Register</button>
                                 <!--    <a href="register-s" id="s-register" class="fxt-btn-fill">Register</a> -->
                              </div>
                              <div class="col-lg-4 col-xs-6">
                                 <button type="button" data-dismiss="modal" class="fxt-btn-fill">Cancel</button>
                              </div>
                              <div class="col-lg-12">
                                 <p class="demo-text">Already have an account?<a href="#" id="close-2" data-toggle="modal" data-target="#myModal-2"> Login Here</a> </p>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
                  <!--  -->

               </div>
            </div>
         </div>
      </div>
      <div id="myModal-2" class="modal fade" role="dialog">
         <div class="modal-dialog login-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div >
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                <div class="t-center">
                     <button class="tab-btns tab-btns-active" id="ce-login">Center Login</button> 
                     <!-- <button class="tab-btns" id="st-login">Student Login</button> -->
                  </div>
                  <div id="contet-ce-login">
                  <h6 class="register-text">Sign into your account</h6>
                  <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> You are Loggeed in!!
                    </div>
                  </div>
                  <div id="fail-msg" class="hide">
                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <strong>Failed!</strong> Username or Password is incorrect.!
                    </div>
                  </div>
                  <form method="POST" id="loginuser" action="{{ route('login') }}">
                  @csrf
                     <div class="form-group">
                        <label  class="input-label">Username</label>
                        <input type="text" class="form-control1"  name="username" id="username" placeholder="Enter Username"  style="text-transform:initial" required="">
                        <span class="text-danger">
                           <strong id="username-error"></strong>
                        </span>
                     </div>
                     <div class="form-group mb-30">
                        <label  class="input-label">Password</label>
                        <input type="password" name="password" style="text-transform:initial" id="password" class="form-control1" placeholder="********" required="">
                        <span class="text-danger">
                           <strong id="password-error"></strong>
                        </span>
                        <!-- <div class="forgot-text"><a href="#" data-toggle="modal" data-target="#forgot-cps" id="close-4">Forgot Password</a></div> -->
                     </div>
                     <div class="form-group">
                        <div class="row">
                           <div class="col-lg-6 col-xs-6">
                              <button type="submit" id="submitForm" class="fxt-btn-fill">Submit</button>
                           </div>
                           <div class="col-lg-6 col-xs-6">
                              <button type="button" data-dismiss="modal" class="fxt-btn-fill">Cancel</button>
                           </div>
                        </div>
                     </div>
                  </form>
                </div>
                <!-- student login -->
                  <div id="contet-st-login" style="display: none;">
                     <div id="success-msg5" class="hide">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> You Are Logged In..!!
                        </div>
                     </div>
                     <form class="row" id="login-form"  method="post" action="{{ route('slogin') }}" >
                        {{ csrf_field() }}
                        <div class="col-lg-12">
                           <h6 class="register-text">Student Login</h6>
                        </div>
                        <div class="form-group col-lg-12">
                           <div class="row">
                              <div class="col-lg-12">
                                 <input type="text" class="form-control1" name="lemail" placeholder="Username" style="text-transform: initial;">
                                 <span class="text-danger"><strong id="lemail-error"></strong></span>
                              </div>
                              <div class="col-lg-12">
                                 <label class="input-label" for="name">Password*</label>
                                 <input type="password" class="form-control1" name="lpassword" placeholder="*****" >
                                 <span class="text-danger"><strong id="lpassword-error"></strong></span>
                                 <div class="forgot-text"><a href="#" data-toggle="modal" data-target="#myModal-6" id="close-4">Forgot Password</a></div>
                              </div>
                              <div class="col-lg-6">
                                 <button type="submit" class="fxt-btn-fill" id="loginForm">Login</button>
                              </div>
                              <div class="col-lg-12">
                                 <p class="demo-text">Don't have an account?<a href="#" id="close-1" data-toggle="modal" data-target="#myModal"> Register Here</a> </p>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
                  <!--  -->
               </div>
            </div>
         </div>
      </div>
       <!-- forgot Password -->
      <div id="myModal-6" class="modal fade" role="dialog">
         <div class="modal-dialog login-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div >
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div id="success-msg7" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="model" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Check your mail for login confirmation!!
                     </div>
                  </div>
                  <form class="row" id="forgot-ps-form"  method="post" action="{{ route('fplogin') }}" >
                     {{ csrf_field() }}
                     <div class="col-lg-12">
                        <h6 class="register-text">Forgot Password </h6>
                     </div>
                     <div class="form-group col-lg-12">
                        <div class="row">
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Registered Email Id or Mobile Number*</label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" name="fpemail" placeholder="Enter Email Id or Mobile Number">
                                 <button class="btn-otp" id="btn-otp1">Send OTP</button>
                              </div>
                              <span class="text-danger"><strong id="fpemail-error"></strong></span>
                           </div>
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Enter OTP Received on Email-Id or Mobile number*</label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" name="fpotp" placeholder="Enter OTP">
                                 <button class="btn-otp v-success" id="btn-otp2">Verify OTP</button>
                              </div>
                              <span class="text-danger"><strong id="fpotp-error"></strong></span>
                           </div>
                           <div class="col-lg-12 mb-30">
                              <label class="input-label" for="name">Date Of Birth*</label>
                              <input type="date" class="form-control1" name="fpdate" placeholder="">
                              <span class="text-danger"><strong id="fpdate-error"></strong></span>
                           </div>
                           <div class="col-lg-6">
                              <button type="submit" class="fxt-btn-fill" id="fpsubmit">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>


      <!-- forgot password center login -->
      <div id="forgot-cps" class="modal fade" role="dialog">
         <div class="modal-dialog login-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div >
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div id="succcess-forgot" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="model" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Check your mail for login confirmation!!
                     </div>
                  </div>
                  <form class="row" id="cforgot-ps-form"  method="post" action="{{ route('cfplogin') }}" >
                     {{ csrf_field() }}
                     <div class="col-lg-12">
                        <h6 class="register-text">Forgot Password </h6>
                     </div>
                     <div class="form-group col-lg-12">
                        <div class="row">
                        	 <div class="col-lg-12">
                        	 	<label class="input-label">Username</label>
                        	 	<input type="text" class="form-control1" name="cuname" placeholder="Enter Username">
                        	 	<span class="text-danger"><strong id="cuname-error"></strong></span>
                        	 </div>
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Registered Email Id or Mobile Number*</label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" name="cfpemail" placeholder="Enter Email Id or Mobile Number">
                                 <button class="btn-otp" id="cbtn-otp1">Send OTP</button>
                              </div>
                              <span class="text-danger"><strong id="cfpemail-error"></strong></span>
                           </div>
                           <div class="col-lg-12 mb-30">
                              <label class="input-label" for="name">Enter OTP Received on Email-Id or Mobile number*</label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" name="cfpotp" placeholder="Enter OTP">
                                 <button class="btn-otp v-success" id="ccbtn-otp2">Verify OTP</button>
                              </div>
                              <span class="text-danger"><strong id="cfpotp-error"></strong></span>
                           </div>
                           
                           <div class="col-lg-6">
                              <button type="submit" class="fxt-btn-fill" id="cfpsubmit">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
       <!-- forgot-center-password -->
       <script>
         $(document).ready(function(){
          var form=$("#cforgot-ps-form");
             $('#cfpsubmit').click(function(e){
                 e.preventDefault();
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                 });
                 $.ajax({
                     url: "{{ url('/cregister-fp') }}",
         
                     method: 'post',
                      data:form.serialize(),
                     success: function(data){
                      console.log(data)
                       if(data.errors) {
                           if(data.errors.cuname){
                               $( '#cuname-error' ).html( "Please Enter Username" );
                           }else{
                               $( '#cuname-error' ).html( "");
                           }
                           if(data.errors.cfpemail){
                               $( '#cfpemail-error' ).html( "Please Enter User ID" );
                           }else{
                               $( '#cfpemail-error' ).html( "");
                           } 
                           if(data.errors.cfpotp){
                               $( '#cfpotp-error' ).html( "Please Enter OTP" );
                           }else{
                               $( '#cfpotp-error' ).html( "");
                           }                          
                       }
                       if(data.success) {
                               $( '#cuname-error' ).html( "");
                               $( '#cfpemail-error' ).html( "");
                               $( '#cfpotp-error' ).html( "");
                           $('#succcess-forgot').removeClass('hide');
                           setInterval(function(){ 
                               $('#succcess-forgot').addClass('hide');
                           }, 3000);
                       }
                     }
                 });
             });
         });
      </script>

       <script type="text/javascript">
         $(function () {
         $("#close-1").on('click', function() {
             $('#myModal-2').modal('hide');
         });
         $("#close-2").on('click', function() {
             $('#myModal').modal('hide');
         });
         });
      </script>
      <script type="text/javascript">
         $(function() {
         $('#CfCoId').change(function(){
         $('#course-selection').show();
         // $('#course-selection' + $(this).val()).show();
         });
         });
      </script>
<script>
function EnableDisable()
{
   if($("#agree-terms").prop("checked") == true)
      {
         $("#save").prop("disabled",false);
      }
      else
      {
         $("#save").prop("disabled",true);
      }
}

function PutValue()
{
   var PrcName=$("#CfCgPrinName").val();
   $( '#PrincipleName' ).html(PrcName);
   $('#holdername').val(PrcName);
}
</script>
<script>
   function bankcheck()
   {
      $('#BankAcNo').attr('type', 'password');
   }   
</script>
      <script>
      $(document).ready(function(){
      var form=$("#loginuser");
            $('#submitForm').click(function(e){
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ url('/login') }}",
                    method: 'post',
                     data:form.serialize(),
                    success: function(data){
                     //console.log(data)
                      if(data.errors) {
                          if(data.errors.username){
                              $( '#username-error' ).html( data.errors.username );
                          }else{
                              $( '#username-error' ).html( "");
                          }
                          if(data.errors.password){
                              $( '#password-error' ).html( data.errors.password );
                          }else{
                              $( '#password-error' ).html( "");
                          }                          
                      }
                      if(data.success) {
                              $( '#username-error' ).html( "");
                              $( '#password-error' ).html( "");
                              var exxon=$('#username').val();

                          $('#success-msg').removeClass('hide');
                          setInterval(function(){ 
                              $('#success-msg').addClass('hide');
                          }, 3000);
                          if(exxon=='admindoa' || exxon=='exxon')
                          {
                           window.location.replace('{{route('admin-doa')}}');
                          }else{
                          window.location.replace('{{route('admin')}}');
                          }
                      }
                      if(data.fail)
                      {
                        $('#fail-msg').removeClass('hide');
                          setInterval(function(){ 
                              $('#fail-msg').addClass('hide');
                          }, 4000);
                        
                      }
                    }
                });
            });

         });
      </script>
<script>
$(document).ready(function(){
   var form=$("#institute");
   $('#save').click(function(e){
       e.preventDefault();
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
           }
       });
       var BankAcNo=$('#BankAcNo').val();
       var reBankAcNo=$('#reBankAcNo').val();
      if(BankAcNo!=reBankAcNo)
      {
         $('#bankacc-error').html( "Please Enter Correct Account No." );
      }
      else{
         $( '#bankacc-error' ).html( " " );
      }
       $.ajax({
           url: "{{ url('/register-u') }}",
           method: 'post',
            data:form.serialize(),
           success: function(data){
            //console.log(data)
             if(data.errors) {
                 if(data.errors.state){
                     $( '#state-error' ).html( data.errors.state[0] );
                 }else{
                     $( '#state-error' ).html("");
                 }
                 if(data.errors.District){
                     $( '#District-error' ).html( data.errors.District[0] );
                 }else{
                     $( '#District-error' ).html( "");
                 } 
                 if(data.errors.College){
                     $( '#College-error' ).html( data.errors.College[0] );
                 }else{
                     $( '#College-error' ).html( "");
                 } 
                 if(data.errors.CfCgPrinName){
                     $( '#CfCgPrinName-error' ).html( data.errors.CfCgPrinName[0] );
                 }else{
                     $( '#CfCgPrinName-error' ).html( "");
                 }
                 if(data.errors.CfCoId){
                     $( '#CfCoId-error' ).html( data.errors.CfCoId[0] );
                 }else{
                     $( '#CfCoId-error' ).html( "");
                 }
                 if(data.errors.CfAdd1){
                     $( '#flatroom-error' ).html( data.errors.CfAdd1[0] );
                 }else{
                     $( '#flatroom-error' ).html( "");
                 }
                 if(data.errors.CfAdd2){
                     $( '#buildingname-error' ).html( data.errors.CfAdd2[0] );
                 }else{
                     $( '#buildingname-error' ).html( "");
                 }
                 if(data.errors.CfAdd3){
                     $( '#roadno-error' ).html( data.errors.CfAdd3[0] );
                 }else{
                     $( '#roadno-error' ).html( "");
                 }
                 if(data.errors.CfAdd4){
                     $( '#area-error' ).html( data.errors.CfAdd4[0] );
                 }else{
                     $( '#area-error' ).html( "");
                 }
                 if(data.errors.CfAdd5){
                     $( '#landmark-error' ).html( data.errors.CfAdd1[0] );
                 }else{
                     $( '#landmark-error' ).html( "");
                 }
                 if(data.errors.Taluka){
                     $( '#taluka-error' ).html( data.errors.Taluka[0] );
                 }else{
                     $( '#taluka-error' ).html( "");
                 }
                 if(data.errors.TnName){
                     $( '#village-error' ).html( data.errors.TnName[0] );
                 }else{
                     $( '#village-error' ).html( "");
                 }
                 if(data.errors.CfPinCod){
                     $( '#pincode-error' ).html( data.errors.CfPinCod[0] );
                 }else{
                     $( '#pincode-error' ).html( "");
                 }

                 if(data.errors.CgLandLineNo){
                     $( '#CgLandLineNo-error' ).html( data.errors.CgLandLineNo[0] );
                 }else{
                     $( '#CgLandLineNo-error' ).html( "");
                 }
                 if(data.errors.CfCgPrinEmail){
                     $( '#CfCgPrinEmail-error' ).html(data.errors.CfCgPrinEmail[0]);
                     $('#PrcEmailbtn').attr('disabled',false);
                     $('#PrcEmailVerifybtn').attr('disabled',false);
                 }else{
                     $( '#CfCgPrinEmail-error' ).html( "");
                 }
                 if(data.errors.CfCgPrinContact){
                     $( '#CfCgPrinContact-error' ).html(data.errors.CfCgPrinContact[0]);
                     $('#PrcOtp').attr('disabled',false);
                     $('#PrcMoVerify').attr('disabled',false);   
                 }else{
                     $( '#CfCgPrinContact-error' ).html( "");
                 }
                 if(data.errors.CoFName){
                     $( '#CoFName-error' ).html( data.errors.CoFName[0] );
                 }else{
                     $( '#CoFName-error' ).html( "");
                 }
                 if(data.errors.CoDesigNation){
                     $( '#codesignation-error' ).html( data.errors.CoDesigNation[0] );
                 }else{
                     $( '#codesignation-error' ).html( "");
                 } 
                 if(data.errors.CoFMobile){
                     $( '#CoFMobile-error' ).html(data.errors.CoFMobile[0]);
                     $('#CofOtpBtn').attr('disabled',false);
                     $('#Cofverfybtn').attr('disabled',false)
                 }else{
                     $( '#CoFMobile-error' ).html( "");
                 } 
                 if(data.errors.CoFEmail){
                     $( '#CoFEmail-error' ).html(data.errors.CoFEmail[0]);
                     $('#cofsendotpbtn').attr('disabled',false);
                     $('#cofverfyemailbtn').attr('disabled',false);
                 }else{
                     $( '#CoFEmail-error' ).html( "");
                 }
                 
                 if(data.errors.BankName){
                     $( '#BankName-error' ).html(data.errors.BankName[0]);
                 }else{
                     $( '#BankName-error' ).html( "");
                 } 
                 if(data.errors.BankAcNo){
                     $( '#BankAcNo-error' ).html( data.errors.BankAcNo[0] );
                 }else{
                     $( '#BankAcNo-error' ).html( "");
                 } 
                  if(data.errors.reBankAcNo){
                     $( '#reBankAcNo-error' ).html(data.errors.reBankAcNo[0]);
                 }else{
                     $( '#reBankAcNo-error' ).html( "");
                 } 
                  if(data.errors.BankIFSCNo){
                     $( '#BankIFSCNo-error' ).html(data.errors.BankIFSCNo[0]);
                 }else{
                     $( '#BankIFSCNo-error' ).html( "");
                 } 
                  if(data.errors.BankBranchName){
                     $( '#BankBranchName-error' ).html(data.errors.BankBranchName[0]);
                 }else{
                     $( '#BankBranchName-error' ).html( "");
                 }                          
             }
             if(data.Mo_error)
             {
               $( '#MobileVerify-error' ).html( "Please Verify Mobile No." );
             }
             else{
               $( '#MobileVerify-error' ).html( " " );
             }
             if(data.Em_error)
             {
               $( '#PrcEmailVerify-error' ).html( "Please Verify Email Id." );
             }
             else{
               $( '#PrcEmailVerify-error' ).html( " " );
             }
             if(data.CoMo_error)
             {
               $( '#CoMobileVerify-error' ).html( "Please Verify Mobile No." );
             }
             else{
               $( '#CoMobileVerify-error' ).html( " " );
             }
             if(data.Emco_error)
             {
               $( '#CoEmailVerify-error' ).html( "Please Verify Email Id." );
             }
             else{
               $( '#CoEmailVerify-error' ).html( " " );
             }
             if(data.ErrorCourse)
             {
               $('#CfCoId-error').html('Please Add Course Details..!');
             }
             else{
               $('#CfCoId-error').html(' ');
             }
             if(data.success) {
                  $('#save').attr('disabled',true);
                     $( '#state-error' ).html( "");
                     $( '#District-error' ).html( "");
                     $( '#College-error' ).html( "");
                     $( '#flatroom-error' ).html( "");
                     $( '#buildingname-error' ).html( "");
                     $( '#roadno-error' ).html( "");
                     $( '#area-error' ).html( "");
                     $( '#landmark-error' ).html( "");
                     $( '#taluka-error' ).html( "");
                     $( '#village-error' ).html( "");
                     $( '#pincode-error' ).html( "");
                     $('#CfCoId-error').html("");
                     $( '#CfCgPrinName-error' ).html( "");
                     $( '#CfCgPrinContact-error' ).html( "");
                     $( '#CoFName-error' ).html( "");
                     $( '#CoFMobile-error' ).html( "");
                     $( '#CoFEmail-error' ).html( "");
                     $( '#BankName-error' ).html( "");
                     $( '#BankAcNo-error' ).html( "");
                     $( '#reBankAcNo-error' ).html( "");
                     $( '#BankIFSCNo-error' ).html( "");
                     $( '#BankBranchName-error' ).html( "");
                 $('#success-msg-2').removeClass('hide');
                 setInterval(function(){ 
                     $('#success-msg-2').addClass('hide');
                     window.location.replace('{{route('nameandmarksentry')}}');
                 }, 4000);
                 
             }
           }
       });
   });
});
</script>

    <!-- register -->
      <script>
         function passcheck()
         {
            $('#StuPass').attr('type', 'password');
         }   
      </script>
      <script>
         $(document).ready(function(){
           var form=$("#register-form");
              $('#s-register').click(function(e){
                  e.preventDefault();
                  $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                      }
                  });
                 
                  $.ajax({
                      url: "{{ url('/register-s') }}",
                      method: 'post',
                       data:form.serialize(),
                      success: function(data){
                       //console.log(data)
                        if(data.errors) {
                            if(data.errors.StuEmail){
                                $( '#StuEmail-error' ).html(data.errors.StuEmail[0]);
                            }else{
                                $( '#StuEmail-error' ).html( "");
                            }
                            if(data.errors.sotp1){
                                $( '#sotp1-error' ).html( data.errors.sotp1[0] );
                            }else{
                                $( '#sotp1-error' ).html( "");
                            } 
                            if(data.errors.StuMob){
                                $( '#StuMob-error' ).html(data.errors.StuMob[0]);
                            }else{
                                $( '#StuMob-error' ).html( "");
                            } 
                            if(data.errors.sotp2){
                                $( '#sotp2-error' ).html( data.errors.sotp2[0]);
                            }else{
                                $( '#sotp2-error' ).html( "");
                            }
                            if(data.errors.StuPass){
                                $( '#StuPass-error' ).html( data.errors.StuPass[0] );
                            }else{
                                $( '#StuPass-error' ).html( "");
                            }
                            if(data.errors.StuPassC){
                                $( '#StuPassC-error' ).html( data.errors.StuPassC[0] );
                            }else{
                                $( '#StuPassC-error' ).html( "");
                            } 
                            if(data.errors.StuDob){
                                $( '#StuDob-error' ).html( data.errors.StuDob[0] );
                            }else{
                                $( '#StuDob-error' ).html( "");
                            } 
                            
                        }
                        if(data.success) {
                                $( '#StuEmail-error' ).html( "");
                                $( '#sotp1-error' ).html( "");
                                $( '#StuMob-error' ).html( "");
                                $( '#sotp2-error' ).html( "");
                                $( '#StuPass-error' ).html( "");
                                $( '#StuPassC-error' ).html( "");
                                $( '#StuDob-error' ).html( "");
                                $( '#StuEmail' ).val("");
                                $( '#StuMob' ).val("");
                                $( '#StuDob' ).val("");
                                $( '#sotp1' ).val("");
                                $( '#sotp2' ).val("");
                                $( '#StuPass' ).val("");
                                $( '#StuPassC' ).val("");
                                
                            $('#success-msg-3').removeClass('hide');
                            setInterval(function(){ 
                                $('#success-msg-3').addClass('hide');
                            }, 3000);
                            $("#StuRegSuccess").html("Registerd Successfully !").css({"color": "green", "font-size": "120%"});
         
         
                        }
                      }
                  });
              });

               $("#StuPassC").on('keyup', function(){
                var password = $("#StuPass").val();
                var confirmPassword = $("#StuPassC").val();
                if (password != confirmPassword)
                    $("#StuPassC-error").html("Password does not match !").css("color","red");
                else
                    $("#StuPassC-error").html("Password match !").css("color","green");
               });
          });
      </script>
      <!-- register -->
      <!-- forgotpassword -->
      <script>
         $(document).ready(function(){
          var form=$("#forgot-ps-form");
             $('#fpsubmit').click(function(e){
                 e.preventDefault();
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                 });
                 $.ajax({
                     url: "{{ url('/register-fp') }}",
         
                     method: 'post',
                      data:form.serialize(),
                     success: function(data){
                      console.log(data)
                       if(data.errors) {
                           if(data.errors.fpemail){
                               $( '#fpemail-error' ).html( "Please Enter Email-Id" );
                           }else{
                               $( '#fpemail-error' ).html( "");
                           }
                           if(data.errors.fpotp){
                               $( '#fpotp-error' ).html( "Please Enter OTP" );
                           }else{
                               $( '#fpotp-error' ).html( "");
                           } 
                           if(data.errors.fpdate){
                               $( '#fpdate-error' ).html( "Please Enter Password" );
                           }else{
                               $( '#fpdate-error' ).html( "");
                           }                          
                       }
                       if(data.success) {
                               $( '#fpemail-error' ).html( "");
                               $( '#fpotp-error' ).html( "");
                               $( '#fpdate-error' ).html( "");
                           $('#success-msg7').removeClass('hide');
                           setInterval(function(){ 
                               $('#success-msg7').addClass('hide');
                           }, 3000);
                       }
                     }
                 });
             });
         });
      </script>
      <!-- forgotpasssword -->
            <script type="text/javascript">
         $(document).ready(function() {
            $('#ce-register').click(function() {
            $("#centernecontent").show();
            $("#contentcenter").hide();
            $("#contentstudent").hide();
            $("#ce-register").addClass('tab-btns-active');
            $("#st-register").removeClass('tab-btns-active');
            });
            $('#st-register').click(function() {
            $("#centernecontent").hide();
            $("#contentcenter").hide();
            $("#contentstudent").show();
            $("#ce-register").removeClass('tab-btns-active');
            $("#st-register").addClass('tab-btns-active');
            });
            $('#ce-login').click(function() {
            $("#contet-ce-login").show();
            $("#contet-st-login").hide();
            $("#ce-login").addClass('tab-btns-active');
            $("#st-login").removeClass('tab-btns-active');
            });
            $('#st-login').click(function() {
            $("#contet-ce-login").hide();
            $("#contet-st-login").show();
            $("#ce-login").removeClass('tab-btns-active');
            $("#st-login").addClass('tab-btns-active');
            });
            });

      </script>
            <!-- login -->
      <script>
         $(document).ready(function(){
          var form=$("#login-form");
             $('#loginForm').click(function(e){
                 e.preventDefault();
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                 });
                 $.ajax({
                     url: "{{ url('/register-l') }}",
         
                     method: 'post',
                      data:form.serialize(),
                     success: function(data){
                      console.log(data)
                       if(data.errors) {
                           if(data.errors.lemail){
                               $( '#lemail-error' ).html( "Please Enter Email-Id" );
                           }else{
                               $( '#lemail-error' ).html( "");
                           }

                           if(data.errors.lpassword){
                               $( '#lpassword-error' ).html( "Please Enter Password" );
                           }else{
                               $( '#lpassword-error' ).html( "");
                           }                          
                       }
                       if(data.success) {
                               $( '#lemail-error' ).html( "");
                               $( '#lpassword-error' ).html( "");
                           $('#success-msg5').removeClass('hide');
                           setInterval(function(){ 
                               $('#success-msg5').addClass('hide');
                           }, 4000);

                           window.location.replace('{{route('form')}}');
                       }
                     }
                 });
             });
         });
      </script>
      <!-- login -->
      <!-- student mobile and email verification  -->
      <script type="text/javascript">
         $(document).ready(function() {
          $('#Mblotp').on('click', function() {
        //console.log("its working..");
       var StuMob=$('#StuMob').val();
       
       if(StuMob=='')
       {
         $('#StuMob-error').html('Please Enter Mobile Number..!');
       }
       else{
         $('#StuMob-error').html('');
       }
      if(StuMob!='') 
      {
         //console.log("its working..");
            $.ajax({
               url: 'SendOtp-s/'+StuMob,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}"},
               dataType: "json",
               
               success:function(data) {
                  
                  if(data)
                  {
                     alert('Otp Send Successfully');
                     $('#Mblotp').attr('disabled',true);
                  }
            }
            });
      }
       
    });

          /*email otp send*/
      $('#Emailotp').on('click', function() {
     
      var StuEmail=$('#StuEmail').val();
       console.log("its working..");
       if(StuEmail=='')
       {
         $('#StuEmail-error').html('Please Enter Email Id..!');
       }
       else{
         $('#StuEmail-error').html('');
       }
       if(StuEmail!='') 
      {
         //console.log("its working..");
            $.ajax({
               url: 'SendEmailOtp-s/'+StuEmail,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}"},
               dataType: "json",
               
               success:function(data) {
                  
                  if(data)
                  {
                     alert('Otp Send Successfully');
                     $('#Emailotp').attr('disabled',true);
                  }
            }
            });
      }

    });

      /*verify mobile otp*/
      $('#StuMoVrfy').on('click', function() {
     
      var sotp2 = $('#sotp2').val();
      var StuMob=$('#StuMob').val();
      //console.log("its working..");
       if(StuMob=='')
       {
         $('#StuMob-error').html('Please Enter Mobile Number..!');
       }
       else{
         $('#StuMob-error').html('');
       }
       if(sotp2=='')
       {
         $('#sotp2-error').html('Enter OTP..!');
       }
       else
       {
         $('#sotp2-error').html('');  
       }
       if(sotp2) {
         console.log("its working..");
             $.ajax({
                url: 'VerifyStuOtp/'+sotp2+'/'+StuMob,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                
                success:function(data) {
                   
                   if(data)
                   {
                      $.each(data, function(key, value){
                         if(value.MobileOtp==sotp2)
                         {
                            alert('Mobile Verified..!');
                            $('#StuMoVrfy').attr('disabled',true);
                         }
                         else
                         {
                           alert('Incorrect OTP..!');
                         }
                      });
                   }
             }
             });
       }
       
    });

      $('#StuEmVrfy').on('click', function() 
    {
       var sotp1 = $('#sotp1').val();
       var StuEmail=$('#StuEmail').val();
       
       
       if(StuEmail=='')
       {
         $('#StuEmail-error').html('Please Enter Email Id..!');
       }
       else{
         $('#StuEmail-error').html('');
       }
       if(sotp1=='')
       {
         $('#sotp1-error').html('Enter OTP..!');
       }
       else
       {
         $('#sotp1-error').html('');
       }
       if(sotp1) 
       {
             $.ajax({
                url: 'VerifyStuEmailOtp/'+sotp1+'/'+StuEmail,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                
                success:function(data) {
                   
                   if(data)
                   {
                      $.each(data, function(key, value){
                         if(value.EmailOtp==sotp1)
                         {
                            alert('Email Id Is Verified..!');
                            $('#StuEmVrfy').attr('disabled',true);
                         }
                         else
                         {
                            alert('Incorrect OTP..!');
                         }
                      });
                   }
             }
             });
       }
       
    });


});
      </script>
   </body>
</html>


