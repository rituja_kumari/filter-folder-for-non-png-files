  <nav class="sidebar sidebar-offcanvas" id="sidebar">
                              <ul class="nav">
                              @if(session('data')['0']['role']!='U')
                                 <li class="nav-item">
                                    <a class="nav-link" href="admin">
                                    <span class="menu-title">Center Profile</span>
                                    <i class="mdi mdi-home menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="reset-password">
                                    <span class="menu-title">Reset Password</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <!-- <li class="nav-item">
                                    <a class="nav-link" href="candidate-information-approval-course-wise">
                                    <span class="menu-title">Candidate Information/ Approval Course Wise</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li> -->
                                 
                                 <li class="nav-item">
                                    <a class="nav-link" href="MarksEntry">
                                    <span class="menu-title">Student Name Entry</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li> 
                                 <li class="nav-item">
                                    <a class="nav-link" href="uploadLedger">
                                    <span class="menu-title">Upload Ledger File</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li> 
                                 <li class="nav-item">
                                    <a class="nav-link" href="NotificationPanel">
                                    <span class="menu-title">Notification Panel</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li> 
                                 <li class="nav-item">
                                    <a class="nav-link" href="ResultLedger">
                                    <span class="menu-title">Result Declared</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                              @else
                                 <li class="nav-item">
                                    <a class="nav-link" href="admin-doa">
                                    <span class="menu-title">DashBoard</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="center-details">
                                    <span class="menu-title">Center Details</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <!-- <li class="nav-item">
                                    <a class="nav-link" href="fac-show">
                                    <span class="menu-title">Faculty Master</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="Co-list">
                                    <span class="menu-title">Course Master</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="br-list">
                                    <span class="menu-title">Branch Master</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="em-list">
                                    <span class="menu-title">Semester / Exam Master</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li> -->
                                 <li class="nav-item">
                                    <a class="nav-link" href="notificationMaster">
                                    <span class="menu-title">Notification </span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="MarksEntry">
                                    <span class="menu-title">Student Marks Details</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="CountReport">
                                    <span class="menu-title">Student Count Report</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="PublishLedger">
                                    <span class="menu-title">Publish Result</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="StudentResult">
                                    <span class="menu-title">Student Result</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li> 
                                 <li class="nav-item">
                                    <a class="nav-link" href="fac-show">
                                    <span class="menu-title">Test Ritu</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li> 
                              @endif
                                 
                                 <!-- <li class="nav-item">
                                    <a class="nav-link" href="#">
                                    <span class="menu-title">Quest Paper Download Course/ Subject Wise</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="#">
                                    <span class="menu-title">Candidate Attendence Report</span>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" data-toggle="collapse" href="#page-layouts" aria-expanded="false" aria-controls="page-layouts">
                                    <span class="menu-title">Mark Entry Course/ Subject Wise</span>
                                      <i class="menu-arrow"></i>
                                    <i class="mdi mdi-apps menu-icon"></i>
                                    </a>
                                    <div class="collapse" id="page-layouts">
                                       <ul class="nav flex-column sub-menu">
                                          <li class="nav-item"> <a class="nav-link" href="#">Mark Entry Report Course Wise</a></li>
                                          <li class="nav-item"> <a class="nav-link" href="#">Mark Entry Report Upload With Sign And stamp</a></li>
                                       </ul>
                                    </div>
                                 </li> -->
                                
                               
                              </ul>
                           </nav>