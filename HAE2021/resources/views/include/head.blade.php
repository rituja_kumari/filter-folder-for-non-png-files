 <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{ url('/') }}/assets/fonts/materialdesignicons.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/css/vendor.bundle.base.css">
      <!-- endinject -->
      <!-- Plugin css for this page -->
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/style.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/fonts/fontawesome.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/select2/select2.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">