 <footer class="footer">
                                 <div class="d-sm-flex justify-content-center justify-content-sm-between">
                                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021 <a >Directorate of Art ,Maharashtra State,Mumbai</a>. All rights reserved.</span>
                                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Powered by <a href="https://smbgroup.co.in/" target="_blank"> SMB Group of Companies </a></span>
                                 </div>
                              </footer>