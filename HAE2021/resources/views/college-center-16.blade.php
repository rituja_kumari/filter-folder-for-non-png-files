 @php
use App\Helpers\tools;
@endphp
 
<style type="text/css">
   .remove-field{background: red;
      color: #fff;
   }
   .remove-field {
    background: red;
    color: #fff;
    float: right;
    right: 50px;
    outline: none;
    position: relative;
    bottom: 20px;
    border: 0;
    padding: 6px 8px;
}
.multi-field{   
    border-bottom: 20px solid #fdf6ee;
    padding-top: 10px;
    }
.multi-field-wrapper{background: #f5f5f5;
    margin: 20px 0;}
.add-field{
   float: right;
   z-index: 999;
    position: relative;
    right: 50px;
    background: #39c0ed;
    border: 0;
    padding: 10px;
    border-radius: 30px 0 30px 0;
    color: #fff;
}
</style>
 <h3><i class="fa fa-edit"></i> Select College/Centre & Course</h3>
                  <section>
                     <div id="success-msg-course" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                     <form id="stcourse-info"  method="post" action="{{ route('course_info') }}" >
                        @csrf
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                              <h5>Please Fill Your Details</h5>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Select Colllege / Centre Name *</label>
                              <select class="form-control1 required" id="centername" name="centername" onclick="getdropdown('centername','stcrstype','course');">
                                 <option value="">Select</option>
                                @foreach($colleges as $clg)
                                 {{ tools::options_select([$clg->CgId=>$clg->CgName]) }}
                                  @endforeach
                                
                              </select>
                              <span class="text-danger"><strong id="centername-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Course Type*</label>
                              <select class="form-control1 required" name="stcrstype" id="stcrstype" onclick="getamount('stcrstype','amount');" onchange="getsubject('stcrstype','subject','subject');">
                                 <option value="">Select</option>
                              </select>
                              <span class="text-danger"><strong id="stcrstype-error"></strong></span>
                           </div>

                            <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Student Type*</label>
                              <select class="form-control1" name="sttype" id="sttype" onchange="getshow('sttype');">
                                 <option value="">Select</option>
                              </select>
                              <span class="text-danger"><strong id="sttype-error"></strong></span>
                           </div>
                           
                        </div>
                        <div class="row">
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <p class="fees-block">Course Fees: <input type="text" id="amount" name="amount" readonly="true"></p>
                           </div>
                        </div>
                        <hr>
                        <div class="stcontents" id="atkt"  style="display: none;">
                          <div class="multi-field-wrapper">
             <div class="multi-fields">
                <div class="multi-field">
          <button type="button" class="remove-field " id="remove-div" ><i class="fa fa-close"></i></button>
                           <div class="row">
                              <div class="form-group col-6 col-lg-6 col-md-6">
                                 <label>Subject</label>
                                 <select class="form-control1" name="subject[]" id="subject">
                                    <option value="">Select</option>
                                   
                                 </select>
                                 <span class="text-danger"><strong id="subject-error"></strong></span>
                              </div>
                              <div class="form-group col-6 col-lg-6 col-md-6">
                                 <label>Status</label>
                                 <select class="form-control1" name="ststatus[]" id="ststatus">
                                    <option value="">Select</option>
                                    <option value="Pass">Pass</option>
                                    <option value="Fail">Fail</option>
                                 </select>
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="obmarks[]" placeholder="Obtained Marks">
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="ofmarks[]" placeholder="Out Of Marks">
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="passyear[]" placeholder="Year">
                              </div>
                           </div>
                         </div>
                       </div>
                        <button type="button" class="add-field">Add More</button>
                     </div>
                        </div>
                         <div class="row">
                           <div class="form-group col-12 col-lg-12 col-md-12">
                            <table class="table-records" border="1" id="data_row">
                              <tr>
                                <th>Qualification</th>
                                <th>status</th>
                                <th>Obtain Marks/Center</th>
                                <th>Out Of Marks/Grade</th>
                                <th>Year</th>
                              </tr>
                            </table>
                           </div>
                         </div>
                      <br>
                        <div class="row">
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <button type="button" class="btn-admin" id="stcourse-save"> save</button>
                           </div>
                        </div>
                        <div>
                             <span id="success_msg_course"></span>
                           </div>
                     </form>
                  </section>
<script>
   $(document).ready(function(){
    var form=$("#stcourse-info");
       $('#stcourse-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
            var url= $("#stcourse-info").attr('action');
           $.ajax({
               url: url,
               method: 'post',
               data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                  if(data.errors.centername){
                         $( '#centername-error' ).html( "Please Select Center/College Name" );
                     }else{
                         $( '#centername-error' ).html( "");
                     }
                     if(data.errors.stcrstype){
                         $( '#stcrstype-error' ).html( "Please Select Course" );
                     }else{
                         $( '#stcrstype-error' ).html( "");
                     }
                     if(data.errors.sttype){
                         $( '#sttype-error' ).html( "Please Select Student Type" );
                     }else{
                         $( '#sttype-error' ).html( "");
                     } 
                                              
                      
                 }
                 if(data.success) {
                        $( '#centername-error' ).html( "");
                         $( '#stcrstype-error' ).html( "");
                         $( '#sttype-error' ).html( "");
                         $( '#success_msg_course' ).html("Data Inserted..........");
                     $('#success-msg-stcourse').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-stcourse').addClass('hide');
                     }, 3000);
                      window.location.replace('{{route('form')}}');
                 }
               }
           });
       });

      $('#stcrstype').on("change", function() {
        var course_id = $('#stcrstype').val();
        var url= 'sttype';
        $.ajax({
            type:"GET",
            url:url+"?CfCoId="+course_id,
            success:function(res){        
            if(res){
               console.log(res);
                $("#sttype").append('<option value="">Select</option>');
               $.each(res,function(key,value){
                     $("#sttype").append('<option value="'+key+'">'+value+'</option>');
              });
               
            }else{
              // $("#"+concat_id).empty();
            }
            }
          });
      });
   });

   
   function getamount(course_id,url)
   {
       $('#amount').val('');
       $('#stsubject1').val('');
        $('#sttype').empty();
      var course_id = $('#'+course_id).val(); 
      var sttype = $('#sttype').val();
        if((course_id)){
          $.ajax({
            type:"GET",
            url:url+"?CfCoId="+course_id,
            success:function(res){        
            if(res){
               console.log(res);
               $.each(res,function(key,value){
                     $('#amount').val(value);
              });
               
            }else{
              // $("#"+concat_id).empty();
            }
            }
          });
        }else{
          $('#amount').val('');
          
        }
   }

   function getsubject(id,concat_id,url)
   {
      var id = $('#'+id).val(); 
      $('#sttype').empty();
        if(id){
          $.ajax({
            type:"GET",
            url:url+"?id="+id,
            success:function(res){        
            if(res){
               console.log(res);
               $.each(res,function(key,value){
                  $("#"+concat_id).append('<option value="'+key+'">'+value+'</option>');
              });
               
            }else{
                $("#"+concat_id).empty();
            }
            }
          });
        }else{
          $("#"+concat_id).empty();
          
        }
   }
   function getshow(id)
   {
      var id = $('#'+id).val(); 
      var course_id = $('#stcrstype').val(); 
      if (id=='1')
      {
         $('#atkt').hide();
      }else{
         $('#atkt').show();
      }
      var url='edu';
$(".row_edu").remove();
      if(id){
          $.ajax({
            type:"GET",
            url:url,
            data:{'sttype':id,'course_id':course_id},
            success:function(res){        
            if(res){
               console.log(res);
               $.each(res,function(key,value){
                  $("#data_row").append('<tr class="row_edu"><td><input type="text" name="edu_type[]" id="edu_type" value="'+value+'" readonly></td><td><select name="edu_status[]" id="edu_status"><option value="">Select</option><option value="YES">YES</option><option value="NO">NO</option></select></td><td><input type="text" name="edu_obt[]" id="edu_obt" style="width:100px;"></td><td><input type="text" name="edu_ofm[]" id="edu_ofm" style="width:100px;"></td><td><input type="text" name="edu_year[]" id="edu_year" style="width:100px;"></td></tr>');
              });
               
            }else{
               $(".row_edu").remove();
            }
            }
          });
        }else{
           $(".row_edu").remove();
          
        }
   }

        $('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
      $('#remove-div').show();
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
        
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
</script>