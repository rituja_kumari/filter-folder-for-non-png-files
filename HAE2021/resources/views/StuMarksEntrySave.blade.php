@php 

   
   $CgCode='';
@endphp
@if($SpCl_value=='ShowBranchMaster')
    <script> 
        var EmptyArr = ["Selbranch","Selexam","EfRollNo","FullName"];
        for (x of EmptyArr) 
        {
            if(x=='EfRollNo')
            { 
                $('#'+x).val('@php echo $CgCode; @endphp');
            }
            else
            {
                $('#'+x).val('');
            }
        }
        $('#AddUpdateBtn').val('Add');
        $('#AddUpdateBtn').attr('onclick',"return ajax_combo3('AddStudentInfo/','','#res','MarksEntry')");
    </script>
   
    @php
        $Brcondition[]=array("ColumnName"=>"CfCoId","Operator"=>"=","ColumnValue"=>$courses);
        $Enccondition=json_encode($Brcondition);
        $branch=sel_branch($Enccondition,'','','','form-control1','',"ajax_combo3('ShowExam/','','#DivExam','MarksEntry')",'1'); 
    @endphp    
  
    {{ $branch }}  
    
@elseif($SpCl_value=='ShowExamMaster')
    <script> 
        var EmptyArr = ["Selexam","EfRollNo","FullName","EfCate"];
        for (x of EmptyArr) 
        {
            if(x=='EfRollNo')
            { 
                $('#'+x).val('@php echo $CgCode; @endphp');
            }
            else
            {
                $('#'+x).val('');
            }
        }
        $('#AddUpdateBtn').val('Add');
        $('#AddUpdateBtn').attr('onclick',"return ajax_combo3('AddStudentInfo/','','#res','MarksEntry')");
    </script>
    @php
        $Excondition[]=array("ColumnName"=>"CfCoId","Operator"=>"=","ColumnValue"=>$courses);
        $Excondition[]=array("ColumnName"=>"CfBrId","Operator"=>"=","ColumnValue"=>$branch);
        $Enccondition=json_encode($Excondition);
        
       echo $exam =sel_exam($Enccondition,'','','','form-control1','',"ajax_combo3('OnExamChange/','','#subjectInfo','MarksEntry')",'1'); 
    @endphp    
    {{ $exam }} 

@elseif($SpCl_value=='OnExamChange')
    <script> 
        var EmptyArr = ["EfRollNo","FullName","EfCate"];
        for (x of EmptyArr) 
        {
            if(x=='EfRollNo')
            { 
                $('#'+x).val('@php echo $CgCode; @endphp');
            }
            else
            {
                $('#'+x).val('');
            }
        }
        $('#AddUpdateBtn').val('Add');
        $('#AddUpdateBtn').attr('onclick',"return ajax_combo3('AddStudentInfo/','','#res','MarksEntry')");
    </script>  
@elseif($SpCl_value=='ShowRollName')
    @if($mode!='ledger' and $mode!='FinalSubmit')
         @include('StuMarksEntrySubList') 
    @endif
    @if($mode=='ledger') 
    <script> 
            var EmptyArr = ["EfRollNo","FullName","EfCate"];
            for (x of EmptyArr) 
            {
                if(x=='EfRollNo')
                { 
                    $('#'+x).val('@php echo $CgCode; @endphp');
                }
                else
                {
                    $('#'+x).val('');
                }
            }
            $('#EfCate').css('pointer-events','');$('#EfCate').attr('onchange',"return ajax_combo3('ShowRollName/','','#subjectInfo','MarksEntry')");
        </script>
    @endif
    @if($mode=='0' or $mode=='ledger' or $mode=='Edit' or $mode=='FinalSubmit') 
      
        @include('StuMarksEntryLedger') 
    @else
   
    @endif
    @if(!empty($FinalSubmissionDoneOrNot))
            
        <script> $('.IsSubmit_disabled').hide();</script>
        @if($mode=='FinalSubmit')
            <script>alert('Final Submission done successfully');</script>
        @endif
    @else 
        <script> $('.IsSubmit_disabled').show(); </script> 
    @endif
   
   
@elseif($SpCl_value=='AutoCalcObtMarks')
   
   {{$SumObt}} 
    @php $StoredPassFail=json_decode($StoredPassFail,true); @endphp   

        @foreach($StoredPassFail as $EsId =>$PassFail )
            @php if($PassFail=='1'){   $PassFailVal='Pass';}elseif($PassFail=='0'){ $PassFailVal='Fail';}else{$PassFailVal=''; }   @endphp
            <script>
                $('#ArrForPassFailsStatus_{{$EsId}}').val('{{$PassFail}}'); 
                $('#SpanPassFailsStatus_{{$EsId}}').text('{{$PassFailVal}}'); 
            </script>
        @endforeach  

@elseif($SpCl_value=='AddStudentInfo')
   
    <script> 
        $('#EfRollNo').val('@php echo $CgCode; @endphp'); 
        $('#FullName').val(''); 
        $('#EfCate').val(''); 
        $('#subjectInfo').html('');
        $( "#EfRollNo" ).focus();
        $('#AddUpdateBtn').val('Add');
        $('#AddUpdateBtn').attr('onclick',"return ajax_combo3('AddStudentInfo/','','#res','MarksEntry')");
        ajax_combo3('OnExamChange/','','#subjectInfo','MarksEntry');
      
        $('#EfCate').css('pointer-events','');$('#EfCate').attr('onchange',"return ajax_combo3('ShowRollName/','','#subjectInfo','MarksEntry')");
    
    </script>
    
@elseif($SpCl_value=='ShowEditView')
    <script> 
        $('#EfRollNo').val('@php echo $CgCode; @endphp'); 
        $('#FullName').val(''); 
        $('#EfCate').val(''); 
        $('#subjectInfo').html('');
        $( "#EfRollNo" ).focus();
        $('#AddUpdateBtn').val('Add');
        $('#AddUpdateBtn').attr('onclick',"return ajax_combo3('AddStudentInfo/','','#res','MarksEntry')");
        ajax_combo3('ShowRollName/','','#subjectInfo','MarksEntry');
    </script>
   
 @elseif($SpCl_value=='DeleteStudent')
    <script> 
        $('#EfRollNo').val('@php echo $CgCode; @endphp');
        $('#FullName').val(''); 
        $('#EfCate').val(''); 
        $('#subjectInfo').html('');
        $( "#EfRollNo" ).focus();
        $('#AddUpdateBtn').val('Add');
        $('#AddUpdateBtn').attr('onclick',"return ajax_combo3('AddStudentInfo/','','#res','MarksEntry')");
        ajax_combo3('ShowRollName/','','#subjectInfo','MarksEntry');
    </script> 
@elseif($SpCl_value=='modal')
dsd
@include('StuMarksEntrySubList')

@elseif($SpCl_value=='CategoryChange')
    @php echo $EfCate=$request->EfCate;@endphp 


@else     
@endif