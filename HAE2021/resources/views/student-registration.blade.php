<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="_token" content="{{csrf_token()}}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/owl.carousel.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/app-2.css" type="text/css"/>
      <link rel="stylesheet" href="{{ url('/') }}/css/parsley.css" type="text/css"/>
      <link rel="stylesheet" href="{{ url('/') }}/css/docs.css" type="text/css"/>
      <!-- Styles -->
      <!-- Styles -->
      <style type="text/css">
         .mr-0{margin: 0;}
         .pt-10{padding-top: 10px;}
         .text-danger {
         color: #a94442;
         font-size: 12px;
         }
         .text-danger strong{font-weight: 100;}
      </style>
   </head>
   <body class="antialiased">
      <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="">
                        <header>
                           <div class="container">
                              <div class="row">
                                 <div class="col-lg-1 col-xs-4">
                                    <div class="logo-img">
                                       <img src="{{ url('/') }}/img/logo11.png" alt="Logo">
                                    </div>
                                 </div>
                                 <div class="col-lg-5 text-header row ">
                                    <p>Directorate of Art ,Maharashtra State,Mumbai<br>
                                       कला संचालनालय, महाराष्ट्र राज्य, मुंबई
                                    </p>
                                 </div>
                                 <div class="col-lg-6">
                                    <ul class="nav-ul">
                                       <li><a href="#" data-toggle="modal" data-target="#myModal-3">Login</a></li>
                                       <li><a href="#" data-toggle="modal" data-target="#myModal-4">Register</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </header>
                        <div class="row">
                           <div class="col-lg-6 m-pd-0">
                              <div class="carousel-wrap">
                                 <div class="owl-carousel">
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/sl1.jpg"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/sl2.jpg"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/sl3.jpg"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/sl4.jpg"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/sl5.jpg"></div>
                                    <div class="item"><img src="{{ url('/') }}/img/sliders/sl6.jpg"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-12 col-pad-0 bg-color">
                              <div class="holder">
                                 <div class="holder-title">
                                    <h6 class=""><b>INSTRUCTIONS</b></h6>
                                 </div>
                                 <ul id="ticker01">
                                    <li>
                                       <a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet</a>
                                       <p>consectetur adipisicing Lorem ipsum dolor sit amet, consectetur </p>
                                    </li>
                                    <li>
                                       <a href="#">Lorem ipsum dolor sit amet, <span class="blink">New</span> </a>
                                       <p>Orem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet</p>
                                    </li>
                                    <li>
                                       <a href="#">consectetur adipisicing Lorem ipsum dolor sit amet, consectetur </a>
                                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet</p>
                                    </li>
                                    <li>
                                       <a href="#">Lorem ipsum dolor sit amet, conse Lorem ipsum dolor sit amet <span class="blink">New</span></a>
                                       <p>consectetur adipisicing Lorem ipsum dolor sit amet, consectetur </p>
                                    </li>
                                    <li>
                                       <a href="#">Lorem ipsum dolor sit amet, <span class="blink">New</span></a>
                                       <p>Orem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet</p>
                                    </li>
                                    <li>
                                       <a href="#">consectetur adipisicing Lorem ipsum dolor sit amet, consectetur </a>
                                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet</p>
                                    </li>
                                 </ul>
                              </div>
                              <div class="holder-2">
                                 <div class="holder-title">
                                    <h6 class=""><b>HELPDESK</b></h6>
                                 </div>
                                 <div class="helpdesk">
                                    <h5 class="">Technical Helpline No ‎:<a href="tel:1800 212 005 599"> 1800 212 005 599</a></h5>
                                    <h5 class="">Email: ‎<a href="mailto:info@decorative-arts.gmail.com"> info@decorative-arts.gmail.com</a></h5>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="{{ url('/') }}/js/jquery.min.js"></script>
      <script src="{{ url('/') }}/js/owl.carousel.min.js"></script>
      <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      <script src="{{ url('/') }}/js/custom.js"></script>
      <script src="{{ url('/')}}/js/institute.js" ></script>
      <!-- login -->
      <div id="myModal-3" class="modal fade" role="dialog">
         <div class="modal-dialog login-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div >
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div id="success-msg5" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="model" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Check your mail for login confirmation!!
                     </div>
                  </div>
                  <form class="row" id="login-form"  method="post" action="{{ route('slogin') }}" >
                     {{ csrf_field() }}
                     <div class="col-lg-12">
                        <h6 class="register-text">Student Login</h6>
                     </div>
                     <div class="form-group col-lg-12">
                        <div class="row">
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Registered Email Id or Mobile Number*</label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" name="lemail" placeholder="Enter Email Id or Mobile Number">
                                 <button class="btn-otp" id="btn-otp1">Send OTP</button>
                              </div>
                              <span class="text-danger"><strong id="lemail-error"></strong></span>
                              <div class="forgot-text"><a href="#" id="close-3" data-toggle="modal" data-target="#myModal-5">Forgot Login</a></div>
                           </div>
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Enter OTP Received on Email-Id or Mobile number*</label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" name="lotp" placeholder="Enter OTP">
                                 <button class="btn-otp v-success" id="btn-otp2">Verify OTP</button>
                              </div>
                              <span class="text-danger"><strong id="lotp-error"></strong></span>
                           </div>
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Password*</label>
                              <input type="text" class="form-control1" name="lpassword" placeholder="" disabled="" readonly="">
                              <span class="text-danger"><strong id="lpassword-error"></strong></span>
                              <div class="forgot-text"><a href="#" data-toggle="modal" data-target="#myModal-6" id="close-4">Forgot Password</a></div>
                           </div>
                           <div class="col-lg-6">
                              <button type="submit" class="fxt-btn-fill" id="loginForm">Login</button>
                           </div>
                           <div class="col-lg-12">
                              <p class="demo-text">Don't have an account?<a href="#" id="close-1" data-toggle="modal" data-target="#myModal-4"> Register Here</a> </p>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- end login -->
      <!-- register -->
      <div id="myModal-4" class="modal fade" role="dialog">
         <div class="modal-dialog register-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div >
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div id="success-msg-3" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="model" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Check your mail for login confirmation!!
                     </div>
                  </div>
                  <form class="row" id="register-form"  method="post" action="{{ route('registerss') }}" >
                     {{ csrf_field() }}
                     <div class="col-lg-12">
                        <h6 class="register-text">Student Registration</h6>
                     </div>
                     <div class="form-group col-lg-12">
                        <div class="row group-shadow">
                           <div class="col-lg-3">
                              <label class="input-label" for="name">Email Id* </label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" id="StuEmail" name="StuEmail" placeholder="Enter Email Id">
                                 <button  type="button" class="btn-otp" id="Emailotp">Send OTP</button>
                              </div>
                              <span class="text-danger"><strong id="StuEmail-error"></strong></span>
                           </div>
                           <div class="col-lg-3">
                              <label class="input-label">Enter OTP Received on Email*</label>
                              <input type="text" class="form-control1" id="sotp1" name="sotp1" placeholder="Enter OTP">
                              <button  type="button" class="btn-otp" id="StuEmVrfy">Verify</button>
                              <span class="text-danger"><strong id="sotp1-error"></strong></span>
                           </div>
                           <div class="col-lg-3">
                              <label class="input-label" for="name">Mobile No.* </label>
                              <div class=" d-flex">
                                 <input type="text" class="form-control1" id="StuMob" name="StuMob" placeholder="Enter Mobile No.">
                                 <button  type="button" class="btn-otp" id="Mblotp">Send OTP</button>
                                 
                              </div>
                              <span class="text-danger"><strong id="StuMob-error"></strong></span>
                           </div>
                           <div class="col-lg-3">
                              <label class="input-label">Enter OTP Received on Mobile*</label>
                              <input type="text" class="form-control1" id="sotp2" name="sotp2" placeholder="Enter OTP">
                               <button  type="button" class="btn-otp" id="StuMoVrfy">Verify</button>
                              <span class="text-danger"><strong id="sotp2-error"></strong></span>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-lg-12 ">
                        <div class="row group-shadow">
                           <div class="col-md-4">
                              <label class="input-label" for="name">Password*  </label>
                              <input type="password" class="form-control1" id="StuPass" name="StuPass" placeholder="Enter Password" onchange='return passcheck();'>
                              <span class="text-danger"><strong id="StuPass-error"></strong></span>
                           </div>
                           <div class="col-md-4">
                              <label class="input-label">Confirm Password*</label>
                              <input type="password" class="form-control1" id="StuPassC" name="StuPassC" placeholder="Re-Enter Password">
                              <span class="text-danger"><strong id="StuPassC-error"></strong></span>
                           </div>
                           <div class="col-md-4">
                              <label class="input-label" for="name">Dat Of Birth*  </label>
                              <input type="date" class="form-control1" name="StuDob" placeholder="Enter Password">
                              <span class="text-danger"><strong id="StuDob-error"></strong></span>
                           </div>
                        </div>
                     </div>
                     <!--    -->
                     
                     <div class="form-group col-lg-6 " >
                        <div class="row">
                           <div class="col-lg-4">
                              <button type="submit" class="fxt-btn-fill" id="s-register">Register</button>
                           </div>
                           <div class="col-lg-4">
                              <button type="button" data-dismiss="modal" class="fxt-btn-fill">Cancel</button>
                           </div>
                           <div class="col-lg-12">
                              <p class="demo-text">Already have an account?<a href="#" id="close-2" data-toggle="modal" data-target="#myModal-3"> Login Here</a> </p>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- end register ------------------------------------------------------->
      <!-- forgot login -->
      <div id="myModal-5" class="modal fade" role="dialog">
         <div class="modal-dialog login-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div >
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div id="success-msg6" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="model" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Check your mail for login confirmation!!
                     </div>
                  </div>
                  <form class="row" id="forgot-login"  method="post" action="{{ route('flogin') }}" >
                     {{ csrf_field() }}
                     <div class="col-lg-12">
                        <h6 class="register-text">Forgot Login</h6>
                     </div>
                     <div class="form-group col-lg-12">
                        <div class="row">
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Select User Id*</label>
                              <select id="usertype" class="form-control1">
                                 <option></option>
                                 <option value="funame">Username</option>
                                 <option value="femail">Email Id</option>
                                 <option value="mnumber">Mobile Number</option>
                              </select>
                           </div>
                           <div class="col-lg-12 userid" id="funame" style="display: none;">
                              <label class="input-label" for="name">Username*</label>
                              <input type="text" class="form-control1" name="fusername*" placeholder="Enter Username" >
                              <span class="text-danger"><strong id="fusername-error"></strong></span>
                           </div>
                           <div class="col-lg-12 userid" id="femail" style="display: none;">
                              <label class="input-label" for="name">Email ID*</label>
                              <input type="text" class="form-control1" name="femailid*" placeholder="Enter Email-Id" >
                              <span class="text-danger"><strong id="femailid-error"></strong></span>
                           </div>
                           <div class="col-lg-12 userid" id="mnumber" style="display: none;">
                              <label class="input-label" for="name">Mobile Number*</label>
                              <input type="text" class="form-control1" name="fmobile*" placeholder="Enter Mobile No." >
                              <span class="text-danger"><strong id="fmobile-error"></strong></span>
                           </div>
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Date of Birth*</label>
                              <input type="text" class="form-control1" name="fdate*" placeholder="Enter Username" >
                              <span class="text-danger"><strong id="fdate-error"></strong></span>
                           </div>
                           <div class="col-lg-12 mb-30">
                              <label class="input-label" for="name">Mothers Name*</label>
                              <input type="text" class="form-control1" name="fmother*" placeholder="Enter Mothers Name" >
                              <span class="text-danger"><strong id="fmother-error"></strong></span>
                           </div>
                           <div class="col-lg-6">
                              <button type="submit" class="fxt-btn-fill" id="forgotlogin">Login</button>
                           </div>
                           <div class="col-lg-6">
                             <button type="button" data-dismiss="modal" class="fxt-btn-fill">Cancel</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- forgot-login -->

      <!-- forgot Password -->
         <div id="myModal-6" class="modal fade" role="dialog">
         <div class="modal-dialog login-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div >
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <div id="success-msg7" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="model" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Check your mail for login confirmation!!
                     </div>
                  </div>
                  <form class="row" id="forgot-ps-form"  method="post" action="{{ route('fplogin') }}" >
                     {{ csrf_field() }}
                     <div class="col-lg-12">
                        <h6 class="register-text">Forgot Password </h6>
                     </div>
                     <div class="form-group col-lg-12">
                        <div class="row">
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Registered Email Id or Mobile Number*</label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" name="fpemail" placeholder="Enter Email Id or Mobile Number">
                                 <button class="btn-otp" id="btn-otp1">Send OTP</button>
                              </div>
                              <span class="text-danger"><strong id="fpemail-error"></strong></span>
                             
                           </div>
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Enter OTP Received on Email-Id or Mobile number*</label>
                              <div class="d-flex">
                                 <input type="text" class="form-control1" name="fpotp" placeholder="Enter OTP">
                                 <button class="btn-otp v-success" id="btn-otp2">Verify OTP</button>
                              </div>
                              <span class="text-danger"><strong id="fpotp-error"></strong></span>
                           </div>
                           <div class="col-lg-12">
                              <label class="input-label" for="name">Date Of Birth*</label>
                              <input type="date" class="form-control1" name="fpdate" placeholder="">
                              <span class="text-danger"><strong id="fpdate-error"></strong></span>
                            
                           </div>
                           <div class="col-lg-6">
                              <button type="submit" class="fxt-btn-fill" id="fpsubmit">Submit</button>
                           </div>
                          
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- forgot password -->
      <!-- Validation Code start -->
      <script type="text/javascript">
         $(function() {
         $('#usertype').change(function(){
         $('.userid').hide();
         $('#' + $(this).val()).show();
         });
         });
      </script>
      <script type="text/javascript">
         $(function () {
         $("#close-1").on('click', function() {
             $('#myModal-3').modal('hide');
         });
          $("#close-2").on('click', function() {
             $('#myModal-4').modal('hide');
         });
          $("#close-3").on('click', function() {
             $('#myModal-3').modal('hide');
         });
          $("#close-4").on('click', function() {
             $('#myModal-3').modal('hide');
         });
                 });
      </script>
      <!-- login -->
      <script>
         $(document).ready(function(){
          var form=$("#login-form");
             $('#loginForm').click(function(e){
                 e.preventDefault();
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                 });
                 $.ajax({
                     url: "{{ url('/register-l') }}",
         
                     method: 'post',
                      data:form.serialize(),
                     success: function(data){
                      console.log(data)
                       if(data.errors) {
                           if(data.errors.lemail){
                               $( '#lemail-error' ).html( "Please Enter Email-Id" );
                           }else{
                               $( '#lemail-error' ).html( "");
                           }
                           if(data.errors.lotp){
                               $( '#lotp-error' ).html( "Please Enter OTP" );
                           }else{
                               $( '#lotp-error' ).html( "");
                           } 
                           if(data.errors.lpassword){
                               $( '#lpassword-error' ).html( "Please Enter Password" );
                           }else{
                               $( '#lpassword-error' ).html( "");
                           }                          
                       }
                       if(data.success) {
                               $( '#lemail-error' ).html( "");
                               $( '#lotp-error' ).html( "");
                               $( '#lpassword-error' ).html( "");
                           $('#success-msg5').removeClass('hide');
                           setInterval(function(){ 
                               $('#success-msg5').addClass('hide');
                           }, 3000);
                       }
                     }
                 });
             });
         });
      </script>
      <!-- login -->
      <!-- forgot-login -->
      <script>
         $(document).ready(function(){
          var form=$("#forgot-login");
             $('#forgotlogin').click(function(e){
                 e.preventDefault();
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                 });
                 $.ajax({
                     url: "{{ url('/register-f') }}",
         
                     method: 'post',
                      data:form.serialize(),
                     success: function(data){
                      console.log(data)
                       if(data.errors) {
                           if(data.errors.fusername){
                               $( '#fusername-error' ).html( "Please Enter Username" );
                           }else{
                               $( '#fusername-error' ).html( "");
                           }
                           if(data.errors.femailid){
                               $( '#femailid-error' ).html( "Please Enter Email-Id" );
                           }else{
                               $( '#femailid-error' ).html( "");
                           } 
                           if(data.errors.fmobile){
                               $( '#fmobile-error' ).html( "Please Enter Mobile No." );
                           }else{
                               $( '#fmobile-error' ).html( "");
                           } 
                            if(data.errors.fdate){
                               $( '#fdate-error' ).html( "Please Select Date Of Birth" );
                           }else{
                               $( '#fdate-error' ).html( "");
                           }  
                            if(data.errors.fdate){
                               $( '#fmother-error' ).html( "Please Enter Mothers Name" );
                           }else{
                               $( '#fmother-error' ).html( "");
                           }                          
                       }
                       if(data.success) {
                               $( '#fusername-error' ).html( "");
                               $( '#femailid-error' ).html( "");
                               $( '#fmobile-error' ).html( "");
                               $( '#fdate-error' ).html( "");
                               $( '#fmother-error' ).html( "");
                           $('#success-msg6').removeClass('hide');
                           setInterval(function(){ 
                               $('#success-msg6').addClass('hide');
                           }, 3000);
                       }
                     }
                 });
             });
         });
      </script>
      <!-- forgot-login -->
      <!-- register -->
      <script>
         function passcheck()
         {
            $('#StuPass').attr('type', 'password');
         }   
      </script>
      <script>
         $(document).ready(function(){
           var form=$("#register-form");
              $('#s-register').click(function(e){
                  e.preventDefault();
                  $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                      }
                  });
                  //chk for passwrd and confirm passwrd
                  var StuPass=$('#StuPass').val();
                  var StuPassC=$('#StuPassC').val();
                  if(StuPass!=StuPassC)
                  {
                      //console.log("its working..");
                     $('#StuPassC-error').html( "Please Enter Correct Password " );
                  }
                  elseif(StuPass==StuPassC){
                     $( '#StuPassC-error' ).html(" ");
                  }
                  $.ajax({
                      url: "{{ url('/register-s') }}",
                      method: 'post',
                       data:form.serialize(),
                      success: function(data){
                       console.log(data)
                        console.log("its working..");
                        if(data.errors) {
                            if(data.errors.StuEmail){
                                $( '#StuEmail-error' ).html(data.errors.StuEmail[0]);
                            }else{
                                $( '#StuEmail-error' ).html( "");
                            }
                            if(data.errors.sotp1){
                                $( '#sotp1-error' ).html( data.errors.sotp1[0] );
                            }else{
                                $( '#sotp1-error' ).html( "");
                            } 
                            if(data.errors.StuMob){
                                $( '#StuMob-error' ).html(data.errors.StuMob[0]);
                            }else{
                                $( '#StuMob-error' ).html( "");
                            } 
                            if(data.errors.sotp2){
                                $( '#sotp2-error' ).html( data.errors.sotp2[0]);
                            }else{
                                $( '#sotp2-error' ).html( "");
                            }
                            if(data.errors.StuPass){
                                $( '#StuPass-error' ).html( data.errors.StuPass[0] );
                            }else{
                                $( '#StuPass-error' ).html( "");
                            }
                            if(data.errors.StuPassC){
                                $( '#StuPassC-error' ).html( data.errors.StuPassC[0] );
                            }else{
                                $( '#StuPassC-error' ).html( "");
                            } 
                            if(data.errors.StuDob){
                                $( '#StuDob-error' ).html( data.errors.StuDob[0] );
                            }else{
                                $( '#StuDob-error' ).html( "");
                            } 
                            
                        }
                        if(data.success) {
                                $( '#StuEmail-error' ).html( "");
                                $( '#sotp1-error' ).html( "");
                                $( '#StuMob-error' ).html( "");
                                $( '#sotp2-error' ).html( "");
                                $( '#StuPass-error' ).html( "");
                                $( '#StuPassC-error' ).html( "");
                                $( '#StuDob-error' ).html( "");

                               $('#success-msg-3').removeClass('hide');
                               setInterval(function(){ 
                                   $('#success-msg-3').addClass('hide');
                               }, 3000);
                              // alert('Successfully Registered!');
                               /*$('#StuEmail').val('');
                               $('#StuMob').val('');
                               $('#StuPass').val('');
                               $('#StuPassC').val('');
                               $('#StuDob').val('');
                               $('#sotp1').val('');
                               $('#sotp2').val('');*/
                        }
                      }
                  });
              });
          });
      </script>
      <!-- register -->


      <!-- forgotpassword -->
      <script>
         $(document).ready(function(){
          var form=$("#forgot-ps-form");
             $('#fpsubmit').click(function(e){
                 e.preventDefault();
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                 });
                 $.ajax({
                     url: "{{ url('/register-fp') }}",
         
                     method: 'post',
                      data:form.serialize(),
                     success: function(data){
                      console.log(data)
                       if(data.errors) {
                           if(data.errors.fpemail){
                               $( '#fpemail-error' ).html( "Please Enter Email-Id" );
                           }else{
                               $( '#fpemail-error' ).html( "");
                           }
                           if(data.errors.fpotp){
                               $( '#fpotp-error' ).html( "Please Enter OTP" );
                           }else{
                               $( '#fpotp-error' ).html( "");
                           } 
                           if(data.errors.fpdate){
                               $( '#fpdate-error' ).html( "Please Enter Password" );
                           }else{
                               $( '#fpdate-error' ).html( "");
                           }                          
                       }
                       if(data.success) {
                               $( '#fpemail-error' ).html( "");
                               $( '#fpotp-error' ).html( "");
                               $( '#fpdate-error' ).html( "");
                           $('#success-msg7').removeClass('hide');
                           setInterval(function(){ 
                               $('#success-msg7').addClass('hide');
                           }, 3000);
                       }
                     }
                 });
             });
         });
      </script>
      <!-- forgotpasssword -->
      <!-- Validation Code end -->

      <!-- Student Registration Mobile and Email Verification  -->
      <script type="text/javascript">
         $(document).ready(function() {
          $('#Mblotp').on('click', function() {
       // console.log("its working..");
       var StuMob=$('#StuMob').val();
       
       if(StuMob=='')
       {
         $('#StuMob-error').html('Please Enter Mobile Number..!');
       }
       else{
         $('#StuMob-error').html('');
       }
      if(StuMob!='') 
      {
         //console.log("its working..");
            $.ajax({
               url: 'SendOtp-s/'+StuMob,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}"},
               dataType: "json",
               
               success:function(data) {
                  
                  if(data)
                  {
                     alert('Otp Send Successfully');
                     $('#Mblotp').attr('disabled',true);
                  }
            }
            });
      }
       
    });

    $('#Emailotp').on('click', function() {
     
      var StuEmail=$('#StuEmail').val();
       console.log("its working..");
       if(StuEmail=='')
       {
         $('#StuEmail-error').html('Please Enter Email Id..!');
       }
       else{
         $('#StuEmail-error').html('');
       }
       if(StuEmail!='') 
      {
         //console.log("its working..");
            $.ajax({
               url: 'SendEmailOtp-s/'+StuEmail,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}"},
               dataType: "json",
               
               success:function(data) {
                  
                  if(data)
                  {
                     alert('Otp Send Successfully');
                     $('#Emailotp').attr('disabled',true);
                  }
            }
            });
      }

    });

    $('#StuMoVrfy').on('click', function() {
     
      var sotp2 = $('#sotp2').val();
      var StuMob=$('#StuMob').val();
      //console.log("its working..");
       if(StuMob=='')
       {
         $('#StuMob-error').html('Please Enter Mobile Number..!');
       }
       else{
         $('#StuMob-error').html('');
       }
       if(sotp2=='')
       {
         $('#sotp2-error').html('Enter OTP..!');
       }
       else
       {
         $('#sotp2-error').html('');  
       }
       if(sotp2) {
         //console.log("its working..");
             $.ajax({
                url: 'VerifyStuOtp/'+sotp2+'/'+StuMob,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                
                success:function(data) {
                   
                   if(data)
                   {
                      $.each(data, function(key, value){
                         if(value.MobileOtp==sotp2)
                         {
                            alert('Mobile Verified..!');
                            $('#StuMoVrfy').attr('disabled',true);
                         }
                         else
                         {
                           alert('Incorrect OTP..!');
                         }
                      });
                   }
             }
             });
       }
       
    });

     $('#StuEmVrfy').on('click', function() 
    {
       var sotp1 = $('#sotp1').val();
       var StuEmail=$('#StuEmail').val();
       
       
       if(StuEmail=='')
       {
         $('#StuEmail-error').html('Please Enter Email Id..!');
       }
       else{
         $('#StuEmail-error').html('');
       }
       if(sotp1=='')
       {
         $('#sotp1-error').html('Enter OTP..!');
       }
       else
       {
         $('#sotp1-error').html('');
       }
       if(sotp1) 
       {
             $.ajax({
                url: 'VerifyStuEmailOtp/'+sotp1+'/'+StuEmail,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                
                success:function(data) {
                   
                   if(data)
                   {
                      $.each(data, function(key, value){
                         if(value.EmailOtp==sotp1)
                         {
                            alert('Email Id Is Verified..!');
                            $('#StuEmVrfy').attr('disabled',true);
                         }
                         else
                         {
                            alert('Incorrect OTP..!');
                         }
                      });
                   }
             }
             });
       }
       
    });

   });
      </script>


   </body>
</html>