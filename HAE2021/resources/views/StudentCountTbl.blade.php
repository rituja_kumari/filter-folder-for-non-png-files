<style>
table tbody tr td {
   padding-top:5px !important;padding-bottom:5px !important;
}
table thead th  {
   padding-top:5px !important;padding-bottom:5px !important;
}
</style>

      @php $ExamId=$ExamName=$CoursId=$Arrlog=$ArrCollegeCode=$ArrCollegeName=$ArrCollegeDsName=array(); @endphp 
      @foreach($CollegeModel as $college)
         @php
            $ArrCollegeCode[$college->CgId]=$college->CgCode;
            $ArrCollegeName[$college->CgId]=$college->CgName;
            $ArrCollegeDsName[$college->CgId]=$college->DsName;
         @endphp
      @endforeach 
      @foreach($ctchangelog as $log)
         @php
            $Arrlog[$log->user_id.'_'.$log->activity_id]=$log->activity_id;
            $ArrlogSub[$log->user_id]=$log->user_id;
            $ArrlogSub1[$log->activity_id]=$log->activity_id;
         @endphp
      @endforeach 
      @foreach($ctchangelogpub as $pub)
         @php
            $ArrlogPub[$pub->user_id]=$pub->user_id;
            $ArrlogPub1[$pub->activity_id]=$pub->activity_id;
         @endphp
      @endforeach 

      @foreach($Exam as $Examinfo)
         @php
            $CoursId[$Examinfo->EmId]=$Examinfo->CfCoId;
            $ExamId[$Examinfo->EmId]=$Examinfo->EmId;
            $ExamName[$Examinfo->EmId]=$Examinfo->EmName;
         @endphp
      @endforeach 
      
   
      
      @foreach($ExamId as $Id)
         @if($SpCl_value!='PDF')   <h3>{{$ExamName[$Id]}}</h3> @endif
        @if ($message = Session::get('success'))

            <div class="alert alert-success alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

            </div>

         @endif
         <table class="table-records" border="1" width="100%" style="border-collapse: collapse;">
            <thead>
               @if($SpCl_value=='PDF')
                  <tr>
                     <th class="br-0" colspan="7"><h2>DIRECTORATE OF ART , MAHARASHTRA STATE, MUMBAI<br>HIGHER ART EXAMINATION 2021 <br> Course : {{$ExamName[$Id]}}</h2></th>    
                  </tr>
               @endif
               <tr style="text-align:center">
                  <th>Sr No</th>
                  <th>College Code</th>
                  <th style="text-align:left">College Name</th>
                  <th>Districts</th>
                  <th>Student Count</th>
                  <th>Submission Status</th>
                  <th>Final Upload</th>
                  @if(session('data')['0']['username']=='exxon')
                  <th>Edit</th>
                  <th>Unpublish</th>
                  @endif
               </tr>
            </thead>
            <tbody>
               @php $count=0; @endphp
               @foreach($CountDetails as $Details)
                  @if($Id==$Details->EfEmId)
                     <tr align="center">
                        <td>{{++$count}}</td>
                        <td>{{$ArrCollegeCode[$Details->EfCgId]}}</td>
                        <td style="text-align:left">{{$ArrCollegeName[$Details->EfCgId]}}</td>
                        <td >{{$ArrCollegeDsName[$Details->EfCgId]}}</td>
                        
                        <td>{{$Details->num}}</td>
                        <td>
                           @if(!empty($Arrlog[$Details->EfCgId.'_'.$Details->EfEmId]))
                              Yes
                           @else
                              No
                           @endif
                        </td>
                        <td>
                           @php
                              $path='ledger/'.$Details->EfCgId.$CoursId[$Details->EfEmId].'.pdf';
                              $path1 = public_path($path);
                              if (file_exists($path1)) 
                              {
                                 echo 'Yes';
                              }
                              else{
                                 echo 'No';
                              }
                           @endphp
                        </td>
                        @if(session('data')['0']['username']=='exxon')
                        <td align="left">
                           @if(!empty($ArrlogSub[$Details->EfCgId]) and !empty($ArrlogSub1[$Details->EfEmId]))
                           <a href="FinalSubmitActive/{{$Details->EfCgId}}/{{$Details->EfEmId}}">Enabled Edit</a>
                           @endif
                        </td>
                        <td> 
                         
                           @if(!empty($ArrlogPub[$Details->EfCgId]) and !empty($ArrlogPub1[$Details->EfEmId]))
                           <a href="UnpublishResult/{{$Details->EfCgId}}/{{$Details->EfEmId}}">Unpublish</a>
                           @endif
                        </td>
                        @endif
                     </tr>
                  @endif
               @endforeach
            </tbody>
         </table><pagebreak>
      @endforeach
      @if($Selexam!='')
         @php  $EncSelexam=base64_encode(get_encrypt($Selexam)); @endphp
         <script>
            $('#DownloadBtn').html('<a href="{{ url('export/'.$EncSelexam) }}"  class="button-web mt-20" target="_BLANK">Download</a>');
         </script>
      @endif
      