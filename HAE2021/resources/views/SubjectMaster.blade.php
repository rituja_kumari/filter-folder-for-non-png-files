
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	  @include('include.head')
</head>
<body class="antialiased">

	<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-account-multiple"></i>
                                       </span>  Subject Master
                                    </h3>
                                 </div>
                                 <div class="table-div">
                                    <div class="row">
                                       <div class="t-left col-6">
                                          <?php
                                             if( session('msg') != ''){
                                             ?>
                                          <p class="success-msg"> {{session('msg')}} </p>
                                          <?php
                                             }
                                             ?>
                                       </div>
                                       <div class=" t-right col-6 al-center">
                                          <a href="es-create" class="button-web">Add Subject</a>
                                       </div>
                                    </div>
                                    <table class="table-records" border="1">
                                       <thead>
                                          <th>Id</th>
                                          <th>Courses</th>
                                          <th>Subject Code</th>
                                          <th>Subjects</th>
                                          <th>Status</th>
                                          <th>Min</th>
                                          <th>Max</th>
                                          <th>Action</th>
                                       </thead>
                                         @php($count=0)
                                       @foreach($ListArr as $value)
                                       <tr>
                                          <td>{{++$count}}</td>
                                            <td>{{$value->CourseName}}</td>
                                            <td>{{$value->SubjectCD}}</td>
                                            <td>{{$value->Subject}}</td>
                                            <td>{{$value->Status}}</td>
                                            <td>{{$value->MAX}}</td>
                                            <td>{{$value->MIN}}</td>
                                          <td><a href="em-edit/{{$value->EmId}}" class="Action-btn"><i class="mdi mdi-lead-pencil
                                             "></i></a> 
                                             <a href="em-delete/{{$value->EmId}}" class="Action-btn" onclick="return confirm('Are You Sure? Want to Delete It.');"><i class="fa fa-trash"></i></a>
                                          </td>
                                       </tr>
                                       @endforeach
                                    </table>
                                 </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')
</body>
</html>