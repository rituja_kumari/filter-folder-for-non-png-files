<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      @include('include.head')
      <!-- Styles -->
   </head>
   <body class="antialiased">
      <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-lead-pencil
                                          "></i>
                                       </span> Edit Course
                                    </h3>
                                 </div>
                                 <div class="table-div">
                                    <div class=" t-left">
                                       <a href="../Co-list" class="button-web"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
                                       <?php
                                          if( session('msg') != ''){
                                          ?>
                                       <p class="alert alert-danger" style="color: red"> {{session('msg')}} </p>
                                       <?php
                                          }
                                          ?>
                                    </div>
                                    <div>
                                    @foreach ($ListArr as $w)
                                       <form method="POST" action="{{route('co.update',[$w->CfCoId])}}">
                                          @csrf
                                          <table class="table-records">
                                           <tr>
                                                <td style="text-align: left;"><label  class="input-label">Faculty <span style="color:red">*</span></label></td>
                                                <td style="text-align: left;"><label  class="input-label">Course Code <span style="color:red">*</span></label></td>
                                                <td style="text-align: left;"><label  class="input-label">Course Name <span style="color:red">*</span></label></td>
                                                <td style="text-align: left;"><label  class="input-label">Course ShortName <span style="color:red">*</span></label></td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <select class="form-control1" name="CfFcId" id="CfFcId" required>
                                                      <option value="">Select Faculty</option>
                                                      @foreach($faculty as $key => $value)
                                                      <option value="{{$key}}" {{ ( $key == $w['CfFcId']) ? 'selected' : '' }}>{{$value}}</option>
                                                      @endforeach
                                                   </select>
                                                </td>
                                                <td><input type="text" name="CoCode" required value="{{$w->CoCode}}" class="form-control1"></td>
                                                <td><input type="text" name="CoName" required value="{{$w->CoName}}" class="form-control1"></td>
                                                <td><input type="text" name="CfCoShName" required placeholder="Enter ShortName" value="{{$w->CfCoShName}}" class="form-control1"></td>
                                               
                                             </tr>
                                             
                                             <!-- 1st row -->
                                          <tr>
                                             <td style="text-align: left;"><label  class="input-label">Pattern <span style="color:red">*</span></label></td>

                                              <td style="text-align: left;"><label  class="input-label">No.of Year/Sem <span style="color:red">*</span></label></td>

                                              <td style="text-align: left;"><label  class="input-label">Max Years <span style="color:red">*</span></label></td>

                                              <td style="text-align: left;"><label  class="input-label">Max Sessions <span style="color:red">*</span></label></td>
                                              <td style="text-align: left;"><label  class="input-label">Carry Forward Marks <span style="color:red">*</span></label></td>
                                          </tr>
                                          <tr>
                                            <td>
                                               <select class="form-control1" name="CoPattern" id="CoPattern" required>
                                                <option value="">Select Pattern</option>
                                                @foreach($pattern as $pkey => $pvalue)
                                                <option value="{{$pkey}}" {{ ( $pkey == $w['CoPattern']) ? 'selected' : '' }}>{{$pvalue}}</option>
                                                   @endforeach
                                                </select>
                                            </td>

                                            <td><input type="number" name="CoYears" required  class="form-control1" value="{{$w->CoYears}}"></td>

                                            <td><input type="number" name="CoMaxYr" required  class="form-control1" value="{{$w->CoMaxYr}}"></td>

                                            <td><input type="number" name="CoMaxSn" required  class="form-control1" value="{{$w->CoMaxSn}}"></td>

                                            <td>
                                                <select class="form-control1" name="CfCaryFrwrd" id="CfCaryFrwrd" required>
                                                <option value="">Select</option>
                                                @foreach($CFM as $mkey => $mvalue)
                                                <option value="{{$mkey}}" {{ ( $mkey == $w['CfCaryFrwrd']) ? 'selected' : '' }}>{{$mvalue}}</option>
                                                   @endforeach
                                                </select>
                                             </td>
                                          </tr>
                                            <!-- 2nd row -->
                                          <tr>
                                             <td style="text-align: left;"><label  class="input-label">CourseType  <span style="color:red">*</span></label></td>

                                             <td style="text-align: left;"><label  class="input-label">Course Category <span style="color:red">*</span></label></td>
                                             
                                             <td style="text-align: left;"><label  class="input-label">Seat No. Generation <span style="color:red">*</span></label></td>

                                             <td style="text-align: left;"><label  class="input-label">Valuation By <span style="color:red">*</span></label></td>

                                              <td style="text-align: left;"><label  class="input-label">Scaling Down % <span style="color:red">*</span></label></td>

                                          </tr>

                                          <tr>
                                            <td>
                                                <select class="form-control1" name="CoRegDist" id="CoRegDist" required>
                                                <option value="">Select</option>
                                                @foreach($cotype as $cokey => $covalue)
                                                <option value="{{$cokey}}" {{ ( $cokey == $w['CoRegDist']) ? 'selected' : '' }}>{{$covalue}}</option>
                                                   @endforeach
                                                </select>
                                             </td>

                                             <td>
                                                <select class="form-control1" name="CoCate" id="CoCate" required>
                                                <option value="">Select</option>
                                                @foreach($cocat as $catkey => $catvalue)
                                                <option value="{{$catkey}}" {{ ( $catkey == $w['CoCate']) ? 'selected' : '' }}>{{$catvalue}}</option>
                                                   @endforeach
                                                </select>
                                             </td>

                                             <td>
                                                <select class="form-control1" name="CoSeatNo" id="CoSeatNo" required>
                                                <option value="">Select</option>
                                                @foreach($coseat as $key1 => $value1)
                                                 <option value="{{$key1}}" {{ ( $key1 == $w['CoSeatNo']) ? 'selected' : '' }}>{{$value1}}</option>
                                                   @endforeach
                                                </select>
                                             </td>

                                             <td>
                                                <select class="form-control1" name="CoMarkList" id="CoMarkList" required>
                                                <option value="">Select</option>
                                                @foreach($coval as $key2 => $value2)
                                                <option value="{{$key2}}" {{ ( $key2 == $w['CoMarkList']) ? 'selected' : '' }}>{{$value2}}</option>
                                                   @endforeach
                                                </select>
                                             </td>

                                             <td><input type="number" name="IsScaleDown" required  class="form-control1" value="{{$w->IsScaleDown}}"></td>
                                          </tr>
                                           

                                             <tr>
                                                
                                                  <td><input type="submit" name="submit" class="button-web" value="update"></td>
                                             </tr>
                                          </table>

                                          <table class="table-records">
                                             <tr>
                                              @php if($w->IsExamDept=='1'){$Checked="Checked";}
                                              if($w->IsAcadDept=='1'){$Checked="Checked";}
                                              if($w->IsElgDept=='1'){$Checked="Checked";}
                                              if($w->CoCalEC=='1'){$Checked="Checked";}
                                              if($w->CoStat=='1'){$Checked="Checked";}
                                               @endphp
                                                <td><input type="checkbox" value="1"  id="IsExamDept" name="IsExamDept" <?php echo $Checked;?> >
                                                <label >Exam Department?</label></td>

                                                <td><input type="checkbox" value="1"  id="IsAcadDept" name="IsAcadDept" <?php echo $Checked;?> >
                                                <label>Academic Department?</label></td>

                                                 <td><input type="checkbox" value="1"  id="IsElgDept" name="IsElgDept" <?php echo $Checked;?>>
                                                <label>Eligibility Department?</label></td>

                                                <td><input type="checkbox" value="1"  id="CoCalEC" name="CoCalEC" <?php echo $Checked;?>>
                                                <label>Custom Earned Credit?</label></td>

                                                <td><input type="checkbox" value="1"  id="CoStat" name="CoStat" <?php echo $Checked;?>>
                                                <label>Is Active?</label></td>
                                             </tr>
                                             
                                         </table>
                                       </form>
                                       @endforeach
                                    </div>
                                 </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')
       <script type="text/javascript">
      $(document).ready(function () {
      $("#ExmFees,#MrkShtFees, #AdmFees").keyup(function () {
      var a = $("#ExmFees").val();
      var b = $("#MrkShtFees").val();
      var c = $("#AdmFees").val();
       var sum = parseInt(a) + parseInt(b)+ parseInt(c);
            if (!isNaN(sum)) {
                $("#TotFees").val(sum);
            }
      });
      });
      </script>
   </body>
</html>