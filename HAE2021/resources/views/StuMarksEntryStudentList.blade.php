

<br>
<style>
table tbody tr td 
{
    padding-top:5px !important;
    padding-bottom:5px !important; font-size:12px!important;
}
table thead tr th
{
    padding-top:5px !important;
    padding-bottom:5px !important; font-size:12px!important;
}
</style>
@foreach($CODES as $codeval)
    @php
        $CodesArrayoFetch[$codeval->CdSeq]=$codeval->CdDesc;
        $CodesArrayoFetch[$codeval->CdCode]=$codeval->CdDesc;
    @endphp
@endforeach
<table class="table-records mt-0" border="1">
    <thead>
        <tr>
            <th>Sr No.</th>
            <th style="text-align:left">Student Name</th>
            <th>Roll Number</th>
            <th>Result</th>
            <th>Catergory</th>
            <th class="IsSubmit_disabled">Action</th>
        </tr>
    </thead>
    <tbody>
        @php($count=0)

        @foreach($AllExamFormsDetails as $Form)
            <tr>
                <td>{{++$count}}</td>
                <td style="text-align:left">{{$Form['FullName']}}</td>
                <td>{{$Form['EfRollNo']}}</td>
                <!-- <td>{{$Form['EfMaxMark']}}</td>
                <td>{{$Form['EfObtMarks']}}</td>
                <td>{{$Form['EfPC']}}</td> -->
                <td> {{$CodesArrayoFetch[$Form['EfResult']]}}</td>
                <td>
                    @if($Form['EfCate']=='R')
                        Regular
                    @elseif($Form['EfCate']=='X')  
                        Repeater
                    @elseif($Form['EfCate']=='ATKT') 
                        ATKT     
                    @endif    
                </td>
                <td width="15%" class="IsSubmit_disabled">  
                    <a  class="Action-btn IsSubmit_disabled" onclick="ajax_combo3('DeleteStudentDetails/{{$Form->EfId}}/{{$Form->EfRgId}}/','','#res','MarksEntry');"><i class="fa fa-trash"></i></a>  | 
                    
                    <a data-toggle="modal" id="smallButton_{{$Form->EfRgId}}" data-target="#smallModal_{{$Form->EfRgId}}" data-attr="show/{{$Form->EfRgId}}" title="show" class='smallButton IsSubmit_disabled' data-prod-id="100" onclick="ShowMod('show/{{$Form->EfRgId}}','smallModal1_{{$Form->EfRgId}}','smallBody_{{$Form->EfRgId}}','MarksEntry');"> <i class="fas fa-eye text-success  fa-lg"></i> </a> 
                </td>
               
            </tr>
            <div class="modal fade smallModal" id="smallModal1_{{$Form->EfRgId}}" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content" style="width: 745px;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" id="smallBody_{{$Form->EfRgId}}">
                                <div> </div>
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
    </tbody>
 </table>
    <script>
    function ShowMod(url,modalId,bodyid,FormId)
    {
        $.ajax({
        url: url,
        type: "post",
        data: $('#' + FormId).serialize(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      
        success: function(data) {
            console.log(data);
          
            if (data.html) 
            {
                $('#'+modalId).modal("show");
                $('#'+bodyid).html(data.html).show();
            }
        }
    });
    }
    </script>