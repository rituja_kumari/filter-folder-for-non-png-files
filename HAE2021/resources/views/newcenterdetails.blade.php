<style type="text/css">
   .mt-20{margin-top: 30px;}
</style>
<form class="row" id="newinstitute" method="POST" action="{{ route('newcenterregistration') }}">
{{ csrf_field() }}                
      <div class="col-lg-12">
         <h6 class="register-text">Fill Below Details For Get Marks Entry Credential.</h6>
      </div>
      <div class="form-group col-lg-12">
         <div class="row group-shadow">
            <div class="col-lg-3">
               <label for="email" class="input-label">State / राज्य*</label>
               <select class="form-control1" name="state" id="state" required="" >
               <option value="">Select</option>
               @foreach($states as $key => $value)
                  <option value="{{ $key }}">{{ $value}}</option>
               @endforeach
               </select>
               <span class="text-danger">
               <strong id="state-error"></strong>
               </span>
            </div>
            <div class="col-lg-3">
               <label  class="input-label">District / जिल्हा*</label>
               <select class="form-control1" name="District" id="District" required="">
                  <option value="">Select</option>
               </select>
               <span class="text-danger">
               <strong id="District-error"></strong>
               </span>
            </div>
            <div class="col-lg-3">
               <label class="input-label">College Name / महाविद्यालयाचे नाव*</label>
               <select class="form-control1" name="College" id="College" required="">
                  <option value="">Select</option>
                  
               </select>
               <span class="text-danger">
               <strong id="College-error"></strong>
               </span>
            </div>
            <div class="col-lg-3">
               <label class="input-label">Center Number / केंद्र क्रमांक*</label>
               <input type="text" name="CenterNo" id="CenterNo" class="form-control1" readonly="" required="">
            </div>
         </div>
    </div>

      <div class="form-group col-lg-12 mb-30">
          <div class=" group-shadow">
        <div class="row mr-0 ">
               <div class="col-md-12">
                   <h6 class="group-head-text">Principal Details</h6>
               </div>
            <div class="col-lg-3">
               <input type="text" name="CfCgPrinName" id="CfCgPrinName" class="form-control1" placeholder="Principal Full Name *" required="">
               <span class="text-danger">
               <strong id="CfCgPrinName-error"></strong>
               </span>
            </div>
             <div class="col-lg-3">
               <input type="text"  name="CfCgPrinContact" id="CfCgPrinContact" class="form-control1 " placeholder="Mobile No / भ्रमणध्वनी क्रमांक*" required="">
               <span class="text-danger">
               <strong id="CfCgPrinContact-error"></strong>
               </span>
               <span class="text-danger">
               <strong id="MobileVerify-error"></strong>
               </span>
               <span class="text-danger">
               <strong id="MobileOtp-error"></strong>
               </span>
            </div>
            <div class="col-lg-3">
               <input type="email" name="CfCgPrinEmail" id="CfCgPrinEmail"  class="form-control1" style="text-transform:initial;" placeholder="Email Id*" required="">
               <span class="text-danger">
               <strong id="CfCgPrinEmail-error"></strong>
               </span>
               <span class="text-danger">
               <strong id="PrcEmailVerify-error"></strong>
               </span>
               
            </div>
            <div class="col-lg-3" >
               <button type="button" id="SendOtpBtn" class="fxt-btn-fill-2 b-info">Send OTP</button> 
            </div>
         </div>
           <div class="row mr-0 mt-20">
            <div class="col-lg-3">
               <input type="text" name="MobileOtp" id="MobileOtp" class="form-control1 " placeholder="Enter OTP:">
               <span class="text-danger">
               <strong id="PrcMoOtp-error"></strong>
               </span>
            </div>
            <div class="col-lg-2">
               <button type="button" id="VerifyMoEm" disabled="" class="fxt-btn-fill-2 v-success">Verify OTP</button>
               
            </div> 
            </div>
        </div>
     </div>

     <div class="form-group col-lg-6 " >
         <div class="row">
            <div class="col-lg-4 col-xs-6">
               <button type="submit" id="newsave" disabled="" class="fxt-btn-fill">Submit</button>
            </div>
            <div class="col-lg-4 col-xs-6">
               <button type="button" data-dismiss="modal" class="fxt-btn-fill">Cancel</button>
            </div>
            <div class="col-lg-4">
               <span id="ExistCollege"></span>
            </div>
         </div>
      </div>
      <div class="form-group col-lg-12">
         <div id="success-msg-2" style="display: none;">
            <div class="alert alert-success">
                <strong>Success!</strong> Data Updated Successfully...!
            </div>
         </div>
      </div> <br>

</form>
<script>
   $(document).ready(function(){
    
      $('#state').on('change', function() {
      $('#District').empty();
      $('#College').empty();
       var menu_id = $(this).val();
       if(menu_id) {
             $.ajax({
                url: 'finddistrict/'+menu_id,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                success:function(data) {
                   //console.log(data);
                   if(data){
                   $('#District').empty();
                   $('#College').empty();
                   $('#District').focus;
                   $('#District').append('<option value="">Select</option>');
                   $.each(data, function(key, value){
                   $('select[name="District"]').append('<option value="'+ value.CfDsId +'">' + value.DsName+ '</option>');
                });
             }else{
                $('#District').empty();
             }
             }
             });
       }else{
          $('#District').empty();
       }
    });

    $('#District').on('change', function() {
      var collegeid = $(this).val();
      $('#College').empty();
      $('#CenterNo').empty();
       if(collegeid) {
             $.ajax({
                url: 'findcollege/'+collegeid,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                success:function(data) {
                   
                   if(data){
                   $('#CenterNo').empty();
                   $('#College').empty();
                   $('#College').focus;
                   $('#College').append('<option value="">Select</option>');
                   $.each(data, function(key, value){
                   $('select[name="College"]').append('<option value="'+ value.CgId +'">' +value.CgCode +'-'+ value.CgName+ '</option>');
                });
             }else{
                $('#College').empty();
             }
             }
             });
       }else{
          $('#College').empty();
       }
    });


    $('#College').on('change', function() {
       var cgcode = $(this).val();
      $('#CfCoId').val("");
       if(cgcode) {
             $.ajax({
                url: 'findcollegecode/'+cgcode,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                success:function(data) {
                   
                   if(data){
                      
                      
                     $('#CenterNo').empty();
                     $.each(data, function(key, value){
                     $('#CenterNo').val(value.CgCode);
                     $('#CgCode').val(value.CgCode);
                    
                     $('#CfCgPrinName').val(value.CfCgPrinName);
                     $('#CfCgPrinContact').val(value.CfCgPrinContact);  
                     $('#CfCgPrinEmail').val(value.CfCgPrinEmail);
                     
                     
                   if(value.CfCgPrinName!=null && value.CfCgPrinContact!=null && value.CfCgPrinEmail!=null)
                   {
                      $("#SendOtpBtn").attr("disabled",true);
                      $('#PrcMoVerify').attr("disabled",true);
                      $('#ExistCollege').html('Already Registered, Please Login..!').css({'color':'green'});
                   }
                   else{
                      $("#SendOtpBtn").attr("disabled",false);
                      $('#PrcMoVerify').attr("disabled",false);
                      $('#ExistCollege').html('');
                   }
                });
             }else{
                $('#CenterNo').empty();
             }
             }
             });
       }else{
          $('#CenterNo').empty();
       }
    });

   });
</script>
<script>
$(document).ready(function(){
 var form=$("#newinstitute");
    $('#SendOtpBtn').click(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ url('newmobileemailotp') }}",

            method: 'post',
            data:form.serialize(),
            success: function(data){
             //console.log(data)
              if(data.errors) {
                  if(data.errors.state){
                     $( '#state-error' ).html( data.errors.state[0] );
                 }else{
                     $( '#state-error' ).html("");
                 }
                 if(data.errors.District){
                     $( '#District-error' ).html( data.errors.District[0] );
                 }else{
                     $( '#District-error' ).html( "");
                 } 
                 if(data.errors.College){
                     $( '#College-error' ).html( data.errors.College[0] );
                 }else{
                     $( '#College-error' ).html( "");
                 } 
                 if(data.errors.CfCgPrinName){
                     $( '#CfCgPrinName-error' ).html( data.errors.CfCgPrinName[0] );
                 }else{
                     $( '#CfCgPrinName-error' ).html( "");
                 }
                 if(data.errors.CfCgPrinContact){
                     $( '#CfCgPrinContact-error' ).html(data.errors.CfCgPrinContact[0]);  
                 }else{
                     $( '#CfCgPrinContact-error' ).html( "");
                 }
                 if(data.errors.CfCgPrinEmail){
                     $( '#CfCgPrinEmail-error' ).html(data.errors.CfCgPrinEmail[0]);
                 }else{
                     $( '#CfCgPrinEmail-error' ).html( "");
                 }
                                            
              }
              if(data.success) 
              {
                  $( '#CfCgPrinContact-error' ).html("Otp Sent On Your Mobile Number and Email-Id..!").css({"color":"green"});
                  $('#VerifyMoEm').attr('disabled',false);
              }
              
              if (data.fail) 
              {
                  $( '#CfCgPrinContact-error' ).html("Something Went Wrong..!");
              }
            }
        });
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
 var form=$("#newinstitute");
    $('#VerifyMoEm').click(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

   var PrcMoOtp = $('#MobileOtp').val();
   if(PrcMoOtp=='')
   {
      $('#PrcMoOtp-error').html('Please Enter OTP..!');
   }
   else
   {
      $('#PrcMoOtp-error').html('');  
   }
        $.ajax({
            url: "{{ url('NewMobileVerify') }}",

            method: 'post',
            data:form.serialize(),
            success: function(data){
             
               if(data)
               {
                $.each(data, function(key, value){
                   if(value.MobileOtp==PrcMoOtp)
                   {
                     $('.body_overlay').hide();
                      $('#PrcMoOtp-error').html('OTP Verified..!').css("color","green");
                      //alert('Mobile Verified..!');
                      $('#VerifyMoEm').attr('disabled',true);
                      $('#SendOtpBtn').attr('disabled',true);
                      $('#newsave').attr('disabled',false);
                   }
                   else
                   {
                     $('.body_overlay').hide();
                     $('#PrcMoOtp-error').html('Incorrect OTP..!');
                     //alert('Incorrect OTP..!');
                   }
                });
               }
            }
        });
    });
});   
</script>
<script type="text/javascript">
$(document).ready(function(){
 var form=$("#newinstitute");
    $('#newsave').click(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{ url('newcenterregistration') }}",

            method: 'post',
            data:form.serialize(),
            success: function(data){
             
               if(data.errors) 
               {
                  if(data.errors.state){
                     $( '#state-error' ).html( data.errors.state[0] );
                 }else{
                     $( '#state-error' ).html("");
                 }
                 if(data.errors.District){
                     $( '#District-error' ).html( data.errors.District[0] );
                 }else{
                     $( '#District-error' ).html( "");
                 } 
                 if(data.errors.College){
                     $( '#College-error' ).html( data.errors.College[0] );
                 }else{
                     $( '#College-error' ).html( "");
                 } 
                 if(data.errors.CfCgPrinName){
                     $( '#CfCgPrinName-error' ).html( data.errors.CfCgPrinName[0] );
                 }else{
                     $( '#CfCgPrinName-error' ).html( "");
                 }
                 if(data.errors.CfCgPrinEmail){
                     $( '#CfCgPrinEmail-error' ).html(data.errors.CfCgPrinEmail[0]);
                 }else{
                     $( '#CfCgPrinEmail-error' ).html( "");
                 }
                 if(data.errors.CfCgPrinContact){
                     $( '#CfCgPrinContact-error' ).html(data.errors.CfCgPrinContact[0]);  
                 }else{
                     $( '#CfCgPrinContact-error' ).html( "");
                 }                           
              }

               if(data.Mo_error)
               {
               $( '#MobileVerify-error' ).html( "Please Verify Mobile No." );
               }
               else{
               $( '#MobileVerify-error' ).html( " " );
               }

               if (data.success) 
               {
                  $('#success-msg-2').css({'display':'block'});
                 setInterval(function(){ 
                     $('#success-msg-2').css({'display':'block'});
                     window.location.replace('{{route('nameandmarksentry')}}');
                 }, 4000);
               }
            }
        });
    });
   
});   
</script>