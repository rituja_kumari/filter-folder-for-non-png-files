--<html>
   <head>
      <meta charset="utf-8">
      <meta name="_token" content="{{csrf_token()}}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css">
      <style type="text/css">
         .t-center{text-align: center;}
         label{font-weight: 100;}
         .table-head-text{    font-size: 13px;
         text-transform: uppercase;
         font-weight: 700;}
         .lable-table{font-weight: 600;
         text-transform: uppercase;
         font-size: 12px;
         color: #000;}
         .mt-30{margin-top: 30px;}
         td{color: #888;padding-left: 10px;}
         .al-center{align-self: center;}
         .clg-name{font-size: 16px;
         text-transform: uppercase;
         color: #c67413;
         font-weight: 600;}
         /*.table-details{background: #fdfdfd;margin-top: 16px;padding: 20px 15px;}*/
         .inner-table tr{border: 1px solid #ddd;}
         .inner-table td {padding: 4px 4px;}
         .brd1px{border: 1px solid #ddd;}
         table{width: 100%;}
      </style>
   </head>
   <body>
      <div class="container">
         <div class="card-body">
            <div class="row" >
               <div class="col-md-1">
               </div>
               <div class="col-md-12 mb-3 row table-details" >
                  <table>
                     <tr style="text-align: center;">
                        <td colspan="2" style="text-align: center;">
                           <table style="text-align: center;width: 100%;">
                              <tr style="text-align: center;">
                                 <td style="width: 40%;text-align: right;"> <img src="{{ url('/') }}/img/logo11.png" style="width:100px;"></td>
                                 <td> <h2 style="font-size:16px;line-height: 26px;text-align: left;">Directorate of Art ,Maharashtra State,Mumbai<br>
                                 कला संचालनालय, महाराष्ट्र राज्य, मुंबई
                              </h2></td>
                              </tr>
                           </table> 
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" style="text-align: center;">
                           <h5 class="clg-name" >
                           {{session('data')['0']['CgName']}}  
                        </td>
                     </tr>
                     <tr >
                        <td><label><span><b>Center Number</b> : {{session('data')['0']['CgCode']}}</span></label></td>
                        <td style="text-align: right;"><label><span><b>State </b> : Maharashtra</span></label><br>
                           <label><span><b>District </b> : {{session('Dist')['0']['DsName']}}</span></label>     
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2">
                           <h5 class="table-head-text" style="margin-top: 30px;">Principal Details</h5>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                              <tr>
                                 <td class="lable-table">Principal Full Name:</td>
                                 <td> {{session('data')['0']['CfCgPrinName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Mobile No:</td>
                                 <td> {{session('data')['0']['CfCgPrinContact']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Office LandLine No:</td>
                                 <td>{{session('data')['0']['CgLandLineNo']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Email Id:</td>
                                 <td>{{session('data')['0']['CfCgPrinEmail']}}</td>
                              </tr>
                           </table>
                        </td>
                        <td>
                           <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                              <tr>
                                 <td class="lable-table">Bank Name:</td>
                                 <td>{{session('data')['0']['BankName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Bank Branch Name:</td>
                                 <td> {{session('data')['0']['BankBranchName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Account No:</td>
                                 <td> {{session('data')['0']['BankAcNo']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">IFSC Code:</td>
                                 <td>{{session('data')['0']['BankIFSCNo']}}</td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                      <tr>
                       
                        <td colspan="2">
                           <h5 class="table-head-text" style="margin-top: 30px;">College Address Details</h5>
                        </td>
                    </tr>
                    <tr>
                         <td>
                           <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                              <tr>
                                 <td class="lable-table"> Flat/Room/Block/House No:</td>
                                 <td>{{session('data')['0']['CfAdd1']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Name of Premises/ Building:</td>
                                 <td> {{session('data')['0']['CfAdd2']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table"> Road/ Street/Post Office:</td>
                                 <td> {{session('data')['0']['CfAdd3']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table"> Area Locality:</td>
                                 <td>{{session('data')['0']['CfAdd4']}}</td>
                              </tr>
                           </table>
                        </td>
                        <td>
                           <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                              <tr>
                                 <td class="lable-table">Landmark:</td>
                                 <td>{{session('data')['0']['CfAdd5']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Taluka:</td>
                                 <td> {{session('data')['0']['Taluka']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table"> Village/City:</td>
                                 <td> {{session('data')['0']['TnName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">PinCode:</td>
                                 <td>{{session('data')['0']['CfPinCod']}}</td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                    
                    
                      <tr>
                        <td>
                           <h5 class="table-head-text" style="margin-top: 30px;">Coordinator Details</h5>
                        </td>
                        
                     </tr>
                     <tr>
                        <td colspan="2">
                           <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                              <tr>
                                 <td class="lable-table">Full Name:</td>
                                 <td>{{session('data')['0']['CoFName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Designation:</td>
                                 <td> {{session('data')['0']['CoDesigNation']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Mobile No:</td>
                                 <td> {{session('data')['0']['CoFMobile']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Email Id:</td>
                                 <td>{{session('data')['0']['CoFEmail']}}</td>
                              </tr>
                           </table>
                        </td>
                       
                     </tr>

                      <tr>
                      <td>
                         <h5 class="table-head-text" style="margin-top: 30px;">Courses</h5>
                      </td>
                     </tr>
                     <tr>
                      <td colspan="2">
                         <table class="inner-table t-center" style="line-height: 30px;border: 1px solid #ddd;text-align: 
                         center;">
                          
                          <tr>
                                <th class="t-center brd1px" rowspan="2">Course Name</th>
                                <th class="t-center brd1px" colspan="3">Intake</th>
                                <th class="t-center brd1px" colspan="3">Admitted Students</th>
                                
                              </tr>
                            <tr>
                                <th class="t-center brd1px">Govt.</th>
                                <th class="t-center brd1px">Aided</th>
                                <th class="t-center brd1px">Unaided</th>
                                <th class="t-center brd1px">Govt.</th>
                                <th class="t-center brd1px">Aided</th>
                                <th class="t-center brd1px">Unaided</th>
                              </tr>
                            <!-- {{session('CourseWiseIntake')}} -->
                              @foreach(Session('CourseWiseIntake') as $x => $test)
                              <tr>
                                <td align="left" class="brd1px">{{$test->CfCoId}} - {{Session('CoName')[$x]['CoName']}}</td>
                                <td class="brd1px">{{$test->InGovt}}</td>
                                <td class="brd1px">{{$test->InAided}}</td>
                                <td class="brd1px">{{$test->InUnAided}}</td>
                                <td class="brd1px">{{$test->AdGovt}}</td>
                                <td class="brd1px">{{$test->AdAided}}</td>
                                <td class="brd1px">{{$test->AdUnAided}}</td>
                                
                              </tr>
                              @endforeach
                        </table>
                      </td>
                     </tr>
                     
                      <tr>
                      <table style="margin-top: 50px;">
                        <tr>
                          <td align="center">College Stamp</td>
                       <td align="center">Signature</td>
                        </tr>
                      </table>
                       
                     </tr>

                  </table>
               </div>
            </div>
         </div>
      </div>
      <script src="{{ url('/') }}/js/jquery.min.js"></script>
      <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
   </body>
</html>

