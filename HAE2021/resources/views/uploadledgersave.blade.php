@if($SpCl_value=='ShowBranchMaster')
    <script> 
        var EmptyArr = ["Selbranch","Selexam"];
        for (x of EmptyArr) 
        {
          $('#'+x).val('');
        }
    </script>
   
    @php
        $Brcondition[]=array("ColumnName"=>"CfCoId","Operator"=>"=","ColumnValue"=>$courses);
        $Enccondition=json_encode($Brcondition);
        $branch=sel_branch($Enccondition,'','','','form-control1','',"ajax_combo3('ULShowExam/','','#DivExam','uploadledger')",'1'); 
    @endphp    
  
    {{ $branch }}  
    
@elseif($SpCl_value=='ShowExamMaster')
    <script> 
        var EmptyArr = ["Selexam"];
        for (x of EmptyArr) 
        {
            $('#'+x).val('');
        }
    </script>
    @php
        $Excondition[]=array("ColumnName"=>"CfCoId","Operator"=>"=","ColumnValue"=>$courses);
        $Excondition[]=array("ColumnName"=>"CfBrId","Operator"=>"=","ColumnValue"=>$branch);
        $Enccondition=json_encode($Excondition);
        
        $exam =sel_exam($Enccondition,'','','','form-control1','',"ajax_combo3('OnExamChange/','','#subjectInfo','uploadledger')",'1'); 
    @endphp    
    {{ $exam }} 
@endif