<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	 @include('include.head')
	<script src="{{ url('/') }}/js/jquery.min.js"></script>
    <script src="{{ url('/') }}/js/owl.carousel.min.js"></script>
    <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/js/custom.js"></script>
</head>
<body class="antialiased">
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-lead-pencil
                                          "></i>
                                       </span> Edit Exam Master
                                    </h3>
                                 </div>
                                 <div class="table-div">
                                    <div class=" t-left">
                                       <a href="../em-list" class="button-web"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
                                    </div>
                                    <div>
                                    @foreach ($ListArr as $w)
                                       <form method="POST" action="{{route('em.update',[$w->EmId])}}">
                                          @csrf
                                          <table class="table-records">
                                             <tr>
                                                 <td style="text-align: left;"><label  class="input-label">Faculty <span style="color:red">*</span></label></td>
                                                 
                                                 <td style="text-align: left;"><label  class="input-label">Course <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Branch <span style="color:red">*</span></label></td>

                                                   <td style="text-align: left;"><label  class="input-label">Serial No <span style="color:red">*</span></label></td>
                                               </tr>
                                             <tr>
                                                <td>
                                                   <select class="form-control1" name="FcId" id="FcId" required>
                                                     <option value="">Select</option>
                                                     @foreach($faculty as $key => $value)
                                                    <option value="{{$key}}" {{ ( $key == $w['FcId']) ? 'selected' : '' }}>{{$value}}</option>
                                                    @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                   <select class="form-control1" name="CfCoId" id="CfCoId" required>
                                                       <option value="">Select</option>
                                                       @foreach($course as $key => $value)
                                                      <option value="{{$key}}" {{ ( $key == $w['CfCoId']) ? 'selected' : '' }}>{{$value}}</option>
                                                      @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                  <select class="form-control1" name="CfBrId" id="CfBrId" required>
                                                     <option value="">Select</option>
                                                     @foreach($branch as $key1 => $value1)
                                                    <option value="{{$key1}}" {{ ( $key1 == $w['CfBrId']) ? 'selected' : '' }}>{{$value1}}</option>
                                                    @endforeach
                                                    </select>
                                                </td>
                                               <td><input type="number" name="EmSrNo" required placeholder="Enter Serial No" class="form-control1" value="{{$w->EmSrNo}}"></td>
                                              </tr>
                                              <!-- 1st -->

                                              <tr>
                                                <td style="text-align: left;"><label  class="input-label">Exam Code  <span style="color:red">*</span></label></td>
                                                
                                                <td style="text-align: left;" colspan="2"><label  class="input-label" >Exam Name <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Total Subjects <span style="color:red">*</span></label></td>
                                                
                                              </tr>
                                              
                                              <tr>
                                               <td><input type="text" name="EmCode" required placeholder="Enter Exam Code" class="form-control1" value="{{$w->EmCode}}"></td>

                                                <td colspan="2"><input type="text" name="EmName" required placeholder="Enter Exam Name" class="form-control1" value="{{$w->EmName}}"></td>

                                                <td><input type="number" name="EmTotSub" required placeholder="Enter Total Subjects" class="form-control1" value="{{$w->EmTotSub}}"></td>
                                             </tr>
                                             <!-- 2nd row -->

                                             <tr>
                                                
                                                <td style="text-align: left;"><label  class="input-label">Total Marks <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Minimum Credits <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Maximum Credits <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Passing Percentage <span style="color:red">*</span></label></td>
                                              </tr>

                                              <tr>
                                                  <td><input type="number" name="EmMaxTot" required placeholder="Enter Total Marks" class="form-control1" value="{{$w->EmMaxTot}}"></td>

                                                  <td><input type="number" name="EmMinCr" required placeholder="Enter Minimum Credits" class="form-control1" value="{{$w->EmMinCr}}"></td>

                                                  <td><input type="number" name="EmMaxCr" required placeholder="Enter Maximum Credits" class="form-control1" value="{{$w->EmMaxCr}}"></td>

                                                  <td><input type="number" name="EmPassPC" required placeholder="Enter Passing Percentage" class="form-control1" value="{{$w->EmPassPC}}"></td>
                                              </tr>
                                              <!-- 3rd row -->

                                              <tr>
                                                 <td style="text-align: left;"><label  class="input-label">Course Year  <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Evaluation Criteria <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Evaluation Deviation (%) <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">College Level Exam <span style="color:red">*</span></label></td>
                                              </tr>
                                              <tr>
                                                <td></td>

                                                <td><!-- evaluation criteria combo --></td>

                                                <td><input type="number" name="EmValDev" required placeholder="Enter Evaluation Deviation" class="form-control1" value="{{$w->EmValDev}}"></td>

                                                <td>
                                                  <select class="form-control1" name="EmColg" id="EmColg" required>
                                                  <option value="">Select</option>
                                                  @foreach($YesNo as $key1 => $value1)
                                                    <option value="{{$key1}}" {{ ( $key1 == $w['EmColg']) ? 'selected' : '' }}>{{$value1}}</option>
                                                   @endforeach
                                                </select>
                                                </td>
                                              </tr>
                                              <!-- 4th row -->

                                              <tr>
                                                <td style="text-align: left;"><label  class="input-label">Allow Lateral Entry <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Professional Internship? <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Revaluation Available? <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Revaluation Subjects Count <span style="color:red">*</span></label></td>
                                              </tr>

                                              <tr>
                                                 <td>
                                                  <select class="form-control1" name="EmLatEntry" id="EmLatEntry" required>
                                                  <option value="">Select</option>
                                                  @foreach($YesNo as $key2 => $value2)
                                                <option value="{{$key2}}" {{ ( $key2 == $w['EmLatEntry']) ? 'selected' : '' }}>{{$value2}}</option>
                                                   @endforeach
                                                </select>
                                                </td>

                                                 <td>
                                                  <select class="form-control1" name="CfIntrnShpExam" id="CfIntrnShpExam" required>
                                                  <option value="">Select</option>
                                                  @foreach($YesNo as $key3 => $value3)
                                                  <option value="{{$key3}}" {{ ( $key3 == $w['CfIntrnShpExam']) ? 'selected' : '' }}>{{$value3}}</option>
                                                   @endforeach
                                                </select>
                                                </td>

                                                 <td>
                                                  <select class="form-control1" name="CfIsReval" id="CfIsReval" >
                                                  <option value="">Select</option>
                                                  @foreach($YesNo as $key4 => $value4)
                                                  <option value="{{$key4}}" {{ ( $key4 == $w['CfIsReval']) ? 'selected' : '' }}>{{$value4}}</option>
                                                   @endforeach
                                                </select>
                                                </td>

                                                <td><input type="number" name="RevalEsCount" class="form-control1" value="{{$w->RevalEsCount}}"></td>
                                              </tr>
                                              <!-- 5th -->
                                              <tr>
                                                <td style="text-align: left;"><label  class="input-label">Revaluation Threshold (%) <span style="color:red">*</span></label></td>
                                               
                                                <td style="text-align: left;"><label  class="input-label">Class Improvement <span style="color:red">*</span></label></td>
                                               
                                                <td style="text-align: left;"><label  class="input-label">Grand Total Exam Sr. No. <span style="color:red">*</span></label></td>
                                                
                                                <td style="text-align: left;"><label  class="input-label">Counter For PRN <span style="color:red">*</span></label></td>
                                                
                                              </tr>
                                               <tr>
                                                 <td><input type="number" name="RevalThreshold" class="form-control1" value="{{$w->RevalThreshold}}"></td>

                                                 <td>
                                                  <select class="form-control1" name="CfIsImprov" id="CfIsImprov" >
                                                  <option value="">Select</option>
                                                  @foreach($CfIsImprov as $key5 => $value5)
                                                <option value="{{$key5}}" {{ ( $key5 == $w['CfIsImprov']) ? 'selected' : '' }}>{{$value5}}</option>
                                                   @endforeach
                                                  </select>
                                                 </td>

                                                  <td><input type="text" name="CfTotLvl"  class="form-control1" placeholder="Comma separated format 2,3,4" value="{{$w->CfTotLvl}}"></td>

                                                  <td><input type="number" name="EmRgNoCnt"  class="form-control1" value="{{$w->EmRgNoCnt}}"></td>

                                              </tr>

                                              <tr>
                                                @php if($w->EmConv=='Y'){$Checked="Checked";}

                                                if($w->EmSpFlag=='Y'){$Checked="Checked";}

                                                if($w->EmGrpClg=='1'){$Checked="Checked";}
                                                
                                               @endphp
                                                <td style="text-align: left;"><label  class="input-label">No. Of Groups <span style="color:red">*</span></label></td>

                                                <td style="text-align: left;"><label  class="input-label">Compulsory Groups <span style="color:red">*</span></label></td>

                                                <td><input type="checkbox"  id="EmConv" name="EmConv" <?php echo $Checked;?>>
                                                <label >Convocation applicable?</label></td>

                                                <td><input type="checkbox" value="Y"  id="EmSpFlag" name="EmSpFlag" <?php echo $Checked;?>>
                                                <label>Appear all subject if failed</label></td>

                                              </tr>

                                              <tr>

                                                <td><input type="number" name="CfOptGrpCnt"  class="form-control1" value="{{$w->CfOptGrpCnt}}"></td>

                                                <td><input type="number" name="CfCmpGrpCnt"  class="form-control1" value="{{$w->CfCmpGrpCnt}}"></td>

                                                <td><input type="checkbox" value="1"  id="EmGrpClg" name="EmGrpClg" <?php echo $Checked;?>>
                                                <label>Group selection at college</label></td>

                                                 <td><input type="submit" name="submit" class="button-web" value="update"></td>
                                              </tr>
                                          </table>
                                       </form>
                                       @endforeach
                                    </div>
                                 </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')
  <!--  -->
<!-- 	<h4>Edit Exam master</h4>

	@foreach ($ListArr as $w)
	 <a href="../em-list">Back</a>
      <form method="POST" action="{{route('em.update',[$w->EmId])}}">
      	@csrf
        <table>
        	
             <tr>
                <td>Course</td>
                <td>
                    <select class="form-control" name="EmCoId" id="EmCoId" required>
                     <option value="">Select</option>
                     @foreach($course as $key => $value)
                    <option value="{{$key}}" {{ ( $key == $w['EmCoId']) ? 'selected' : '' }}>{{$value}}</option>
                    @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>Branch</td>
                <td>
                    <select class="form-control" name="EmBrId" id="EmBrId" required>
                     <option value="">Select</option>
                     @foreach($branch as $key => $value)
                    <option value="{{$key}}" {{ ( $key == $w['EmBrId']) ? 'selected' : '' }}>{{$value}}</option>
                    @endforeach
                    </select>
                </td>
            </tr>
             <tr>
                <td>Exam Code :</td>
                <td><input type="name" name="EmCode" required value="{{$w->EmCode}}"></td>
            </tr>
            <tr>
                <td>Exam Name :</td>
                <td><input type="name" name="EmName" required value="{{$w->EmName}}"></td>
            </tr>
            <tr>
                <td>Exam Short Name :</td>
                <td><input type="name" name="EmShName" required value="{{$w->EmShName}}"></td>
            </tr>
            <tr>
                <td>Sequence :</td>
                <td><input type="number" name="EmSrNo" required value="{{$w->EmSrNo}}"></td>
            </tr>
             <tr>
                <td></td>
    			<td><input type="submit" name="submit"></td>
    		</tr>
        </table>
      </form>

	@endforeach -->

	<script type="text/javascript">
    //on chng faculty #FcId
    $(document).ready(function() {
       $('#FcId').on('change', function() {
        //console.log("its working..");
          var menu_id = $(this).val();
          if(menu_id) {
                $.ajax({
                   url: '../findcourse/'+menu_id,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data) {
                      console.log(data);
                      if(data){
                      $('#CfCoId').empty();
                      $('#CfBrId').empty();
                      $('#CfCoId').focus;
                      $('#CfCoId').append('<option value="">Select Course</option>');
                      $.each(data, function(key, value){
                      $('select[name="CfCoId"]').append('<option value="'+ value.CfCoId +'">' + value.CoName+ '</option>');
                   });
                }else{
                   $('#CfCoId').empty();
                   $('#CfBrId').empty();
                }
                }
                });
          }else{
             $('#CfCoId').empty();
             $('#CfBrId').empty();
          }
       });

   });

     //on chng course #CfCoId
    $(document).ready(function() {
       $('#CfCoId').on('change', function() {
        //console.log("its working..");
          var menu_id = $(this).val();
          if(menu_id) {
                $.ajax({
                   url: '../findbranch/'+menu_id,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data) {
                      console.log(data);
                      if(data){
                      $('#CfBrId').empty();
                      $('#CfBrId').focus;
                      $('#CfBrId').append('<option value="">Select Branch</option>');
                      $.each(data, function(key, value){
                      $('select[name="CfBrId"]').append('<option value="'+ value.CfBrId +'">' + value.branch_name+ '</option>');
                   });
                }else{
                   $('#CfBrId').empty();
                }
                }
                });
          }else{
             $('#CfBrId').empty();
          }
       });

   });


</script>
</body>
</html>