<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      @include('include.head')
      <!-- Styles -->
   </head>
   <body class="antialiased">
      <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-plus
                                          "></i>
                                       </span> Add Faculty Master
                                    </h3>
                                 </div>
                                 <div class="table-div">
                                    <div class=" t-left">
                                       <a href="fac-show" class="button-web"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
                                       <?php
                                             if( session('msg') != ''){
                                             ?>
                                          <p class="alert alert-danger" style="color: red"> {{session('msg')}} </p>
                                          <?php
                                             }
                                             ?>
                                    </div>
                                    <div class=" t-right">
                                         
                                    </div>
                                    <div>
                                       <form method="post" action="fac-submit">
                                          @csrf
                                          <table class="table-records">
                                             <tr>
                                                <td style="text-align: left;"><label  class="input-label">Faculty Code <span style="color:red">*</span></label></td>
                                                 <td style="text-align: left;"><label  class="input-label">Faculty Name <span style="color:red">*</span></label></td>
                                                 <td></td>
                                             </tr>
                                             <tr>
                                                <td><input type="name" name="FcCode" required placeholder="Enter Code" class="form-control1"></td>
                                                <td><input type="name" name="FcDesc" required placeholder="Enter Name" class="form-control1"></td>
                                                <td><input type="submit" name="submit" class="button-web"></td>
                                             </tr>
                                          </table>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')
   </body>
</html>