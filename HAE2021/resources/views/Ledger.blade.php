@php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);
    // echo json_encode($ArrayOfTieData1);//die(); 
    $SchemaArray=json_decode($Extra,true);
    $DecodesGroupArray=json_decode($GroupInfo,true);
    foreach($Decoded_ArrForEndOfSubjectId as $ArrSubjectInfo)
    {
        $SubjectInformation[$ArrSubjectInfo['EsId']]=$ArrSubjectInfo;
    }
   
    $ArrayForMinMax=array("OBT"=>"","EsMaxMarks"=>"Max Marks","EsPassMarks"=>"Min Marks");
    $StudentWiseSubjectDetails=$ArrFailStud=$ArrPassStud=$ArrAbsentStud=$ArrFirstStud=$ArrSecondStud=$ArrPassClsStud=$ArrDistClsStud=array();
    //unset($ArrPassStud);
   // unset($ArrFailStud);
    foreach($AllExamFormsDetails as $Form)
    {
        $RgNo=$Form['RgNo'];
        if($Form['EfResult']=='P')
        {
            $ArrPassStud[]=$RgNo;
        }
        elseif($Form['EfResult']=='F')
        {
            $ArrFailStud[]=$RgNo;
        }
        elseif($Form['EfResult']=='A')
        {
            $ArrAbsentStud[]=$RgNo;
        }

        if($Form['EfGrdPts']=='F')
        {
            $ArrFirstStud[]=$RgNo;
        }
        elseif($Form['EfGrdPts']=='S')
        {
            $ArrSecondStud[]=$RgNo;
        } 
        elseif($Form['EfGrdPts']=='P')
        {
            $ArrPassClsStud[]=$RgNo;
        } 
        elseif($Form['EfGrdPts']=='D')
        {
            $ArrDistClsStud[]=$RgNo;
        }
        $StudentWiseSubData=array();
        foreach($Decoded_AllStudentMArksInfo as $MarksInfo)
        {
            $ChildEsId=$MarksInfo['ChildEsId'];
            if($MarksInfo['RgNo']==$RgNo)
            {
                $StudentWiseSubData[$ChildEsId]=$MarksInfo;
            }
        }
        $StudentWiseSubjectDetails[$RgNo]=$StudentWiseSubData;
    }


    $InvalidChildEsId=array('RESULT','CLASS','OBT','REMARK');
    $Rowspan='5'; $CountOfDecodesGroupArray=0;
    if($EmId=='1' or $EmId=='7')
    {
        $Rowspan='5';
    }
    elseif($EmId=='4')
    {
        $Rowspan='6';
    }
   
    elseif(!empty($DecodesGroupArray)){ 
        $Rowspan='6';   $CountOfDecodesGroupArray=sizeof($DecodesGroupArray)-1;
        
    }
   if(empty($Mode))
   {
        $Mode='Genral';
   }
   
@endphp
@foreach($CODES as $codeval)
             @php
                $CodesArrayoFetch[$codeval->CdSeq]=$codeval->CdDesc;
                $CodesArrayoFetch[$codeval->CdCode]=$codeval->CdDesc;
             @endphp
    @endforeach  
          @php   $ClassArrayoFetch=array("D"=>"Dist","F"=>"I","S"=>"II","P"=>"Pass","-"=>"-"); @endphp 
  
<style>
 @page { 
    margin-top:100px;
    margin-right:10px;
    margin-bottom:10px;
    margin-left:10px;
   
    } 
body { margin: 0px; }
td{
    word-wrap: break-word;
}
#footer { position: fixed; left: 625px; bottom: -140px; right: 0px; height: 150px;}
#footer .page:after { content: counter(page, decimal-leading-zero); }
#header { margin-bottom: 100px;position: fixed;top: -100px; left: 0px; right: 0px; padding: 10px; text-align: center; font-weight: bold; } 

</style>

<table   border="0" width="100%" style="border-collapse: collapse;font-size:15px !important;" id="header"> <!-- margin: 100px 10px;  style="word-wrap:break-word"-->
    <thead>
            <tr >
                <th class="br-0" ><h2>DIRECTORATE OF ART , MAHARASHTRA STATE, MUMBAI<br>HIGHER ART EXAMINATION 2021<br>( Course : {{$CourseName}} ) <br>College : {{$CgCode}}  &nbsp; {{$CgName}}</h2></th>    
            </tr>
            
            <tr><th class="br-0" ></th></tr>
            <tr><th class="br-0"  ></th></tr>
            <tr><th class="br-0" ></th></tr>
    </thead>
</table>
     
<div id="footer">
     <span class="page" style="font-size:12px;" > <?php  $PAGE_NUM ?></span>
   </div>
<table border="1" width="100%" style="border-collapse: collapse;font-size:12px !important;">
    <thead>
            
        @if(!empty($DecodesGroupArray))
          
            <tr>
                <td></td>

                @foreach($DecodesGroupArray as $GroupKey => $SchemaInfo)
                @php 
                if($Mode=='Publish' and $EmId!='7')
                {  
                    $Headcolspan=$SchemaInfo['colspan']+1;
                }
                elseif($Mode=='Publish' and $EmId=='7' and $GroupKey==$CountOfDecodesGroupArray)
                {
                    $Headcolspan=$SchemaInfo['colspan']+1;
                } 
                else
                {
                    $Headcolspan=$SchemaInfo['colspan'];
                } 
                @endphp
                        <td colspan="{{$Headcolspan}}" rowspan="{{$SchemaInfo['rowspan']}}" align="center">{{ $SchemaInfo['HeadOfPass']}}</td>        
                @endforeach
            </tr>
        @endif
        <tr>
            <td rowspan="{{$Rowspan}}" align="center" width="20%">Student Name</td>
            <td align="center"> Seat Number </td>
           
            @foreach($SchemaArray as $SchemaInfo)
                @php 
                    
                    unset($ArrToCheckSubGroupornot);
                    unset($ArrColspanCount);
                    if($SchemaInfo['EsId']!='-11')
                    {
                        $SubjectDetails=$SubjectInformation[$SchemaInfo['EsId']]; 
                    }

                    $ColspanCount='';
                    $ArrToCheckSubGroupornot[]=$SchemaInfo['IsSubPara'];
                    $RowspanCount=1;
                    $RowspanCount=$SchemaInfo['rowspan'];
                    if($SchemaInfo['IsSubPara']=='1')
                    {
                        foreach($SchemaInfo['EsPara'] as $Parameter)
                        {
                            $ArrColspanCount[]=sizeof($Parameter['SubParaEsId']);
                        
                        }
                        $ColspanCount=array_sum($ArrColspanCount);
                    }
                    else
                    {
                        $ColspanCount=sizeof($SchemaInfo['EsPara']);
                    }
                @endphp
                @if(( $SchemaInfo['EsIdName']!='Remark' and $SchemaInfo['EsIdName']!='REMARK' and $Mode=='Publish') or ($Mode!='Publish'))
               
                    <td colspan="{{$ColspanCount}}" rowspan="{{$RowspanCount}}"  align="center">
                            @if($SchemaInfo['EsIdName']=='NA')
                            @elseIF($SchemaInfo['EsIdName']=='fetchfromdb')
                                {{$SubjectDetails['EsName']}} 
                            @else
                                {{$SchemaInfo['EsIdName']}} 
                            @endif
                    </td>
               
               
                @endif
            @endforeach
            @if($Mode=='Publish')
                <td rowspan="{{$Rowspan}}" align="center">%AGE</td>
                <td rowspan="{{$Rowspan}}" align="center">Remark</td>
            @endif
        </tr> 
       
    @php
                
       if(in_array('1',$ArrToCheckSubGroupornot))
       {
            @endphp  @include('GroupLedger') @php 
       }
       else//SIMPLE
       { 
            @endphp  @include('GenralLedger') @php
       }
    @endphp
   
    
</table>
<br>
@if($Mode=='Publish')
    <table class="table-records" border="0"  width="100%" style="padding-top:25px" >  
        <tr>
            <td>Total Candidates : {{ count($AllExamFormsDetails) }}</td>
            <td>Pass : {{ count($ArrPassStud) }}</td>
            <td>Fail : {{ count($ArrFailStud) }}</td>
            <td>First Class : {{ count($ArrFirstStud) }}</td>
            <td>Second Class : {{ count($ArrSecondStud) }}</td>
            <td>Pass Class : {{ count($ArrPassClsStud) }}</td>
            <td>Absent : {{ count($ArrAbsentStud) }}</td>
            <td>Dist : {{ count($ArrDistClsStud) }}</td>
        </tr>
    </table> 
    <table class="table-records" border="0"  width="100%" style="padding-top:25px" > 
            <!-- <tr>
                <td align="center" >----------------------------------------------------</td>
                <td align="center" >----------------------------------------------------</td>
                <td align="center">Dean / Principal Signature</td>
            </tr> -->
            <tr>
                <td align="right" style="padding-right: 180px !important;"> <img src="{{ url('/') }}/img/COE_SIGNATURE.png" width="10%" ></td>
            </tr> 
            <tr>
                <td align="right" style="padding-right: 150px !important;"> CONTROLLER OF EXAMINATION</td>
            </tr>
            <tr>
                <!-- <td align="center">Committee Member 1</td>
                <td align="center" >Committee Member 2</td> 
                <td align="center" >College Stamp</td>-->
                <td align="right" style="padding-right: 85px !important;"> DIRECTORATE OF ART MAHARASHTRA STATE</td>
            </tr>
            <tr>
                <td align="right" style="padding-right: 250px !important;"> MUMBAI</td>
            </tr>
    </table>

@else
    <img src="{{ url('/') }}/img/pramanpatra.png" width="100%" >
@endif


