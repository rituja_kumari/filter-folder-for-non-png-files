<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	   @include('include.head')
 
</head>
<body class="antialiased">
  <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-plus
                                          "></i>
                                       </span> Add Subject
                                    </h3>
                                 </div>
                                 <div class="table-div">
                                    <div class=" t-left">
                                       <a href="em-list" class="button-web"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
                                    </div>
                                    <div>
                                       <form method="post" action="es-submit">
                                          @csrf
                                          <table class="table-records">
                                            <tr>
                                                 <td style="text-align: left;"><label  class="input-label">Faculty <span style="color:red">*</span></label></td>
                                                 
                                                 <td style="text-align: left;"><label  class="input-label">Course <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Subject Name <span style="color:red">*</span></label></td>


                                                 <td style="text-align: left;"><label  class="input-label">Subject Code <span style="color:red">*</span></label></td>

                                               </tr>

                                               <tr>
                                              <td>
                                                 <select class="form-control1" name="FcId" id="FcId" required>
                                                      <option value="">Select</option>
                                                      @foreach($faculty as $key => $value)
                                                      <option value="{{$key}}">{{$value}}</option>
                                                      @endforeach
                                                   </select>
                                              </td>
                                              <td>
                                                  <select class="form-control1" name="EsCoId" id="EsCoId" required>
                                                        <option value="">Select</option>
                                                            
                                                 </select>
                                              </td>
                                              <td><input type="text" name="EsName" required placeholder="Enter Name" class="form-control1" required=""></td>
                                             <td><input type="text" name="EsCode" required placeholder="Enter code" class="form-control1" required=""></td>
                                              
                                              </tr>

                                               <tr>
                                                 
                                                 <td style="text-align: left;"><label  class="input-label">Passing Marks <span style="color:red">*</span></label></td>
                                                 
                                                 <td style="text-align: left;"><label  class="input-label">Total Marks <span style="color:red">*</span></label></td>
                                                 
                                                 <td style="text-align: left;"><label  class="input-label">Distinction Marks <span style="color:red">*</span></label></td>
                                                
                                                <td style="text-align: left;"><label  class="input-label">Credits <span style="color:red">*</span></label></td>
                                                <td style="text-align: left;"><label  class="input-label">Print Seq. <span style="color:red">*</span></label></td>
                                              </tr>
                                             
                                              <tr>
                                                <td><input type="number" name="EsPassMarks" required placeholder="Enter Passing Marks" class="form-control1"></td>
                                                <td><input type="number" name="EsExclTot" required placeholder="Enter Total Marks" class="form-control1"></td>
                                                <td><input type="number" name="EsDistMarks" required placeholder="Enter Distinction Marks" class="form-control1"></td>
                                                <td><input type="number" name="EsCredit" required placeholder="Enter Credits" class="form-control1"></td>
                                                <td><input type="number" name="EsPrint" required placeholder="Sequence" class="form-control1"></td>
                                                <td><input type="submit" name="submit" class="button-web"></td>
                                             </tr>
                                          </table>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')

 
<script type="text/javascript">
    //on chng faculty #FcId
    $(document).ready(function() {
       $('#FcId').on('change', function() {
        //console.log("its working..");
          var menu_id = $(this).val();
          if(menu_id) {
                $.ajax({
                   url: 'findcourse/'+menu_id,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data) {
                      console.log(data);
                      if(data){
                      $('#EsCoId').empty();
                      
                      $('#EsCoId').focus;
                      $('#EsCoId').append('<option value="">Select Course</option>');
                      $.each(data, function(key, value){
                      $('select[name="EsCoId"]').append('<option value="'+ value.CfCoId +'">' + value.CoName+ '</option>');
                   });
                }else{
                   $('#EsCoId').empty();
                   
                }
                }
                });
          }else{
             $('#EsCoId').empty();
            
          }
       });

   });

    


</script>
</body>
</html>