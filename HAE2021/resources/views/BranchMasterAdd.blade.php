<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  @include('include.head')
     <script src="{{ url('/') }}/js/jquery.min.js"></script>
      <script src="{{ url('/') }}/js/owl.carousel.min.js"></script>
      <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      <script src="{{ url('/') }}/js/custom.js"></script>
</head>
<body class="antialiased">

<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                 <div class="page-header">
                                    <h3 class="page-title">
                                       <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                       <i class="mdi mdi-plus
                                          "></i>
                                       </span> Add Branch Master
                                    </h3>
                                 </div>
                                 <div class="table-div">
                                    <div class=" t-left">
                                       <a href="br-list" class="button-web"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
                                        <?php
                                          if( session('msg') != ''){
                                          ?>
                                       <p class="alert alert-danger" style="color: red"> {{session('msg')}} </p>
                                       <?php
                                          }
                                          ?>
                                    </div>
                                    <div>
                                       <form method="post" action="br-submit">
                                          @csrf
                                          <table class="table-records">
                                            <tr>
                                                 <td style="text-align: left;"><label  class="input-label">Faculty <span style="color:red">*</span></label></td>
                                                 
                                                 <td style="text-align: left;"><label  class="input-label">Course <span style="color:red">*</span></label></td>

                                                 <td style="text-align: left;"><label  class="input-label">Branch Code <span style="color:red">*</span></label></td>
                                              </tr>
                                             <tr>
                                              <td>
                                                 <select class="form-control1" name="FcId" id="FcId" required>
                                                      <option value="">Select Faculty</option>
                                                      @foreach($faculty as $key => $value)
                                                      <option value="{{$key}}">{{$value}}</option>
                                                      @endforeach
                                                   </select>
                                              </td>
                                              <td>
                                                  <select class="form-control1" name="CoId" id="CoId" required>
                                                        <option value="">Select</option>
                                                            
                                                 </select>
                                              </td>
                                                <td><input type="text" name="branch_code" required placeholder="Enter Code" class="form-control1"></td>
                                              
                                              </tr>
                                              <tr>
                                                <td style="text-align: left;"><label  class="input-label">Branch Name <span style="color:red">*</span></label></td> 

                                                 <td style="text-align: left;"><label  class="input-label">Admission Open? <span style="color:red">*</span></label></td>
                                                 <td></td>

                                               <!--  <td><input type="submit" name="submit" class="button-web"></td> -->
                                             </tr>
                                             <tr>
                                                <td><input type="text" name="branch_name" required placeholder="Enter Name" class="form-control1"></td>

                                                <td>
                                                <select class="form-control1" name="IsActive" id="IsActive" required>
                                                <option value="">Select</option>
                                                @foreach($admopn as $key1 => $value1)
                                                <option value="{{$key1}}">{{$value1}}</option>
                                                   @endforeach
                                                </select>
                                             </td>
                                             <td><input type="submit" name="submit" class="button-web"></td>
                                             </tr>
                                          </table>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('include.scripts')
  <!--  -->
	<!-- <a href="br-list">Back</a>
    <form method="post" action="br-submit">
    	@csrf
    	<table>
            <tr>
                <td>Faculty :</td>
                <td>
                    <select class="" name="FcId" id="FcId" required>
                     <option value="">Select</option>
                     @foreach($faculty as $key => $value)
                     <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>Course :</td>
                <td>
                 <select class="" name="CoId" id="CoId" required>
                        <option value="">Select</option>
                            
                 </select>
                </td>
            </tr>

            <tr>
                <td>Code</td>
                <td><input type="name" name="BrCode" required></td>
            </tr>

            <tr>
                <td>Name</td>
                <td><input type="name" name="BrName" required></td>
            </tr>

    		<tr>
                <td></td>
    			<td><input type="submit" name="submit"></td>
    		</tr>
    	</table>
    </form> -->



<script type="text/javascript">
    //on chng faculty #FcId
    $(document).ready(function() {
       $('#FcId').on('change', function() {
        //console.log("its working..");
          var menu_id = $(this).val();
          if(menu_id) {
                $.ajax({
                   url: 'findcoursebr/'+menu_id,
                   type: "GET",
                   data : {"_token":"{{ csrf_token() }}"},
                   dataType: "json",
                   success:function(data) {
                      console.log(data);
                      if(data){
                      $('#CoId').empty();
                      $('#CoId').focus;
                      $('#CoId').append('<option value="">Select Course</option>');
                      $.each(data, function(key, value){
                      $('select[name="CoId"]').append('<option value="'+ value.CfCoId +'">' + value.CoName+ '</option>');
                   });
                }else{
                   $('#CoId').empty();
                }
                }
                });
          }else{
             $('#CoId').empty();
          }
       });

   });

</script>
</body>
</html>