@php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
       <meta name="_token" content="{{csrf_token()}}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{ url('/') }}/assets/fonts/materialdesignicons.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/css/vendor.bundle.base.css">
      <!-- endinject -->
      <!-- Plugin css for this page -->
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/style.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/fonts/fontawesome.min.css">
      <!-- Styles -->
   </head>
   <body class="antialiased">
      <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                       
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           @include('include.sidebar')
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper">
                                   @yield('content-marksentry')
                                    @include('include.footer')
                              </div>
                              <!-- content-wrapper ends -->
                              <!-- partial:partials/_footer.html -->
                             
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->

                        </div>

                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="{{ url('/') }}/assets/vendors/js/vendor.bundle.base.js"></script>
      <!-- endinject -->
      <!-- Plugin js for this page -->
      <script src="{{ url('/') }}/assets/vendors/chart.js/Chart.min.js"></script>
      <script src="{{ url('/') }}/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
      <!-- End plugin js for this page -->
      <!-- inject:js -->
      <script src="{{ url('/') }}/assets/js/off-canvas.js"></script>
      <script src="{{ url('/') }}/assets/js/hoverable-collapse.js"></script>
      <script src="{{ url('/') }}/assets/js/misc.js"></script>
      <script src="{{ url('/') }}/assets/js/settings.js"></script>
      <script src="{{ url('/') }}/assets/js/todolist.js"></script>
      <!-- endinject -->
      <!-- Custom js for this page -->
      <script src="{{ url('/') }}/assets/js/dashboard.js"></script>
   </body>
</html>