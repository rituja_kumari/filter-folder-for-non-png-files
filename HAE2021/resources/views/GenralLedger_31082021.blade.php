<tr>
    <td></td>
    @foreach($SchemaArray as $SchemaInfo)
    
        @foreach($SchemaInfo['EsPara'] as $Parameter)
        
            @php  
                $paperId=$Parameter['SubEsId'];
                $ArrPaperId=explode(',',$paperId);
                $paperTitle=$Parameter['SubTitle'];
                if($paperId!='-11' and sizeof($ArrPaperId)=='1'){ $SubjectDetails=$SubjectInformation[$paperId];}
                
                if($paperTitle=='fetchfromdb')
                {
                    $title=$SubjectDetails['EsName'];
                }
                else
                {
                    $title=$paperTitle;
                }
                
            @endphp
           
                @if($paperTitle!='NA')    
                    <td align="center">{{$title}} </td>
                @endif
            
        @endforeach
    
    @endforeach
</tr>
    @php $count=1; @endphp
    @foreach($ArrayForMinMax as $INDEX => $Minmax)
        <tr style="background-color: #e8e5e4;">
            <td align="center">{{$Minmax}} </td>
            @foreach($SchemaArray as $SchemaInfo)
        
                @foreach($SchemaInfo['EsPara'] as $Parameter)
                    
                    @php  
                    $SubjectDetails=$paperId='';$SubjectDetails=array();
                        $paperId=$Parameter['SubEsId'];
                        $paperChildId=$Parameter['SubChildEsId'];
                        $ArrPaperId=explode(',',$paperId);
                        if($paperId!='-11' and sizeof($ArrPaperId)=='1')
                        {
                            $SubjectDetails=$SubjectInformation[$paperId];
                        }
                        elseif($paperId!='-11' and sizeof($ArrPaperId)> '1')
                        {
                            unset($ArrOfTotalOfPId);
                            IF($INDEX!='OBT')
                            {
                                foreach($ArrPaperId as $ArrPId)
                                {
                                    $PSubjectDetails=$SubjectInformation[$ArrPId];
                                    $ArrOfTotalOfPId[]=$PSubjectDetails[$INDEX];
                                }
                                $SubjectDetails[$INDEX]=array_sum($ArrOfTotalOfPId);   
                            }
                        } 
                        elseif($paperChildId=='OBT')
                        {
                            $SubjectDetails=$GrandTotal;
                        }else
                        {
                            $SubjectDetails=array("EsMaxMarks"=>'','EsPassMarks'=>'');
                        }
                        @endphp
                        @if(($Parameter['SubChildEsId']!='REMARK' and $Mode=='Publish') or ($Mode!='Publish'))
                        <td align="center">
                            @IF($INDEX!='OBT') 
                                @php if($SubjectDetails[$INDEX]>0){ echo (int)$SubjectDetails[$INDEX]; }@endphp 
                               
                            @else
                                @php echo $count++; @endphp 
                            @ENDIF</td>
                        @endif
                    @endforeach
        
                @endforeach    
                @if(!empty($DecodesGroupArray))

                    @foreach($DecodesGroupArray as $GroupKey => $SchemaInfo)
                        @if($SchemaInfo['key']=='total')
                            <td align="center" > @IF($INDEX!='OBT')  @php echo intval($GrandTotal[$INDEX]); @endphp  @ENDIF</td>   
                        @endif
                    
                    @endforeach
                @endif
            </tr>           
        @endforeach 
</thead>
              
        @foreach($AllExamFormsDetails as $Form)
            @php
            $ClassName='-';
                if($Form['EfGrdPts']!='-'){
                    $ClassName=$ClassArrayoFetch[$Form['EfGrdPts']];
                }       
                $ExamForReleInfo=array("RESULT"=> $CodesArrayoFetch[$Form->EfResult],"CLASS"=>$ClassName,"OBT"=>$Form['EfObtMarks'],"REMARK"=>"");
               
            @endphp
            <tr>
                <td style="text-align:left">{{$Form['FullName']}} </td>
                <td width="6%">{{$Form['EfRollNo']}}</td>
            
                    @foreach($SchemaArray as $SchemaInfo)
                
                        @foreach($SchemaInfo['EsPara'] as $Parameter)
                            
                            @php  
                            $PassFailStat=$Marks=$SubjectDetails=$paperId=''; unset($ArrForMarks);
                            $SubChildEsId=$Parameter['SubChildEsId'];
                            if(($SubChildEsId!='REMARK' and $Mode=='Publish') or ($Mode!='Publish'))
                            {
                                $ArrSubChildEsId=explode(',',$SubChildEsId);
                                unset($SingleRecordsForStu);
                                $SingleRecordsForStu=$StudentWiseSubjectDetails[$Form['RgNo']];
                               
                                if(!empty($SingleRecordsForStu))
                                {
                                    if(sizeof($ArrSubChildEsId)=='1' and !in_array($SubChildEsId,$InvalidChildEsId))
                                    {
                                        $Marks=$SingleRecordsForStu[$SubChildEsId]['ObtMarks'];
                                        $PassFailStat=$SingleRecordsForStu[$SubChildEsId]['PassFailStat'];
                                    }
                                    elseif(sizeof($ArrSubChildEsId)>'1')
                                    {
                                        foreach($ArrSubChildEsId as $ChildId)
                                        {
                                            $ArrForMarks[]=$SingleRecordsForStu[$ChildId]['ObtMarks'];
                                        }
                                        $Marks=array_sum($ArrForMarks);
                                    }
                                    elseif(in_array($SubChildEsId,$InvalidChildEsId))
                                    {
                                        $Marks=$ExamForReleInfo[$SubChildEsId];
                                    }
                                    if($Marks=='-11.10')
                                    {
                                        $Marks='AB';
                                    }
                                    elseif($Marks=='-12.00')
                                    {
                                        $Marks='NA';
                                    }
                                    elseif($Marks< 0)
                                    {
                                        $Marks='NA';
                                    }
                                    elseif(is_numeric($Marks))
                                    {
                                        $Marks=intval(round($Marks));
                                    }
                                }
                                else{
                                    $Marks='';
                                }
                                @endphp
                                <td align="center">  @if($PassFailStat=='0') <span style="color:red"> {{$Marks}} </span> @else <span> {{$Marks}} </span> @endif </td>
                                @php
                            }
                            @endphp
                           
                        @endforeach
                    @endforeach
                    @if(!empty($DecodesGroupArray))
                    @foreach($DecodesGroupArray as $GroupKey => $SchemaInfo)
                        @if($SchemaInfo['key']=='total')
                            <td align="center" > @IF($INDEX!='OBT')   {{$ExamForReleInfo['OBT']}}  @ENDIF</td>        
                        @endif
                    
                    @endforeach
                @endif
                @if($Mode=='Publish')
                    <td align="center">{{$Form['EfPC']}}</td>
                    <td align="center" width="6%">  @if($Form->EfResult=='A') WITHDRAWL @endif</td>
                @endif
            </tr>
        @endforeach