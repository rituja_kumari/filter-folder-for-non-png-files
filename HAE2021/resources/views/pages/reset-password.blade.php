@extends('reset-ps')
@section('content-reset')
<style type="text/css">
   .reset-ps{background: #fff;
    padding: 30px;}
</style>
<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span> Reset Password
   </h3>
 <!--   <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
         <li class="breadcrumb-item active" aria-current="page">
            <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
         </li>
      </ul>
   </nav> -->
</div>

<div class="row" >
  
   <div class="col-lg-6 col-xs-12">
      <div class="reset-ps">
            <!-- <div id="success-reset" class="hide">
                <div class="alert alert-info alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="model" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                    <strong>Success!</strong> Password Reset Successfully!!
                </div>
            </div> -->
                <form id="reset-ps-form"  method="POST" action="{{ route('rspassword') }}">
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12">
                    <label>Enter Username</label>
                    <input type="text" name="CgCode" value="{{session('data')['0']['CgCode']}}" class="form-control1" readonly />

                    </div> 
                    <div class="col-12 col-lg-12 col-md-12">
                    <label>Enter New Password</label>
                    <input type="text" name="cnewps" maxlength="10" id="cnewps" class="form-control1">
                    <span class="text-danger"><strong id="newps-error"></strong></span>
                    </div> 
                    <div class="col-12 col-lg-12 col-md-12">
                    <label>Confirm New Password</label>
                    <input type="password" name="cconfirmpass" maxlength="10" id="cconfirmpass" class="form-control1">
                    <span class="text-danger"><strong id="cnewps-error"></strong></span>
                    </div> 
                    <div class="col-12 col-lg-12 col-md-12">
                        <button type="submit" class="btn-admin" id="reset-ps"> Reset Password</button>
                    </div>
                    <span id="StuRegSuccess"></span>
                </div>
                </form>
      </div>
               
   </div>
</div>
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<!-- reset password -->
  <script>
         $(document).ready(function(){
          var form=$("#reset-ps-form");
             $('#reset-ps').click(function(e){
                 e.preventDefault();
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                 });
                var BankAcNo=$('#cnewps').val();
                var reBankAcNo=$('#cconfirmpass').val();
                if(BankAcNo!=reBankAcNo)
                {
                    $('#cnewps-error').html( "Password and Confirm Password Do Not Match..!" );
                    return false;
                }
                else{
                    $( '#cnewps-error' ).html( " " );
                }
                 $.ajax({
                     url: "{{ url('/rst-url') }}",
                     method: 'post',
                      data:form.serialize(),
                     success: function(data){
                      //console.log(data)
                       if(data.errors) {
                           if(data.errors.cnewps){
                               $( '#newps-error' ).html( "Please Enter New Password Minimum length Is 4" );
                           }else{
                               $( '#newps-error' ).html( "");
                           }
                           if(data.errors.cconfirmpass){
                               $( '#cnewps-error' ).html( "Please Enter Confirm Password" );
                           }else{
                               $( '#cnewps-error' ).html( "");
                           }                          
                       }
                       if(data.success) {
                               $( '#newps-error' ).html( "");
                               $( '#cnewps-error' ).html( "");
                            $('#success-reset').removeClass('hide');
                            setInterval(function(){ 
                               $('#success-reset').addClass('hide');
                               window.location.replace('{{route('admin')}}');
                           }, 3000);
                           $("#StuRegSuccess").html("Password Reset Successfully !").css({"color": "green"});
                       }
                     }
                 });
             });
         });
      </script>

@stop
