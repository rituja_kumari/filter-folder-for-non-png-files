@php
use App\Helpers\tools;
@endphp
<h3><i class="fa fa-address-book"></i> Correspondence Address</h3>
                 <section>
                     <div id="success-msg-address" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                     <form  id="stadress-info"  method="post" action="{{ route('address_update') }}" >
                        @csrf
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                            <input type="hidden" name="address_id" value="{{$add_per_id}}">
                              <h5>Present Address</h5>
                           </div>
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <label>Home/BLDG Name</label>
                              <textarea name="staddress" class="required form-control1"  rows="3" cols="3" style="height: 60px;">
                               {{$add_per_add}}
                                 </textarea>
                              <span class="text-danger"><strong id="staddress-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <div class="col-sm-12 pd-0">
                                <label>Street</label>
                                 <input type="text" name="street" class="form-control1"  value="{{$add_street}}">
                                 <span class="text-danger"><strong id="ststate-error"></strong></span>
                              </div>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <div class="col-sm-12 pd-0">
                                  <label>Landmark</label>
                                 <input type="text" name="landmark" class="form-control1"  value="{{$add_landmark}}">
                                 <span class="text-danger"><strong id="landmark-error"></strong></span>
                              </div>
                           </div>
                            <div class="form-group col-4 col-md-4 col-lg-4">
                               <label>State*</label>
                              <select class="form-control1" name="state">
                                 <option value="">Please Select</option>
                                 <option value="27" <?= ($add_state=='27')?'selected':'' ?>>MAHARASHTRA</option>
                              </select>
                              <span class="text-danger"><strong id="state-error"></strong></span>
                           </div>

                           <div class="form-group col-4 col-md-4 col-lg-4">
                                 <label>Distric*</label>
                              <select class="form-control1" id="stdistrictname" name="stdistrictname" onclick="getdropdown('stdistrictname','subdist','subdist');">
                                  <option value="">Please Select</option>
                                 @foreach($district as $dist)
                                 {{ tools::options_select([$dist->DistrictCode=>$dist->DsName],$add_district) }}
                                  @endforeach
                                
                              </select>
                              <span class="text-danger"><strong id="stdistrictname-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <label>Sub District*</label>
                              <select class="form-control1 required" name="subdist" id="subdist">
                              <option value="">Please Select</option>
                              </select>
                              <span class="text-danger"><strong id="subdist-error"></strong></span>
                           </div>
                     
                     
                           <div class="form-group col-4 col-md-4 col-lg-4">
                            <label>Pincode*</label>
                              <input  name="postalcode" type="text" class="required form-control1" value="{{$add_pincode}}">
                              <span class="text-danger"><strong id="postalcode-error"></strong></span>
                           </div>
                     
                    
                    
                     
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <button type="submit" class="btn-admin" id="address-info-save"> save</button>
                           </div>

                           <div>
                             <span id="success_msg_address"></span>
                           </div>

                     </form>
                  </section>
<script>
   $(document).ready(function(){
    getdropdown('stdistrictname','subdist','subdist');
    var subdistexist='<?php  echo $add_sub_district;?>';
    if(subdistexist!='')
    {
    setInterval(function(){ 
      $('#subdist').val(subdistexist);
      }, 2000);
   }
    var form=$("#stadress-info");
       $('#address-info-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           var url= $("#stadress-info").attr('action')
           $.ajax({
               url: url,
               method: 'post',
               data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                    if(data.errors.staddress){
                         $( '#staddress-error' ).html( "Please Enter Address" );
                     }else{
                         $( '#staddress-error' ).html( "");
                     }
                     if(data.errors.stdistrictname){
                         $( '#stdistrictname-error' ).html( "Please Select District Name" );
                     }else{
                         $( '#stdistrictname-error' ).html( "");
                     } 
                     if(data.errors.state){
                         $( '#state-error' ).html( "Please Select state Name" );
                     }else{
                         $( '#state-error' ).html( "");
                     } 
                     if(data.errors.subdist){
                         $( '#subdist-error' ).html( "Please select subdistrict" );
                     }else{
                         $( '#subdist-error' ).html( "");
                     } 
                     if(data.errors.postalcode){
                         $( '#postalcode-error' ).html( "Please Enter Postal Code" );
                     }else{
                         $( '#postalcode-error' ).html( "");
                     }     
                      
                 }
                 if(data.success) {
                         $( '#staddress-error' ).html( "");
                         $( '#state-error' ).html( "");
                         $( '#stdistrictname-error' ).html( "");
                         $( '#subdist-error' ).html( "");
                         $( '#postalcode-error' ).html( "");
                         $( '#success_msg_address' ).html("Correspondence Address Saved Successfully, Click On Next Button..!").css({"color": "green", "font-size": "15px"});
                     $('#success-msg').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg').addClass('hide');
                     }, 3000);
                    //window.location.replace('{{route('form')}}');
                 }
               }
           });
       });
   });
</script>