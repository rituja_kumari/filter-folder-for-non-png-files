
<h3><i class="fa fa-edit"></i> Course</h3>
                  <section>
 <div id="success-msg-course" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="model" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Check your mail for login confirmation!!
                     </div>
                  </div>
                    <form id="stcourse-info"  method="post" action="#" >
                     <div class="row">
                        <div class="col-12 col-lg-12 col-md-12">
                           <h5>Please Fill Your Details</h5>
                        </div>
                        <div class="form-group col-4 col-lg-4 col-md-4">
                           <label>Course Type*</label>
                          <select class="form-control1 required" name="stcrstype">
                             <option>Course 1</option>
                             <option>Course 2</option>
                             <option>Course 3</option>
                          </select>
                           <span class="text-danger"><strong id="stcrstype-error"></strong></span>
                        </div>
                        <div class="form-group col-4 col-lg-4 col-md-4">
                            <label>Student Type*</label>
                          <select class="form-control1" name="sttype" id="sttype">
                             <option value="stcontents1">Regular </option>
                             <option value="stcontents2">AT/KT</option>
                             <option value="stcontents3">Repeatater </option>
                          </select>
                           <span class="text-danger"><strong id="sttype-error"></strong></span>
                        </div>
                        <div class="form-group col-4 col-lg-4 col-md-4">
                           <label>Passing Year*</label>
                           <input  name="stuyear" type="text" class="required form-control1" placeholder="">
                           <span class="text-danger"><strong id="stuyear-error"></strong></span>
                        </div>
                     </div>
                     <div class="stcontents" id="stcontents1"  style="display: none;">
                     </div>
                     <div class="stcontents" id="stcontents2"  style="display: none;">
                        <div class="row">
                       <div class="form-group col-4 col-lg-4 col-md-4">
                           <label>Subject</label>
                          <select class="form-control1">
                             <option>Subject 1</option>
                             <option>Subject 2</option>
                             <option>Subject 3</option>
                          </select>
                        </div>
                        <div class="form-group col-4 col-lg-4 col-md-4">
                           <label>Status</label>
                          <select class="form-control1" id="statuspass">
                             <option value="pass">Pass</option>
                             <option value="fail">Fail</option>
                          </select>
                        </div>
                        <div class="form-group statuspass col-4 col-lg-4 col-md-4" id="pass" style="display: none;">
                        </div>
                         <div class="form-group statuspass col-4 col-lg-4 col-md-4" id="fail" style="display: none;">
                           <label>Marks</label>
                          <input type="text" class="form-control1"  name="">
                        </div>

                     </div>
                  </div>
                   <div class="stcontents" id="stcontents3"  style="display: none;">
                        <div class="row">
                       <div class="form-group col-4 col-lg-4 col-md-4">
                           <label>Subject</label>
                          <select class="form-control1">
                             <option>Subject 1</option>
                             <option>Subject 2</option>
                             <option>Subject 3</option>
                          </select>
                        </div>
                        <div class="form-group col-4 col-lg-4 col-md-4">
                           <label>Status</label>
                          <select class="form-control1">
                             <option>Pass</option>
                             <option>Fail</option>
                          </select>
                        </div>
                         <div class="form-group col-4 col-lg-4 col-md-4" style="display: none;">
                           <label>Marks</label>
                          <input type="text" class="form-control1" id="stmarks" name="">
                        </div>

                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group col-12 col-lg-12 col-md-12">
                        <button type="submit" class="btn-admin" id="stcourse-save"> save</button>
                     </div>
                  </div>
               </form>
                  </section>