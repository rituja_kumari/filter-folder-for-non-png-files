<h3><i class="fa fa-rupee"></i> Payment</h3>
   <section>
      <form id="amtpay"  method="post" action="{{ route('payment') }}" >
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <div class="row">
          <div class="col-lg-12 col-md-12">
            <input type="hidden" name="hidden_id" value="{{ session('studata')[0]['StuId'] }}">
            <table class="table-records" border="1">
              <tr>
                <th>Sr.No</th>
                <th> Click On CheckBox</th>
                <th>College Name</th>
                <th>Course Name</th>
                <th>From Type</th>
                <th>Amount</th>
              </tr>
              <?php
                $Ammount=0;
              ?>
              @php($count=0)

              @foreach($course_info as $cour_info) 
              <tr>
                <td>{{++$count}}</td>
                <td><input type="checkbox" name="chkamt_{{$cour_info->course_id}}[]" class="checkbox1" value="{{$cour_info->amount}}"></td>
                <td>{{ \App\CollegeModel::where('CgId',$cour_info->CgId)->first()->CgName }}</td>
                <td>{{ \App\Course::where('CfCoId',$cour_info->course_id)->first()->CoName }}</td>
                <td>{{ \App\Qualification::where('TypeId',$cour_info->st_type)->first()->Type }}</td>
                <td>{{ $cour_info->amount }}</td>
                <input type="hidden" name="CfCoIdArr[]" value="{{$cour_info->course_id}}">
                <input type="hidden" name="CgIdArr[]" value="{{$cour_info->CgId}}">
                <input type="hidden" name="StTypeArr[]" value="{{$cour_info->st_type}}">
                
              </tr>
              @endforeach
                <tr>
                <td colspan="5">Total Ammount: </td>
                <td align="left" ><input type="text" name="Ammount" id="Ammount" class="form-control1" value=""></td>
              </tr>
            </table>
          </div>
          <?php
          

          ?>

          <input type="text" name="msg" value=""/>
          <span id="pay_error" class="text-danger"></span>
         </div>
         <br>
         <div class="form-group col-12 col-md-12 col-lg-12">
             <button type="submit" id="submit" class="btn-admin" id="btnconfirm" disabled=""> Pay</button>
          </div>
      </form>
   </section>
      <script type="text/javascript">
function do_this(){
  var checkboxes = document.getElementsByName('chkamt[]');
  var button = document.getElementById('toggle');

  if(button.value == 'Select All'){
    for (var i in checkboxes){
      checkboxes[i].checked = 'FALSE';

    }
    button.value = 'Deselect'
  }else{
    for (var i in checkboxes){
      checkboxes[i].checked = '';
    }
    button.value = 'Select All';
  }
}

$(".checkbox1").change(function () {
    var count = 0;
    var message="";
    var table_abc = document.getElementsByClassName("checkbox1");
    for (var i = 0; table_abc[i]; ++i) {

        if (table_abc[i].checked) 
        {
            var value = table_abc[i].value;
            var row = table_abc[i].parentNode.parentNode;
            message += row.cells[1].innerHTML;
            message += row.cells[2].innerHTML;
            count += Number(table_abc[i].value);
        }
    }
    alert(message);
    $('#Ammount').val(count);
    if (count > 0) 
    {
      $('#submit').attr('disabled',false);
    }
    else{$('#submit').attr('disabled',true);}

});
   </script>


