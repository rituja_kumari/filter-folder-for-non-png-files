 <div class="col-lg-12 col-md-12 col-12 mt-30">
      <div class="card">
         <div class="card-body">
            <table id="userTable" class="table-records" border="1">
               <thead>
                  <tr>
                     <th>Sr. No</th>
                     <th>Course </th>
                     <th>Branch </th>
                     <th>Semester</th>
                     <th>Subject</th>
                     <th>Head</th>
                     <th>Paper</th>
                     <th class="w-24">Action</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td>1</td>
                     <td>Drawing Grade Examination</td>
                     <td>NA</td>
                     <td>Elementary Drawing Grade Examination</td>
                     <td>Object Drawing</td>
                     <td>-</td>
                     <td>-</td>
                     <td><a href="#" class="Action-btn"><i class="fa fa-eye"></i></a> <a href="#" class="Action-btn"><i class="fa fa-trash"></i></a> <a href="#" class="Action-btn">View Q-Paper</a></td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>