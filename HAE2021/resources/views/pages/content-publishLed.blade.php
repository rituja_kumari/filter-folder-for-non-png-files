@extends('marks-entry')
@section('content-marksentry')

<style type="text/css">
  .reset-ps{background: #fff;
   padding: 30px;}
   .mb-10{margin-bottom: 10px;}
   @media(max-width:768px)
   {
       .reset-ps{
   padding: 10px;}
   }
   .body_overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: fixed;left: 0;top: 0;width: 100%;height: 100%;width: 100%;height: 100%;display: none;}
	.body_overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<script src="{{ url('/')}}/js/ajax_js.js" ></script>
<script>
   $(function() {
      $(".datepicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
});
function assn_prg_do_this()
		{
			var checkboxes = document.getElementsByName('ArrToPublish[]');
			
			var button = document.getElementById('toggle');

			if(button.value == 'Select All'){
				button.value = 'Deselect All';
				for (var i in checkboxes){
					checkboxes[i].checked = 'FALSE';
				}
			}
			else{
				button.value = 'Select All';
				for (var i in checkboxes){
					checkboxes[i].checked = '';
				}
			}
		}
</script>

<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span>Higher Art Examination - 2021
   </h3>
</div>
<div class="row" >
<div class="body_overlay"><div><img src="{{ url('/') }}/img/loading11.gif" width="80px" height="80px"/></div></div>
   <div class="col-12">
   <div class="reset-ps">
      <div class="reset-ps">
         <form id="MarksEntry" method="POST" >
            <div class="row">
               <div class=" col-lg-4 col-md-4" id="DivExam">
                     @php
                    
                     $Excondition[]=array("ColumnName"=>"EntryStat","Operator"=>"=","ColumnValue"=>'1');
                     $Excondition[]=array("ColumnName"=>"EmId","Operator"=>"!=","ColumnValue"=>'');
                        $Enccondition=json_encode($Excondition);
                        $exam =sel_exam($Enccondition,'','','','form-control1','',"ajax_combo3('LedgerList/','','#CollegeList','MarksEntry')",'1');  
                     @endphp
               </div>
               @if(session('data')['0']['username']=='exxon')
               <div class="col-4 col-lg-4 col-md-4" style="margin-top: 25px;">
                    <button type="button" class="btn btn-primary" id="Save" onclick="ajax_combo3('SavePublishLedger/','','#res','MarksEntry')">Publish</button>
               </div>
               @endif
            <div class="col-12 col-lg-12 col-md-12" id="CollegeList"> </div>
           
            <div id="res"></div>
            <div class="reset-ps mt-30"></div>
         </form>
      </div>
     
   </div>
</div>

<script src="{{ url('/') }}/js/jquery.min.js"></script>

<!-- reset password  -->
@stop