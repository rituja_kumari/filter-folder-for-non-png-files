@extends('Notification-panel')
@section('content-NotificationPanel')
<style type="text/css">
  .reset-ps{background: #fff;
   padding: 30px;}
   .mb-10{margin-bottom: 10px;}
   @media(max-width:768px)
   {
       .reset-ps{
   padding: 10px;}
   }
   .body_overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: fixed;left: 0;top: 0;width: 100%;height: 100%;width: 100%;height: 100%;display: none;}
	.body_overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<script src="{{ url('/')}}/js/ajax_js.js" ></script>
<script>
   $(function() {
      $(".datepicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
});
</script>

<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span>Notification Panel
   </h3>
</div>
<div class="row" >
<div class="body_overlay"><div><img src="{{ url('/') }}/img/loading11.gif" width="80px" height="80px"/></div></div>
   <div class="col-12">
   <div class="reset-ps">
      <div class="reset-ps">
         <form id="MarksEntry" method="POST" >
            <div class="row">
               <div class="col-12 col-lg-12 col-md-12" id="subjectInfo"> 
                  <table class="table-records" border="1" width="100%" style="border-collapse: collapse;">
                     <thead>
                        <tr>
                           <th>Sr No.</th>
                           <th>Title</th>
                           <th>Attachment</th>
                        </tr>
                     </thead>
                     <tbody>
                        @php $c=1; @endphp
                        @foreach($notification_master as $Notifications)
                           
                           <tr>
                           <td>{{$c++}}</td>
                           <td>{{$Notifications->NotiTitle}}</td>
                           <td>
                              <a href="{{url('/'.$Notifications->NotifyFilePath) }}" target="_BLANK"> Doc</a>
                             </td>
                        </tr>
                        @endforeach
                       
                     </tbody> 
                     
                  </table>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<script src="{{ url('/') }}/js/jquery.min.js"></script>
@stop