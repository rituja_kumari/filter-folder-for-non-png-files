
 <h3><i class="fa fa-user"></i> Personal Information</h3>
                  <section>
                     <div id="success-msg-st" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="model" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Check your mail for login confirmation!!
                     </div>
                  </div>
                     <form  id="personnal-info"  method="post" action="{{ route('personalinfo') }}" >
                     <div class="row">
                        <div class="col-12 col-lg-12 col-md-12">
                           <h5>Please Fill Your Details</h5>
                        </div>
                        <div class="form-group col-12 col-lg-12 col-md-12">
                           <input  name="stname" type="text" class="required form-control1" placeholder="Name As Per Certificate/Marksheet">
                           <span class="text-danger"><strong id="stname-error"></strong></span>
                        </div>
                        <div class="form-group col-6 col-md-6 col-lg-6">
                           <input  name="fathername" type="text" class="required form-control1" placeholder="Father/Husband Name *">
                            <span class="text-danger"><strong id="fathername-error"></strong></span>
                        </div>
                        <div class="form-group col-6 col-md-6 col-lg-6">
                           <input  name="mothername" type="text" class="required form-control1" placeholder="Mother Name  *">
                            <span class="text-danger"><strong id="mothername-error"></strong></span>
                        </div>
                        <div class="form-group col-6 col-md-6 col-lg-6">
                           <input  name="stmobile" type="text" class="required form-control1" placeholder="Mobile*">
                           <span class="text-danger"><strong id="stmobile-error"></strong></span>
                        </div>
                        <div class="form-group col-6 col-md-6 col-lg-6">
                           <input  name="stemail" type="text" class="required form-control1" placeholder="Email-ID  *">
                           <span class="text-danger"><strong id="stemail-error"></strong></span>
                        </div>
                        <div class="form-group col-4 col-md-4 col-lg-4">
                           <label class="w-100">Gender</label>
                           <div class="form-check ">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="gender" id="optionsRadios1" value="option1"> Male </label>
                           </div>
                           <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="gender" id="optionsRadios2" value="option2" > Female </label>
                           </div>
                            <span class="text-danger"><strong id="gender-error"></strong></span>
                        </div>
                        <div class="form-group col-4 col-md-4 col-lg-4">
                           <label>Physically Handicapped</label>
                           <div class="form-check ">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input"  id="handicap1" name="tab" value="igottwo" onclick="show2();"> Yes </label>
                           </div>
                           <div class="form-check">
                              <label class="form-check-label">
                              <input type="radio" class="form-check-input"  id="handicap2" name="tab" value="igotnone" onclick="show1();"> No </label>
                           </div>
                           <span class="text-danger"><strong id="tab-error"></strong></span>
                        </div>
                         <div class="form-group col-4 col-md-4 col-lg-4" id="div1" style="display: none;">
                           <label>PWD Type</label>
                           <select class="form-control1">
                              <option>Select</option>
                              <option>option 1</option>
                              <option>2</option>
                           </select>
                         </div>
                        <div class="form-group col-4 col-md-4 col-lg-4">
                           <label>Date Of Birth* </label>
                           <input  name="birthdate" type="date" class="required form-control1" placeholder="Birthdate *">
                            <span class="text-danger"><strong id="birthdate-error"></strong></span>
                        </div>
                        <div class="form-group col-4 col-md-4 col-lg-4">
                           <label>Age</label>
                           <input  name="age" type="text" class="required form-control1" placeholder="">
                           <span class="text-danger"><strong id="age-error"></strong></span>
                        </div>
                        <div class="form-group col-4 col-md-4 col-lg-4 ">
                           <label class="">Category</label>
                           <div class="col-sm-12 pd-0">
                              <select class="form-control1" name="category1">
                                 <option>Category1</option>
                                 <option>Category2</option>
                                 <option>Category3</option>
                                 <option>Category4</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group col-4 col-md-4 col-lg-4 ">
                           <label class="">Caste</label>
                           <div class="col-sm-12 pd-0">
                              <select class="form-control1" name="caste">
                                 <option>Category1</option>
                                 <option>Category2</option>
                                 <option>Category3</option>
                                 <option>Category4</option>
                              </select>
                           </div>
                        </div>
                         <div class="form-group col-12 col-lg-12 col-md-12">
                           <button type="submit" class="btn-admin" id="personal-info-save"> save</button>
                         </div>
                     </div>
                  </form>
                  </section>

