@extends('forms')
@section('content-form')
<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span> Student Application 
   </h3>
   <!--    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
         <li class="breadcrumb-item active" aria-current="page">
            <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
         </li>
      </ul>
      </nav> -->
</div>
<style type="text/css">
   section .row{margin: 0;}
   .form-group{margin-bottom: 0;}
   section h5{font-size: 14px;
   margin-bottom: 16px;
   text-transform: uppercase;
   font-weight: 500;
   color: #000;}
   .fees-block{
   background: #fff;
   padding: 10px;
   font-size: 16px;
   font-weight: 600;
   }
   .mb-10{margin-bottom: 10px;}
   .checkbox{float: left;
   width: 25px;
   height: 25px;
   margin-right: 10px;}

</style>
<!--vertical wizard-->
<div class="container">
<div class="row">
   <div class="col-12 grid-margin">
      <div class="card">
         <div class="card-body">
            <h4 class="card-title">Student Application Information <i class="fa fa-info"></i> </h4>
            <div class="row">
                <div>
                    <span>Personal Information</span>
                </div>
                 <table class="table-records-2" border="1">
                   <thead>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>DOB</th>
                      <th>Gender</th>
                      <th>Category</th>
                      <th>Caste</th>
                      <th>PH</th>
                      <th>PH Type</th>
                   </thead>
                   @php($count=0)
                    @foreach($personal as $per)  
                    <tr>
                      <td>{{++$count}}</td>
                      <td>{{ $per->cname}}</td>
                      <td>{{ $per->email}}</td>
                      <td>{{ $per->mobile}}</td>
                      <td>{{ $per->birthdate}}</td>
                      <td>{{ $per->gender}}</td>
                      <td>{{ \App\Category::where('CatCd',$per->category)->first()->CatNm }}</td>
                      <td>{{ $per->caste}}</td>
                      <td>{{ $per->pwd }}</td>
                      @if($per->pwd=='Yes')
                      <td>{{ \App\Pwd::where('DisabilityTypeID',$per->ph_type)->first()->DisabilityTypeNameEng }}</td>
                      @else
                      <td>-</td>
                      @endif
                    </tr>
                    @endforeach
                 </table>
            </div>
             <div class="row">
                <div>
                    <span>Address Information</span>
                </div>
                 <table class="table-records-2" border="1">
                   <thead>
                      <th>Address</th>
                      <th>Sub District</th>
                      <th>District</th>
                      <th>State</th>
                      <th>Pincode</th>
                   </thead>
                    @foreach($address as $add)  
                    <tr>
                      <td>{{ $add->address}},{{ $add->street }},{{ $add->landmark }}</td>
                      <td>{{ \App\Subdistric::where('SubDistrictCode',$add->sub_district)->first()->SubDistrict }}</td>
                      <td>{{ \App\District::where('DistrictCode',$add->district)->first()->DsName }}</td>
                      <td>{{ \App\StateModel::where('StateId',$add->state)->first()->StateName }}</td>
                      <td>{{ $add->pincode }}</td>
                    </tr>
                    @endforeach
                 </table>
            </div>
            <div class="row">
                <div>
                    <span>Course Information</span>
                </div>
                 <table class="table-records-2" border="1">
                   <thead>
                      <th>Id</th>
                      <th>College Name</th>
                      <th>Course Name</th>
                      <th>Form Type</th>
                      <th>Amount</th>
                   </thead>
                   @php($count=0)
                    @foreach($courses as $cour)  
                    <tr>
                      <td>{{++$count}}</td> 
                      <td>{{ $cour->CgName }}</td>
                      <td>{{ $cour->CoName }}</td>
                      <td>{{ \App\Qualification::where('TypeId',$cour->st_type)->first()->Type }}</td>
                      <td>{{ $cour->amount }}</td>
                    </tr>
                    @endforeach
                 </table>
            </div>
         </div>
      </div>
   </div>
   <!-- content-wrapper ends -->
</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{ url('/') }}/js/jquery.min.js"></script>
@stop