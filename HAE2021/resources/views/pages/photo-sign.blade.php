 <h3><i class="fa fa-puzzle-piece"></i> Photo and Sign Upload</h3>
                  <section>
                     <div id="success-msg-stphoto" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                     <form id="stphoto-info"  method="post" action="{{ route('upload')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                              <h5>Please Fill Your Details</h5>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Photo*</label>
                              <!--        <img src="{{ url('/') }}/img/user-alt-512.png" class="w-100"> -->
                              <img id="blah" src="{{ url('/') }}/public/uploads/photo/{{$StuId}}.jpg" alt="your image" class="w-100 mb-10">
                              <input type='file' onchange="readURL(this);" class="form-control1" name="photo">
                              <span class="text-danger"><strong id="photo-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Sign*</label>
                              <img id="blah2" src="{{ url('/') }}/public/uploads/photo/{{$StuId}}.jpg" alt="Sign Image" class="w-100 mb-10">
                              <input type='file' onchange="readsign(this);" class="form-control1" name="sign">
                              <span class="text-danger"><strong id="sign-error"></strong></span>
                           </div>
                           <div class="form-group col-6 col-md-6 col-lg-6">
                              <button type="submit" class="btn-admin" id="stphoto-save"> save</button>
                           </div>
                           <div>
                             <span id="success_msg_photo"></span>
                           </div>
                        </div>
                     </form>
                  </section>


<!-- <script type="text/javascript">
       $('#stphoto-save').click(function(e){
    var form=$("#stphoto-info");
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
            var url= $("#stphoto-info").attr('action');
            alert();
           $.ajax({
               url: url,
               method: 'post',
               data:form.serialize(),
               processData: false,
               contentType: false,
               cache: false,
               success: function(data){
                console.log(data)
                 if(data.errors) {
                  if(data.errors.sign){
                         $( '#sign-error' ).html( "Please Select Sign" );
                     }else{
                         $( 'sign-error' ).html("");
                     }
                     if(data.errors.photo){
                         $( '#photo-error' ).html( "Please Select Photo" );
                     }else{
                         $( '#photo-error' ).html( "");
                     }
                                              
                      
                 }
                 if(data.success) {
                        $( 'sign-error' ).html("");
                         $( '#photo-error' ).html( "");
                     $('#success_msg_photo').removeClass('hide');
                     setInterval(function(){ 
                         $('#success_msg_photo').addClass('hide');
                     }, 3000);
                      window.location.replace('{{route('form')}}');
                 }
               }
           });
       });
</script> -->