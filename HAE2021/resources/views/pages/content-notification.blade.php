@extends('notification')
@section('content-notification')
<style type="text/css">
   .reset-ps{background: #fff;
    padding: 30px;}
    .body_overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: fixed;left: 0;top: 0;width: 100%;height: 100%;width: 100%;height: 100%;display: none;}
	.body_overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">

<script src="{{ url('/')}}/js/ajax_js.js" ></script>
<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span>Add Notification
   </h3>
 
</div>

<div class="row" >
<div class="body_overlay"><div><img src="{{ url('/') }}/img/loading11.gif" width="80px" height="80px"/></div></div>
   <div class="col-lg-12 col-md-12 col-12">
      <div class="reset-ps">
      @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
      @php    $date = date('Y-m-d'); @endphp
                <form id="NotiMaster" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-6 col-lg-6 col-md-6">
                        <label>Title</label>
                        <textarea class="form-control1" rows="3" name="NotiTitle" id="NotiTitle"></textarea>
                    </div> 
                   
                    <div class="col-6 col-lg-6 col-md-6">
                        <label>Browse PDF</label>
                        <input type="file" name="NotifyFilePath" id="NotifyFilePath"  class="form-control1">
                    </div> 
                    
                    <div class="col-6 col-lg-6 col-md-6">
                        <label>Start Date</label>
                        <input type="date" name="StartDate" id="StartDate"  class="form-control1"  min="{{$date}}">
                    </div> 
                     <div class="col-6 col-lg-6 col-md-6">
                        <label>End Date</label>
                        <input type="date" name="EndDate" id="EndDate"  class="form-control1"  min="{{$date}}">
                    </div> 
                    
                    <div class="col-12 col-lg-12 col-md-12">
                    <button type="submit" class="btn btn-primary" id="Save">Submit</button>
                    </div>
                </div>
                <div id="res">
                @include('NotificationMasterTbl')
                </div>
                </form>
      </div>
               
   </div>
</div><script src="{{ url('/') }}/js/jquery.min.js"></script>
<script>
   
   $(document).ready(function (e) {
      $.ajaxSetup({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
         $('#NotiMaster').submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
               type:'POST',
               url: "{{ url('SaveNotiMaster')}}",
               data: formData,   
               cache:false,
               contentType: false,
               processData: false,
               beforeSend: function() {
                  $('.body_overlay').show();
               },
               success: function(data)
               {
                  if (data.errors) 
                  {
                     alert(data.errors);
                  }
                  if (data.html) 
                  {
                     $("#NotiMaster")[0].reset();
                     $('#res').html(data.html);
                  }
                  if (data.ValidatorErrors) 
                  {
                     $.each(data.ValidatorErrors, function(index, jsonObject) {
                        $.each(jsonObject, function(key, val) {
                              alert(val);   $('.body_overlay').hide(); exit();
                        });
                     });
                  }
                  $('.body_overlay').hide();
               },
               error: function(data){
               console.log(data);
               }
            });
         });
});
</script>
@stop
