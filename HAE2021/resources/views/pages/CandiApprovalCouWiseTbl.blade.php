<table class="table-records" border="1">
   <thead>
      <th>Sr No.</th>
      <th>Student Name</th>
      <th>Amount</th>
      <th>Payment Status</th>
      <th>Status</th>
      @if($ActionStatus=='2' or $ActionStatus=='3')<th>Remark</th> @endif
      <th>Action</th>
   </thead>
   <tbody>
         @php($count=0)
        
         @foreach($usersinfo as $key => $value)
            <tr>
               <td> {{++$count}}</td>
               <td> {{$value->cname}}</td>
               <td> {{$value->amount}}</td>
               <td></td>
               <td> 
                  @if($ActionStatus=='0' or $ActionStatus=='2')
                     <input type="checkbox" name="ActionStu[{{$value->id}}]" id="ActionStu" value="{{$value->id}}">
                  @elseif($ActionStatus=='1')
                     Approved
                  @elseif($ActionStatus=='3')
                     Rejected
                  @endif   
               </td>
               @if($ActionStatus=='2')<td><input type="text" name="Remark[{{$value->id}}]" id=""></td>
               @else  <input type="hidden" name="Remark[{{$value->id}}]" id=""> @endif
               @if($ActionStatus=='3')<td> {{$value->StuRemk}} </td> @endif
               <td>
                     <a href="app_print/{{$value->StuId}}" class="Action-btn" target="_BLANK"><i class="mdi mdi-eye"></i></a>  
                     <!-- <a href="#" class="Action-btn"><i class="fa fa-trash"></i></a> -->
                     <input type="hidden" name="AllStudent[]" value="{{$value->id}}" >
                  </td>
            </tr>
         @endforeach
</tbody>
</table>

         