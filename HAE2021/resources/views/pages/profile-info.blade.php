@php
use App\Helpers\tools;
@endphp


<h3><i class="fa fa-user"></i> Personal Information</h3>
                  <section>
                     <div id="success-msg-st" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                    
               <form  id="personnal-info"  method="post" action="{{ route('form_store') }}" >
                        @csrf
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                         
                              <h5>Please Fill Your Details  </h5>
                           </div>
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <input  name="stname" type="text" class="required form-control1" placeholder="Name (SSC Certificate)" value="{{ old('stname',$stname ? $stname: '') }}" readonly="true">
                              <span class="text-danger"><strong id="stname-error"></strong></span>
                           </div>
                           <div class="form-group col-6 col-md-6 col-lg-6">
                              <input  name="stmobile" type="text" class="required form-control1" placeholder="Mobile*" value="{{ old('stmobile',$StuMob ? $StuMob: '') }}" readonly="true">
                              <span class="text-danger"><strong id="stmobile-error"></strong></span>
                           </div>
                           <div class="form-group col-6 col-md-6 col-lg-6">
                              <input  name="stemail" type="text" class="required form-control1" placeholder="Email-ID  *" value="{{ old('stemail',$StuEmail ? $StuEmail: '') }}"  readonly="true">
                              <span class="text-danger"><strong id="stemail-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <label class="w-100">Gender*</label>
                              <div class="form-check ">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input" name="gender" id="optionsRadios1" value="male">Male </label>
                              </div>
                              <div class="form-check">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input" name="gender" id="optionsRadios2" value="female"> Female </label>
                              </div>
                              <span class="text-danger"><strong id="gender-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <label>Person With Disability (PWD)?*</label>
                              <div class="form-check">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input"  id="handicap1" name="pwd" value="Yes" onclick="show2();" > Yes </label>
                              </div>
                              <div class="form-check">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input"  id="handicap2" name="pwd" value="No" onclick="show1();" > No </label>
                              </div>
                              <span class="text-danger"><strong id="tab-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4" id="div1" style="display:none;">
                              <label>PWD Type</label>
                              <select class="form-control1" name="ph_type" id="ph_type">
                                  <option value="">Select</option>
                                 @foreach($pwd as $p)
                                 {{ tools::options_select([$p->DisabilityTypeID=>$p->DisabilityTypeNameEng]) }}
                                  @endforeach
                               
                              </select>
                                 <span class="text-danger"><strong id="ph_type-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <label>Date Of Birth* </label>
                              <input  name="birthdate" type="date" class="required form-control1" placeholder="Birthdate *" >
                              <span class="text-danger"><strong id="birthdate-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4 ">
                              <label class="">Category</label>
                              <div class="col-sm-12 pd-0">
                                 <select class="form-control1" name="category">
                                  <option value="">Select</option>
                                 @foreach($category as $cate)
                                 {{ tools::options_select([$cate->CatCd=>$cate->CatNm]) }}
                                  @endforeach
                                 
                                 </select>
                                 <span class="text-danger"><strong id="category-error"></strong></span>

                              </div>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4 ">
                              <label class="">Caste</label>
                              <div class="col-sm-12 pd-0">
                                 <input type="text" name="caste" class="form-control1" placeholder="Caste">
                              </div>
                           </div>
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <button type="button" class="btn-admin" id="personal-info-save"> save</button>
                           </div>
                            <div>
                             <span id="success_msg_profile"></span>
                           </div>
                        </div>
                     </form>
                  </section>

                  <!-- validation personal info -->
<script>
   $(document).ready(function(){
    var form=$("#personnal-info");
       $('#personal-info-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           var url= $("#personnal-info").attr('action')
           // alert(url);
           $.ajax({
               url: url,
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.stname){
                         $( '#stname-error' ).html( "Please Enter Name" );
                     }else{
                         $( '#stname-error' ).html( "");
                     }
                     
                     if(data.errors.stmobile){
                         $( '#stmobile-error' ).html( "Please Enter Mobile" );
                     }else{
                         $( '#stmobile-error' ).html( "");
                     } 
                     if(data.errors.stemail){
                         $( '#stemail-error' ).html( "Please Enter Email-ID" );
                     }else{
                         $( '#stemail-error' ).html( "");
                     }                          
                     if(data.errors.gender){
                         $( '#gender-error' ).html( "Please Select Gender" );
                     }else{
                         $( '#gender-error' ).html( "");
                     } 
                     if(data.errors.tab){
                         $( '#tab-error' ).html( "Please Select Physically Handicapped " );
                     }else{
                         $( '#tab-error' ).html( "");
                     } 
                     if(data.errors.birthdate){
                         $( '#birthdate-error' ).html( "Please Enter Date Of Birth" );
                     }else{
                         $( '#birthdate-error' ).html( "");
                     }
                 }
                 if(data.success) {
                         $( '#stname-error' ).html( "");
                         $( '#stmobile-error' ).html( "");
                         $( '#stemail-error' ).html( "");
                         $( '#gender-error' ).html( "");
                         $( '#tab-error' ).html( "");
                         $( '#birthdate-error' ).html( "");
                        $( '#success_msg_profile').html("Data Inserted.......");
                     $('#success-msg-st').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-st').addClass('hide');
                     }, 3000);
                     window.location.replace('{{route('form')}}');
                 }
               }
           });
       });
   });
</script>