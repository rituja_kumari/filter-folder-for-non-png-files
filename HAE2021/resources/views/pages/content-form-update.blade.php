@extends('forms')
@section('content-form')
@php
use App\Helpers\tools;
@endphp
<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span> Student Registration 
   </h3>
   <!--    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
         <li class="breadcrumb-item active" aria-current="page">
            <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
         </li>
      </ul>
      </nav> -->
</div>
<style type="text/css">
   section .row{margin: 0;}
   .form-group{margin-bottom: 0;}
   section h5{font-size: 14px;
   margin-bottom: 16px;
   text-transform: uppercase;
   font-weight: 500;
   color: #000;}
   .fees-block{
   background: #fff;
   padding: 10px;
   font-size: 16px;
   font-weight: 600;
   }
   .mb-10{margin-bottom: 10px;}
   .checkbox{float: left;
   width: 25px;
   height: 25px;
   margin-right: 10px;}
</style>
<!--vertical wizard-->
<div class="row">
   <div class="col-12 grid-margin">
      <div class="card">
         <div class="card-body">
            <h4 class="card-title">Student Information Form <i class="fa fa-info"></i> </h4>
            <div id="example-vertical-wizard">
               <div>
                  <h3><i class="fa fa-user"></i> Personal Information</h3>
                  <section>
                     <div id="success-msg-st" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                    
               <form  id="personnal-info-update"  method="post" action="{{ route('form.update',$personal[0]->id) }}" >
                        @csrf
                         @method('PATCH')
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                              <h5>Please Fill Your Details</h5>
                           </div>
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <input  name="stname" type="text" class="required form-control1" placeholder="Name (SSC Certificate)" value="{{ old('stname',$stname ? $stname: '') }}" readonly="true">
                              <span class="text-danger"><strong id="stname-error"></strong></span>
                           </div>
                           <div class="form-group col-6 col-md-6 col-lg-6">
                              <input  name="stmobile" type="text" class="required form-control1" placeholder="Mobile*" value="{{ old('stmobile',$StuMob ? $StuMob: '') }}" readonly="true">
                              <span class="text-danger"><strong id="stmobile-error"></strong></span>
                           </div>
                           <div class="form-group col-6 col-md-6 col-lg-6">
                              <input  name="stemail" type="text" class="required form-control1" placeholder="Email-ID  *" value="{{ old('stemail',$StuEmail ? $StuEmail: '') }}" readonly="true">
                              <span class="text-danger"><strong id="stemail-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <label class="w-100">Gender*</label>
                              <div class="form-check ">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input" name="gender" id="optionsRadios1" value="male"  <?= $personal[0]->gender=='male' ? 'checked' : '' ?>> Male </label>
                              </div>
                              <div class="form-check">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input" name="gender" id="optionsRadios2" value="female"  <?= $personal[0]->gender=='female' ? 'checked' : '' ?>> Female </label>
                              </div>
                              <span class="text-danger"><strong id="gender-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <label>Person With Disability (PWD)?*</label>
                              <div class="form-check ">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input"  id="handicap1" name="pwd" value="Yes" onclick="show2();" <?= $personal[0]->pwd=='Yes' ? 'checked' : '' ?>> Yes </label>
                              </div>
                              <div class="form-check">
                                 <label class="form-check-label">
                                 <input type="radio" class="form-check-input"  id="handicap2" name="pwd" value="No" onclick="show1();" <?= $personal[0]->pwd=='No' ? 'checked' : '' ?>> No </label>
                              </div>
                              <span class="text-danger"><strong id="tab-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4" id="div1" style="display: <?= $personal[0]->pwd=='Yes' ? 'block' : 'none' ?>;">
                              <label>PWD Type</label>
                              <select class="form-control1" name="ph_type" id="ph_type">
                                 <option value="">Select</option>
                                 @foreach($pwd as $p)
                                 {{ tools::options_select([$p->DisabilityTypeID=>$p->DisabilityTypeNameEng],
                              old('ph_type',$personal ? $personal[0]->ph_type : '') ) }}
                                  @endforeach
                              </select>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <label>Date Of Birth* </label>
                              <input  name="birthdate" type="date" class="required form-control1" placeholder="Birthdate *" value="{{ old('birthdate',$personal ? $personal[0]->birthdate: '') }}">
                              <span class="text-danger"><strong id="birthdate-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4 ">
                              <label class="">Category</label>
                              <div class="col-sm-12 pd-0">
                                 <select class="form-control1" name="category">
                                 <option value="">Select</option>
                                 @foreach($category as $cate)
                                 {{ tools::options_select([$cate->CatCd=>$cate->CatNm],
                              old('category',$personal ? $personal[0]->category : '') ) }}
                                  @endforeach
                                 </select>
                              </div>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4 ">
                              <label class="">Caste</label>
                              <div class="col-sm-12 pd-0">
                                 <input type="text" name="caste" class="form-control1" placeholder="Caste" value="{{ old('caste',$personal ? $personal[0]->caste: '') }}">
                              </div>
                           </div>
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <button type="submit" class="btn-admin" id="personal-info-save"> save</button>
                           </div>
                        </div>
                     </form>
                  </section>
                  <h3><i class="fa fa-address-book"></i> Correspondence Address</h3>
                 <section>
                     <div id="success-msg-address" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                     <form  id="stadress-info"  method="post" action="{{ route('address') }}" >
                        @csrf
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                              <h5>Present Address</h5>
                           </div>
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <textarea name="staddress" class="required form-control1" placeholder="Home/BLDG Name" rows="3" cols="3" style="height: 60px;">
                                 <?= empty($address)?'':$address[0]->address; ?>
                                 </textarea>
                              <span class="text-danger"><strong id="staddress-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <div class="col-sm-12 pd-0">
                                 <input type="text" name="street" class="form-control1" placeholder="Street" value="<?= empty($address)?'':$address[0]->street; ?>">
                                 <span class="text-danger"><strong id="ststate-error"></strong></span>
                              </div>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <div class="col-sm-12 pd-0">
                                 <input type="text" name="landmark" class="form-control1" placeholder="Landmark" value="<?= empty($address)?'':$address[0]->landmark; ?>">
                                 <span class="text-danger"><strong id="landmark-error"></strong></span>
                              </div>
                           </div>
                            <div class="form-group col-4 col-md-4 col-lg-4">
                              
                              <select class="form-control1" name="state">
                                 <option>State*</option>
                                 <option value="27" selected=<?= empty($address[0]->state=='27')?"selected":""; ?>>MAHARASHTRA</option>
                              </select>
                              <span class="text-danger"><strong id="state-error"></strong></span>
                           </div>

                           <div class="form-group col-4 col-md-4 col-lg-4">
                              
                              <select class="form-control1" id="stdistrictname" name="stdistrictname" onclick="getdropdown('stdistrictname','subdist','subdist');">
                                 <option>Distric*</option>
                                 @foreach($district as $dist)
                                 {{ tools::options_select([$dist->DistrictCode=>$dist->DistrictNameEng],old('stdistrictname',$address ? $address[0]->district : '')) }}
                                  @endforeach
                              </select>
                              <span class="text-danger"><strong id="stdistrictname-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <select class="form-control1 required" name="subdist" id="subdist">
                                 <option>Sub District*</option>
                              </select>
                              <span class="text-danger"><strong id="subdist-error"></strong></span>
                           </div>
                     
                     
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <input  name="postalcode" type="text" class="required form-control1" placeholder="Pincode*" value="<?= empty($address)?'':$address[0]->pincode; ?>">
                              <span class="text-danger"><strong id="postalcode-error"></strong></span>
                           </div>
                     
                    
                     
                      <!-- <div class="col-12 col-lg-12 col-md-12">
                              <h5>Permanent address &nbsp;&nbsp;&nbsp;<input  type="checkbox" name="check_addr" onclick="Ad_as_abv(this);">&nbsp;Same as Present Address</h5> 
                           </div> -->
                           <!-- <div class="form-group col-12 col-lg-12 col-md-12">
                              <textarea name="p_address" class="required form-control1" placeholder="Home/BLDG Name" rows="6" cols="6" style="height: 60px;">value="<?= empty($address)?'':$address[0]->p_address; ?>"</textarea>
                              <span class="text-danger"><strong id="p_address-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <div class="col-sm-12 pd-0">
                                 <input type="text" name="p_street" class="form-control1" placeholder="Street" value="<?= empty($address)?'':$address[0]->p_street; ?>">
                                 <span class="text-danger"><strong id="p_street-error"></strong></span>
                              </div>
                           </div>
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <div class="col-sm-12 pd-0">
                                 <input type="text" name="p_landmark" class="form-control1" placeholder="Landmark" value="<?= empty($address)?'':$address[0]->p_landmark; ?>">
                                 <span class="text-danger"><strong id="p_landmark-error"></strong></span>
                              </div>
                           </div>

                           <div class="form-group col-4 col-md-4 col-lg-4">
                              
                              <select class="form-control1" name="p_state">
                                 <option>State*</option>
                                 <option value="27">MAHARASHTRA</option>
                              </select>
                              <span class="text-danger"><strong id="p_state-error"></strong></span>
                           </div>

                           <div class="form-group col-4 col-md-4 col-lg-4">
                              
                              <select class="form-control1" id="p_district" name="p_district" onclick="getdropdown('p_district','p_subdist','subdist');">
                                 <option>District*</option>
                                 @foreach($district as $dist)
                                 {{ tools::options_select([$dist->DistrictCode=>$dist->DistrictNameEng],old('p_district',$address ? $address[0]->p_district : '')) }}
                                  @endforeach
                              </select>
                              <span class="text-danger"><strong id="p_district-error"></strong></span>
                           </div>
                            <div class="form-group col-4 col-md-4 col-lg-4">
                              <select class="form-control1 required" name="p_subdist" id="p_subdist">
                                 <option>Sub District*</option>
                              </select>
                              <span class="text-danger"><strong id="subdist-error"></strong></span>
                           </div>
                     
                           <div class="form-group col-4 col-md-4 col-lg-4">
                              <input  name="p_postalcode" type="text" class="required form-control1" placeholder="Pincode*" value="<?= empty($address)?'':$address[0]->p_postalcode; ?>">
                              <span class="text-danger"><strong id="p_postalcode-error"></strong></span>
                           </div> -->
                     
                     
                     
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <button type="submit" class="btn-admin" id="address-info-save"> save</button>
                           </div>
                        </div>
                     </form>
                  </section>
                  <h3><i class="fa fa-edit"></i> Select College/Centre & Course</h3>
                  <section>
                     <div id="success-msg-course" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                     <form id="stcourse-info"  method="post" action="{{ route('course_info') }}" >
                        @csrf
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                              <h5>Please Fill Your Details</h5>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Select Colllege / Centre Name *</label>
                              <select class="form-control1 required" id="centername" name="centername" onclick="getdropdown('centername','stcrstype','course');">
                                 <option>Select</option>
                                @foreach($colleges as $clg)
                                 {{ tools::options_select([$clg->CgId=>$clg->CgName]) }}
                                  @endforeach
                              </select>
                              <span class="text-danger"><strong id="centername-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Course Type*</label>
                              <select class="form-control1 required" name="stcrstype" id="stcrstype">
                                 <option>Select</option>
                              </select>
                              <span class="text-danger"><strong id="stcrstype-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Student Type*</label>
                              <select class="form-control1" name="sttype" id="sttype">
                                 <option>Select</option>
                                 <option value="regular">Regular </option>
                                 <option value="atkt">AT/KT</option>
                                 <option value="repeater">Repeatater </option>
                              </select>
                              <span class="text-danger"><strong id="sttype-error"></strong></span>
                           </div>
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <p class="fees-block">Course Fees: <span>20,000/-</span></p>
                           </div>
                        </div>
                        <div class="stcontents" id="stcontents1"  style="display: none;">
                        </div>
                        <div class="stcontents" id="stcontents2"  style="display: none;">
                           <div class="row">
                              <div class="form-group col-6 col-lg-6 col-md-6">
                                 <label>Subject</label>
                                 <select class="form-control1" name="stsubject1">
                                    <option>Subject 1</option>
                                    <option>Subject 2</option>
                                    <option>Subject 3</option>
                                 </select>
                              </div>
                              <div class="form-group col-6 col-lg-6 col-md-6">
                                 <label>Status</label>
                                 <select class="form-control1" name="ststatus1">
                                    <option value="pass">Pass</option>
                                    <option value="fail">Fail</option>
                                 </select>
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="obmarks1" placeholder="Obtained Marks">
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="ofmarks1" placeholder="Out Of Marks">
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="passyear1" placeholder="Year">
                              </div>
                           </div>
                        </div>
                        <div class="stcontents" id="stcontents3"  style="display: none;">
                           <div class="row">
                              <div class="form-group col-6 col-lg-6 col-md-6">
                                 <label>Subject</label>
                                 <select class="form-control1" name="stsubject2">
                                    <option>Subject 1</option>
                                    <option>Subject 2</option>
                                    <option>Subject 3</option>
                                 </select>
                              </div>
                              <div class="form-group col-6 col-lg-6 col-md-6">
                                 <label>Status</label>
                                 <select class="form-control1" name="ststatus2">
                                    <option value="pass">Pass</option>
                                    <option value="fail">Fail</option>
                                 </select>
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="obmarks2" placeholder="Obtained Marks">
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="ofmarks2" placeholder="Out Of Marks">
                              </div>
                              <div class="form-group col-4 col-lg-4 col-md-4">
                                 <input type="text" class="form-control1"  name="passyear2" placeholder="Year">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group col-12 col-lg-12 col-md-12">
                              <button type="submit" class="btn-admin" id="stcourse-save"> save</button>
                           </div>
                        </div>
                     </form>
                  </section>
                  <h3><i class="fa fa-graduation-cap"></i> Qualification</h3>
                  <section>
                     <div class="row">
                        <div class="col-12 col-lg-12 col-md-12">
                           <h5>Please Fill Your Details</h5>
                        </div>
                        <div class="form-group col-12 col-lg-12 col-md-12 qatypes">
                           <div id="success-msg-qualification1" class="hide" style="display: none;">
                              <div class="alert alert-info alert-dismissible fade in" role="alert">
                                 <button type="button" class="close" data-dismiss="model" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                                 <strong>Success!</strong> Check your mail for login confirmation!!
                              </div>
                           </div>
                           <form id="qualification1-info"  method="post" action="{{ route('qualification') }}" >
                              @csrf
                              <div class="row">
                                 <div class="content-selection" id="content-selection" style="display: none;">
                                    <div class="row">
                                       <div class="form-group col-12 col-md-12 col-lg-12 ">
                                          <h5>SSC/10th</h5>
                                       </div>
                                       <div class="form-group col-3 col-lg-3 col-md-3 ">
                                          <select class="form-control1">
                                             <option>Status</option>
                                             <option>Pass</option>
                                             <option>Fail</option>
                                          </select>
                                       </div>
                                       <div class="form-group col-3 col-lg-3 col-md-3">
                                          <input type="text" class="form-control1"  name="obmarks3" placeholder="Obtained Marks">
                                       </div>
                                       <div class="form-group col-3 col-lg-3 col-md-3">
                                          <input type="text" class="form-control1"  name="ofmarks3" placeholder="Out Of Marks">
                                       </div>
                                       <div class="form-group col-3 col-lg-3 col-md-3">
                                          <input type="text" class="form-control1"  name="passyear3" placeholder="Year">
                                       </div>
                                       <div class="form-group col-12 col-md-12 col-lg-12 ">
                                          <h5>Intermediate Grade</h5>
                                       </div>
                                       <div class="form-group col-3 col-lg-3 col-md-3 ">
                                          <select class="form-control1">
                                             <option>Status</option>
                                             <option>Pass</option>
                                             <option>Fail</option>
                                          </select>
                                       </div>
                                       <div class="form-group col-3 col-lg-3 col-md-3">
                                          <input type="text" class="form-control1"  name="crnameintermediate" placeholder="Center Name">
                                       </div>
                                       <div class="form-group col-3 col-lg-3 col-md-3">
                                          <input type="text" class="form-control1"  name="Grade" placeholder="Grade">
                                       </div>
                                       <div class="form-group col-3 col-lg-3 col-md-3">
                                          <input type="text" class="form-control1"  name="passyear4" placeholder="Year">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group col-6 col-lg-6 col-md-6">
                                    <button type="submit" class="btn-admin" id="quatype-save1"> save</button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </section>
                  <h3><i class="fa fa-puzzle-piece"></i> Photo and Sign Upload</h3>
                  <section>
                     <div id="success-msg-stphoto" class="hide" style="display: none;">
                        <div class="alert alert-info alert-dismissible fade in" role="alert">
                           <button type="button" class="close" data-dismiss="model" aria-label="Close">
                           <span aria-hidden="true">×</span>
                           </button>
                           <strong>Success!</strong> Check your mail for login confirmation!!
                        </div>
                     </div>
                     <form id="stphoto-info"  method="post" action="{{ route('stphoto') }}" >
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                              <h5>Please Fill Your Details</h5>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Photo*</label>
                              <!--        <img src="{{ url('/') }}/img/user-alt-512.png" class="w-100"> -->
                              <img id="blah" src="{{ url('/') }}/img/blankimg.jpg" alt="your image" class="w-100 mb-10">
                              <input type='file' onchange="readURL(this);" class="form-control1" name="photo">
                              <span class="text-danger"><strong id="photo-error"></strong></span>
                           </div>
                           <div class="form-group col-4 col-lg-4 col-md-4">
                              <label>Sign*</label>
                              <img id="blah2" src="{{ url('/') }}/img/blankimg.jpg" alt="Sign Image" class="w-100 mb-10">
                              <input type='file' onchange="readsign(this);" class="form-control1" name="sign">
                              <span class="text-danger"><strong id="sign-error"></strong></span>
                           </div>
                           <div class="form-group col-6 col-md-6 col-lg-6">
                              <button type="submit" class="btn-admin" id="stphoto-save"> save</button>
                           </div>
                        </div>
                     </form>
                  </section>
                  <h3><i class="fa fa-rupee"></i> Payment</h3>
                  <section>
                     <form id="finalsubmission"  method="post" action="#" >
                        <div class="row">
                           <div class="col-12 col-lg-12 col-md-12">
                              <input type="checkbox" class="checkbox" name="">  
                              <p>I ................. hereby declare that the information furnished above is true, complete and correct to the best of my knowledge and belief.</p>
                           </div>
                           <div class="form-group col-12 col-md-12 col-lg-12">
                              <button type="submit" class="btn-admin"> Confirm</button>
                              <button type="submit" class="btn-admin"> Payment</button>
                           </div>
                        </div>
                     </form>
                  </section>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- content-wrapper ends -->
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<script type="text/javascript">
   function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
   
          reader.onload = function (e) {
              $('#blah')
                  .attr('src', e.target.result)
                  .width(150)
                  .height(200);
          };
   
          reader.readAsDataURL(input.files[0]);
      }
   }
</script>
<script type="text/javascript">
   function readsign(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
   
          reader.onload = function (e) {
              $('#blah2')
                  .attr('src', e.target.result)
                  .width(150)
                  .height(200);
          };
   
          reader.readAsDataURL(input.files[0]);
      }
   }
</script>
<!-- main-panel ends -->
<script type="text/javascript">
   $(function() {
   $('#selectcourse').change(function(){
   $('#content-selection').show();
   // $('#course-selection' + $(this).val()).show();
   });
   });
</script>
<!-- validation personal info -->
<script>
   $(document).ready(function(){
    var form=$("#personnal-info-update");
       $('#personal-info-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           var url= $("#personnal-info-update").attr('action')
           $.ajax({
               url: url,
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.stname){
                         $( '#stname-error' ).html( "Please Enter Name" );
                     }else{
                         $( '#stname-error' ).html( "");
                     }
                     
                     if(data.errors.stmobile){
                         $( '#stmobile-error' ).html( "Please Enter Mobile" );
                     }else{
                         $( '#stmobile-error' ).html( "");
                     } 
                     if(data.errors.stemail){
                         $( '#stemail-error' ).html( "Please Enter Email-ID" );
                     }else{
                         $( '#stemail-error' ).html( "");
                     }                          
                     if(data.errors.gender){
                         $( '#gender-error' ).html( "Please Select Gender" );
                     }else{
                         $( '#gender-error' ).html( "");
                     } 
                     if(data.errors.tab){
                         $( '#tab-error' ).html( "Please Select Physically Handicapped " );
                     }else{
                         $( '#tab-error' ).html( "");
                     } 
                     if(data.errors.birthdate){
                         $( '#birthdate-error' ).html( "Please Enter Date Of Birth" );
                     }else{
                         $( '#birthdate-error' ).html( "");
                     }
                 }
                 if(data.success) {
                         $( '#stname-error' ).html( "");
                         $( '#stmobile-error' ).html( "");
                         $( '#stemail-error' ).html( "");
                         $( '#gender-error' ).html( "");
                         $( '#tab-error' ).html( "");
                         $( '#birthdate-error' ).html( "");
                     $('#success-msg-st').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-st').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<!--  -->
<!-- validation communication address -->
<script>
   $(document).ready(function(){
    var form=$("#stadress-info");
       $('#address-info-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           var url= $("#stadress-info").attr('action')
         alert(url);
           $.ajax({
               url: url,
               method: 'post',
               data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.staddress){
                         $( '#staddress-error' ).html( "Please Enter Address" );
                     }else{
                         $( '#staddress-error' ).html( "");
                     }
                     if(data.errors.stcityname){
                         $( '#stcityname-error' ).html( "Please Enter City Name" );
                     }else{
                         $( '#stcityname-error' ).html( "");
                     } 
                     if(data.errors.stdistrictname){
                         $( '#stdistrictname-error' ).html( "Please Select District Name" );
                     }else{
                         $( '#stdistrictname-error' ).html( "");
                     } 
                     if(data.errors.ststate){
                         $( '#ststate-error' ).html( "Please Enter Street Name" );
                     }else{
                         $( '#ststate-error' ).html( "");
                     } 
                     if(data.errors.stcounrty){
                         $( '#stcounrty-error' ).html( "Please Enter Landmark" );
                     }else{
                         $( '#stcounrty-error' ).html( "");
                     } 
                     if(data.errors.postalcode){
                         $( '#postalcode-error' ).html( "Please Enter Postal Code" );
                     }else{
                         $( '#postalcode-error' ).html( "");
                     }                          
                      
                 }
                 if(data.success) {
                         $( '#staddress-error' ).html( "");
                         $( '#stcityname-error' ).html( "");
                         $( '#stdistrictname-error' ).html( "");
                         $( '#ststate-error' ).html( "");
                         $( '#stcounrty-error' ).html( "");
                         $( '#postalcode-error' ).html( "");
                     $('#success-msg-address').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-address').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<!-- validation communication address -->
<!-- qualification type 1 -->
<script>
   $(document).ready(function(){
      getdropdown('stdistrictname','subdist','subdist');
    var form=$("#qualification1-info");
       $('#quatype-save1').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/qualification1-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.state1){
                         $( '#state1-error' ).html( "Please Enter State" );
                     }else{
                         $( '#state1-error' ).html( "");
                     }
                     if(data.errors.yearname1){
                         $( '#yearname1-error' ).html( "Please Enter Year" );
                     }else{
                         $( '#yearname1-error' ).html( "");
                     } 
                     if(data.errors.monthname1){
                         $( '#monthname1-error' ).html( "Please Enter Month" );
                     }else{
                         $( '#monthname1-error' ).html( "");
                     } 
                     if(data.errors.seatno1){
                         $( '#seatno1-error' ).html( "Please Select Seat No." );
                     }else{
                         $( '#seatno1-error' ).html( "");
                     } 
                     if(data.errors.centercode1){
                         $( '#centercode1-error' ).html( "Please Select Center Code" );
                     }else{
                         $( '#centercode1-error' ).html( "");
                     }                         
                      
                 }
                 if(data.success) {
                         $( '#state1-error' ).html( "");
                         $( '#yearname1-error' ).html( "");
                         $( '#monthname1-error' ).html( "");
                         $( '#seatno1-error' ).html( "");
                         $( '#centercode1-error' ).html( "");
                     $('#success-msg-qualification1').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-qualification1').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<!-- end -->
<!-- qualificationtype 2 -->
<script>
   $(document).ready(function(){
    var form=$("#qualification2-info");
       $('#quatype-save2').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/qualification2-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.state2){
                         $( '#state2-error' ).html( "Please Enter State" );
                     }else{
                         $( '#state2-error' ).html( "");
                     }
                     if(data.errors.yearname2){
                         $( '#yearname2-error' ).html( "Please Enter Year" );
                     }else{
                         $( '#yearname2-error' ).html( "");
                     } 
                     if(data.errors.monthname2){
                         $( '#monthname2-error' ).html( "Please Enter Month" );
                     }else{
                         $( '#monthname2-error' ).html( "");
                     } 
                     if(data.errors.seatno2){
                         $( '#seatno2-error' ).html( "Please Select Seat No." );
                     }else{
                         $( '#seatno2-error' ).html( "");
                     } 
                     if(data.errors.centercode2){
                         $( '#centercode2-error' ).html( "Please Select Center Code" );
                     }else{
                         $( '#centercode2-error' ).html( "");
                     }                         
                      
                 }
                 if(data.success) {
                         $( '#state2-error' ).html( "");
                         $( '#yearname2-error' ).html( "");
                         $( '#monthname2-error' ).html( "");
                         $( '#seatno2-error' ).html( "");
                         $( '#centercode2-error' ).html( "");
                     $('#success-msg-qualification2').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-qualification2').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<!-- end -->
<!-- qualificationtype 3 -->
<script>
   $(document).ready(function(){
    var form=$("#qualification3-info");
       $('#quatype-save3').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/qualification3-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.state3){
                         $( '#state3-error' ).html( "Please Enter State" );
                     }else{
                         $( '#state3-error' ).html( "");
                     }
                     if(data.errors.yearname3){
                         $( '#yearname3-error' ).html( "Please Enter Year" );
                     }else{
                         $( '#yearname3-error' ).html( "");
                     } 
                     if(data.errors.monthname3){
                         $( '#monthname3-error' ).html( "Please Enter Month" );
                     }else{
                         $( '#monthname3-error' ).html( "");
                     } 
                     if(data.errors.seatno3){
                         $( '#seatno3-error' ).html( "Please Select Seat No." );
                     }else{
                         $( '#seatno3-error' ).html( "");
                     } 
                     if(data.errors.centercode3){
                         $( '#centercode3-error' ).html( "Please Select Center Code" );
                     }else{
                         $( '#centercode3-error' ).html( "");
                     }                         
                      
                 }
                 if(data.success) {
                         $( '#state3-error' ).html( "");
                         $( '#yearname3-error' ).html( "");
                         $( '#monthname3-error' ).html( "");
                         $( '#seatno3-error' ).html( "");
                         $( '#centercode3-error' ).html( "");
                     $('#success-msg-qualification3').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-qualification3').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<!-- end -->
<!-- qualificationtype 4 -->
<script>
   $(document).ready(function(){
    var form=$("#qualification4-info");
       $('#quatype-save4').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/qualification4-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.state4){
                         $( '#state4-error' ).html( "Please Enter State" );
                     }else{
                         $( '#state4-error' ).html( "");
                     }
                     if(data.errors.yearname4){
                         $( '#yearname4-error' ).html( "Please Enter Year" );
                     }else{
                         $( '#yearname4-error' ).html( "");
                     } 
                     if(data.errors.monthname4){
                         $( '#monthname4-error' ).html( "Please Enter Month" );
                     }else{
                         $( '#monthname4-error' ).html( "");
                     } 
                     if(data.errors.seatno4){
                         $( '#seatno4-error' ).html( "Please Select Seat No." );
                     }else{
                         $( '#seatno4-error' ).html( "");
                     } 
                     if(data.errors.centercode4){
                         $( '#centercode4-error' ).html( "Please Select Center Code" );
                     }else{
                         $( '#centercode4-error' ).html( "");
                     }                         
                      
                 }
                 if(data.success) {
                         $( '#state4-error' ).html( "");
                         $( '#yearname4-error' ).html( "");
                         $( '#monthname4-error' ).html( "");
                         $( '#seatno4-error' ).html( "");
                         $( '#centercode4-error' ).html( "");
                     $('#success-msg-qualification4').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-qualification4').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<!-- end -->
<!-- course 2 -->
<script>
   $(document).ready(function(){
    var form=$("#stcourse-info");
       $('#stcourse-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
            var url= $("#stcourse-info").attr('action');
            alert();
           $.ajax({
               url: url,
               method: 'post',
               data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                  if(data.errors.centername){
                         $( '#centername-error' ).html( "Please Select Center/College Name" );
                     }else{
                         $( '#centername-error' ).html( "");
                     }
                     if(data.errors.stcrstype){
                         $( '#stcrstype-error' ).html( "Please Select Course" );
                     }else{
                         $( '#stcrstype-error' ).html( "");
                     }
                     if(data.errors.sttype){
                         $( '#sttype-error' ).html( "Please Select Student Type" );
                     }else{
                         $( '#sttype-error' ).html( "");
                     } 
                                              
                      
                 }
                 if(data.success) {
                        $( '#centername-error' ).html( "");
                         $( '#stcrstype-error' ).html( "");
                         $( '#sttype-error' ).html( "");
                     $('#success-msg-stcourse').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-stcourse').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<!-- end -->
<!-- photo -->
<script>
   $(document).ready(function(){
    var form=$("#stphoto-info");
       $('#stphoto-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/stphoto-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.photo){
                         $( '#photo-error' ).html( "Please Select Photo" );
                     }else{
                         $( '#photo-error' ).html( "");
                     }
                     if(data.errors.sign){
                         $( '#sign-error' ).html( "Please Select Sign " );
                     }else{
                         $( '#sign-error' ).html( "");
                     } 
                                              
                      
                 }
                 if(data.success) {
                         $( '#photo-error' ).html( "");
                         $( '#sign-error' ).html( "");
                     $('#success-msg-stphoto').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-stphoto').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<script type="text/javascript">
   $(function() {
   $('#qutype').change(function(){
    $('.qatypes').hide();
    $('#' + $(this).val()).show();
   });
   });
</script>
<script type="text/javascript">
   $(function() {
   $('#sttype').change(function(){
    $('.stcontents').hide();
    $('#' + $(this).val()).show();
   });
   });
</script>
<script type="text/javascript">
   function show1(){
   document.getElementById('div1').style.display ='none';
   }
   function show2(){
   document.getElementById('div1').style.display = 'block';
   }

   function getdropdown(get_val_id,concat_id,url)
   {
      var id = $('#'+get_val_id).val(); 
        if(id){
          $.ajax({
            type:"GET",
            url:url+"?id="+id,
            success:function(res){        
            if(res){
               console.log(res);
              $("#"+concat_id).empty();
       $("#"+concat_id).append('<option>Select</option>');
              $.each(res,function(key,value){
                $("#"+concat_id).append('<option value="'+key+'">'+value+'</option>');
              });
            
            }else{
              $("#"+concat_id).empty();
            }
            }
          });
        }else{
          $("#"+concat_id).empty();
        }
   }
</script>
@stop