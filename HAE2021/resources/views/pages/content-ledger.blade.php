@extends('marks-entry')
@section('content-marksentry')
@php $SessionalData=json_decode(Session('data'),true); @endphp
<style type="text/css">
   .reset-ps{background: #fff;
   padding: 30px;}
   .mb-10{margin-bottom: 10px;}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<script src="{{ url('/')}}/js/ajax_js.js" ></script>
<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span>Upload Ledger
   </h3>
</div>
<div class="row" >
   <div class="col-12">
      <div class="reset-ps">
         <form id="uploadledger" method="POST" action="{{ route('ledger.upload.post') }}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
               <table class="table-records mt-0" border="1">
                  <thead>
                     <tr>
                        <th>Sr No.</th>
                        <th>Course Name</th>
                        <th>Upload File</th>
                        <th>View File</th>
                        
                     </tr>
                  </thead>
                  @php $SrNo=1; @endphp
                  @foreach($FinalSubmittedData as $key => $CoId)
                  <tbody>
                     <tr>
                        <td>@php echo $SrNo++; @endphp</td>
                        <td>
                           {{ \App\Course::where('CfCoId',$CoId->activity)->where('EntryStat',1)->first()->CoName }}
                        </td>
                        <td><input type="file" name="filenames[]" class="form-control1"></td>
                        <td>
                           @php $path='ledger/'.$SessionalData['0']['CgId'].$CoId->activity.'.pdf';
                              $path1 = public_path($path);
                           @endphp
                           @if(file_exists($path1))
                           
                           <a target='_blank' href='public/ledger/{{$SessionalData['0']['CgId']}}{{$CoId->activity}}.pdf' style='color: green;' >view</a>
                           @else
                           <span style="color: red;">Not Uploaded file</span>
                           @endif
                        </td>
                        <input type="hidden" name="ArrOFCoId[]" value="{{$CoId->activity}}">
                     </tr>
                  </tbody>
               @endforeach
               </table><br><br>
               <div class="col-md-12">
                  <input type="submit" class="print-btn" value="submit" name="">
               </div>
                  <div class="col-md-12">
                  @if ($message = Session::get('success'))

                  <div class="alert alert-success alert-block">

                      <button type="button" class="close" data-dismiss="alert">×</button>

                          <strong>{{ $message }}</strong>

                  </div>

                  @endif
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                     <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>Sorry!</strong> Please upload pdf file only.<br><br>
                      <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                  </div>
                  @endif
                  </div>
            </div>
         </form>
      </div>
   </div>
</div>
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<!-- reset password -->
@stop