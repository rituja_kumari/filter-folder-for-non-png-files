@extends('question-paper-upload')
@section('content-qt')
<style type="text/css">
   label{font-weight: 100;}
   .pd-20{padding: 20px;}
   .w-24{width: 24%;}
</style>
<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-pencil"></i>
      </span> Question Paper Upload
   </h3>
  
</div>
<div class="row" >
  
  <div class="col-lg-12 col-md-12 col-12 ">
    <div id="success-msg-qt" class="hide" style="display: none;">
                     <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> Data Updated Successfully!!
                     </div>
                  </div>
      <form class="bg-white pd-20" id="qt-paper-upload"  method="post" action="{{ route('qtpaperupload') }}">
         <div class="row">
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Session </label>
               <select class="form-control1" name="qtsession">
                  <option value="">Select</option>
                  <option>Option 1</option>
               </select>
                  <span class="text-danger"><strong id="qtsession-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Faculty </label>
               <select class="form-control1" name="qtfaculty">
                  <option value="">Select</option>
                  <option>Option 1</option>
               </select>
                  <span class="text-danger"><strong id="qtfaculty-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Courses </label>
               <select class="form-control1" name="qtcourse">
                  <option value="">Select</option>
                  <option>Option 1</option>
               </select>
                  <span class="text-danger"><strong id="qtcourse-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Branch/ Scheme </label>
               <select class="form-control1" name="qtbranch">
                  <option value="">Select</option>
                  <option>Option 1</option>
               </select>
                  <span class="text-danger"><strong id="qtbranch-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Semester/ Year </label>
               <select class="form-control1" name="qtsemester">
                  <option value="">Select</option>
                  <option>Option 1</option>
               </select>
                  <span class="text-danger"><strong id="qtsemester-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Subject </label>
               <select class="form-control1" name="qtsubject">
                  <option value="">Select</option>
                  <option>Option 1</option>
               </select>
                  <span class="text-danger"><strong id="qtsubject-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">QPaper Upload </label>
               <input type="file" name="" class="form-control1" name="qtpaper">
                  <span class="text-danger"><strong id="qtpaper-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Live Date</label>
               <input type="date" name="" class="form-control1" name="qtlivedate">
                  <span class="text-danger"><strong id="qtlivedate-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Live Time From</label>
               <input type="text" name="" class="form-control1" name="qtlivetime">
                  <span class="text-danger"><strong id="qtlivetime-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Live Time Till</label>
               <input type="text" name="" class="form-control1" name="qttimetill">
                  <span class="text-danger"><strong id="qttimetill-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Enter Password</label>
               <input type="text" name="" class="form-control1" name="qtpass">
                  <span class="text-danger"><strong id="qtpass-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <label class="input-label">Confirm Password</label>
               <input type="text" name="" class="form-control1" name="qtconfirmpass">
                  <span class="text-danger"><strong id="qtconfirmpass-error"></strong></span>
            </div>
            <div class="col-lg-3 col-md-3">
               <button type="submit" class="print-btn" id="qtsubmit">Submit</button>
            </div>
         </div>
      </form>
   </div>
    @include('pages.qtpaper-table')
</div>

 <script src="{{ url('/') }}/js/jquery.min.js"></script>
<!-- validation -->
  @include('pages.validation.validation-qt-paper')
      <!-- end validation -->
@stop