
<html>
   <head>
      <meta charset="utf-8">
      <meta name="_token" content="{{csrf_token()}}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css">
      <style type="text/css">
         .t-center{text-align: center;}
         label{font-weight: 100;}
         .table-head-text{    font-size: 13px;
         text-transform: uppercase;
         font-weight: 700;}
         .lable-table{font-weight: 600;
         text-transform: uppercase;
         font-size: 12px;
         color: #000;}
         .mt-30{margin-top: 30px;}
         td{color: #888;padding-left: 10px;}
         .al-center{align-self: center;}
         .clg-name{font-size: 16px;
         text-transform: uppercase;
         color: #c67413;
         font-weight: 600;}
         /*.table-details{background: #fdfdfd;margin-top: 16px;padding: 20px 15px;}*/
         .inner-table tr{border: 1px solid #ddd;}
         .inner-table td {padding: 4px 4px;}
         .brd1px{border: 1px solid #ddd;}
         table{width: 100%;}
      </style>
   </head>
   <body>
      <div class="container">
         <div class="card-body">
            <div class="row" >
               <div class="col-md-1">
               </div>
               <div class="col-md-12 mb-3 row table-details" >
                  <table>
                     <tr style="text-align: center;">
                        <td colspan="2" style="text-align: center;">
                           <table style="text-align: center;width: 100%;">
                              <tr style="text-align: center;">
                                 <td style="width: 40%;text-align: right;"> <img src="{{ url('/') }}/img/logo11.png" style="width:100px;"></td>
                                 <td> <h2 style="font-size:16px;line-height: 26px;text-align: left;">Directorate of Art ,Maharashtra State,Mumbai<br>
                                 कला संचालनालय, महाराष्ट्र राज्य, मुंबई
                              </h2></td>
                              </tr>
                           </table> 
                        </td>
                     </tr>
                   

                    
                     <tr>
                        <td colspan="2">
                           <h5 class="table-head-text" style="margin-top: 30px;">Personal Information</h5>
                        </td>
                     </tr>
                     <tr>
                         @php($count=0)
                    @foreach($personal as $per) 
                        <td style="width: 50%;">
                            <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                   <thead>
                   
                    <tr>
                      <td class="lable-table">Id</td>
                      <td>{{++$count}}</td>
                    </tr>
                     <tr>
                      <td class="lable-table">Name</td>
                      <td>{{ $per->cname}}</td>
                    </tr>
                     <tr>
                      <td class="lable-table">Email</td>
                      <td>{{ $per->email}}</td>
                    </tr>
                     <tr>
                      <td class="lable-table">Mobile</td>
                      <td>{{ $per->mobile}}</td>
                    </tr>
                     <tr>
                      <td class="lable-table">DOB</td>
                      <td>{{ $per->birthdate}}</td>
                    </tr>
                   
                     
                   </thead>
                  <!--  @php($count=0)
                    @foreach($personal as $per)  
                    <tr>
                      <td>{{++$count}}</td>
                      <td>{{ $per->cname}}</td>
                      <td>{{ $per->email}}</td>
                      <td>{{ $per->mobile}}</td>
                      <td>{{ $per->birthdate}}</td>
                      <td>{{ $per->gender}}</td>
                      <td>{{ \App\Category::where('CatCd',$per->category)->first()->CatNm }}</td>
                      <td>{{ $per->caste}}</td>
                      <td>{{ $per->pwd }}</td>
                      @if($per->pwd=='Yes')
                      <td>{{ \App\Pwd::where('DisabilityTypeID',$per->ph_type)->first()->DisabilityTypeNameEng }}</td>
                      @else
                      <td>-</td>
                      @endif
                    </tr>
                    @endforeach -->
                 </table>
                        </td>
                        <td>
                         <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                            <tr>
                      <td class="lable-table">Gender</td>
                      <td>{{ $per->gender}}</td>
                    </tr>
                     <tr>
                      <td class="lable-table">Category</td>
                      <td>{{ \App\Category::where('CatCd',$per->category)->first()->CatNm }}</td>
                    </tr>
                     <tr>
                      <td class="lable-table">Caste</td>
                      <td>{{ $per->caste}}</td>
                    </tr>
                     <tr>
                      <td class="lable-table">PH</td>
                      <td>{{ $per->pwd }}</td>
                    </tr>
                     <tr>
                      <td class="lable-table">PH Type</td>
                       @if($per->pwd=='Yes')
                      <td>{{ \App\Pwd::where('DisabilityTypeID',$per->ph_type)->first()->DisabilityTypeNameEng }}</td>
                      @else
                       <td>-</td>
                        @endif
                    </tr>
                         </table>
                     
                       </td>
                     </tr>
                      @endforeach
                      <tr>
                       
                        <td colspan="2">
                           <h5 class="table-head-text" style="margin-top: 30px;">Address Information</h5>
                        </td>
                    </tr>
                    <tr>
                         <td colspan="2">
                            <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                   <thead>
                      <th class="t-center brd1px">Address</th>
                      <th class="t-center brd1px">Sub District</th>
                      <th class="t-center brd1px">District</th>
                      <th class="t-center brd1px">State</th>
                      <th class="t-center brd1px">Pincode</th>
                   </thead>
                    @foreach($address as $add)  
                    <tr>
                      <td class="t-center brd1px">{{ $add->address}},{{ $add->street }},{{ $add->landmark }}</td>
                      <td class="t-center brd1px">{{ \App\Subdistric::where('SubDistrictCode',$add->sub_district)->first()->SubDistrict }}</td>
                      <td class="t-center brd1px">{{ \App\District::where('DistrictCode',$add->district)->first()->DsName }}</td>
                      <td class="t-center brd1px">{{ \App\StateModel::where('StateId',$add->state)->first()->StateName }}</td>
                      <td class="t-center brd1px">{{ $add->pincode }}</td>
                    </tr>
                    @endforeach
                 </table>
                        </td>
                      
                     </tr>
                    
                    
                      <tr>
                        <td>
                           <h5 class="table-head-text" style="margin-top: 30px;">Course Information</h5>
                        </td>
                        
                     </tr>
                     <tr>
                        <td colspan="2">
                               <table class="inner-table" style="line-height: 30px;border: 1px solid #ddd;">
                   <thead>
                      <th class="t-center brd1px">Id</th>
                      <th class="t-center brd1px">College Name</th>
                      <th class="t-center brd1px">Course Name</th>
                      <th class="t-center brd1px">Form Type</th>
                      <th class="t-center brd1px">Amount</th>
                   </thead>
                   @php($count=0)
                    @foreach($courses as $cour)  
                    <tr>
                      <td class="t-center brd1px">{{++$count}}</td> 
                      <td class="t-center brd1px">{{ $cour->CgName }}</td>
                      <td class="t-center brd1px">{{ $cour->CoName }}</td>
                      <td class="t-center brd1px">{{ \App\Qualification::where('TypeId',$cour->st_type)->first()->Type }}</td>
                      <td class="t-center brd1px">{{ $cour->amount }}</td>
                    </tr>
                    @endforeach
                 </table>
                        </td>
                       
                     </tr>

                    

                  </table>
               </div>
            </div>
         </div>
      </div>
      <script src="{{ url('/') }}/js/jquery.min.js"></script>
      <script src="https://use.fontawesome.com/826a7e3dce.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
   </body>
</html>
