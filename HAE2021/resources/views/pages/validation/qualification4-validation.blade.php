<script>
   $(document).ready(function(){
    var form=$("#qualification4-info");
       $('#quatype-save4').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/qualification4-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.state4){
                         $( '#state4-error' ).html( "Please Enter State" );
                     }else{
                         $( '#state4-error' ).html( "");
                     }
                     if(data.errors.yearname4){
                         $( '#yearname4-error' ).html( "Please Enter Year" );
                     }else{
                         $( '#yearname4-error' ).html( "");
                     } 
                     if(data.errors.monthname4){
                         $( '#monthname4-error' ).html( "Please Enter Month" );
                     }else{
                         $( '#monthname4-error' ).html( "");
                     } 
                     if(data.errors.seatno4){
                         $( '#seatno4-error' ).html( "Please Select Seat No." );
                     }else{
                         $( '#seatno4-error' ).html( "");
                     } 
                     if(data.errors.centercode4){
                         $( '#centercode4-error' ).html( "Please Select Center Code" );
                     }else{
                         $( '#centercode4-error' ).html( "");
                     }                         
                      
                 }
                 if(data.success) {
                         $( '#state4-error' ).html( "");
                         $( '#yearname4-error' ).html( "");
                         $( '#monthname4-error' ).html( "");
                         $( '#seatno4-error' ).html( "");
                         $( '#centercode4-error' ).html( "");
                     $('#success-msg-qualification4').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-qualification4').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>