
<!-- validation personal info -->
<script>
   $(document).ready(function(){
    var form=$("#personnal-info");
       $('#personal-info-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           var url= $("#personnal-info").attr('action')
           alert(url);
           $.ajax({
               url: url,
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.stname){
                         $( '#stname-error' ).html( "Please Enter Name" );
                     }else{
                         $( '#stname-error' ).html( "");
                     }
                     
                     if(data.errors.stmobile){
                         $( '#stmobile-error' ).html( "Please Enter Mobile" );
                     }else{
                         $( '#stmobile-error' ).html( "");
                     } 
                     if(data.errors.stemail){
                         $( '#stemail-error' ).html( "Please Enter Email-ID" );
                     }else{
                         $( '#stemail-error' ).html( "");
                     }                          
                     if(data.errors.gender){
                         $( '#gender-error' ).html( "Please Select Gender" );
                     }else{
                         $( '#gender-error' ).html( "");
                     } 
                     if(data.errors.tab){
                         $( '#tab-error' ).html( "Please Select Physically Handicapped " );
                     }else{
                         $( '#tab-error' ).html( "");
                     } 
                     if(data.errors.birthdate){
                         $( '#birthdate-error' ).html( "Please Enter Date Of Birth" );
                     }else{
                         $( '#birthdate-error' ).html( "");
                     }
                 }
                 if(data.success) {
                         $( '#stname-error' ).html( "");
                         $( '#stmobile-error' ).html( "");
                         $( '#stemail-error' ).html( "");
                         $( '#gender-error' ).html( "");
                         $( '#tab-error' ).html( "");
                         $( '#birthdate-error' ).html( "");
                     $('#success-msg-st').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-st').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>
<!--  -->