<script>
   $(document).ready(function(){
    var form=$("#qualification3-info");
       $('#quatype-save3').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/qualification3-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.state3){
                         $( '#state3-error' ).html( "Please Enter State" );
                     }else{
                         $( '#state3-error' ).html( "");
                     }
                     if(data.errors.yearname3){
                         $( '#yearname3-error' ).html( "Please Enter Year" );
                     }else{
                         $( '#yearname3-error' ).html( "");
                     } 
                     if(data.errors.monthname3){
                         $( '#monthname3-error' ).html( "Please Enter Month" );
                     }else{
                         $( '#monthname3-error' ).html( "");
                     } 
                     if(data.errors.seatno3){
                         $( '#seatno3-error' ).html( "Please Select Seat No." );
                     }else{
                         $( '#seatno3-error' ).html( "");
                     } 
                     if(data.errors.centercode3){
                         $( '#centercode3-error' ).html( "Please Select Center Code" );
                     }else{
                         $( '#centercode3-error' ).html( "");
                     }                         
                      
                 }
                 if(data.success) {
                         $( '#state3-error' ).html( "");
                         $( '#yearname3-error' ).html( "");
                         $( '#monthname3-error' ).html( "");
                         $( '#seatno3-error' ).html( "");
                         $( '#centercode3-error' ).html( "");
                     $('#success-msg-qualification3').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-qualification3').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>