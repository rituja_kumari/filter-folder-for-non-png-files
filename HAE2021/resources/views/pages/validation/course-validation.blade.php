<script>
   $(document).ready(function(){
    var form=$("#stcourse-info");
       $('#stcourse-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
            var url= $("#stcourse-info").attr('action');
            alert();
           $.ajax({
               url: url,
               method: 'post',
               data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                  if(data.errors.centername){
                         $( '#centername-error' ).html( "Please Select Center/College Name" );
                     }else{
                         $( '#centername-error' ).html( "");
                     }
                     if(data.errors.stcrstype){
                         $( '#stcrstype-error' ).html( "Please Select Course" );
                     }else{
                         $( '#stcrstype-error' ).html( "");
                     }
                     if(data.errors.sttype){
                         $( '#sttype-error' ).html( "Please Select Student Type" );
                     }else{
                         $( '#sttype-error' ).html( "");
                     } 
                                              
                      
                 }
                 if(data.success) {
                        $( '#centername-error' ).html( "");
                         $( '#stcrstype-error' ).html( "");
                         $( '#sttype-error' ).html( "");
                     $('#success-msg-stcourse').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-stcourse').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>