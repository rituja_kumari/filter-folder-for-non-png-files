<script>
   $(document).ready(function(){
    var form=$("#stphoto-info");
       $('#stphoto-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/stphoto-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.photo){
                         $( '#photo-error' ).html( "Please Select Photo" );
                     }else{
                         $( '#photo-error' ).html( "");
                     }
                     if(data.errors.sign){
                         $( '#sign-error' ).html( "Please Select Sign " );
                     }else{
                         $( '#sign-error' ).html( "");
                     } 
                                              
                      
                 }
                 if(data.success) {
                         $( '#photo-error' ).html( "");
                         $( '#sign-error' ).html( "");
                     $('#success-msg-stphoto').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-stphoto').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>