<script>
    $(document).ready(function(){
          var form=$("#qt-paper-upload");
             $('#qtsubmit').click(function(e){
                 e.preventDefault();
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                 });
                 $.ajax({
                     url: "{{ url('/qt-paper-upload-url') }}",
         
                     method: 'post',
                      data:form.serialize(),
                     success: function(data){
                      console.log(data)
                       if(data.errors) {
                           if(data.errors.qtsession){
                               $( '#qtsession-error' ).html( "Please Select Session" );
                           }else{
                               $( '#qtsession-error' ).html( "");
                           }
                           if(data.errors.qtfaculty){
                               $( '#qtfaculty-error' ).html( "Please Select Faculty" );
                           }else{
                               $( '#qtfaculty-error' ).html( "");
                           } 
                           if(data.errors.qtcourse){
                               $( '#qtcourse-error' ).html( "Please Select Course" );
                           }else{
                               $( '#qtcourse-error' ).html( "");
                           }
                           if(data.errors.qtbranch){
                               $( '#qtbranch-error' ).html( "Please Select Branch" );
                           }else{
                               $( '#qtbranch-error' ).html( "");
                           }
                           if(data.errors.qtsemester){
                               $( '#qtsemester-error' ).html( "Please Select Semester" );
                           }else{
                               $( '#qtsemester-error' ).html( "");
                           }
                           if(data.errors.qtsubject){
                               $( '#qtsubject-error' ).html( "Please Select Subject" );
                           }else{
                               $( '#qtsubject-error' ).html( "");
                           }
                           if(data.errors.qtpaper){
                               $( '#qtpaper-error' ).html( "Please Upload Question Paper" );
                           }else{
                               $( '#qtpaper-error' ).html( "");
                           }                          
                           if(data.errors.qtlivedate){
                               $( '#qtlivedate-error' ).html( "Please Select Live Date" );
                           }else{
                               $( '#qtlivedate-error' ).html( "");
                           }
                           if(data.errors.qtlivetime){
                               $( '#qtlivetime-error' ).html( "Please Enter Live Time From" );
                           }else{
                               $( '#qtlivetime-error' ).html( "");
                           }
                           if(data.errors.qttimetill){
                               $( '#qttimetill-error' ).html( "Please Enter Live Time Till" );
                           }else{
                               $( '#qttimetill-error' ).html( "");
                           }
                           if(data.errors.qtpass){
                               $( '#qtpass-error' ).html( "Please Enter Password" );
                           }else{
                               $( '#qtpass-error' ).html( "");
                           }
                           if(data.errors.qtconfirmpass){
                               $( '#qtconfirmpass-error' ).html( "Please Re- Enter Password" );
                           }else{
                               $( '#qtconfirmpass-error' ).html( "");
                           }
                       }
                       if(data.success) {
                               $( '#qtsession-error' ).html( "");
                               $( '#qtfaculty-error' ).html( "");
                               $( '#qtcourse-error' ).html( "");
                               $( '#qtbranch-error' ).html( "");
                               $( '#qtsemester-error' ).html( "");
                               $( '#qtsubject-error' ).html( "");
                               $( '#qtpaper-error' ).html( "");
                               $( '#qtlivedate-error' ).html( "");
                               $( '#qtlivetime-error' ).html( "");
                               $( '#qttimetill-error' ).html( "");
                               $( '#qtpass-error' ).html( "");
                               $( '#qtconfirmpass-error' ).html( "");
                           $('#success-msg-qt').removeClass('hide');
                           setInterval(function(){ 
                               $('#success-msg-qt').addClass('hide');
                           }, 3000);
                       }
                     }
                 });
             });
         });
</script>