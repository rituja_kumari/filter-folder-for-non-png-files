<script>
   $(document).ready(function(){
      getdropdown('stdistrictname','subdist','subdist');
    var form=$("#qualification1-info");
       $('#quatype-save1').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/qualification1-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.state1){
                         $( '#state1-error' ).html( "Please Enter State" );
                     }else{
                         $( '#state1-error' ).html( "");
                     }
                     if(data.errors.yearname1){
                         $( '#yearname1-error' ).html( "Please Enter Year" );
                     }else{
                         $( '#yearname1-error' ).html( "");
                     } 
                     if(data.errors.monthname1){
                         $( '#monthname1-error' ).html( "Please Enter Month" );
                     }else{
                         $( '#monthname1-error' ).html( "");
                     } 
                     if(data.errors.seatno1){
                         $( '#seatno1-error' ).html( "Please Select Seat No." );
                     }else{
                         $( '#seatno1-error' ).html( "");
                     } 
                     if(data.errors.centercode1){
                         $( '#centercode1-error' ).html( "Please Select Center Code" );
                     }else{
                         $( '#centercode1-error' ).html( "");
                     }                         
                      
                 }
                 if(data.success) {
                         $( '#state1-error' ).html( "");
                         $( '#yearname1-error' ).html( "");
                         $( '#monthname1-error' ).html( "");
                         $( '#seatno1-error' ).html( "");
                         $( '#centercode1-error' ).html( "");
                     $('#success-msg-qualification1').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-qualification1').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>