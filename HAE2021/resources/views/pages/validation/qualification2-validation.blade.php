<script>
   $(document).ready(function(){
    var form=$("#qualification2-info");
       $('#quatype-save2').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           $.ajax({
               url: "{{ url('/qualification2-url') }}",
   
               method: 'post',
                data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.state2){
                         $( '#state2-error' ).html( "Please Enter State" );
                     }else{
                         $( '#state2-error' ).html( "");
                     }
                     if(data.errors.yearname2){
                         $( '#yearname2-error' ).html( "Please Enter Year" );
                     }else{
                         $( '#yearname2-error' ).html( "");
                     } 
                     if(data.errors.monthname2){
                         $( '#monthname2-error' ).html( "Please Enter Month" );
                     }else{
                         $( '#monthname2-error' ).html( "");
                     } 
                     if(data.errors.seatno2){
                         $( '#seatno2-error' ).html( "Please Select Seat No." );
                     }else{
                         $( '#seatno2-error' ).html( "");
                     } 
                     if(data.errors.centercode2){
                         $( '#centercode2-error' ).html( "Please Select Center Code" );
                     }else{
                         $( '#centercode2-error' ).html( "");
                     }                         
                      
                 }
                 if(data.success) {
                         $( '#state2-error' ).html( "");
                         $( '#yearname2-error' ).html( "");
                         $( '#monthname2-error' ).html( "");
                         $( '#seatno2-error' ).html( "");
                         $( '#centercode2-error' ).html( "");
                     $('#success-msg-qualification2').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-qualification2').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>