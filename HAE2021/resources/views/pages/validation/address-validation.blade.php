<script>
   $(document).ready(function(){
    var form=$("#stadress-info");
       $('#address-info-save').click(function(e){
           e.preventDefault();
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });
           var url= $("#stadress-info").attr('action')
         alert(url);
           $.ajax({
               url: url,
               method: 'post',
               data:form.serialize(),
               success: function(data){
                console.log(data)
                 if(data.errors) {
                     if(data.errors.staddress){
                         $( '#staddress-error' ).html( "Please Enter Address" );
                     }else{
                         $( '#staddress-error' ).html( "");
                     }
                     if(data.errors.stcityname){
                         $( '#stcityname-error' ).html( "Please Enter City Name" );
                     }else{
                         $( '#stcityname-error' ).html( "");
                     } 
                     if(data.errors.stdistrictname){
                         $( '#stdistrictname-error' ).html( "Please Select District Name" );
                     }else{
                         $( '#stdistrictname-error' ).html( "");
                     } 
                     if(data.errors.ststate){
                         $( '#ststate-error' ).html( "Please Enter Street Name" );
                     }else{
                         $( '#ststate-error' ).html( "");
                     } 
                     if(data.errors.stcounrty){
                         $( '#stcounrty-error' ).html( "Please Enter Landmark" );
                     }else{
                         $( '#stcounrty-error' ).html( "");
                     } 
                     if(data.errors.postalcode){
                         $( '#postalcode-error' ).html( "Please Enter Postal Code" );
                     }else{
                         $( '#postalcode-error' ).html( "");
                     }                          
                      
                 }
                 if(data.success) {
                         $( '#staddress-error' ).html( "");
                         $( '#stcityname-error' ).html( "");
                         $( '#stdistrictname-error' ).html( "");
                         $( '#ststate-error' ).html( "");
                         $( '#stcounrty-error' ).html( "");
                         $( '#postalcode-error' ).html( "");
                     $('#success-msg-address').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-msg-address').addClass('hide');
                     }, 3000);
                 }
               }
           });
       });
   });
</script>