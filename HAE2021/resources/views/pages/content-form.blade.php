@extends('forms')
@section('content-form')
@php
$StuId=session('studata')[0]['StuId'];
$personal = \App\Personal::get()->where('StuId',$StuId);
foreach($personal as $key=>$per)
{
   $per_id=$per->id;
   $gender=$per->gender;
   $pwd_type=$per->pwd;
   $ph_type=$per->ph_type;
   $birthdate=$per->birthdate;
   $cate_per=$per->category;
   $caste=$per->caste;
}

$address = \App\Address::get()->where('StuId',$StuId);
foreach($address as $key=>$add_per)
{
   $add_per_id=$add_per->id;
   $add_per_add=$add_per->address;
   $add_street=$add_per->street;
   $add_landmark=$add_per->landmark;
   $add_state=$add_per->state;
   $add_district=$add_per->district;
   $add_sub_district=$add_per->sub_district;
   $add_pincode=$add_per->pincode;
}
$course_info = \App\CourseInfo::get()->where('StuId',$StuId);
$st_lock_status = \App\Status::where('StuId',$StuId)->first()->st_lock_status;
@endphp
<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span> Student Registration 
   </h3>
   <!--    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
         <li class="breadcrumb-item active" aria-current="page">
            <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
         </li>
      </ul>
      </nav> -->
</div>
<style type="text/css">
   section .row{margin: 0;width: 100%;}
   .form-group{margin-bottom: 0;}
   section h5{font-size: 14px;
   margin-bottom: 16px;
   text-transform: uppercase;
   font-weight: 500;
   color: #000;}
   .fees-block{
   background: #fff;
   padding: 10px;
   font-size: 16px;
   font-weight: 600;
   }
   .mb-10{margin-bottom: 10px;}
   .checkbox{float: left;
   width: 25px;
   height: 25px;
   margin-right: 10px;}
   [role=menu] li:last-child {
    display: none!important;
} 

</style>
<!--vertical wizard-->
<div class="row">
   <div class="col-12 grid-margin">
      <div class="card">
         <div class="card-body">
            <h4 class="card-title">Student Information Form <i class="fa fa-info"></i> </h4>
            <div id="example-vertical-wizard">
             
               @if($st_lock_status == "No")
              <div>              
                @if(isset($per_id))
                 @include('pages.profile-info-update')
                @else
                 @include('pages.profile-info')
                @endif
                <!-- include file -->

                @if(isset($add_per_id))
                  @include('pages.communication-address-update')
                @else
                @include('pages.communication-address')
                @endif
                  
                   @include('pages.college-center')
                   <!-- @include('pages.courses') -->
                  @include('pages.photo-sign')
                   @include('pages.qualification')
                    @include('pages.printstudent')  
                  <!-- @include('pages.payment') -->
               </div>
                @else
                <div>
                   @include('pages.printstudent')
                   @include('pages.payment')
                </div>
               @endif
            </div>
         </div>
      </div>
   </div>
   <!-- content-wrapper ends -->
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<!-- main-panel ends -->
<script type="text/javascript">
   function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
   
          reader.onload = function (e) {
              $('#blah')
                  .attr('src', e.target.result)
                  .width(150)
                  .height(200);
          };
   
          reader.readAsDataURL(input.files[0]);
      }
   }
</script>
<script type="text/javascript">
   function readsign(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
   
          reader.onload = function (e) {
              $('#blah2')
                  .attr('src', e.target.result)
                  .width(150)
                  .height(200);
          };
   
          reader.readAsDataURL(input.files[0]);
      }
   }
</script>
<!-- main-panel ends -->
<script type="text/javascript">
   $(function() {
   $('#selectcourse').change(function(){
   $('#content-selection').show();
   // $('#course-selection' + $(this).val()).show();
   });
   });
</script>


<script type="text/javascript">
   $(function() {
   $('#qutype').change(function(){
    $('.qatypes').hide();
    $('#' + $(this).val()).show();
   });
   });
</script>
<script type="text/javascript">
   // $(function() {
   // $('#sttype').change(function(){
   //  $('.stcontents').hide();
   //  $('#' + $(this).val()).show();
   // });
   // });
</script>
<script type="text/javascript">
   function show1(){
   document.getElementById('div1').style.display ='none';
   }
   function show2(){
   document.getElementById('div1').style.display = 'block';
   }

   function getdropdown(get_val_id,concat_id,url)
   {
    
      var id = $('#'+get_val_id).val(); 
        if(id){
          $.ajax({
            type:"GET",
            url:url+"?id="+id,
            success:function(res){        
            if(res){
               console.log(res);
              $("#"+concat_id).empty();
       $("#"+concat_id).append('<option>Select</option>');
              $.each(res,function(key,value){
                $("#"+concat_id).append('<option value="'+key+'">'+value+'</option>');
              });
            
            }else{
              $("#"+concat_id).empty();
            }
            }
          });
        }else{
          $("#"+concat_id).empty();
        }
   }
</script>
@stop