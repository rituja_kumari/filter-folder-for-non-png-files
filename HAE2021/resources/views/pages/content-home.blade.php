@extends('admin')
@section('content')
<style type="text/css">
   
         label{font-weight: 100;}
         .table-head-text{    font-size: 13px;
         text-transform: uppercase;
         font-weight: 700;}
         .lable-table{font-weight: 600;
         text-transform: uppercase;
         font-size: 12px;
         color: #000;}
         td{color: #888;padding-left: 10px;}
         .clg-name{font-size: 16px;
         text-transform: uppercase;
         color: #c67413;
         font-weight: 600;}
         /*.table-details{background: #fdfdfd;margin-top: 16px;padding: 20px 15px;}*/
         /*.inner-table tr{border: 1px solid #ddd;}*/
         .inner-table td {padding: 4px 4px;}
         .td-width td{width: 50%;}
         .brd1px{border: 1px solid #ddd;}
         table{width: 100%;}
</style>

<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span> Profile
   </h3>
 <!--   <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
         <li class="breadcrumb-item active" aria-current="page">
            <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
         </li>
      </ul>
   </nav> -->
</div>
@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

    <button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif

@if (count($errors) > 0)

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif
<div class="row" >

   <div class="col-lg-4 col-md-4">
      <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#">
                    <img class="img" src="{{ url('/') }}/img/university.png">
                  </a>
                </div>
                <div class="card-body">
                  <h6 class="card-category text-gray">    {{session('data')['0']['CgName']}}   ({{session('data')['0']['CgCode']}})</h6>
                  <h4 class="card-title">Maharashtra,  {{session('Dist')['0']['DsName']}}</h4>
                  <p class="card-description">
                   Kindly print the center profile and get the signature of the principle and college stamp. Then scan and re-upload on the given portel.
                  </p>
                   <iframe src="printpage" name="frame" class="hide" style="display: none;"></iframe>
                  <input type="button" onclick="frames['frame'].print()" value="Print" class="print-btn ml-auto">
                </div>
              </div>
<div class="mt-59">
              <div class="card ">
                <div class="card-header browse-file">
                  <h4 class="card-title mb-0">Browse and upload your file here</h4>
                  
                </div>
               <div class="card-body">
                 <div class="form-group">
         <form class="mt-20" action="{{ route('file.upload.post') }}" method="POST" enctype="multipart/form-data">
      @csrf
               
               <input type="file" name="file" class="form-control1">
               <input type="submit" class="print-btn ml-auto" value="submit" name="" style="display: none;"><br>
               @if(session('data')['0']['CsAuthFile']!='') <span style="color:green;"> file Already uploaded..!</span>&nbsp;&nbsp;&nbsp;
               <span><a target='_blank'  href='public/uploads/{{session('data')['0']['CsAuthFile']}}'>view</a></span>
               @endif
         </form>
      </div>

   </div>
              </div>
           </div>
   </div>
   <div class="col-lg-8 col-md-8">
      <div class="card mt-30">
         <div class="card-header card-header-primary">
                  <h4 class="card-title mb-0 text-white">PRINCIPAL DETAILS</h4>
                  
                </div>

                 <div class="card-body">
                  <table>
                     <tr>
                        <td colspan="2">
                           <table class="inner-table td-width" style="line-height: 30px;">
                              <tbody>
                                   <tr>
                                 <td class="lable-table">Principal Full Name:</td>
                                 <td> {{session('data')['0']['CfCgPrinName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Mobile No:</td>
                                 <td> {{session('data')['0']['CfCgPrinContact']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Office LandLine No:</td>
                                 <td>{{session('data')['0']['CgLandLineNo']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Email Id:</td>
                                 <td>{{session('data')['0']['CfCgPrinEmail']}}</td>
                              </tr>
                           </tbody></table>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2">
                           <table class="inner-table td-width" style="line-height: 30px;">
                              <tbody>  <tr>
                                 <td class="lable-table">Bank Name:</td>
                                 <td>{{session('data')['0']['BankName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Bank Branch Name:</td>
                                 <td> {{session('data')['0']['BankBranchName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Account No:</td>
                                 <td> {{session('data')['0']['BankAcNo']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">IFSC Code:</td>
                                 <td>{{session('data')['0']['BankIFSCNo']}}</td>
                              </tr>
                           </tbody></table>
                        </td>
                     </tr>
                  </table>
                </div>
      </div>
       <div class="card mt-30">
          <div class="card-header card-header-primary">
                  <h4 class="card-title mb-0 text-white">COORDINATOR DETAILS</h4>
                  
                </div>
                <div class="card-body">
                   <table>
                      <tr>
                        <td>
                           <table class="inner-table td-width" style="line-height: 30px;">
                              <tbody>
                                <tr>
                                 <td class="lable-table">Full Name:</td>
                                 <td>{{session('data')['0']['CoFName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Designation:</td>
                                 <td> {{session('data')['0']['CoDesigNation']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Mobile No:</td>
                                 <td> {{session('data')['0']['CoFMobile']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Email Id:</td>
                                 <td>{{session('data')['0']['CoFEmail']}}</td>
                              </tr>
                           </tbody></table>
                        </td>
                       
                     </tr>
                   </table>
                </div>
      </div>
   </div>
   <div class="col-md-12 col-lg-12 mt-30">
       <div class="card mt-30">
         <div class="card-header card-header-primary">
                  <h4 class="card-title mb-0 text-white">COLLEGE ADDRESS DETAILS</h4>
                  
                </div>
           

                  <div class="card-body">
                <table class="table-responsive">
                   <tr>
                         <td>
                           <table class="inner-table td-width" style="line-height: 30px;">
                              <tbody>
                                 <tr>
                                 <td class="lable-table"> Flat/Room/Block/House No:</td>
                                 <td>{{session('data')['0']['CfAdd1']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Name of Premises/ Building:</td>
                                 <td> {{session('data')['0']['CfAdd2']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table"> Road/ Street/Post Office:</td>
                                 <td> {{session('data')['0']['CfAdd3']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table"> Area Locality:</td>
                                 <td>{{session('data')['0']['CfAdd4']}}</td>
                              </tr>
                           </tbody></table>
                        </td>
                        <td>
                           <table class="inner-table td-width" style="line-height: 30px;">
                              <tbody>
                                 <tr>
                                 <td class="lable-table">Landmark:</td>
                                 <td>{{session('data')['0']['CfAdd5']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">Taluka:</td>
                                 <td> {{session('data')['0']['Taluka']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table"> Village/City:</td>
                                 <td> {{session('data')['0']['TnName']}}</td>
                              </tr>
                              <tr>
                                 <td class="lable-table">PinCode:</td>
                                 <td>{{session('data')['0']['CfPinCod']}}</td>
                              </tr>
                           </tbody></table>
                        </td>
                     </tr>
                </table>
             </div>
          </div>
   </div>

  <div class="col-md-12 col-lg-12 mt-30">
       <div class="card mt-30">
         <div class="card-header card-header-primary">
                  <h4 class="card-title mb-0 text-white">COURSES</h4>
                  
                </div>
           

                  <div class="card-body">
                     <table class="table-responsive">
                        <tr>
                      <td colspan="2">
                         <table class="inner-table t-center" style="line-height: 30px;border: 1px solid #ddd;text-align: 
                         center;">
                          
                          <tbody><tr>
                                <th class="t-center brd1px lable-table" align="left" rowspan="2">Course Name</th>
                                <th class="t-center brd1px lable-table" colspan="3">Intake</th>
                                <th class="t-center brd1px lable-table" colspan="3">Admitted Students</th>
                                
                              </tr>
                            <tr>
                                <th class="t-center brd1px lable-table">Govt.</th>
                                <th class="t-center brd1px lable-table">Aided</th>
                                <th class="t-center brd1px lable-table">Unaided</th>
                                <th class="t-center brd1px lable-table">Govt.</th>
                                <th class="t-center brd1px lable-table">Aided</th>
                                <th class="t-center brd1px lable-table">Unaided</th>
                              </tr>
                           @if(Session::has('CourseWiseIntake'))
                              @foreach(Session('CourseWiseIntake') as $x => $test)
                              <tr>
                              
                                <td class="brd1px" align="left">{{$test->CfCoId}} - {{Session('CoName')[$x]['CoName']}}</td>
                                <td class="brd1px">{{$test->InGovt}}</td>
                                <td class="brd1px">{{$test->InAided}}</td>
                                <td class="brd1px">{{$test->InUnAided}}</td>
                                <td class="brd1px">{{$test->AdGovt}}</td>
                                <td class="brd1px">{{$test->AdAided}}</td>
                                <td class="brd1px">{{$test->AdUnAided}}</td>
                                
                              </tr>
                              @endforeach
                           @endif 
                              </tbody></table>
                      </td>
                     </tr>
                     </table>
                  </div>
</div>
</div>
<!-- 
   <div class="col-12">
      <span class="d-flex align-items-center purchase-popup">
      <p class="mb-0">Center Profile Details </p>
      <iframe src="printpage" name="frame" class="hide" style="display: none;"></iframe>
      <input type="button" onclick="frames['frame'].print()" value="Print" class="print-btn ml-auto">
      </span>
   </div> -->
   <div class="col-lg-12 col-md-12 col-12 mt-30">
      
               
   </div>
</div>



@stop
