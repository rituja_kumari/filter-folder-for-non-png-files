@extends('StudentCount')
@section('content-StuCountRpt')

<style type="text/css">
  .reset-ps{background: #fff;
   padding: 30px;}
   .mb-10{margin-bottom: 10px;}
   @media(max-width:768px)
   {
       .reset-ps{
   padding: 10px;}
   }
   .body_overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: fixed;left: 0;top: 0;width: 100%;height: 100%;width: 100%;height: 100%;display: none;}
	.body_overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<script src="{{ url('/')}}/js/ajax_js.js" ></script>
<script>
   $(function() {
      $(".datepicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
});
</script>

<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span>Higher Art Examination - 2021
   </h3>
</div>
<div class="row" >
<div class="body_overlay"><div><img src="{{ url('/') }}/img/loading11.gif" width="80px" height="80px"/></div></div>
   <div class="col-12">
   <div class="reset-ps">
      <div class="reset-ps">
         <form id="MarksEntry" method="POST" >
            <div class="row">
          
            <div class=" col-lg-4 col-md-4" id="DivExam">
               @php
               $Excondition[]=array("ColumnName"=>"EntryStat","Operator"=>"=","ColumnValue"=>'1');
                  $Enccondition=json_encode($Excondition);
                  $exam =sel_exam($Enccondition,'','','','form-control1','',"ajax_combo3('ShowReportListExamWise/','','#subjectInfo','MarksEntry')",'1');  
               @endphp
            </div>
            <div class=" col-lg-4 col-md-4" id="DownloadBtn" style="margin-top:30px;">
              
               <a href="{{ url('export/') }}"  class="button-web mt-20" target="_BLANK">Download</a>
            </div>
            <div class="col-12 col-lg-12 col-md-12" id="subjectInfo"> 
               @include('StudentCountTbl') 
            </div>
            <div id="res"></div>
            <div class="reset-ps mt-30"></div>
         </form>
      </div>
   </div>
</div>
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<!-- reset password  -->
@stop