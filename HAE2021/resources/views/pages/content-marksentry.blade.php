@extends('marks-entry')
@section('content-marksentry')

<style type="text/css">
  .reset-ps{background: #fff;
   padding: 30px;}
   .mb-10{margin-bottom: 10px;}
   @media(max-width:768px)
   {
       .reset-ps{
   padding: 10px;}
   }
   .body_overlay {background-color: rgba(0, 0, 0, 0.6);z-index: 999;position: fixed;left: 0;top: 0;width: 100%;height: 100%;width: 100%;height: 100%;display: none;}
	.body_overlay div {position:absolute;left:50%;top:50%;margin-top:-32px;margin-left:-32px;}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<script src="{{ url('/')}}/js/ajax_js.js" ></script>
<script>
   $(function() {
      $(".datepicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
});
</script>

<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span>Higher Art Examination - 2021
   </h3>
</div>
<div class="row" >
<div class="body_overlay"><div><img src="{{ url('/') }}/img/loading11.gif" width="80px" height="80px"/></div></div>
   <div class="col-12">
   <div class="reset-ps">
      <div class="reset-ps">
         <form id="MarksEntry" method="POST" >
            <div class="row">
               <div class=" col-lg-4 col-md-4">
                  @php 
                     $condition[]=array("ColumnName"=>"CfFcId","Operator"=>"=","ColumnValue"=>'1');
                     $Enccondition=json_encode($condition);
                     $CourseDetails=sel_course($Enccondition,'','','','form-control1','','',"ajax_combo3('ShowBranch/','','#DivBranch','MarksEntry')",'1');  @endphp
               </div>
               <div class="col-lg-4 col-md-4" id="DivBranch">
                  @php
                     if(empty($Brcondition))
                     {
                        $Brcondition[]=array("ColumnName"=>"CfCoId","Operator"=>"=","ColumnValue"=>'');
                     }
                     $Enccondition=json_encode($Brcondition);
                     $branch=sel_branch($Enccondition,'','','','form-control1','',"ajax_combo3('ShowExam/','','#DivExam','MarksEntry')",'1'); 
                  @endphp 
               </div>
               <div class=" col-lg-4 col-md-4" id="DivExam">
                     @php
                     $Excondition[]=array("ColumnName"=>"CfCoId","Operator"=>"=","ColumnValue"=>'');
                     $Excondition[]=array("ColumnName"=>"CfBrId","Operator"=>"=","ColumnValue"=>'');
                        $Enccondition=json_encode($Excondition);
                        $exam =sel_exam($Enccondition,'','','','form-control1','',"ajax_combo3('ShowSubject/','','#DivSubject','MarksEntry')",'1');  
                     @endphp
               </div>
               @if(session('data')['0']['role']!='U')
               <div class=" col-lg-3 col-md-3">
                  <label>Roll No.</label><span style="color:red">*</span>
                  <input type="text" name="EfRollNo" id="EfRollNo"  maxlength="3" class="form-control1" placeholder="Enter Roll Number" value="" >
               </div>
               <div class=" col-lg-5 col-md-5">
                  <label>Student Name</label><span style="color:red">*</span>
                  <input type="text" name="FullName" id="FullName" class="form-control1" placeholder="Surname First Name Father/husband Name" onkeyup="this.value = this.value.toUpperCase();">
               </div>
               <div class=" col-lg-2 col-md-2">
                  <label>Category</label><span style="color:red">*</span>
                  <select class="form-control1" name="EfCate" id="EfCate" onchange="ajax_combo3('ShowRollName/','','#subjectInfo','MarksEntry')">
                     <option value="">Select</option>
                     <option value="R">Regular</option>
                     <option value="X">Repeater</option>
                     <option value="ATKT">ATKT</option>
                     <option value="VAC">Vacation</option>
                  </select>
                  <!-- </div><input type="text" class="datepicker" /> -->
                  <!-- <div class="col-lg-2 col-md-2">
                     <input type="button" class="btn-admin mt-30 IsSubmit_disabled" type="button" id="AddUpdateBtn" value="Add" onclick="ajax_combo3('AddStudentInfo/','','#res','MarksEntry')">
                  </div> -->
               </div>
               @endif
            <div class="col-12 col-lg-12 col-md-12" id="subjectInfo"> </div>
           
            <div id="res"></div>
            <div class="reset-ps mt-30"></div>
         </form>
      </div>
     
   </div>
</div>

<script src="{{ url('/') }}/js/jquery.min.js"></script>

<!-- reset password  -->
@stop