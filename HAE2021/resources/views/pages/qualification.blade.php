<h3><i class="fa fa-rupee"></i> Payment</h3>
   <section>
      <form id="finalsubmission"  method="post" action="{{route('confirm_status')}}" >
      @csrf
         <div class="row">
            <input type="hidden" name="hidden_id" value="{{ session('studata')[0]['StuId'] }}">
               <div class="col-12 col-lg-12 col-md-12">
               <input type="checkbox" class="checkbox" name="st_lock_status" value="Yes">  
               <p>I <b> {{ \App\StudLogin::where('StuId',session('studata')[0]['StuId'])->first()->stname }} </b> hereby declare that the information furnished above is true, complete and correct to the best of my knowledge and belief.</p>
               </div>
               <div>
                 <a href="{{route('app_print')}}" target="_blank" style="padding-left: 20px;">Check Your Details click here</a>
               </div>
               <hr>
               <div class="form-group col-12 col-md-12 col-lg-12">
                  <button type="button" class="btn-admin" id="btnconfirm"> Confirm</button>
               </div>
         </div>
      </form>
   </section>
   <script type="text/javascript">
      $("#btnconfirm").click(function (e) {
                if (confirm("Are you sure?")) {
                    var url= $("#finalsubmission").attr('action');
                    e.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                     var form=$("#finalsubmission");
                    $.ajax({
                        type:"post",
                        url:url,
                        data:form.serialize(),
                        success:function(success){        
                        if(success){
                           alert('Your Profile Lock');
                          location.reload();
                           
                        }else{
                          // $("#"+concat_id).empty();
                        }
                        }
                      });
                } else {
                    console.log('User clicked no.');
                }
            });
   </script>
