@extends('admin')
@section('content-adminpage')
  <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Dashboard
              </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="row">
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-danger card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{ url('/') }}/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3">Total Center Registered <i class="mdi mdi-home-variant mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{sizeof(session('RegCollege'))}}</h2>
                    
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-info card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{ url('/') }}/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3">Pass Student Persent <i class="mdi  mdi-account-multiple mdi-24px float-right"></i>
                    </h4>
                    @php
                      $Percentage=session('StuCountPass')/session('StudentCount')*100;
                    @endphp
                    <h6 class="card-text">{{$Percentage}}</h6>
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-info card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{ url('/') }}/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3">Total Student Registration <i class="mdi  mdi-account-multiple mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{session('StudentCount')}}</h2>
                    
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-success card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{ url('/') }}/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3">Total Pass Student <i class="mdi mdi-clipboard mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{session('StuCountPass')}}</h2>
                    
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-success card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{ url('/') }}/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3">Total Fail Student <i class="mdi mdi-clipboard mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{session('StuCountFail')}}</h2>
                    
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-success card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{ url('/') }}/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3">Total Absent Student <i class="mdi mdi-clipboard mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{session('StuCountAbsent')}}</h2>
                    
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-success card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{ url('/') }}/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3">Total ATKT Student <i class="mdi mdi-clipboard mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{session('StuCountATKT')}}</h2>
                    
                  </div>
                </div>
              </div>
            </div>
          
            @stop