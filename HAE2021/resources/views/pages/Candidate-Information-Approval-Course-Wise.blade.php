@extends('course-wise')
@section('course-wise')
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ url('/') }}/js/jquery.min.js"></script>
<script src="{{ url('/')}}/js/ajax_js.js" ></script>

<style type="text/css">   .form-control2{border: 0;
    border-bottom: 1px solid #888;
    background: transparent;
    padding: 6px;}
    .mb-30{margin-bottom: 30px;}
  .t-center{text-align: center;}</style>
<div class="page-header">
   <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
      </span> Student Information
   </h3>
 <!--   <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
         <li class="breadcrumb-item active" aria-current="page">
            <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
         </li>
      </ul>
   </nav> -->
</div>
<!-- <div class="t-center mb-30"> -->
<div class="col-lg-12 col-md-12 col-12 ">
  <form method="POST"  name="StudApproval" id="StudApproval" >
  <div class="row">
  <div class="col-md-12"><span id="success_msg_course"></span></div>
      <div class="col-md-3">
         <select class="form-control2" name="CfCoId" id="CfCoId" onchange="ajax_combo3('ShowStudentType/','#CfCoId','#StuTypDiv','StudApproval')" style="width:190px;">
         <option value="">Select Course</option>
         @foreach(Session('CourseWiseIntake') as $x => $test)
            <option value="{{$test->CfCoId}}">{{$test->CfCoId}} - {{Session('CoName')[$x]['CoName']}}</option>
         @endforeach  
         </select>
      </div>
    
      <div id="StuTypDiv" class="col-md-2">
         <select class="form-control2" id="StudType" name="StudType" >
            <option >Student Type</option>
         </select>
      </div> 
      <div id="ActionStatusDiv" class="col-md-2">
         <select class="form-control2" name="ActionStatus" id="ActionStatus" onchange="ajax_combo3('ShowTbl/','','#res','StudApproval')">
               <option value="">Action</option>
               <option value="0">Pending</option>
               <option value="1">Approval</option>
               <option value="2">Reject</option>
               <option value="3">Rejected</option>
         </select>
      </div>
      <div class="col-md-2">
         <button type="button" class="btn-admin" id="personal-info-save" onclick="ajax_combo3('SubmitApproval/','#personal-info-save','#res','StudApproval')"> save</button> 
      </div>
       <div class="col-md-2">
         <a class="btn-admin" href="{{route()}}"> PDF</a>
      </div>
      
   </div>
   <div  id="res"> <script>//ajax_combo3('indexlistview/','','#res','StudApproval');</script> </div>
  </form>
</div>

@stop
