<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

   <head>
      <meta charset="utf-8">
         <meta name="_token" content="{{csrf_token()}}" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{ url('/') }}/assets/fonts/materialdesignicons.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/css/vendor.bundle.base.css">
      <!-- endinject -->
      <!-- Plugin css for this page -->
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/css/style.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/fonts/fontawesome.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/select2/select2.min.css">
      <link rel="stylesheet" href="{{ url('/') }}/assets/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
      <!-- Styles -->
        <style type="text/css">
         .main-panel{width: 100%;}
         .navbar .navbar-brand-wrapper .navbar-brand img{float: right;}
         .navbar .navbar-brand-wrapper{background: transparent;}
         .navbar .navbar-menu-wrapper .navbar-toggler{display: none;}
      </style>
   </head>
   <body class="antialiased">
      <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
         <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
               <div class="grid grid-cols-1 md:grid-cols-2">
                  <div class="login-13 tab-box">
                     <div class="container-scroller">
                        @include('include.header')
                       
                        <!-- partial -->
                        <div class="container-fluid page-body-wrapper">
                           <!-- partial -->
                           <!-- partial:partials/_sidebar.html -->
                           <!-- @include('include.sidebar-2') -->
                           <!-- partial -->
                           <div class="main-panel">
                              <div class="content-wrapper-2">
                                   @yield('content-form')
                                    @include('include.footer')
                              </div>
                             
                              <!-- partial -->
                           </div>
                           <!-- main-panel ends -->
                        </div>
                        <!-- page-body-wrapper ends -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="{{ url('/') }}/assets/vendors/js/vendor.bundle.base.js"></script>
      
      <script src="{{ url('/') }}/assets/vendors/chart.js/Chart.min.js"></script>
      <script src="{{ url('/') }}/assets/vendors/typeahead.js/typeahead.bundle.min.js"></script>

      <script src="{{ url('/') }}/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
      
      <script src="{{ url('/') }}/assets/js/off-canvas.js"></script>
      <script src="{{ url('/') }}/assets/js/hoverable-collapse.js"></script>
      <script src="{{ url('/') }}/assets/js/misc.js"></script>
      <script src="{{ url('/') }}/assets/js/settings.js"></script>
      <script src="{{ url('/') }}/assets/js/todolist.js"></script>
      
      <script src="{{ url('/') }}/assets/js/dashboard.js"></script>
      <script src="{{ url('/') }}/assets/vendors/select2/select2.min.js"></script>
      <script src="{{ url('/') }}/assets/js/file-upload.js"></script>
     <script src="{{ url('/') }}/assets/js/typeahead.js"></script>
      <script src="{{ url('/') }}/assets/js/select2.js"></script>
    <script src="{{ url('/') }}/assets/vendors/jquery-steps/jquery.steps.min.js"></script>
    <script src="{{ url('/') }}/assets/js/wizard.js"></script>
   </body>
</html>