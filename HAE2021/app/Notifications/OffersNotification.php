<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;
//use Notification;
class OffersNotification extends Notification
{
    use Queueable;
    public $offerData;

    public $userSchema;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
   
    public function __construct($offerData,$userSchema)
    {
        $this->offerData = $offerData;
        $this->userSchema = $userSchema;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // print_r($this->offerData);
        // print_r($this->userSchema);
        // echo $this->offerData['offerUrl'];
     
        return (new MailMessage)
                    ->greeting(new HtmlString("<br>") )  
                    ->line(new HtmlString("माननीय अधिष्ठाता / प्राचार्य ,<br>".$this->userSchema['CfCgPrinName'].'<br>'.$this->userSchema['CgName']))   
                    ->from('sender@example.com')
                    ->bcc('savio.g@smbgroup.co.in')
                    ->replyTo('replyTo@example.com')
                    ->subject('Notification from DOA')
                   ->line('कला संचालनालयाच्या कार्यालयातून एक अतिशय महत्वाची माहिती आहे.The Notifcation is for '.$this->offerData['NotiTitle'])
                    ->action('   सूचना  ', url($this->offerData['offerUrl']))
                    ->line(new HtmlString("आपला आभारी<br>कला संचालनालय,<br>महाराष्ट्र राज्य, मुंबई"))
                    ->salutation(new HtmlString(" <br>"));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            $notifiable,$this->offerData
        ];
    }
}
