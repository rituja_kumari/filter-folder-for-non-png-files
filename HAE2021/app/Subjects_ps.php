<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    protected $table="subjects";
     protected $fillable = [
        'EsCoId', 'EsName', 'EsCode', 'EsPassMarks', 'EsExclTot', 'EsDistMarks', 'EsCredit', 'EsPrint',
    ];
}
