<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ctchangelog extends Model
{
     protected $table="CtChangeLog";
     const CREATED_AT = 'activity_time';
     const UPDATED_AT = NULL;
}
