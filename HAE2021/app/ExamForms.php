<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamForms extends Model
{
    protected $table="ExamForms";
    const CREATED_AT = 'EntryTime';
    const UPDATED_AT = NULL;
    protected $fillable = [];
}