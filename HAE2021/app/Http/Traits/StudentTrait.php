<?php
namespace app\Http\Traits;
use Illuminate\Support\Facades\DB;

trait StudentTrait {
    public function candidateListForCenterApproval($input) 
    {
       $SessionalData=json_decode(Session('data'),true);
       extract($input);
       if($ActionStatus=='2'){ $ActionStat='0';}
       elseif($ActionStatus=='3'){ $ActionStat='2';}
       else{$ActionStat=$ActionStatus; }
        $usersinfo = DB::table('personal_info')
        ->join('course_info', 'personal_info.StuId', '=', 'course_info.StuId')
        ->where('course_info.CgId', '=', $SessionalData['0']['CgId'])
        ->where('course_info.course_id', '=', $CfCoId)
         ->where('course_info.st_type', '=', $StudType)
         ->where('course_info.ApprovedStat', '=', $ActionStat)
        ->select('personal_info.cname', 'course_info.amount','personal_info.StuId','course_info.id','course_info.StuRemk')
        ->get();
        $SpCl_value="ShowTbl";
        $returnHTML = view('CandiApprovalCouWise',compact('SpCl_value','usersinfo','ActionStatus'))->RENDER();

        return response()->json(['html'=>$returnHTML]);
    }
}