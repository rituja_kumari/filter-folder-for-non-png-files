<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstituteRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'state' => 'required',//|unique:posts|max:255
            'District' => 'required',
            'College' => 'required',
            'CfCgPrinName' => 'required|max:255',
            'CgLandLineNo' => 'required:max:16',
            'CfCgPrinContact' => 'required|max:10|unique:colleges|regex:/^([0-9\s\-\+\(\)]*)$/',
            'CfCgPrinEmail' => 'required|email|unique:colleges',
            'CoFName' => 'required',
            'CoFMobile' => 'required|max:10|unique:colleges',
            'CoFEmail' => 'required|email|unique:colleges',
            'CoSMobile' => 'regex:/^([0-9\s\-\+\(\)]*)$/|max:10|unique:colleges',
            'CoSEmail' => 'email|unique:colleges',
            'BankName' => 'required',
            'BankAcNo' => 'required',
            'reBankAcNo' => 'required',
            'BankIFSCNo' => 'required',
            'BankBranchName' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'state.required' => 'A state is required',
            'District.required' => 'A District is required',
        ];
    }
}
