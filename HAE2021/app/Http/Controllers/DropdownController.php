<?php

namespace App\Http\Controllers;
use Response;
use Illuminate\Http\Request;
use DB;
class DropdownController extends Controller
{
    public function getsubdistrict(Request $request)
	{
		$subdists = DB::table("sub_district_master")
		->where("DistrictCode",$request->id)
		->pluck("SubDistrict","SubDistrictCode");
		return response()->json($subdists);
	}

	public function getcourse(Request $request)
	{
		$courses = DB::table("cointakes")
		->join('courses', 'cointakes.CfCoId', '=', 'courses.CfCoId')
		->where("cointakes.CgId",$request->id)
		->where("cointakes.EntryStat",1)
		->pluck("courses.CoName","courses.CfCoId");
		return response()->json($courses);
	}

	public function getamount(Request $request)
	{
		$amt = DB::table("courses")
		->where("CfCoId",$request->CfCoId)
		->pluck("TotFees","CfCoId");
		return response()->json($amt);
	}

	public function getsubject(Request $request)
	{
		$subjects = DB::table("subject_master")
		->where("CourseCode",$request->id)
		->pluck("Subject","id");
		return response()->json($subjects);
	}

	public function getQualification(Request $request)
	{
		$qualifications = DB::table("qualification")
		->where("CourseCode",$request->course_id)
		->where("TypeId",$request->sttype)
		->pluck("Lable","id");
		return response()->json($qualifications);
	}

	public function getstType(Request $request)
	{
		$sttypes = DB::table("qualification")
		->where("CourseCode",$request->CfCoId)
		->pluck("Type","TypeId");
		return response()->json($sttypes);
	}

}
