<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ctchangelog;
use Artisan;
class uploadLedger extends Controller
{ 
    public function index()
    {
        Artisan::call('cache:clear');
        Artisan::call('view:clear');
        Artisan::call('config:clear');
        $SessionalData=json_decode(Session('data'),true);
        //$FinalSubmittedData = ctchangelog::where('activity_for','FinalSubmit')->where('user_id', $SessionalData['0']['CgId'])->distinct()->dd();
        $FinalSubmittedData = ctchangelog::select('activity')
                                ->where([
                                    ['activity_for', '=', 'FinalSubmit'],
                                    ['user_id', '=', $SessionalData['0']['CgId']]
                                ])
                                ->get();

        return view('pages.content-ledger',compact('FinalSubmittedData'));
    }
    
    public function ledgerUpload(Request $request)
    {
        $SessionalData=json_decode(Session('data'),true);
        
        $request->validate([
            'filenames' => 'required',
            'filenames.*' => 'mimes:pdf',
        ]);

        if($request->hasfile('filenames'))
         {
            foreach($request->file('filenames') as $key => $file)
            {
                $name = $SessionalData['0']['CgId'].$request->ArrOFCoId[$key].'.'.$file->extension();  
                 
                if(\File::exists(public_path('ledger'), $name))
                {
                    \File::delete(public_path('ledger'), $name);
                }
                $file->move(public_path('ledger'), $name);
                $data[] = $name; 
            }
         }
        
        return back()
                ->with('success','You have successfully upload file.')
                ->with('file',$data);
    } 
     
}
