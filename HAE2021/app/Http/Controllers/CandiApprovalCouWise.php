<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\StudentTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Response;
use App\CourseInfo;
use App\ctchangelog;

class CandiApprovalCouWise extends Controller
{
    
   use StudentTrait;
 
    public function index()
    {
        $SessionalData=json_decode(Session('data'),true);
        $usersinfo = CourseInfo::where('CgId', $SessionalData['0']['CgId'])
                   ->get();
        $SpCl_value="ShowTbl";        
       $returnHTML = view('CandiApprovalCouWise',compact('SpCl_value','usersinfo'))->RENDER();

        return response()->json(['html'=>$returnHTML]);
    }   
    public function ShowStudentType($id)
    {
        $SpCl_value="ShowStudentType";
        $returnHTML = view('CandiApprovalCouWise',compact('SpCl_value','id'))->RENDER();
        return response()->json(['html'=>$returnHTML]);
    } 
    public function ShowAction($id)
    {
        $SpCl_value="ShowAction";
        $returnHTML = view('CandiApprovalCouWise',compact('SpCl_value','id'))->RENDER();
        return response()->json(['html'=>$returnHTML]);
    }
    public function ShowTbl(Request $request)
    {
        $input = $request->input();
        return $this->candidateListForCenterApproval($input);
    }
    public function CandiApprovalPdf(Request $request)
    {
        $pdf = PDF::loadView('users.index',compact('user'));
        return $pdf->download('pdfview.pdf');
    }
    public function SubmitApproval(Request $request)
    {
        $input = $request->input();
        extract($input);
        $SpCl_value="SubmitApproval";
        $stpvalidator = Validator::make($request->all(), [
            'CfCoId' => 'required',
            'StudType' => 'required',
            'ActionStatus' => 'required'
        ],
        [
            'CfCoId.required' => 'Please Select Course',
            'StudType.required' => 'Please Select Student Type',
            'ActionStatus.required' => 'Please Select Action',
        ]);
          
        $input = $request->all();
        $returnHTML = view('CandiApprovalCouWise',compact('SpCl_value','input'))->RENDER();
        if ($stpvalidator->passes()) 
        {
            if(empty($ActionStu))
            {
                return Response::json(['errors' => "Please select atleast one student details...!"]);
            }
            if($ActionStatus=='0')
            { 
                $ApprovalStat='1';
                $ApproveMsg="Approved";
            }
            elseif($ActionStatus=='2')
            { 
                $ApprovalStat='2';
                $ApproveMsg="Reject";
            }
          
            foreach($ActionStu as $id)
            {
                $UpdateApprovStat= CourseInfo::where('id', $id)
                ->where('ApprovedStat', '0')
                ->update(['ApprovedStat' => $ApprovalStat,'StuRemk'=>$Remark[$id]]);

                $changelog = new ctchangelog;
                $changelog->user_id = $id;
                $changelog->activity = $ApprovalStat;
                $changelog->activity_details = $Remark[$id];
                $changelog->activity_type = 'CLG_APPROVE';
                $changelog->save();
            }
            // try 
            // { 
            //     $UpdateApprovStat=DB::table('course_info')->whereIn('id', $ActionStu)->update(array('ApprovedStat' => $ApprovalStat));//->toSql();
            // } 
            // catch(\Illuminate\Database\QueryException $ex){ 
               
            //     dd($ex->getMessage()); 
            //   }
            if($UpdateApprovStat)
            {
                $SuceeMsg="Students ".$ApproveMsg." Successfully...!";
            }
            else
            {
                $SuceeMsg="Unable to ".$ApproveMsg.".PLease try again...!";
            }
        $returnHTML = $this->candidateListForCenterApproval($input);
        return response()->json(['html'=>$returnHTML->original['html'],'errors' => $SuceeMsg]);
        }

       return Response::json(['ValidatorErrors' => $stpvalidator->errors()]);
    }
  
    
}
