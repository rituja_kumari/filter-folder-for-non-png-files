<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Faculty;
use App\Course;
use App\Branch;
use App\codes;

use Illuminate\Http\Request;

class ExamController extends Controller
{
    public function index()
    {
        //
    }

   
    public function create()
    {
         $faculty = Faculty::where('EntryStat','1')
        ->pluck('FcDesc','CfFacId')
        ->all();

        $YesNo= codes::where('CdType','OTHER')
        ->pluck('CdDesc','CdSeq')->all();

        $CfIsImprov= codes::where('CdType','CL_IMP')
        ->pluck('CdDesc','CdSeq')->all();

        return view('ExamMasterAdd',compact('faculty', 'YesNo','CfIsImprov'));
    }

    
    public function store(Request $request)
    {
        $CfCoId=$request->input('CfCoId');
        $CfBrId=$request->input('CfBrId');
        $EmSrNo=$request->input('EmSrNo');
        $EmCode=$request->input('EmCode');
        $EmName=$request->input('EmName');
        $EmTotSub=$request->input('EmTotSub');
        $EmMaxTot=$request->input('EmMaxTot');
        $EmMinCr=$request->input('EmMinCr');
        $EmMaxCr=$request->input('EmMaxCr');
        $EmPassPC=$request->input('EmPassPC');
        $EmValDev=$request->input('EmValDev');
        $EmColg=$request->input('EmColg');
        $EmLatEntry=$request->input('EmLatEntry');
        $CfIntrnShpExam=$request->input('CfIntrnShpExam');
        $CfIsReval=$request->input('CfIsReval');
        $RevalEsCount=$request->input('RevalEsCount');
        $RevalThreshold=$request->input('RevalThreshold');
        $CfIsImprov=$request->input('CfIsImprov');
        $CfTotLvl=$request->input('CfTotLvl');
        $EmRgNoCnt=$request->input('EmRgNoCnt');
        $CfOptGrpCnt=$request->input('CfOptGrpCnt');
        $CfCmpGrpCnt=$request->input('CfCmpGrpCnt');
        

        $Ccode= Course::select('CoCode')->where('CfCoId',$CfCoId)->get(); 
        foreach ($Ccode as  $value) {
             $coursecode=$value->CoCode;
        }

        if($request->has('EmConv')){
            $EmConv=$request->input('EmConv');
        }else{
            $EmConv='N';
        }

        if($request->has('EmSpFlag')){
            $EmSpFlag=$request->input('EmSpFlag');
        }else{
            $EmSpFlag='N';
        }

        if($request->has('EmGrpClg')){
            $EmGrpClg=$request->input('EmGrpClg');
        }else{
            $EmGrpClg='0';
        }


        $isExist = Exam::select('EmCode')
                  ->where('EmCode', $EmCode)
                  ->where('EntryStat','1')
                  ->exists();

       /* $IsSeqExist = Exam::where('EmSrNo', $EmSrNo)
        ->where('EmBrId',$EmBrId)
        ->where('EntryStat','1')
        ->get('EmSrNo');*/
        

        //return $request->input();
        $insert_obj=new Exam();
        $insert_obj->CfCoId=$CfCoId;
        $insert_obj->CfBrId=$CfBrId;
        $insert_obj->EmSrNo=$EmSrNo;
        $insert_obj->EmCode=$EmCode;
        $insert_obj->EmName=$EmName;
        $insert_obj->EmTotSub=$EmTotSub;
        $insert_obj->EmMaxTot=$EmMaxTot;
        $insert_obj->EmMinCr=$EmMinCr;
        $insert_obj->EmMaxCr=$EmMaxCr;
        $insert_obj->EmPassPC=$EmPassPC;
        $insert_obj->EmValDev=$EmValDev;
        $insert_obj->EmColg=$EmColg;
        $insert_obj->EmLatEntry=$EmLatEntry;
        $insert_obj->CfIntrnShpExam=$CfIntrnShpExam;
        $insert_obj->CfIsReval=$CfIsReval;
        $insert_obj->RevalEsCount=$RevalEsCount;
        $insert_obj->RevalThreshold=$RevalThreshold;
        $insert_obj->CfIsImprov=$CfIsImprov;
        $insert_obj->CfTotLvl=$CfTotLvl;
        $insert_obj->EmRgNoCnt=$EmRgNoCnt;
        $insert_obj->CfOptGrpCnt=$CfOptGrpCnt;
        $insert_obj->CfCmpGrpCnt=$CfCmpGrpCnt;
        $insert_obj->EmConv=$EmConv;
        $insert_obj->EmSpFlag=$EmSpFlag;
        $insert_obj->EmGrpClg=$EmGrpClg;
        $insert_obj->EntryStat='1';
        $insert_obj->EmCoCode=$coursecode;
        $insert_obj->EmShName='';
        $insert_obj->EmStreams='';
        $insert_obj->EmYrSrNo='1';
        $insert_obj->EmATKTSub='0';
        $insert_obj->EmElgblCd='';
        $insert_obj->EmLedger='';
        $insert_obj->EmMarkList='';
        $insert_obj->EmRejImpr='';
        $insert_obj->EmEval='';
        $insert_obj->EmVal='1';
        $insert_obj->EmDiv='';
        $insert_obj->EmDivFirstAtt='';
        $insert_obj->EmGrps='0';
        $insert_obj->EmSubTot='';
        $insert_obj->EmPrnCounter='0';
        $insert_obj->EntryBy='1';
        //$insert_obj->EntryTime='1';

        $insert_obj->save();

        $request->session()->flash('msg','Exam Details Submited!');
        return redirect('em-list');
    }

    
    public function show(Request $request)
    {
         return view('ExamMaster')->with('ListArr',Exam::where('EntryStat','1')->get());
    }

    
    public function edit($id)
    {
        $ListArr = Exam::where('EmId',$id)->get(); 

        $faculty = Faculty::where('EntryStat', '1')
        ->pluck('FcDesc','CfFacId')->all();

        $course = Course::where('EntryStat', '1')
        ->pluck('CoName','CfCoId', 'CfFcId')->all();

        $branch = Branch::where('EntryStat', '1')
        ->pluck('branch_name','CfBrId')->all();

        $YesNo= codes::where('CdType','OTHER')
        ->pluck('CdDesc','CdSeq')->all();

        $CfIsImprov= codes::where('CdType','CL_IMP')
        ->pluck('CdDesc','CdSeq')->all();
      //  dd($course);
        return view('ExamMasterEdit',compact('ListArr','faculty','course','branch','YesNo','CfIsImprov'));
    }

   
    public function update(Request $request, $id)
    {
        //print_r($request->input());
        Exam::where('EmId', $id)->update([
             'EmCoId' => $request->input('EmCoId'),
             'EmSrNo' => $request->input('EmSrNo'),
             'EmCode' => $request->input('EmCode'),
             'EmName' => $request->input('EmName'),
             'EmShName' => $request->input('EmShName'),
             'EmBrId' => $request->input('EmBrId'),
        ]);

        $request->session()->flash('msg','Exam Details updated!');
        return redirect('em-list');
    }

   
    public function destroy(Request $request, $id)
    {
        Exam::where('EmId', $id)->update([
            'EntryStat' => '0',
        ]);
        $request->session()->flash('msg','Record Deleted Successfully...!');
        return redirect('em-list');
    }

     public function findcourse(Request $request,$id)
    {
        $course = Course::where('CfFcId',$id)
        ->where('EntryStat','1')
        ->get();
        return response()->json($course); 
    }

    public function findbranch(Request $request,$id)
    {
        $branch = Branch::where('CfBrId',$id)
        ->where('EntryStat','1')
        ->get();
        return response()->json($branch); 

    }
}
