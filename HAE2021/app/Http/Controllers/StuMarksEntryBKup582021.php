<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use App\Course;
use App\ExamSub;
use App\icrregnforms;
use App\ExamForms;
use App\CtStuMarksMaster;
use App\CtStuMarksMasterLog;
use App\Exam;
use App\codes;
use App\ctchangelog;
use App\MarksheetSchema;
use App\CollegeModel;
use PDF;
use Mpdf\Mpdf;


class StuMarksEntry extends Controller
{
    public function index()
    {
        $CourseDetails = Course::where('EntryStat', 1)->where('CfFcId', 1)->get();
        return view('pages.content-marksentry',$CourseDetails);
    }
    public function ShowBranch($id=null,Request $request)
    {
        $courses=$request->input('Selcourse');
        $SpCl_value="ShowBranchMaster";
        $returnHTML = view('StuMarksEntrySave',compact('SpCl_value','courses'))->RENDER();
        return response()->json(['html'=>$returnHTML]);  
    }
    
    public function ShowExam($id=null,Request $request)
    {
        $courses = $request->input('Selcourse');
        $branch = $request->input('Selbranch');
        $SpCl_value="ShowExamMaster";
        $returnHTML = view('StuMarksEntrySave',compact('courses','SpCl_value','branch'))->RENDER();
        return response()->json(['html'=>$returnHTML]);  
    }  
    
    public function AutoCalcObtMarks($id=null,Request $request)
    {
        $SpCl_value="AutoCalcObtMarks";
        $input = $request->input();
        extract($input);
        $StoredPassFail=array();
        $SumObt=array_sum($ArrForObtMarks);
        foreach($ArrForObtMarks as $EsId =>$ObtMarks)
        {
            $PassFail=0;
            $MinMarks=$ArrForPassMarks[$EsId];
            if($ObtMarks >=$MinMarks and $ObtMarks!="AB" and is_numeric($ObtMarks))
            {
                $PassFail=1;
            }
            elseif($ObtMarks=='NA')
            {
                $PassFail='-11';
            }
            $StoredPassFail[$EsId]=$PassFail;
        }
       
        $StoredPassFail=json_encode($StoredPassFail);
        $returnHTML = view('StuMarksEntrySave',compact('SumObt','SpCl_value','StoredPassFail'))->RENDER();
        return response()->json(['html'=>$returnHTML]);
    } 
    public function show($id,Request $request)
    {
        return $this->ShowRollName($id,'modal',$request); // return $this->ShowRollName($id,'1',$request);
    } 
    public function OnExamChange($id=NULL,Request $request)
    {
        return $this->ShowRollName($id=NULL,'ledger',$request); //return $this->ShowRollName($id=NULL,'2',$request);
    } 
    public function ShowEdit($id=NULL,Request $request)
    {
        return $this->ShowRollName($id,'Edit',$request);     
    } 
    
    public function EditStudentInfo($id=NULL,Request $request)
    {
        return $this->AddStudentInfo($id,$request,'Edit');
    } 
   
    function dynamic_Ledgerpdf($CoId=0,$EmId=0,$BrId=0,$CgId=0)
    {
        $CoId=get_decrypt(base64_decode($CoId));
        $EmId=get_decrypt(base64_decode($EmId));
        $BrId=get_decrypt(base64_decode($BrId));
        $CgId=get_decrypt(base64_decode($CgId));
        $CODES = codes::select('CdCode','CdDesc','CdSeq')->where('CdType', '=','RES')->get();
        $CLASSCODES = codes::select('CdCode','CdDesc','CdSeq')->where('CdType', '=','CLASS')->get();
        $ArrForEndOfSubjectId = ExamSub::select('EsId','EsMaxMarks','EsPassMarks','EsName') ->whereIn('EsType', ['S','P'])->where('EsStrId', '=',$BrId)->where('EsEmId', '=',$EmId)->where('EntryStat', '=','1')->orderBy('EsPrint', 'asc')->orderBy('EsId', 'asc')->get();
        if(!empty($EmId))
        {
            $ExamInfo = Exam::select('EmMaxTot','EmPassPC')->where('EmId', '=',$EmId)->get();
            $EmMaxTot=intval($ExamInfo[0]->EmMaxTot);
            $EmPassPC=$ExamInfo[0]->EmPassPC;

            $MarksheetSchema = MarksheetSchema::select('GroupInfo','Extra')->where('EmId', '=',$EmId)->where('EntryStat', '=','1')->get();
            $GroupInfo=$MarksheetSchema[0]->GroupInfo;
            $Extra=$MarksheetSchema[0]->Extra;
        }
        $CourseData = Course::select('CoName')->where('CfCoId', '=',$CoId)->get();
        $CourseName=$CourseData[0]['CoName'];
        $SessionalData=json_decode(Session('data'),true);
        $ExamFormsQuery = ExamForms::join('icrregnforms', 'EfRgId', '=', 'RgNo')
        ->where('EfStrId', '=',$BrId)->where('EfEmId', '=',$EmId)->where('ResProStat', '=','1');
        $CgName='';
        if(!empty($SessionalData['0']['CgName']))
        {
            $CgName=$SessionalData['0']['CgName'];
            $ExamFormsQuery =$ExamFormsQuery->where('EfCgId', '=',$SessionalData['0']['CgId']);
        }
        elseif ($CgId!=0 and !empty($CgId)) 
        {
            $CgName1 = CollegeModel::select('CgName')
                    ->where([
                        ['CgId', '=', $CgId],
                    ])
                    ->get();
            $CgName=$CgName1['0']['CgName'];        
            $ExamFormsQuery =$ExamFormsQuery->where('EfCgId', '=',$CgId);
        }
       $Decoded_ArrForEndOfSubjectId=json_decode($ArrForEndOfSubjectId,true);
       $GrandTotal=array("EsMaxMarks"=>$EmMaxTot,"EsPassMarks"=>$EmPassPC,"OBT"=>"");
       $SessionalData=json_decode(Session('data'),true);
    //    $ExamFormsQuery = ExamForms::join('icrregnforms', 'EfRgId', '=', 'RgNo')
    //     ->where('EfStrId', '=',$BrId)->where('EfEmId', '=',$EmId)->where('EfCgId', '=',$SessionalData['0']['CgId'])->where('ResProStat', '=','1');
        $AllExamFormsDetails=$ExamFormsQuery ->orderBy('EfRollNo', 'asc')->get(['RgNo','FullName','EfId','EfRgId','EfRollNo','EfMaxMark','EfObtMarks','EfPC','EfResult','EfGrdPts','EfCate']);
        $AllStudentMArksInfo = CtStuMarksMaster::select('EsId','ObtMarks','PassFailStat','GradeDesc','RgNo','ChildEsId')->where('EntryFlag', '=','1')->where('EmId', '=',$EmId)->where('CgId', '=',$CgId)->get();
        $Decoded_AllStudentMArksInfo=json_decode($AllStudentMArksInfo,true);
   // return  $returnHTML = view('Ledger',compact('Decoded_ArrForEndOfSubjectId','GrandTotal','GroupInfo','Extra','AllExamFormsDetails','Decoded_AllStudentMArksInfo','CODES','EmId','CourseName','CgName'))->RENDER();
        $SpCl_value="LedgerInPdf";
        $pdf = \App::make('dompdf.wrapper');
        $pdf->setPaper('legal', 'landscape');
        $pdf = PDF::loadView('Ledger', compact('Decoded_ArrForEndOfSubjectId','GrandTotal','GroupInfo','Extra','AllExamFormsDetails','Decoded_AllStudentMArksInfo','CODES','EmId','CourseName','CgName','CLASSCODES'));
        $pdf->setPaper('legal', 'landscape');
       return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
    }
    function pdf($CoId=0,$EmId=0,$BrId=0)
    {
        $CoId=get_decrypt(base64_decode($CoId));
        $EmId=get_decrypt(base64_decode($EmId));
        $BrId=get_decrypt(base64_decode($BrId));
        $SpCl_value="LedgerInPdf";
        $pdf = \App::make('dompdf.wrapper');
        
        $pdf->setPaper('legal', 'landscape');
        $SessionalData=json_decode(Session('data'),true);
        $CODES = codes::select('CdCode','CdDesc','CdSeq')->where('CdType', '=','RES')->get();
        $CourseData = Course::select('CoName')->where('CfCoId', '=',$CoId)->get();
        $CourseName=$CourseData[0]['CoName'];
        $CgName=$SessionalData['0']['CgName'];
        
        $CLASSCODES = codes::select('CdCode','CdDesc')->where('CdType', '=','CLASS')->get();

        $ExamFormsQuery = ExamForms::join('icrregnforms', 'EfRgId', '=', 'RgNo')
        ->where('EfStrId', '=',$BrId)->where('EfEmId', '=',$EmId)->where('EfCgId', '=',$SessionalData['0']['CgId'])->where('ResProStat', '=','1');
        $AllExamFormsDetails=$ExamFormsQuery ->orderBy('EfRollNo', 'asc')->get(['RgNo','FullName','EfId','EfRgId','EfRollNo','EfMaxMark','EfObtMarks','EfPC','EfResult','EfGrdPts','EfCate']);

        $ArrForEndOfSubjectId = ExamSub::select('EsId','EsMaxMarks','EsPassMarks')->where('CfEndOfSub', '=','Y')->where('EsStrId', '=',$BrId)->where('EsEmId', '=',$EmId)->where('EntryStat', '=','1')->orderBy('EsPrint', 'asc')->orderBy('EsId', 'asc')->get();//EsSeq
        $EmMaxTot = $EmPassPC='';
        if(!empty($EmId))
        {
            $ExamInfo = Exam::select('EmMaxTot','EmPassPC')->where('EmId', '=',$EmId)->get();
            $EmMaxTot=$ExamInfo[0]->EmMaxTot;
            $EmPassPC=$ExamInfo[0]->EmPassPC;
        }

        $Decoded_ArrForEndOfSubjectId=json_decode($ArrForEndOfSubjectId,true);
        $EsNameOfEndOfHierachy=$ArrOfChildEsId=array();
        unset($ArrChildEsId);
        foreach($Decoded_ArrForEndOfSubjectId as $EndOfSubjectId)
        {
            $ArrOfChildEsId[$EndOfSubjectId['EsId']]=$ChildEsId=GetChildEsId($EndOfSubjectId['EsId'], '');
            $ArrChildEsId=explode('|',$ChildEsId); 
            $SubjectName='';
            $Subjectnformation = json_decode(ExamSub::select('EsName')->where('EsId', '=',$EndOfSubjectId['EsId'])->limit(1)->get(),true);
            $EsNameOfEndOfHierachy[$EndOfSubjectId['EsId']]=$Subjectnformation[0]['EsName'];
          
        }
        $StudentMArksInfo='';
        
        $AllStudentMArksInfo = CtStuMarksMaster::select('EsId','ObtMarks','PassFailStat','GradeDesc','RgNo')->whereIn('ChildEsId', $ArrOfChildEsId)->where('EntryFlag', '=','1')->where('EmId', '=',$EmId)->where('CgId', '=',$SessionalData['0']['CgId'])->get();
        
        $EsNameOfEndOfHierachy=json_encode($EsNameOfEndOfHierachy);
        $ArrOfChildEsId=json_encode($ArrOfChildEsId);
        $mode='ledger';
        $pdf = PDF::loadView('StuMarksEntryLedger', compact('ArrForEndOfSubjectId','EsNameOfEndOfHierachy','ArrOfChildEsId','EmMaxTot','CODES','CLASSCODES','mode','StudentMArksInfo','EmPassPC','AllExamFormsDetails','AllStudentMArksInfo','SpCl_value','CgName','CourseName'));
        $pdf->setPaper('legal', 'landscape');

       return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
     } 
    
   
   
    public function FinalSubmission($id=NULL,Request $request)
    {
        $qtvalidator = Validator::make($request->all(), [
        'Declaration'  => 'required',
        ],
        [
            'Declaration.required' => 'Please Check the Checkbox before final submitting the student data',
        ]);
        if ($qtvalidator->passes()) 
        { 
            $SessionalData=json_decode(Session('data'),true);
            if (ctchangelog::where('activity_for', '=','FinalSubmit')->where('user_id', '=',$SessionalData['0']['CgId'])->where('activity_id', '=',$request->Selexam)->exists()) 
            {
                return Response::json(['errors' =>'Student data already submitted by college...!']);
            }
            else{
                $InsertForctchangelog = new ctchangelog;
                $InsertForctchangelog->user_id = $SessionalData['0']['CgId'];
                $InsertForctchangelog->activity = $request->Selcourse;
                $InsertForctchangelog->activity_id = $request->Selexam;
                $InsertForctchangelog->activity_for ='FinalSubmit';
                $InsertForctchangelog->save();
                return  $this->ShowRollName($id,'FinalSubmit',$request);
            }
            
        }
        return Response::json(['ValidatorErrors' => $qtvalidator->errors()]);
    } 

    
    public function DelStudentDetails($id=null,$EfRgId=NULL,$Extra=NULL,Request $request)
    {
        $SpCl_value="DeleteStudent";//DB::enableQuerylog();   // dd(DB::getQuerylog());die();
        $Isdel=ExamForms::where('EfId', $id)->where('ResProStat','=','1')->update(['ResProStat' => 0]);
        $users = CtStuMarksMaster::where('RgNo', '=',$EfRgId)->where('EntryFlag','=','1')->update(['EntryFlag'=>0]);
     
        if($Isdel)
        {
            $returnHTML = view('StuMarksEntrySave',compact('Isdel','SpCl_value'))->RENDER();
            return response()->json(['html'=>$returnHTML,'errors' =>'Student Details Deleted Successfully...!']);
        }
        else
        {
            $returnHTML = view('StuMarksEntrySave',compact('Isdel','SpCl_value'))->RENDER();
            return response()->json(['html'=>$returnHTML,'errors' =>'Unable to Delete Student Details...!']);
        }
    }
    public function ShowRollName($id=null,$mode='0',Request $request)
    {
        $SpCl_value="ShowRollName";
        
        $SessionalData=json_decode(Session('data'),true);
        $CODES = codes::select('CdCode','CdDesc','CdSeq')->where('CdType', '=','RES')->get();
        $CLASSCODES = codes::select('CdCode','CdDesc')->where('CdType', '=','CLASS')->get();
   
        // $ExamFormsQuery = ExamForms::join('icrregnforms', 'EfRgId', '=', 'RgNo')
        //     ->where('EfStrId', '=',$request->input('Selbranch'))->where('EfEmId', '=',$request->input('Selexam'))->where('EfCgId', '=',!empty($SessionalData['0']['CgId']))->where('ResProStat', '=','1');
        $ExamFormsQuery = ExamForms::join('icrregnforms', 'EfRgId', '=', 'RgNo')
            ->where('EfStrId', '=',$request->input('Selbranch'))->where('EfEmId', '=',$request->input('Selexam'))->where('ResProStat', '=','1');
        $ctchangelogQuery = ctchangelog::select('id')->where('activity_id', '=',$request->input('Selexam'))->where('activity_for', '=','FinalSubmit');
        if(!empty($SessionalData['0']['CgId']))
        {
            $ExamFormsQuery=$ExamFormsQuery->where('EfCgId', '=',$SessionalData['0']['CgId']);
            $ctchangelogQuery=$ctchangelogQuery->where('user_id', '=',$SessionalData['0']['CgId']);
        }
        $FinalSubmissionDoneOrNot=$ctchangelog=$ExamForms='';
        $AllExamFormsDetails=$ExamFormsQuery ->orderBy('EfRollNo', 'asc')->get(['RgNo','FullName','EfId','EfRgId','EfRollNo','EfMaxMark','EfObtMarks','EfPC','EfResult','EfGrdPts','EfCate']);
        if($id!='undefined' and !empty($id))
        {
           $ExamForms=$ExamFormsQuery->where('RgNo', '=',$id)->orderBy('EfRollNo', 'asc')->get(['RgNo','FullName','EfId','EfRgId','EfRollNo','EfMaxMark','EfObtMarks','EfPC','EfResult','EfGrdPts','EfCate']);
        }
        $ctchangelog=$ctchangelogQuery->skip(0)->take(1)->get();
        //$ctchangelog = ctchangelog::select('id')->where('user_id', '=',$SessionalData['0']['CgId'])->where('activity_id', '=',$request->input('Selexam'))->where('activity_for', '=','FinalSubmit')->skip(0)->take(1)->get();
        if(!empty($ctchangelog) and $ctchangelog!='[]')
        { 
            $FinalSubmissionDoneOrNot=$ctchangelog[0]->id; 
        }
        
        $ArrForEndOfSubjectId = ExamSub::select('EsId','EsMaxMarks','EsPassMarks','EsPSubId')->where('CfEndOfSub', '=','Y')->where('EsStrId', '=',$request->input('Selbranch'))->where('EsEmId', '=',$request->input('Selexam'))->where('EntryStat', '=','1')->orderBy('EsPrint', 'asc')->orderBy('EsId', 'asc')->get();
        $EmMaxTot = $EmPassPC='';
        if(!empty($request->input('Selexam')))
        {
            $ExamInfo = Exam::select('EmMaxTot','EmPassPC')->where('EmId', '=',$request->input('Selexam'))->get();
            $EmMaxTot=$ExamInfo[0]->EmMaxTot;
            $EmPassPC=$ExamInfo[0]->EmPassPC;
        }

        $Decoded_ArrForEndOfSubjectId=json_decode($ArrForEndOfSubjectId,true);
        $EsNameOfEndOfHierachy=$ArrOfChildEsId=array();
        unset($ArrChildEsId);
        foreach($Decoded_ArrForEndOfSubjectId as $EndOfSubjectId)
        {
            $ArrOfChildEsId[$EndOfSubjectId['EsId']]=$ChildEsId=GetChildEsId($EndOfSubjectId['EsId'], '');
            $ArrChildEsId=explode('|',$ChildEsId); 
            $SubjectName='';
            $Subjectnformation = json_decode(ExamSub::select('EsName')->where('EsId', '=',$EndOfSubjectId['EsId'])->limit(1)->get(),true);
            $Subjectnformation2 = json_decode(ExamSub::select('EsName')->where('EsId', '=',$EndOfSubjectId['EsPSubId'])->limit(1)->get(),true);
            $EsNameOfEndOfHierachy[$EndOfSubjectId['EsId']]=$Subjectnformation[0]['EsName'];
            $EsNameOfEndOfHierachy[$EndOfSubjectId['EsPSubId']]=$Subjectnformation2[0]['EsName'];
          
        }
        $StudentMArksInfo='';
        if($id!='undefined' and !empty($id))
        {
            $StudentMArksInfo = CtStuMarksMaster::select('EsId','ObtMarks','PassFailStat','GradeDesc','RgNo','ChildEsId','Extra')->whereIn('ChildEsId', $ArrOfChildEsId)->where('RgNo', '=',$id)->where('EntryFlag', '=','1')->where('EmId', '=',$request->input('Selexam'))->get();
        }
        if(!empty($SessionalData['0']['CgId']))
        {
            //$AllStudentMArksInfo = CtStuMarksMaster::select('EsId','ObtMarks','PassFailStat','GradeDesc','RgNo','ChildEsId')->whereIn('ChildEsId', $ArrOfChildEsId)->where('EntryFlag', '=','1')->where('EmId', '=',$request->input('Selexam'))->where('CgId', '=',$SessionalData['0']['CgId'])->get();
            if($SessionalData['0']['CgId']=='14' and $request->input('Selexam')=='7')
            {
                $AllStudentMArksInfo=[];
            }
            else {
                $AllStudentMArksInfo = CtStuMarksMaster::select('EsId','ObtMarks','PassFailStat','GradeDesc','RgNo','ChildEsId')->whereIn('ChildEsId', $ArrOfChildEsId)->where('EntryFlag', '=','1')->where('EmId', '=',$request->input('Selexam'))->where('CgId', '=',$SessionalData['0']['CgId'])->get();
            }
        }
        else
        {
            $AllStudentMArksInfo = CtStuMarksMaster::select('EsId','ObtMarks','PassFailStat','GradeDesc','RgNo','ChildEsId')->whereIn('ChildEsId', $ArrOfChildEsId)->where('EntryFlag', '=','1')->where('EmId', '=',$request->input('Selexam'))->get();
        }
       // $AllStudentMArksInfo = CtStuMarksMaster::select('EsId','ObtMarks','PassFailStat','GradeDesc','RgNo','ChildEsId')->whereIn('ChildEsId', $ArrOfChildEsId)->where('EntryFlag', '=','1')->where('EmId', '=',$request->input('Selexam'))->get();
        
        $EsNameOfEndOfHierachy=json_encode($EsNameOfEndOfHierachy);
        $ArrOfChildEsId=json_encode($ArrOfChildEsId);
        $returnHTML = view('StuMarksEntrySave',compact('SpCl_value','ArrForEndOfSubjectId','EsNameOfEndOfHierachy','ArrOfChildEsId','EmMaxTot','CODES','CLASSCODES','ExamForms','mode','StudentMArksInfo','request','EmPassPC','AllExamFormsDetails','AllStudentMArksInfo','FinalSubmissionDoneOrNot'))->RENDER();
        return response()->json(['html'=>$returnHTML]);
        
    }

    public function AddStudentInfo($id=null,Request $request,$mode='add')
    {
        $AtktCriteria=array("2"=>"2","4"=>"2","6"=>"2","8"=>"2");
        $CLASSCODES = codes::select('CdCode','CdDesc','CdSeq')->where('CdType', '=','CLASS')->orderBy('CdSeq', 'asc')->get();
        $CLASSCODES=json_decode($CLASSCODES,true);
       
        $SpCl_value="AddStudentInfo";
        $input = $request->input();
        extract($input);
        $qtvalidator = Validator::make($request->all(), [
            'Selcourse'  => 'required',
            'Selbranch'  => 'required',
            'Selexam'  => 'required',
            'EfRollNo'  => 'required',
            'FullName'  => 'required',
            'EfCate'  => 'required',
            'ArrForObtMarks.*'  => 'required',
           // 'ArrForPassFailsStatus.*'  => 'required',
            //'FinalResultStatus'  => 'required',
            //'FinalResultClass'  => 'required',
            ],
            [
                'Selcourse.required' => 'Please Select Course',
                'Selbranch.required' => 'Please Select Branch',
                'Selexam.required' => 'Please Select Exam',
                'EfRollNo.required' => 'Please Enter Rolll Number',
                'FullName.required' => 'Please Enter Full Name',
                'EfCate.required' => 'Please Select Category',
                'ArrForObtMarks.*.required' => 'Please Enter Obtained Marks For All Subjects',
                //'ArrForPassFailsStatus.*.required' => 'Please Enter Pass/Fail/Absent Status For All Subjects',
                //'FinalResultStatus.required' => 'Please Select Final Result Status',
                //'FinalResultClass.required' => 'Please Select Final Result Class',
            ]);
            // if($FinalResultStatus!='A' and empty($FinalResultStatus))
            // {
            //     return Response::json(['errors' =>'Please Select Final Result Class...!']);
            // }
       
        if ($qtvalidator->passes()) 
        { 
            
            $SessionalData=json_decode(Session('data'),true);
            $EfRollNo=$SessionalData['0']['CgCode'].'/'.$EfRollNo;
            if (ExamForms::where('EfRollNo', '=',$EfRollNo)->where('ResProStat', '=','1')->where('EfStrId', '=',$request->input('Selbranch'))->where('EfEmId', '=',$request->input('Selexam'))->where('EfCgId', '=',$SessionalData['0']['CgId'])->exists() and $mode=='add') 
            {
                return Response::json(['errors' =>'Duplicate Roll Number Found...!']);
            } 
           
            foreach($ArrForObtMarks as $SubId => $ObtMarks)
            {
                $MaxMarks=$ArrForMaxMarks[$SubId];
                $MinMarks=$ArrForPassMarks[$SubId];
                
                $EsName=$ArrForEsName[$SubId];
                if($ObtMarks!="NA")
                {
                    $PassFail=$ArrForPassFailsStatus[$SubId];
                    if((!is_numeric($ObtMarks) and $ObtMarks!="AB" ) or ($ObtMarks<0) )
                    {
                        return Response::json(['errors' =>'Invalid Marks Format for '.$EsName]);
                    }
                    if($ObtMarks >$MaxMarks and $ObtMarks!="AB" )
                    {
                        return Response::json(['errors' =>'Obtained Marks Should not be greater than Max marks for '.$EsName]);
                    }
                    if((is_numeric($ObtMarks) and ($ObtMarks < $MinMarks and $PassFail=='1' or $ObtMarks > $MinMarks and $PassFail=='0') ) or ($ObtMarks=='AB' and $PassFail=='1'))
                    {
                        return Response::json(['errors' =>'Invalid Pass/Fail/Absent Status added for  '.$EsName]);
                    }
                }
            }
           if(empty($ArrForPassFailsStatus)){ $ArrForPassFailsStatus=array();}
            $ValidPassfailStat=array_unique($ArrForPassFailsStatus); //dont use array filter 
            $ValidObtMarks=array_filter(array_unique($ArrForObtMarks));
            $ExamInfo = Exam::select('EmPassPC')->where('EmId', '=',$request->input('Selexam'))->get();
            $EmPassPC=$ExamInfo[0]->EmPassPC;
            $TotalObtainedMarks=array_sum($ArrForObtMarks);
              
            $FinalResultStatus='H';
            if($mode=='add')
            {
                $CoCode = Course::where('CfCoId', '=',$Selcourse)->first()->CoCode;//add
                $UniNumber=GenrateUniqNumber();
                
                if (ExamForms::where('EfRgId', '=',$UniNumber)->where('ResProStat', '=','1')->exists()) 
                {
                    return Response::json(['errors' =>'Duplicate UiqId Found...!']);
                }
            }
            else
            {
                $UniNumber=$id;
            }
          
            $Percent=($TotalObtainedMarks/$TotalMaxMarks) * 100;
            $EfPC=number_format($Percent, 2);
      
            $FinalResultStatus='F';
            if($mode=='add')
            {
                $IcrData=array("StuId"=>'0',"RgNo"=>$UniNumber,"CgCode"=>$SessionalData['0']['CgCode'],"CgId"=>$SessionalData['0']['CgId'],"CoCode"=>$CoCode,"Branch"=>$Selbranch,"Name"=>$FullName,"EmId"=>$Selexam,"uniq_id"=>$UniNumber,"user_id"=>$SessionalData['0']['CgId'],"CfFcId"=>'1',"CfCoId"=>$Selcourse,"CfApprovStat"=>"H","CfIsProv"=>'N',"BatchNo"=>'0',"CfSnSrNo"=>'0',"Batch"=>'0',"Cate"=>'0',"Title"=>'0','Father'=>'0','Mother'=>'0','Sex'=>'0','BirthDate'=>'1999-07-01','Caste'=>'0','Nation'=>'0','Medium'=>'0','Addr1'=>'0','Addr2'=>'0','Addr3'=>'0','Place'=>'0','Pin'=>'0','Addr1P'=>'0','Addr2P'=>'0','Addr3P'=>'0','PlaceP'=>'0','PinP'=>'0','Sub'=>'0','ExamSkip'=>'0','ProcFlag'=>'0','Reli'=>'0','Elgb'=>'0','UID'=>'0','LandLine'=>'0','Aadhar'=>'0','PhyHand'=>'0','LangId'=>'0','Marital'=>'0','password'=>'0','CfVoterId'=>'0','CfQuotaId'=>'0','CfDomicile'=>'0','CfState'=>'0','CfAdmiDate'=>'1999-07-01','CfAdmiCate'=>'0','CfCetType'=>'0','CfCetMk'=>'0','CfCetPer'=>'0','CfLastName'=>'0','CfFnameMar'=>'0','CfMnameMar'=>'0','CfLnameMar'=>'0','CfMotherMar'=>'0','CfUrduMk'=>'0','CfEngliMk'=>'0','CfPhyMk'=>'0','CfChemMk'=>'0','CfBioMk'=>'0','CfPcbMk'=>'0','CfRegElgFee'=>'0','CfEligiDate'=>'1999-07-01','FullName'=>$FullName);

                $ExamformData=array("EfShId"=>'0',"EfStrId"=>$Selbranch,"EfEmId"=>$Selexam,"EfEmSrNo"=>'0',"EfCgCode"=>$SessionalData['0']['CgCode'],"EfCgId"=>$SessionalData['0']['CgId'],"EfRgId"=>$UniNumber,"EfFormNo"=>'0',"EfRollNo"=>$EfRollNo,"EfCate"=>$EfCate,"EfMaxMark"=>$TotalMaxMarks,"EfObtMarks"=>$TotalObtainedMarks,"EfGraceMarks"=>"0","EfPC"=>$EfPC,"EfResult"=>$FinalResultStatus,"EfStatus"=>'0',"EfPrefCent"=>'0',"EfEcId"=>'0',"EfThfFlag"=>'0','EfThfMarks'=>'0','EfSpFlag'=>'0','EfCent'=>'0','EfIncMarks'=>'0','EfIncCode'=>'0','EfExcp'=>'0','EfDecl'=>'0','EfIncUsed'=>'0','EfRuleRem'=>'0','EfAObtMarks'=>'0','EfAMaxMarks'=>'0','EfAResult'=>'0','EfDivGrace'=>'0','EfDivPC'=>'0','EfDiv'=>'0','EfPress'=>'0','EfLimit'=>'0','EfNextSr'=>'0','EfDivShort'=>'0','EfSrNo'=>'0','EfCertNo'=>'0','EfStateNo'=>'0','EfGP_CR'=>'0','EfCredits'=>'0','EfSubGrace'=>'0','EfPassMarks'=>'0','EfMaxGp'=>'0','EfOvrGrace'=>'0','EfTotal'=>'0','EfSnNo'=>'0','EfRemark2'=>'0','EfSubOrd'=>'0','EfIncCnt'=>'0','EfResult2'=>'0','EfSemeResult'=>'0','EfBatch'=>'0','EntryUser'=>$SessionalData['0']['CgId'],'EfGrdPts'=>'','ResProStat'=>'1');
            }
          
            try
            {
                if($mode=='add')
                {
                    $InsertForIcr = new icrregnforms;
                    foreach($IcrData as $IcrKey=>$Data)
                    {
                        $InsertForIcr->$IcrKey = $Data;
                    }
                    $InsertForIcr->save();
                }
                else
                {
                    $users = icrregnforms::where('RgNo', '=',$id)->update(['FullName'=>$FullName,'Name'=>$FullName]);
                }
                
                try
                {
                    if($mode=='add')
                    {
                        $InsertForExamForms = new ExamForms;
                        foreach($ExamformData as $ExmKey=>$Data)
                        {
                            $InsertForExamForms->$ExmKey = $Data;
                        }
                        $InsertForExamForms->save();
                    }
                    else
                    {
                        $users = ExamForms::where('EfRgId', '=',$id)->where('ResProStat', '=','1')->update(["EfObtMarks"=>$TotalObtainedMarks,"EfPC"=>$EfPC,"EfResult"=>$FinalResultStatus,'EfGrdPts'=>$FinalResultClass,"EfRollNo"=>$EfRollNo,"EfCate"=>$EfCate]);
                    }
                    try
                    {
                        if($mode=='Edit')
                        {
                            $OldMarksEntry = CtStuMarksMaster::where('RgNo', '=',$id)->where('EntryFlag','=','1');
                            $OldMarksEntry->delete();
                        }
                        $YearOfPassing=''; $FailCounter=0;
                       
                        foreach($ArrForObtMarks as $SubId => $ObtMarks)
                        {
                            unset($StudentMarksData);
                            $MaxMarks=$ArrForMaxMarks[$SubId];
                            $PassMarks=$ArrForPassMarks[$SubId];
                            $ChildId=$ArrForChildEsId[$SubId];
                            $YearOfPassing=$PassedYear[$SubId];
                            $PrevCenterCode=$PrevCenter[$SubId];
                            $PrevRollNo=$PrevRoll[$SubId];
                            $ArrExtraField=json_encode(array("PrevRollNo"=>$PrevRollNo,"PrevCenter"=>$PrevCenterCode));
                            $PEsId=$ArrEsPSubId[$SubId];

                            if($ObtMarks=='AB'){ $ObtMarks='-11.1'; }
                            if($ObtMarks=='NA')
                            { 
                                $ObtMarks='-12'; 
                                $PassFailStat='-11';
                            }
                            else{
                                $PassFailStat=$ArrForPassFailsStatus[$SubId]; 
                            }
                            unset($StudentMarksData);
                            $StudentMarksData=array("EmId"=>$Selexam,"EsId"=>$PEsId,"ChildEsId"=>$ChildId,"ChildSeq"=>'0',"CgId"=>$SessionalData['0']['CgId'],"RgNo"=>$UniNumber,"EfId"=>'0',"PassMarks"=>$PassMarks,"MaxMarks"=>$MaxMarks,"ObtMarks"=>$ObtMarks,"PassFailStat"=>$PassFailStat,"EntryUser"=>$SessionalData['0']['CgId'],'TObtMarks'=>'0','MarksBeforeSdp'=>'0','PPassFailStat'=>'0','TGrace'=>'0','SGrace'=>'0','ThfMarks'=>'0','EntryFlag'=>'1','GradeDesc'=>$YearOfPassing,'IsReval'=>'0',"Extra"=>$ArrExtraField);//,"IpAddress"=>$IpsAddress,"MacAddress"=>$MacAddress  
                            $InsertForStuMarks = new CtStuMarksMaster;
                            $InsertForStuMarksLog = new CtStuMarksMasterLog;
                            foreach($StudentMarksData as $MarkKey=>$Data)
                            {
                                $InsertForStuMarks->$MarkKey = $Data;
                                $InsertForStuMarksLog->$MarkKey = $Data;
                            }
                            $InsertForStuMarks->save();
                            $InsertForStuMarksLog->save();
                        }
                        
                        $ArrSubjectMasterDetails=ExamSub::select('EsId','EsMaxMarks','EsPassMarks')->where('EsStrId', '=',$Selbranch)->where('EsEmId', '=',$Selexam)->where('EntryStat', '=','1')->where('EsType', '=','S')->orderBy('EsPrint', 'asc')->orderBy('EsId', 'asc')->get();
                        $Decoded_ArrSubjectMasterDetails=json_decode($ArrSubjectMasterDetails,true);
                        unset($ArrForSubjectPassFailStat);
                        unset($CoreSubject);
                       //print_r($ArrEsPSubId);
                        foreach($Decoded_ArrSubjectMasterDetails as $ArrForS_Lvl_Id)
                        {
                            unset($MarksClub);unset($StudentMarksData);$SubjectPassFailStat=1;  unset($SubjectChildWisePassFail);
                            $SubjectId=$CoreSubject[]=$ArrForS_Lvl_Id['EsId'];
                            if (CtStuMarksMaster::where('ChildEsId', '=','|'.$SubjectId.'|')->where('EntryFlag', '=','1')->where('RgNo', '=',$UniNumber)->where('EmId', '=',$Selexam)->exists()) 
                            {

                            }
                            else
                            {
                                $SubjectEsMaxMarks=$ArrForS_Lvl_Id['EsMaxMarks'];
                                $SubjectEsPassMarks=$ArrForS_Lvl_Id['EsPassMarks'];unset($MarksClub);
                                foreach($ArrForObtMarks as $SubId => $ObtMarks)
                                {
                                    if($ArrEsPSubId[$SubId]==$SubjectId)
                                    {
                                        $MarksClub[]=$ObtMarks;
                                        $SubjectChildWisePassFail[]=$ArrForPassFailsStatus[$SubId];
                                    }
                                }
                               // print_r($SubjectChildWisePassFail);die();
                                $TotalMarks=array_sum($MarksClub);
                                $UniqueMarks=array_unique($MarksClub);
                                if(in_array('NA',$UniqueMarks) and sizeof($UniqueMarks)=='1'){
                                    $SubjectPassFailStat='-11'; 
                                }
                                elseif($TotalMarks<$SubjectEsPassMarks)
                                {  
                                    $SubjectPassFailStat=0; 
                                    $FailCounter++;
                                }
                                elseif(in_array('0',$SubjectChildWisePassFail))
                                {  
                                    $SubjectPassFailStat=0; $FailCounter++;
                                }
                                if($SubjectPassFailStat!='-11'){  $ValidPassfailStat[]=$SubjectPassFailStat; }
                               
                                $ArrForSubjectPassFailStat[]=$SubjectPassFailStat;
                                $StudentMarksData=array("EmId"=>$Selexam,"EsId"=>$SubjectId,"ChildEsId"=>'|'.$SubjectId.'|',"ChildSeq"=>'0',"CgId"=>$SessionalData['0']['CgId'],"RgNo"=>$UniNumber,"EfId"=>'0',"PassMarks"=>$SubjectEsPassMarks,"MaxMarks"=>$SubjectEsMaxMarks,"ObtMarks"=>$TotalMarks,"PassFailStat"=>$SubjectPassFailStat,"EntryUser"=>$SessionalData['0']['CgId'],'TObtMarks'=>'0','MarksBeforeSdp'=>'0','PPassFailStat'=>'0','TGrace'=>'0','SGrace'=>'0','ThfMarks'=>'0','EntryFlag'=>'1','GradeDesc'=>$YearOfPassing,'IsReval'=>'0');
                            
                                $InsertForStuMarks1 = new CtStuMarksMaster;
                                $InsertForStuMarksLog1 = new CtStuMarksMasterLog;
                                foreach($StudentMarksData as $MarkKey=>$Data)
                                {
                                    $InsertForStuMarks1->$MarkKey = $Data;
                                    $InsertForStuMarksLog1->$MarkKey = $Data;
                                }
                                $InsertForStuMarks1->save();
                                $InsertForStuMarksLog1->save();
                            }
                        }

                       // print_r($ValidPassfailStat);
                          //result process
                        if(in_array('0',$ValidPassfailStat))
                        {
                        //ECHO $FailCounter.'='.$AtktCriteria[$Selexam];DIE();
                            if(!empty($AtktCriteria[$Selexam]) and $FailCounter==$AtktCriteria[$Selexam])
                            {
                                $FinalResultStatus='K';
                            }
                            else{
                                $FinalResultStatus='F';
                            }
                            
                        }
                        elseif(!in_array('0',$ValidPassfailStat))
                        {
                            $FinalResultStatus='P';
                        }
                        if($FinalResultStatus=='P' and $TotalObtainedMarks < $EmPassPC)
                        { 
                            $FinalResultStatus='F';
                        }
                        if($FinalResultStatus=='F' and sizeof($ValidObtMarks)=='1'and in_array('AB',$ValidObtMarks) )
                        {
                            $FinalResultStatus='AB';   
                        }
                        if($FinalResultStatus=='AB'){  $FinalResultStatus='A'; }
                        
                    
                        $FinalResultClass='-';
                        if($FinalResultStatus=='P')
                        { 
                            foreach($CLASSCODES as $ArrClass)
                            {
                                $CdCode=$ArrClass['CdCode'];
                                $CdSeq=$ArrClass['CdSeq'];
                                if($CdSeq<=$EfPC) 
                                {
                                    $FinalResultClass=$CdCode;
                                }
                            }
                        }
                        else{
                            $FinalResultClass='-';
                        }
                    $users = ExamForms::where('EfRgId', '=',$UniNumber)->where('ResProStat', '=','1')->update(["EfObtMarks"=>$TotalObtainedMarks,"EfPC"=>$EfPC,"EfResult"=>$FinalResultStatus,'EfGrdPts'=>$FinalResultClass]);
                    }
                    catch(Exception $e)
                    {
                        return Response::json(['errors' =>'Something went wrong please try again...!']);
                    }
                }
                catch(Exception $e)
                {
                    return Response::json(['errors' =>'Something went wrong please try again...!']);
                }
              
            }
            catch(Exception $e)
            {
				return Response::json(['errors' =>'Something went wrong please try again...!']);
			}
            if($mode=='Edit')
            {
                $Msg='Student Details Updated Successfully';
            }
            else
            {
                $Msg='Student Details Saved Successfully';
            }
            $returnHTML = view('StuMarksEntrySave',compact('SpCl_value'))->RENDER();
            return response()->json(['errors'=>$Msg,'html'=>$returnHTML]);
        }
        return Response::json(['ValidatorErrors' => $qtvalidator->errors()]);
      
        //return response()->json(['html'=>$returnHTML]);
        
    } 

}
