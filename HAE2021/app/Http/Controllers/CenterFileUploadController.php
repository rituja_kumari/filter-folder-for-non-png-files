<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CollegeModel;
use App\DOA;

class CenterFileUploadController extends Controller
{
    public function fileUpload()
    {
        return view('admin');
    }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fileUploadPost(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:pdf,xlx,csv|max:2048',
        ]);
  
        $fileName = session('data')['0']['CgCode'].'.'.$request->file->extension();  
        if(\File::exists(public_path('uploads'), $fileName))
        {
            \File::delete(public_path('uploads'), $fileName);
        }
        $request->file->move(public_path('uploads'), $fileName);
        
        $affected = DOA::where('CgCode', session('data')['0']['CgCode'])
        ->update([
            'CsAuthFile' => $fileName
            ]);

        return back()
            ->with('success','You have successfully upload file.')
            ->with('file',$fileName);
   
    }

}
