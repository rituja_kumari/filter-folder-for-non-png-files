<?php

namespace App\Http\Controllers;
use App\Exam;
use App\Faculty;
use App\Course;
use App\Branch;
use App\Subjects;

use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        $faculty = Faculty::where('EntryStat','1')
        ->pluck('FcDesc','id')
        ->all();
       
        return view('SubjectMasterAdd',compact('faculty'));
    }

    public function store(Request $request)
    {
       $EsCoId=$request->input('EsCoId');
       $EsName=$request->input('EsName');
       $EsCode=$request->input('EsCode');
       $EsPassMarks=$request->input('EsPassMarks');
       $EsExclTot=$request->input('EsExclTot');
       $EsDistMarks=$request->input('EsDistMarks');
       $EsCredit=$request->input('EsCredit');
       $EsPrint=$request->input('EsPrint');

        //insertion 
       $insert_obj=new Subjects();
       $insert_obj->EsCoId=$EsCoId;
       $insert_obj->EsName=$EsName;
       $insert_obj->EsCode=$EsCode;
       $insert_obj->EsPassMarks=$EsPassMarks;
       $insert_obj->EsExclTot=$EsExclTot;
       $insert_obj->EsDistMarks=$EsDistMarks;
       $insert_obj->EsCredit=$EsCredit;
       $insert_obj->EsPrint=$EsPrint;
       $insert_obj->save();
       $request->session()->flash('msg','Record Added Successfully...!');
             return redirect('es-list');
    }

    public function show(Request $request)
    {
        return view('SubjectMaster')->with('ListArr',Subjects::select("*")->get());
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }


    public function findcourse(Request $request,$id)
    {
        $course = Course::where('CfFcId',$id)
        ->where('EntryStat','1')
        ->get();
        return response()->json($course); 
    }


}
