<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Response;
use Redirect,File;
use App\notification_master;

use Notification;
use App\Http\Controllers\NotificationController;
use App\CollegeModel;


use App\Notifications\OffersNotification;


use App\Http\Controllers\auth;
use notifications;
class NotificationMaster extends Controller
{
    
    public function index()
    { 
        $notification_master = notification_master::where('EntryStat', 1)->where('NotifyStat', 1)->get();
        return view('pages.content-notification',['notification_master'=>$notification_master]);
    }
    public function NotificationMasterTbl()
    {
        $notification_master = notification_master::where('EntryStat', 1)->where('NotifyStat', 1)->get();
         $returnHTML = view('NotificationMasterTbl',compact('notification_master'))->RENDER();//die();
         return response()->json(['html'=>$returnHTML]);
    } 
    public function notification_list(){
       echo $notifications = auth()->user()->Notification()->get();
    
        //return view('users.notification.index')->with('notifications',$notifications);
    }
    public function Delete($id)
    {
        // DB::enableQuerylog();
        $Isdel=DB::table('notification_master')
            ->where('NotifyId', $id)
            ->update(['NotifyStat' => 0]);
        //  dd(DB::getQuerylog());die();
        if($Isdel)
        {
          return redirect('notificationMaster')->with('status', 'Notification Details Deleted...!');;
        }
        else
        {
          return redirect('notificationMaster')->with('status', 'Unable to Delete Notification Details...!');;
        }
    }
   
    public function SaveRecords(Request $request)
    {
        $input = $request->input();
        $qtvalidator = Validator::make($request->all(), [
        'NotiTitle'  => 'required',
        'EndDate'  => 'required',
        'StartDate'  => 'required',
        'NotifyFilePath'  => 'required|mimes:pdf',
        ],
        [
            'NotiTitle.required' => 'Please Select Title',
            'NotifyFilePath.required' => 'Please upload notification',
        ]);

        if ($qtvalidator->passes()) 
        {
            $fileName = time().'.'.$request->NotifyFilePath->extension();  
            $request->NotifyFilePath->move('uploads/notification', $fileName);
            $Insnotification = new notification_master;
            $Insnotification->NotifyFor = '0';
            $Insnotification->UsrRole = 'C';
            $Insnotification->UsrDeptId = '-11';
            $Insnotification->SnNo = '-11';
            $Insnotification->ExmId = '-11';
            $Insnotification->CfFcId = '-11';
            $Insnotification->CfCoId = '-11';
            $Insnotification->BrId = '-11';
            $Insnotification->EmId = '-11';
            $Insnotification->NotifyStat = '1';
            $Insnotification->EntryStat = '1';
            $Insnotification->EntryUser = '1';
            $Insnotification->FromDate =$request->StartDate;
            $Insnotification->ToDate = $request->EndDate;
            $Insnotification->NotifyLevel = '0';
            $Insnotification->NotifyFilePath = 'uploads/notification/'.$fileName;
            $Insnotification->NotiTitle = $request->NotiTitle;
            $Insnotification->save();
         
            if($Insnotification)
            {
              
                $SuceeMsg="Notifcation Details Save successfully...!";
                //$userSchema = CollegeModel::select('CgCode','CgName','CfCgPrinEmail','CfCgPrinContact','CfCgPrinName','CgId')
                //$userSchema = CollegeModel::where('CfCgPrinEmail','<>',null)->where('CfCgPrinContact','<>',null)->skip(0)->take(1)->get();
                $userSchema = CollegeModel::where('CfCgPrinEmail','<>',null)->where('CfCgPrinContact','<>',null)->get();
                $user = DB::table('notification_master')
                ->select('NotifyId')
                ->where('NotifyFilePath','uploads/notification/'.$fileName)
                ->where('NotiTitle',$request->NotiTitle)
                ->where('EntryStat','1')
                ->where('NotifyStat','1')
                ->skip(0)->take(1)->get();
                
                $GetId= json_decode($user,true);
                $offerData = [
                    'NotiTitle' => $request->NotiTitle,
                    'body' => 'You received an offer.',
                    'thanks' => 'Thank you',
                    'offerText' => 'Check out the offer',
                    'offerUrl' => url('/uploads/notification/'.$fileName),
                    'NotifyId' => $GetId[0]['NotifyId']
                ];
                
               
                foreach($userSchema as $subscriber)
                {
                    $Arr=json_decode($subscriber,true);
                    $Email=$subscriber->CfCgPrinEmail;
                   // $Email='anujabenkar1994@gmail.com';
                    Notification::route('mail' ,$Email) //Sending mail to subscriber
                        ->notify(new OffersNotification($offerData,$Arr)); //With new post
                    Notification::send($subscriber, new OffersNotification($offerData,$Arr));
                }
       
            }
            else
            {
                $SuceeMsg="Unable to save notification details.PLease try again...!";
            }
            $returnHTML = $this->NotificationMasterTbl();
            return response()->json(['html'=>$returnHTML->original['html'],'errors' => $SuceeMsg]);
        }
        return Response::json(['ValidatorErrors' => $qtvalidator->errors()]);
    }
}
