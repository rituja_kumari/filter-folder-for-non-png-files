<?php

namespace App\Http\Controllers;
use App\Faculty;
use Illuminate\Http\Request;
use Response;
class FacultyController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
         return view('FacultyMasterAdd');
    }

    public function store(Request $request)
    {


      $FcCode=$request->input('FcCode');
      $FcDesc=$request->input('FcDesc');
     
     //chk if duplicate
        $isExist = Faculty::select("*")
                  ->where('FcCode', $FcCode)
                  ->where('EntryStat','1')
                  ->exists();

   
        if ($isExist) 
        {
         // dd('found');
          $request->session()->flash('msg','Faculty Code Already Exist...!');
          return redirect('fac-create');
        }
        else{
          //return $request->input();
          $insert_obj=new Faculty();
          $insert_obj->FcCode=$FcCode;
          $insert_obj->FcDesc=$FcDesc;
          $insert_obj->EntryStat='1';
          $insert_obj->FcSpFlag=0;
          $insert_obj->SFacId=0;
          $insert_obj->SFacCode=0;
          $insert_obj->save();
          $request->session()->flash('msg','Record Added Successfully...!');
          return redirect('fac-show');
        }
        
    }

    public function show(Request $request)
    {
         return view('FacultyMaster')->with('ListArr',Faculty::all());
        //  return view('FacultyMaster')->with('ListArr',Faculty::where('EntryStat','1')->get());
    }
   
    public function edit($id)
    {
      $ListArr = Faculty::where('CfFacId',$id)->get(); 
      return view('FacultyMasterEdit',compact('ListArr'));
        //return view('FacultyMasterEdit')->with('ListArr',Faculty::find($id));
    }

    public function update(Request $request, $id)
    {
        //print_r($request->input());

        $FcCode=$request->input('FcCode');
        $FcDesc=$request->input('FcDesc');
        //chk if duplicate
        $isExist = Faculty::select("*")
                  ->where('FcCode', $FcCode)
                  ->where('EntryStat','1')
                  ->where('CfFacId','<>', $id)
                  ->exists();

   
        if ($isExist) 
        {
          //dd('found');
          $request->session()->flash('msg','Faculty Code Already Exist...!');
          //return view('FacultyMasterEdit');
          return redirect('fac-edit/'.$id);
          
        }
        else
        {
          //dd('update');
           Faculty::where('CfFacId', $id)->update([
             'FcCode' => $FcCode,
             'FcDesc' => $FcDesc,
             'EntryStat' => '1',
            ]);
          /*$insert_obj= Faculty::find($request->id);
          $insert_obj->FcCode=$FcCode;
          $insert_obj->FcDesc=$FcDesc;
          $insert_obj->EntryStat='1';
          $insert_obj->save();*/

          $request->session()->flash('msg','Record Updated Successfully...!');
          return redirect('fac-show');
        }
    }

    public function destroy(Request $request, $id)
    {
      // print_r($id);die;
       $update_obj=Faculty::find($request->id);
      //  $update_obj->EntryStat='0';
      //  $update_obj->save();
       Faculty::destroy(array('id',$id));
       $request->session()->flash('msg','Record Deleted Successfully...!');
       return redirect('fac-show');
    }
}
