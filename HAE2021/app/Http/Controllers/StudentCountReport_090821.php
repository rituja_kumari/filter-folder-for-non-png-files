<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Exam;
use App\ExamForms;
use App\CollegeModel;
use App\ctchangelog;

use PDF;
class StudentCountReport extends Controller
{
    private $CollegeModel;
    private $Exam;
    private $ctchangelog;
    public function __construct(Request $request)
    {
        $this->CollegeModel=CollegeModel::select('CgId','CgName','CgCode','DsName')->orderBy('CgCode', 'asc')->get();
        $this->Exam=Exam::select('EmId','EmName','CfCoId')->get();
        $this->ctchangelog=ctchangelog::select('user_id','activity_id')->where('activity_for','FinalSubmit')->get();
    }

    public function index()
    {
      $SpCl_value="ReportlistExmWise";
        $CountDetails = ExamForms::select('EfEmId','EfCgId',DB::raw('count(*) as num'))->where('ResProStat','1')->groupBy('EfEmId', 'EfCgId')->get();
        return view('pages.content-StuCountRpt', ['CountDetails' => $CountDetails,'Exam' => $this->Exam,'CollegeModel' => $this->CollegeModel,'ctchangelog' => $this->ctchangelog,'Selexam'=>'','SpCl_value'=>$SpCl_value]);
    } 
   
    public function ShowReportListExamWise($id=NULL,Request $request)
    {
      $Selexam=$request->input('Selexam');
      $SpCl_value="ReportlistExmWise";
      $CountDetails = ExamForms::select('EfEmId','EfCgId',DB::raw('count(*) as num'))->where('ResProStat','1')->where('EfEmId',$Selexam)->groupBy('EfEmId', 'EfCgId')->get();
      $Exam=Exam::select('EmId','EmName','CfCoId')->where('EmId',$Selexam)->get();
      $returnHTML = view('StudentCountTbl', ['CountDetails' => $CountDetails,'Exam' => $Exam,'CollegeModel' => $this->CollegeModel,'ctchangelog' => $this->ctchangelog,'Selexam'=>$Selexam,'SpCl_value'=>$SpCl_value])->RENDER();
      return response()->json(['html'=>$returnHTML]);  
    }
    function export($EmId=0)
    {
      
        $EmId=get_decrypt(base64_decode($EmId));
        if($EmId!='0')
        {
            $Exam=Exam::select('EmId','EmName','CfCoId')->where('EmId',$EmId)->get();
            $CountDetails = ExamForms::select('EfEmId','EfCgId',DB::raw('count(*) as num'))->where('ResProStat','1')->where('EfEmId',$EmId)->groupBy('EfEmId', 'EfCgId')->get();
        }
        else
        {
            $Exam=$this->Exam;
            $CountDetails = ExamForms::select('EfEmId','EfCgId',DB::raw('count(*) as num'))->where('ResProStat','1')->groupBy('EfEmId', 'EfCgId')->get();
        }
        $SpCl_value="PDF";
        //return  $returnHTML = view('StudentCountTbl',['CountDetails' => $CountDetails,'Exam' => $Exam,'CollegeModel' => $this->CollegeModel,'ctchangelog' => $this->ctchangelog,'Selexam'=>$EmId,'SpCl_value'=>$SpCl_value])->RENDER();
       
        $pdf = \App::make('dompdf.wrapper');
        $pdf->setPaper('legal', 'landscape');
        $pdf = PDF::loadView('StudentCountTbl', ['CountDetails' => $CountDetails,'Exam' => $Exam,'CollegeModel' => $this->CollegeModel,'ctchangelog' => $this->ctchangelog,'Selexam'=>$EmId,'SpCl_value'=>$SpCl_value]);
        $pdf->setPaper('legal', 'landscape');
      return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
    }
 
}
