<?php

namespace App\Http\Controllers;
use App\Http\Controllers\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use App\Course;
use App\ExamSub;
use App\icrregnforms;
use App\ExamForms;
use App\CtStuMarksMaster;
use App\CtStuMarksMasterLog;
use App\Exam;
use App\codes;
use App\ctchangelog;
use App\MarksheetSchema;
use App\CollegeModel;
use App\notification_master;
use App\Notifications\PublishNotification;
use PDF;
use Mpdf\Mpdf;


class PublishLedgerController extends Controller
{
    public function index()
    {
        return view('pages.content-publishLed');
    }
     
    public function CollegeResultLedger()
    {

        $SessionalData=json_decode(Session('data'),true);
        $CgId=$SessionalData['0']['CgId'];
        $CgCode=$SessionalData['0']['CgCode'];
        $PUBLISH_LEDGER_Details = ctchangelog::select('activity_id')->where('activity_for','PUBLISH_LEDGER')->where('user_id',$CgId)->get();
        //unset($ArrForExamId);
        $ArrForExamId=array();
        foreach($PUBLISH_LEDGER_Details as $DetEmId)
        {
            $ArrForExamId[]=$DetEmId->activity_id;
        }
       
        $Exams = Exam::select('EmId','EmName','CfBrId','CfCoId')->distinct()->whereIn('EmId',$ArrForExamId)->where('EntryStat','1')->get();
        return view('CollegeResultLedger',["Exams"=>$Exams,'CgId'=>$CgId,'CgCode'=>$CgCode]);
    }
   
    
    public function Publish_BULK_Ledger($id=NULL)
    {
        $CgCode=get_decrypt(base64_decode($id));
        $pathToFile='uploads/ATD2/'.$CgCode.'.pdf';
        return response()->file($pathToFile);
    }
    public function LedgerList($id=NULL,Request $request)
    {
        //return view('pages.content-publishLed');
        $Selexam=$request->Selexam;
       
        $Exams = Exam::select('CfBrId','CfCoId')->distinct()->where('EmId',$Selexam)->where('EntryStat','1')->get();
       
        $CfBrId=$Exams[0]->CfBrId;
        $CfCoId=$Exams[0]->CfCoId;
        $ExamForms = ExamForms::select('EfCgId')->where('EfEmId',$Selexam)->where('ResProStat','1')->get();
        $ctchangelog = ctchangelog::select('user_id')->where('activity_for','FinalSubmit')->where('activity_id',$Selexam)->get();
        $PUBLISH_LEDGER_Details = ctchangelog::select('user_id')->where('activity_for','PUBLISH_LEDGER')->where('activity_id',$Selexam)->get();
        
        unset($ArrOfActiveCgId);
        foreach($ExamForms as $aa)
        {
            $ArrOfActiveCgId[]= $aa->EfCgId;
        }
   
        $CollegeModel = CollegeModel::select('CgId','CgCode','CgName')->whereIn('CgId',$ArrOfActiveCgId)->get();
        $returnHTML = view('PublishLedgerList',compact('CollegeModel','CfCoId','CfBrId','Selexam','ctchangelog','PUBLISH_LEDGER_Details'))->RENDER();
        return response()->json(['html'=>$returnHTML]);  
    } 
    
    public function SavePublishLedger($id=NULL,Request $request)
    {
        $Selexam=$request->Selexam;
        $ArrToPublish=$request->ArrToPublish;
    
        if(empty($ArrToPublish))
        {    
            return Response::json(['errors' =>'Please select Atleast one checkbox...!']);
        }
    
        foreach($ArrToPublish as $CgId)
        {
            $InsertForctchangelog = new ctchangelog;
            $InsertForctchangelog->user_id = $CgId;
            $InsertForctchangelog->activity = $Selexam;
            $InsertForctchangelog->activity_id = $Selexam;
            $InsertForctchangelog->activity_for ='PUBLISH_LEDGER';
            $InsertForctchangelog->save(); 
        }
   
        $returnHTML = view('PublishLedgerSave')->RENDER();
        return response()->json(['errors'=>"Ledger Published Successfully",'html'=>$returnHTML]);  
    }

}
