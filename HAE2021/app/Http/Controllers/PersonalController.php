<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;
use Response;
use App\Pwd;
use App\Category;
use App\Personal;
use App\StudLogin;
use App\CollegeModel;
use App\District;
use App\Address;
use App\CourseInfo;
use App\Course;
use App\Qualification;


class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $StuId=session('studata')[0]['StuId'];
        $studlogin = StudLogin::get()->where('StuId',$StuId);
        $pwd = Pwd::all(); 
        $district = District::all(); 
        $category = Category::all(); 
        $colleges = CollegeModel::all();

        foreach($studlogin as $val)
        {
            $stname=$val->stname;
            $StuEmail=$val->StuEmail;
            $StuMob=$val->StuMob;
            $StuDob=$val->StuDob;
        }
      

        $courses = DB::table("course_info")
        ->leftjoin('colleges', 'colleges.CgId', '=', 'course_info.CgId')
        ->leftjoin('courses', 'courses.CfCoId', '=', 'course_info.course_id')
        ->where("course_info.StuId",$StuId)
        ->get();

        return view('pages.content-form',['pwd'=>$pwd,'category'=>$category,'stname'=>$stname,'StuEmail'=>$StuEmail,'StuMob'=>$StuMob,'StuDob' =>$StuDob,'colleges'=>$colleges,'district'=>$district,'courses'=>$courses]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stpvalidator = Validator::make($request->all(), [
             'stname' => 'required',
             'stmobile' => 'required|max:10',
             'stemail' => 'required',
             'gender' => 'required',
             'pwd' => 'required',
             'birthdate' => 'required'
         ]);
 
        $input = $request->all();
// dd($stpvalidator->errors());
        if ($stpvalidator->passes()) {
            $personal = new Personal();
            $StuId=session('studata')[0]['StuId'];
            $personal->StuId=$StuId;
            $personal->cname=$request->input('stname');
            $personal->mobile=$request->input('stmobile');
            $personal->email=$request->input('stemail');
            $personal->gender=$request->input('gender');
            $personal->pwd=$request->input('pwd');
            $personal->birthdate=$request->input('birthdate');
            $personal->ph_type=$request->input('ph_type');
            $personal->category=$request->input('category');
            $personal->caste=$request->input('caste');
            $personal->save();
 
            return Response::json(['success' => '1']);
             }
         return Response::json(['errors' => $stpvalidator->errors()]);
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         $stpvalidator = Validator::make($request->all(), [
             'stname' => 'required',
             'stmobile' => 'required|max:10',
             'stemail' => 'required',
             'gender' => 'required',
             'pwd' => 'required',
             'birthdate' => 'required'
         ]);
 

        if ($stpvalidator->passes()) 
        {
            $personal = Personal::find($request->input('personal_id'));
            $personal->gender=$request->input('gender');
            $personal->pwd=$request->input('pwd');
            $personal->birthdate=$request->input('birthdate');
            $personal->ph_type=$request->input('ph_type');
            $personal->category=$request->input('category');
            $personal->caste=$request->input('caste');
            $personal->save();   
            
           return Response::json(['success' => '1']);
        }
         return Response::json(['errors' => $stpvalidator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

}
