<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Login;

class AuthController extends Controller
{
    public function authenticate($username, $password)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
        
        $GetOtp = Login::select("username","password","role","firsttime")
        ->where([
            ['username', '=', $username],
            ['password', '=', $password],
        ])
        ->get();

        return response()->json($GetOtp);
        
        if (Auth::attempt($credentials)) {
            return redirect()->intended('admin');
        }

        return redirect('login')->with('error', 'Oppes! You have entered invalid credentials');
    }
}
