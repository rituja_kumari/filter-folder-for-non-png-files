
    <tr>
        <td></td>
        @foreach($SchemaArray as $SchemaInfo)
        
            @foreach($SchemaInfo['EsPara'] as $Parameter)
             
                @if(($Parameter['SubParaEsId'][0]['SubChildEsId']!='REMARK' and $Mode=='Publish') or ($Mode!='Publish'))
                <td  align="center" colspan="@php echo sizeof($Parameter['SubParaEsId']); @endphp">
                    @if($Parameter['SubParaName']=='NA')
                    @else
                        {{ $SubjectInformation[$Parameter['SubParaName']]['EsName'] }}
                    @endif
                    
                </td>
                @endif
            @endforeach
        @endforeach
    </tr>
    <tr>
        <td></td>
        @foreach($SchemaArray as $SchemaInfo)
        
            @foreach($SchemaInfo['EsPara'] as $Parameter)
                @foreach($Parameter['SubParaEsId'] as $SubParaameterEsId)
                    @php  
                        $SubParapaperId=$SubParaameterEsId['SubEsId'];
                        $SubParapaperTitle=$SubParaameterEsId['SubTitle'];
                        
                        $ArrPaperId=explode(',',$SubParapaperId);
                        if($SubParapaperId!='-11' and sizeof($ArrPaperId)=='1'){ $SubjectDetails=$SubjectInformation[$SubParapaperId];}
                        if($SubParapaperTitle=='fetchfromdb')
                        {
                            $title=$SubjectDetails['EsName'];
                        }
                        else
                        {
                            $title=$SubParapaperTitle;
                        } 
                    @endphp
                    @if(($SubParaameterEsId['SubChildEsId']!='REMARK' and $Mode=='Publish') or ($Mode!='Publish'))
                        <td>@if($title!='NA')  {{ $title}}  @ENDIF</td>
                   @endif
                @endforeach
            @endforeach
        @endforeach
    </tr>
    @php  $count=1; @endphp  
    @foreach($ArrayForMinMax as $INDEX => $Minmax)
        <tr style="background-color: #e8e5e4;">
            <td align="center">{{$Minmax}} </td>
            @foreach($SchemaArray as $SchemaInfo)
        
                @foreach($SchemaInfo['EsPara'] as $Parameter)
                    @foreach($Parameter['SubParaEsId'] as $SubParaameterEsId)
                        @php  
                            $SubParapaperId=$SubParaameterEsId['SubEsId'];
                            $SubpaperChildId=$SubParaameterEsId['SubChildEsId'];
                            $ArrPaperId=explode(',',$SubParapaperId);
                            if($SubParapaperId!='-11' and sizeof($ArrPaperId)=='1')
                            {
                                $SubjectDetails=$SubjectInformation[$SubParapaperId];
                            }
                            elseif($SubParapaperId!='-11' and sizeof($ArrPaperId)> '1' and $SubParapaperId!='-12.00')
                            {
                                unset($ArrOfTotalOfPId);
                                IF($INDEX!='OBT')
                                {
                                    foreach($ArrPaperId as $ArrPId)
                                    {
                                        $PSubjectDetails=$SubjectInformation[$ArrPId];
                                        $ArrOfTotalOfPId[]=$PSubjectDetails[$INDEX];
                                    }
                                    $SubjectDetails[$INDEX]=array_sum($ArrOfTotalOfPId);   
                                }
                            } 
                            elseif($SubpaperChildId=='OBT')
                            {
                                $SubjectDetails=$GrandTotal;
                            } 
                            else
                            {
                                $SubjectDetails=array("EsMaxMarks"=>'','EsPassMarks'=>'');
                            }
                        @endphp
                        @if(($SubParaameterEsId['SubChildEsId']!='REMARK' and $Mode=='Publish') or ($Mode!='Publish'))
                            <td align="center">
                                @IF($INDEX!='OBT') 
                                    @php if($SubjectDetails[$INDEX]>0){ echo (int)$SubjectDetails[$INDEX]; }@endphp 
                                @else
                                    @php echo $count++; @endphp    
                                @ENDIF
                            </td>
                        @endif
                    @endforeach
                @endforeach
    
            @endforeach 
            @if(!empty($DecodesGroupArray))   
                @foreach($DecodesGroupArray as $GroupKey => $SchemaInfo)
                    @if($SchemaInfo['key']=='total')
                        <td align="center" > @IF($INDEX!='OBT')   {{$GrandTotal[$INDEX]}}  @ENDIF</td>        
                    @endif
                
                @endforeach
            @ENDIF
        </tr>           
    @endforeach 
        </thead>
            @foreach($AllExamFormsDetails as $Form)
                @php
                    $ClassName='-';
                    if($Form['EfGrdPts']!='-'){
                        $ClassName=$ClassArrayoFetch[$Form['EfGrdPts']];
                    } 
                    $ExamForReleInfo=array("RESULT"=> $CodesArrayoFetch[$Form->EfResult],"CLASS"=>$ClassName,"OBT"=>$Form['EfObtMarks'],"REMARK"=>"");
                @endphp
                <tr>
                    <td style="text-align:left">{{$Form['FullName']}} </td>
                    <td width="6%">{{$Form['EfRollNo']}}</td>
                    @foreach($SchemaArray as $SchemaInfo)
                
                        @foreach($SchemaInfo['EsPara'] as $Parameter)
                            @foreach($Parameter['SubParaEsId'] as $SubParaameterEsId)
                                    @php  
                                        $PassFailStat=$Marks=$SubjectDetails=$paperId=''; unset($ArrForMarks);
                                        $SubParapaperId=$SubParaameterEsId['SubEsId'];
                                       
                                        $SubParapaperChildId=$SubParaameterEsId['SubChildEsId']; 
                                        if(($SubParapaperChildId!='REMARK' and $Mode=='Publish') or ($Mode!='Publish'))
                                        {
                                            $ArrSubChildEsId=explode(',',$SubParapaperChildId);
                                            $SingleRecordsForStu=$StudentWiseSubjectDetails[$Form['RgNo']];
                                            if(!empty($SingleRecordsForStu))
                                            {
                                                if(sizeof($ArrSubChildEsId)=='1' and !in_array($SubParapaperChildId,$InvalidChildEsId))
                                                {
                                                $PassFailStat=$SingleRecordsForStu[$SubParapaperChildId]['PassFailStat'];
                                                    $Marks=$SingleRecordsForStu[$SubParapaperChildId]['ObtMarks'];
                                                    
                                                }
                                                elseif(sizeof($ArrSubChildEsId)>'1')
                                                {
                                                    foreach($ArrSubChildEsId as $ChildId)
                                                    {
                                                        $ArrForMarks[]=$SingleRecordsForStu[$ChildId]['ObtMarks'];
                                                    }
                                                    $Marks=array_sum($ArrForMarks);

                                                }
                                                elseif(in_array($SubParapaperChildId,$InvalidChildEsId))
                                                {
                                                    $Marks=$ExamForReleInfo[$SubParapaperChildId];
                                                }
                                            
                                                if($Marks=='-11.10')
                                                {
                                                    $Marks='AB';
                                                }
                                                elseif($Marks=='-12.00')
                                                {
                                                    $Marks='NA';
                                                }
                                                elseif($Marks< 0)
                                                {
                                                    $Marks='NA';
                                                }
                                                elseif(is_numeric($Marks))
                                                {
                                                    $Marks=intval($Marks);
                                                }
                                            }
                                            @endphp
                                            <td align="center"  >
                                                @IF($INDEX!='OBT') 
                                                    @if($PassFailStat=='0') <span style="color:red"> {{$Marks}} </span> @else <span> {{$Marks}} </span> @endif
                                                    
                                                @else
                                                    @php echo $count++; @endphp    
                                                @ENDIF
                                            </td>@php
                                        }
                                        @endphp
                            @endforeach
                        @endforeach 
                    @endforeach 
                    @if($Mode=='Publish')
                    <td align="center" >{{$Form['EfPC']}}</td>
                    <td align="center">  @if($Form->EfResult=='A') WITHDRAWL @endif </td>
                    @endif
                </tr>
            @endforeach 