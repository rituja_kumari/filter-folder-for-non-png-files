<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DistrictModel;
use App\CollegeModel;
use App\Course;
use App\CollegeIntakeModel;
use App\ExamForms;
use App\Exports\CollegeExport;
use Excel;
use DB;

class CenterController extends Controller
{
    
    public function index()
    {
        
    }

   
    public function create()
    {

        $district = DistrictModel::where('EntryStat','1')
        ->pluck('DsName','CfDsId')
        ->all();
        $collegelist = CollegeModel::select("*")->whereNotNull('CfCgPrinEmail')
        ->whereNotNull('CfCgPrinContact')
        ->get();
        
        return view('CenterDetails',compact('district','collegelist'));
    }

    public function ShowCollege($id=null,Request $request)
    {
        $CfDsId=$request->input('CfDsId');
        $SpCl_value="ShowCollegeMaster";
        $returnHTML = view('CenterDetailSave',compact('SpCl_value','CfDsId'))->RENDER();
        return response()->json(['html'=>$returnHTML]);  
    }

    public function ShowColData($id=null,Request $request)
    {
        $CfDsId=$request->input('CfDsId');
        $collegelist = CollegeModel::select("*")->where('CfDsId','=',$CfDsId)
        ->get();
        $SpCl_value="showlist";
        $returnHTML = view('CenterDetailSave',compact('SpCl_value','collegelist'))->RENDER();
        return response()->json(['html'=>$returnHTML]);
    }
    public function ShowColDataSingle($id=null,Request $request)
    {
        $center=$request->input('center');
        $collegelist = CollegeModel::select("*")->where('CgId','=',$center)
        ->get();
        $SpCl_value="showlist";
        $returnHTML = view('CenterDetailSave',compact('SpCl_value','collegelist'))->RENDER();
        return response()->json(['html'=>$returnHTML]);

    }
    public function store(Request $request)
    {
        return redirect('center-details');
    }

    
    public function show(Request $request, $CfDsId)
    {
        
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }

    public function findcolg($id)
    {
        $collegeval = CollegeModel::where('CfDsId',$id)
        ->whereNotNull('CfCgPrinEmail')
        ->whereNotNull('CfCgPrinContact')
        ->get();
        return response()->json($collegeval);
    }

    public function RegisterClg($id)
    {
        if($id==111)
        {
        $Regcollege = CollegeModel::whereNotNull('CfCgPrinEmail')
        ->whereNotNull('CfCgPrinContact')
        ->get();
        }
        else
        {
        $Regcollege = CollegeModel::where('CfDsId',$id)
        ->whereNotNull('CfCgPrinEmail')
        ->whereNotNull('CfCgPrinContact')
        ->get();

        }
        
        return response()->json($Regcollege);
    }

     public function RegisterClgC($id)
    {
        $RegcollegeC = CollegeModel::where('CgId',$id)
        ->whereNotNull('CfCgPrinEmail')
        ->whereNotNull('CfCgPrinContact')
        ->get();
        
        return response()->json($RegcollegeC);
    }
    

   

    public function exportIntoExcel(Request $request)
    {
        // dd($request);
        return Excel::download(new CollegeExport($request->CfDsId, $request->center),'colleges.csv');
    }

    public function exportIntoCsv()
    {
        return Excel::download(new CollegeExport,'colleges.csv');
    }


}
