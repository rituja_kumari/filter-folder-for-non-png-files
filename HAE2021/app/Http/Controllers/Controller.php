<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     function json_response( $status , $data=null , $message=null ){
    	$json;
    	if($status){
    		$json['status']='success';
    		$json['data']=count($data)?$data:null;
    		$json['message']=$message!=null? $message:count($data).' Records Found';
    	}else{
    		$json['status']='success';
    		$json['errors']=count($data)?$data:null;
    		$json['message']=$message!=null?$message:'Something went wrong';

    	}
    	return response()->json($json);
    }

}
