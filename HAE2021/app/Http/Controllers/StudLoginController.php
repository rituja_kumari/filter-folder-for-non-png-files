<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\StudLogin;
use App\Status;
use App\SendOtpModel;
use App\SecureQue;
use Illuminate\Support\Facades\Validator;
use Response;

class StudLoginController extends Controller
{
    public function index()
    {

    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    // student registration validation
     public function registers(Request $request)
    {
        $stvalidators = Validator::make($request->all(), [
            'StuEmail' => 'required|email|unique:studlogin',
            'sotp1' => 'required',
            'StuMob' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|unique:studlogin',
            'sotp2' => 'required',
            'StuPass' => 'required',
            'StuPassC' => 'required',
            'StuDob' => 'required',
        ],

        [
            'StuEmail.required' => 'Please Enter Email Id',
            'StuEmail.unique' => 'Email Id already exist',
            'sotp1.required' => 'Enter OTP',
            'StuMob.required' => 'Please Enter Mobile No',
            'StuMob.unique' => 'This Mobile Number is already exist',
            'StuMob.max' => 'Length Should not be greater than 10',
            'sotp2.required' => 'Enter OTP',
            'StuPass.required' => 'Please Enter Password',
            'StuPassC.required' => 'Please Re-Enter Password',
            'StuDob.required' => 'Please Select Date Of Birth',
        ]);

        // $input = $request->all();

        if ($stvalidators->passes()) {

        //chk for empty mobile verify and email verify
         $MoVerify = SendOtpModel::select("MobileVerify")
        ->where([
            ['MoEmId', '=', $request->StuMob],
        ])->get();

         if($MoVerify->isEmpty() || $MoVerify[0]['MobileVerify']!='Y')
        {
            return response()->json(['Mo_error' => '1']);
        }

        $EmailVerifyOfPrc = SendOtpModel::select("EmailVerify")
            ->where([
                ['MoEmId', '=', $request->StuEmail],
            ])->get();

         if($EmailVerifyOfPrc->isEmpty() || $EmailVerifyOfPrc[0]['EmailVerify']!='Y')
        {
            return response()->json(['Em_error' => '1']);
        }   

        // insert data into studlogin table
        
        $StuEmail=$request->input('StuEmail');
        $stname=$request->input('StuName');
         $StuMob=$request->input('StuMob');
         $StuPass=$request->input('StuPass');
         $StuPassC=$request->input('StuPassC');
         $StuDob=$request->input('StuDob');

           
        $id = DB::table('studlogin')->insertGetId(
            [
                'stname'=>$stname,
                'StuEmail'=>$StuEmail,
                'StuMob'=>$StuMob,
                'StuPass'=>$StuPass,
                'StuDob'=>$StuDob,
                'EntryStat'=>'1',
               ]
            );
          
           
            if ($id) {
            $status=new Status();
            $status->StuId=$id;
            $status->save();
                /*if($status->save())
                {
                    $msg_details ="https://control.msg91.com/api/sendhttp.php?authkey=115487ASLXYRme3U5757b138&mobiles=".$request->input('StuMob')."&message=Dear%20Sir,%20Your%20user%20name:-".$request->input('StuEmail')."%20and%20Password%20:-%20".$request->input('StuPass')."%20from%20DOA-Powered%20by%20SMB.&sender=SMBONL&route=4&DLT_TE_ID=1307161518429402176";
                    $send_msg = file_get_contents($msg_details);  
                }*/
            } 

           return Response::json(['success' => '1']);

        }
        return Response::json(['errors' => $stvalidators->errors()]);
    }

    public function SendOtp($MoNo)  /*send mobile OTP */
    {
        $StuId = SendOtpModel::select("id")
        ->where([
            ['MoEmId', '=', $MoNo],
        ])
        ->get();
        
        $fourRandomDigit = mt_rand(1000,9999);
        if(empty($StuId) || sizeof($StuId)==0 )
        {
            $StuInUp= SendOtpModel::create([ 
                'MoEmId' => $MoNo,
                'MobileOtp' => $fourRandomDigit
            ]);
        }
        elseif(!empty($StuId))
        {
            $StuInUp = SendOtpModel::where([
                ['MoEmId', '=', $MoNo],
                ])
              ->update([
                  'MobileOtp' => $fourRandomDigit
                  ]);
        }
        if(!empty($StuInUp)) // sms send
        {
            $msg_details ="https://control.msg91.com/api/sendhttp.php?authkey=115487ASLXYRme3U5757b138&mobiles=".$MoNo."&message=Dear%20User,%20Your%20OTP%20for%20mobile%20verification%20is%20:-%20".$fourRandomDigit."%20from%20DOA-%20Powered%20by%20SMB.&sender=SMBONL&route=4&DLT_TE_ID=1307161518438747032";
            $send_msg = file_get_contents($msg_details); 
        }
        return response()->json($StuInUp);
    }

     public function VerifyStuOtp($MoOtp, $MoNo) 
     {
        //student mobile verification code here
        $GetOtp = SendOtpModel::select("MobileOtp")
        ->where([
            ['MoEmId', '=', $MoNo],
        ])
        ->get();
       
        if($GetOtp[0]['MobileOtp']==$MoOtp)
        {
            $StuVrUp = SendOtpModel::where([
                ['MoEmId', '=', $MoNo],
                ])
              ->update([
                  'MobileVerify' => 'Y'
                  ]);
        }
        return response()->json($GetOtp);
     }

     public function SendEmailOtp($Email)  /*send Email OTP*/
     {
        $StuEmailId = SendOtpModel::select("id")
        ->where([
            ['MoEmId', '=', $Email],
        ])
        ->get();

        $fourRandomDigit = mt_rand(1000,9999);
        if(empty($StuEmailId) || sizeof($StuEmailId)==0 )
        {
            $StuEmInUp= SendOtpModel::create([
                'MoEmId' => $Email,
                'EmailOtp' => $fourRandomDigit
            ]);
        }
        elseif(!empty($StuEmailId))
        {
            $StuEmInUp = SendOtpModel::where([
                ['MoEmId', '=', $Email],
                ])
              ->update([
                  'EmailOtp' => $fourRandomDigit
                  ]);
        }

        if(!empty($StuEmInUp))
        {
            $details = [
                'title' => 'Dear user, Greetings from DOA.',
                'body' => '
                Your OTP for email-id verification is '.$fourRandomDigit
            ];
           
            \Mail::to($Email)->send(new \App\Mail\MyTestMail($details));
            return response()->json($StuEmInUp);
            //return view('registration');
        }
     }

     public function VerifyStuEmailOtp($mailotp, $email)
     {
        $GetEmailOtp = SendOtpModel::select("EmailOtp")
        ->where([
            ['MoEmId', '=', $email]
            ])
        ->get();
        if($GetEmailOtp[0]['EmailOtp']==$mailotp)
        {
            $StuVrEUp = SendOtpModel::where([
                ['MoEmId', '=', $email]
                ])
              ->update([
                  'EmailVerify' => 'Y'
                  ]);
        }
        return response()->json($GetEmailOtp);
     }

    /* public function slogin(Request $request)
    {
        $svalidator = Validator::make($request->all(), [
            'lemail' => 'required',
            'lpassword' => 'required'
        ]);

        $input = $request->all();

        if ($svalidator->passes()) {

            $Getuserdetails = StudLogin::select("StuEmail","StuPass")
            ->where([
            ['StuEmail', '=', $request->lemail],
            ['StuPass', '=', $request->lpassword],
            ])->get();

          echo  $UserCount = $Getuserdetails->count(); 
            if($UserCount > 0)
            {
                echo "hii".$request->session()->put('data',$Getuserdetails);
            
                return Response::json(['success' => '1']);
                return redirect('admin');
            }
            else{
            return Response::json(['fail' => '1']);
            }
        }
        return Response::json(['errors' => $svalidator->errors()]);
    }*/
}
