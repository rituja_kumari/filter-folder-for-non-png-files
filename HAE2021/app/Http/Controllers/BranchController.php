<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Branch;
use App\Faculty;
use App\Course;
use App\codes;
use Illuminate\Support\Facades\DB;
class BranchController extends Controller
{
   
    public function index()
    {
        //
    }

    
    public function create()
    {
        $faculty = Faculty::where('EntryStat','1')
        ->pluck('FcDesc','CfFacId')
        ->all();

        $admopn= codes::where('CdType','OTHER')
        ->pluck('CdDesc','CdSeq')->all();

        return view('BranchMasterAdd',compact('faculty','admopn'));
    }

    
    public function store(Request $request)
    {
       // return $request->input();
        $branch_code=$request->input('branch_code');
        $branch_name=$request->input('branch_name');
        $CfCoId=$request->input('CoId');
        $IsActive=$request->input('IsActive');

        $Ccode= Course::select('CoCode')->where('CfCoId',$CfCoId)->get(); 
        foreach ($Ccode as  $value) {
             $coursecode=$value->CoCode;
        }

        $isExist = Branch::select("*")
                  ->where('branch_code', $branch_code)
                  ->where('EntryStat','1')
                  ->exists();

        if ($isExist) 
        {
            $request->session()->flash('msg','Branch Code Already Exist...!');
            return redirect('br-create');
        }
        else
        {
            $insert_obj=new Branch();
             $insert_obj->branch_code=$branch_code;
             $insert_obj->branch_name=$branch_name;
             $insert_obj->course_code=$coursecode;
             $insert_obj->CfCoId=$CfCoId;
             $insert_obj->pattern_code='NA';
             $insert_obj->pattern_name='Not Applicable';
             $insert_obj->PBrId='0';
             $insert_obj->IsActive=$IsActive;
             $insert_obj->EntryStat='1';
             $insert_obj->save();

             $request->session()->flash('msg','Record Added Successfully...!');
             return redirect('br-list');
        }
    }

    public function show(Request $request)
    {
        return view('BranchMaster')->with('ListArr',Branch::where('EntryStat','1')->get());
    }

    
    public function edit(Request $request,$id)
    {
         $ListArr = Branch::where('CfBrId',$id)->get(); 
         $faculty = Faculty::where('EntryStat', '1')
         ->pluck('FcDesc','CfFacId')->all();
         $course = Course::where('EntryStat', '1')
         ->pluck('CoName','CfCoId')->all();
         $admopn= codes::where('CdType','OTHER')
        ->pluck('CdDesc','CdSeq')->all();

 /*   $coid=Branch::select('CfCoId')->where('CfBrId', $id)->get();
        foreach ($coid as  $value) {
             echo $a= $value->CfCoId; 
        }

    $query = DB::table('Faculty as f')->select('f.FcDesc','f.CfFacId')->join('courses as c','f.CfFacId','=','c.CfFcId')->where('CfCoId', '=', $a)->pluck('f.FcDesc','f.CfFacId')->all();*/
         
    return view('BranchMasterEdit',compact('ListArr','faculty','course','admopn'));
    }

    
    public function update(Request $request, $id)
    {
       // print_r($request->input());
        $branch_code=$request->input('branch_code');
        $branch_name=$request->input('branch_name');
        $CfCoId=$request->input('CfCoId');
        //$FcId=$request->input('FcId');
        $IsActive=$request->input('IsActive');

        $Ccode= Course::select('CoCode')->where('CfCoId',$CfCoId)->get(); 
        foreach ($Ccode as  $value) {
             $coursecode=$value->CoCode;
        }

        $isExist = Branch::select("*")
                  ->where('branch_code', $branch_code)
                  ->where('EntryStat','1')
                  ->where('CfBrId','<>', $id)
                  ->exists();

        if ($isExist) 
        {
            $request->session()->flash('msg','Branch Code Already Exist...!');
            return redirect('br-edit/'.$id);
        }
        else
        {
            Branch::where('CfBrId', $id)->update([
             'branch_code' => $branch_code,
             'branch_name' => $branch_name,
             'course_code' => $coursecode,
             'CfCoId' =>$CfCoId,
             'IsActive' =>$IsActive,
            // 'FcId' => $request->input('FcId'),
             'EntryStat' => '1',
            ]);

            $request->session()->flash('msg','Record Updated Successfully...!');
            return redirect('br-list');
        }
    }

    
    public function destroy(Request $request, $id)
    {
        Branch::where('CfBrId', $id)->update([
            'EntryStat' => '0',
        ]);
        $request->session()->flash('msg','Record Deleted Successfully...!');
        return redirect('br-list');
    }

    public function findcoursebr(Request $request,$id)
    {

        $course = Course::where('CfFcId',$id)
        ->where('EntryStat','1')
        ->get();
        return response()->json($course); 

   /* $course=Course::select('CoName','CfCoId')->where('CfFcId',$request->id)->take(100)->get();
    return response()->json($course);   */
    }
}
