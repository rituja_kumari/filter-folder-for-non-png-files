<?php

namespace App\Http\Controllers;

use App\DOA;
use App\StateModel;
use App\DistrictModel;
use App\CollegeModel;
use App\Faculty;
use App\SendOtpModel;
use App\Login;
use App\StudLogin;
use App\Course;
use App\CollegeIntakeModel;
use App\ExamForms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Support\Facades\DB;
class DOAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = StateModel::pluck('StateName','StateId')->all();
        $CoIds = Course::pluck('CoName','CfCoId')->all();
        return view('registration',compact('states','CoIds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'PrcName' => 'required',
            'Mobile' => 'required'
        ]);
        DOA::create([
            'username' => $request->PrcName,
            'password' => $request->Mobile,
            'role' => 'C',
            'firsttime' => 'Y'
        ]);
        $request->session()->flash('msg','Data Submited');
        return view('registration');
                        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DOA  $dOA
     * @return \Illuminate\Http\Response
     */
    public function show(DOA $dOA)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DOA  $dOA
     * @return \Illuminate\Http\Response
     */
    public function edit(DOA $dOA)
    {
        //
    }
    
    public function qtpaperupload(Request $request)
    {
    $qtvalidator = Validator::make($request->all(), [
    'qtsession' => 'required',
    'qtfaculty' => 'required',
    'qtcourse' => 'required',
    'qtbranch' => 'required',
    'qtsemester' => 'required',
    'qtsubject' => 'required',
    'qtpaper' => 'required',
    'qtlivedate' => 'required',
    'qtlivetime' => 'required',
    'qttimetill' => 'required',
    'qtpass' => 'required',
    'qtconfirmpass' => 'required'
    ]);

    $input = $request->all();

    if ($qtvalidator->passes()) {

    return Response::json(['success' => '1']);

    }
    return Response::json(['errors' => $qtvalidator->errors()]);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:10',
            'password' => 'required|min:4',
        ]);

        $input = $request->all();
        
        if ($validator->passes()) 
        {
            $Getuserdetails = Login::select("username","password","role","firsttime")
            ->where([
                ['username', '=', $request->username],
                ['password', '=', $request->password],
            ])
            ->get();
            //return $Getuserdetails[0]['role'];
            
            $UserCount = $Getuserdetails->count();
            if($UserCount > 0)
            {
                if($request->username=='admindoa' or $request->username=='exxon')
                {
                    $RegCollege = CollegeModel::select('CgId')
                    ->where([
                        ['CfCgPrinContact', '!=', ''],
                        ['CfCgPrinEmail', '!=', ''],
                        ['CgCode', '!=', '999']
                    ])
                    ->get();
                    $StuCountRegis = ExamForms::where('ResProStat','1')->get()->count();
                    $StuCountPass = ExamForms::where('EfResult','P')->where('ResProStat','1')->get()->count();
                    $StuCountFail = ExamForms::where('EfResult','F')->where('ResProStat','1')->get()->count();
                    $StuCountAbsent = ExamForms::where('EfResult','A')->where('ResProStat','1')->get()->count();
                    $StuCountATKT = ExamForms::where('EfResult','K')->where('ResProStat','1')->get()->count();
                    $request->session()->put(['data' => $Getuserdetails, 'RegCollege' => $RegCollege,'StudentCount' => $StuCountRegis,'StuCountPass' => $StuCountPass,'StuCountFail' => $StuCountFail,'StuCountAbsent' =>$StuCountAbsent,'StuCountATKT'=>$StuCountATKT]);
                }
                else
                {
                    $CollegeDetails = CollegeModel::select("CgId","CgCode","CgName","CfCgPrinName","CfCgPrinContact","CfCgPrinEmail","CgLandLineNo","CoFName","CoFMobile","CoFEmail","CoDesigNation","BankName","BankAcNo","BankIFSCNo","BankBranchName","CfAdd1","CfCoId","CfAdd2","CfAdd3","CfAdd4","CfAdd5","Taluka","TnName","CfPinCod","CfDsId","CsAuthFile")
                    ->where([
                        ['CgCode', '=', $request->username],  
                    ])
                    ->get();
                    //return $CollegeDetails[0]['CfDsId'];
                    $district = DistrictModel::select("DsName")
                    ->where([
                        ['CfDsId', '=', $CollegeDetails[0]['CfDsId']],
                    ])
                    ->get();
                    
                    $GetCourseDetails = CollegeIntakeModel::select("CfCoId","InGovt","InAided","InUnAided","AdGovt","AdAided","AdUnAided")->orderBy('CfCoId')
                    ->where([
                        ['CgId', '=', $CollegeDetails[0]['CgId']],
                        ['EntryStat', '=', '1'],
                    ])
                    ->get();

                    $ArrForStudentType=array();
                    if (sizeof($Getuserdetails)!=0) 
                    {
                        
                        unset($ArrOfCoId);$ArrOfCoId=array();$GetCoName=array();
                        foreach($GetCourseDetails as $CId)
                        {
                            $ArrOfCoId[]=$CId['CfCoId'];
                            //anuja
                            $ArrForStudentType[$CId['CfCoId']] =
                            DB::table('qualification')->select('TypeId','Type')->distinct()->where('CourseCode',$CId['CfCoId'])->get();
                        }
                    
                        if (sizeof($ArrOfCoId)!=0) 
                        {
                            
                        
                        $GetCoName= Course::select("CoName")->orderBy('CfCoId')
                        ->whereIn('CfCoId', $ArrOfCoId)
                        ->get();
                        }
                        else
                        {$ArrOfCoId="";}
                    }else{$GetCoName="";}
                    
                    $request->session()->put(['data' => $CollegeDetails, 'Dist' => $district, 'CourseWiseIntake' => $GetCourseDetails, 'UserDetail' => $Getuserdetails, 'CoName' => $GetCoName,'StudentType'=>($ArrForStudentType)]);
                }
                
                return Response::json(['success' => '1']);
                //return redirect('admin');
                //return Response::json(['success' => '1','checkuser' => $Getuserdetails]);
            }
            else{
                return Response::json(['fail' => '1']);
            }
        }
        return Response::json(['errors' => $validator->errors()]);
    }

    public function rspassowd(Request $request) // CENTER RESET PASSWORD
    {
        $qsphototpvalidator = Validator::make($request->all(), [
        'cnewps' => 'required|min:4',
        'cconfirmpass' => 'required'
        ]);

        $input = $request->all();

        if ($qsphototpvalidator->passes()) 
        {
            $rspasscenter = Login::where('username', session('data')['0']['CgCode'])
            ->update([
            'password' => $request->input('cconfirmpass'),
            'role' => 'C',
            ]);
            if($rspasscenter==1)
            {
                return Response::json(['success' => '1']);
            }

        }
        return Response::json(['errors' => $qsphototpvalidator->errors()]);
    }
 //center-forgot-password
 public function centerforgotps(Request $request)
 {
     $cforgotvalidate = Validator::make($request->all(), [
         'cuname' => 'required',
         'cfpemail' => 'required',
          'cfpotp' => 'required'
     ]);

     $input = $request->all();

     if ($cforgotvalidate->passes()) {

         return Response::json(['success' => '1']);

     }
     return Response::json(['errors' => $cforgotvalidate->errors()]);
 }

    public function randomPassword() {
        $alphabet = 'abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    public function AddCourse($cgid,$coid,$ingovt,$inaided,$inunaided,$adgovt,$adaided,$adunaided)
    {
        $ChekDuplicate = CollegeIntakeModel::select("id")
            ->where([
                ['CgId', '=', $cgid],
                ['CfCoId', '=', $coid],
                ['EntryStat', '=', '1'],
            ])
            ->get();
            $UserCount = $ChekDuplicate->count();
            if($UserCount > 0)
            {
                return response()->json(['duplicate' => '1']);
            }
        $CoInsert=CollegeIntakeModel::create([
            'CgId' => $cgid,
            'CfCoId' => $coid,
            'InGovt' => $ingovt,
            'InAided' => $inaided,
            'InUnAided' => $inunaided,
            'AdGovt' => $adgovt,
            'AdAided' => $adaided,
            'AdUnAided' => $adunaided,
            'EntryStat' => '1'
        ])
        ->get();
        return Response::json(['success' => '1']);

    }

    public function CourseDel($id)
    {
        $affectedup = CollegeIntakeModel::where('id', $id)
        ->update([
            'EntryStat' => '0'
            ]);
            
            if($affectedup==1)
            {
                return redirect()->route('registration')
                    ->with('error','Data Deleted Successfully');
            }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DOA  $dOA
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //return $request->input('CfCoId');
        // if($request->input('CfCoId')=='')
        // {
        //     $implodecfcoid=array("1","2");
            
        // }
        // else{
        // $implodecfcoid="|".implode("|",$request->input('CfCoId'))."|";
        // }
        //dd($request->College);
        
        $validatedData = Validator::make($request->all(), [
            'state' => 'required',
            'District' => 'required',
            'College' => 'required',
            'CfAdd1' => 'required',
            'CfAdd2' => 'required',
            'CfAdd3' => 'required',
            'CfAdd4' => 'required',
            'CfAdd5' => 'required',
            'Taluka' => 'required',
            'TnName' => 'required',
            'CfPinCod' => 'required',
            'CfCgPrinName' => 'required',
            'CgLandLineNo' => 'required|max:20',
            'CfCgPrinContact' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|unique:colleges',
            'CfCgPrinEmail' => 'required|email|unique:colleges',
            'CoFName' => 'required',
            'CoDesigNation' => 'required',
            'CoFMobile' => 'required|unique:colleges|max:10',
            'CoFEmail' => 'required|email|unique:colleges',
            'BankName' => 'required',
            'BankBranchName' => 'required',
            'BankAcNo' => 'required|unique:colleges',
            'reBankAcNo' => 'required',
            'BankIFSCNo' => 'required'
            
        ],
        [
            'state.required' => 'Please Select State',
            'District.required' => 'Please Select District',
            'College.required' => 'Please Select College',
            'CfAdd1.required' => 'Please Enter Flat/Room/Block/House No',
            'CfAdd2.required' => 'Please Enter Name of Premises/ Building',
            'CfAdd3.required' => 'Please Enter Road/ Street/Post Office',
            'CfAdd4.required' => 'Please Enter Area Locality',
            'CfAdd5.required' => 'Please Enter Landmark',
            'Taluka.required' => 'Please Enter Taluka',
            'TnName.required' => 'Please Enter Village/City',
            'CfPinCod.required' => 'Please Enter Pincode',
            'CfCgPrinName.required' => 'Please Enter Principal Name',
            'CgLandLineNo.required' => 'Please Enter LandLine Number',
            'CgLandLineNo.max' => 'Length Should be less than 20',
            'CfCgPrinContact.required' => 'Please Enter Mobile Number',
            'CfCgPrinContact.unique' => 'This Mobile Number is already exist',
            'CfCgPrinContact.max' => 'Length Should not be greater than 10',
            'CfCgPrinEmail.required' => 'Please Enter Email Id',
            'CfCgPrinEmail.unique' => 'Email Id already exist',
            'CoFName.required' => 'Please Enter Coordinator Name',
            'CoDesigNation.required' => 'Please Enter Designation',
            'CoFMobile.required' => 'Please Enter Mobile Number',
            'CoFMobile.unique' => 'This Mobile Number is already exist',
            'CoFMobile.max' => 'Length Should not be greater than 10',
            'CoFEmail.required' => 'Please Enter Email Id',
            'CoFEmail.unique' => 'Email Id already exist',
            'BankName.required' => 'Please Enter Bank Name',
            'BankBranchName.required' => 'Please Enter Branch Name',
            'BankAcNo.required' => 'Please Enter Account Number',
            'BankAcNo.unique' => 'Bank Account Number already exist',
            'reBankAcNo.required' => 'Re-Enter Bank Account Number',
            'BankIFSCNo.required' => 'Please Enter IFSC Number',

        ]);
        
        if ($validatedData->passes()) 
        {
            $MoVerify = SendOtpModel::select("MobileVerify")
            ->where([
                ['CgId', '=', $request->College],
                ['MoEmId', '=', $request->CfCgPrinContact],
            ])->get();
           
            //print_r($MoVerify);
            if($MoVerify->isEmpty() || $MoVerify[0]['MobileVerify']!='Y')
            {
                return response()->json(['Mo_error' => '1']);
            }
            
            $EmailVerifyOfPrc = SendOtpModel::select("EmailVerify")
            ->where([
                ['CgId', '=', $request->College],
                ['MoEmId', '=', $request->CfCgPrinEmail],
            ])->get();
           
            if($EmailVerifyOfPrc->isEmpty() || $EmailVerifyOfPrc[0]['EmailVerify']!='Y')
            {
                return response()->json(['Em_error' => '1']);
            }

            $ChekDuplicate = CollegeIntakeModel::select("id")
            ->where([
                ['CgId', '=', $request->College],
                ['EntryStat', '=', '1'],
            ])
            ->get();
            $UserCount = $ChekDuplicate->count();
            if($UserCount == 0)
            {
                return response()->json(['ErrorCourse' => '1']);
            }
            // $CoMoVerify = SendOtpModel::select("MobileVerify")
            // ->where([
            //     ['CgId', '=', $request->College],
            //     ['MoEmId', '=', $request->CoFMobile],
            // ])->get();
           
            // if($CoMoVerify->isEmpty() || $CoMoVerify[0]['MobileVerify']!='Y')
            // {
            //     return response()->json(['CoMo_error' => '1']);
            // }

            // $EmailVerifyOfCo = SendOtpModel::select("EmailVerify")
            // ->where([
            //     ['CgId', '=', $request->College],
            //     ['MoEmId', '=', $request->CoFEmail],
            // ])->get();
           
            // if($EmailVerifyOfCo->isEmpty() || $EmailVerifyOfCo[0]['EmailVerify']!='Y')
            // {
            //     return response()->json(['Emco_error' => '1']);
            // }
                
            $affected = DOA::where('CgId', $request->input('College'))
              ->update([
                  'CfCgPrinName' => $request->input('CfCgPrinName'),
                  'CgLandLineNo' => $request->input('CgLandLineNo'),
                  'CfCgPrinContact' => $request->input('CfCgPrinContact'),
                  'CfCgPrinEmail' => $request->input('CfCgPrinEmail'),
                  'CoFName' => $request->input('CoFName'),
                  'CoDesigNation' => $request->input('CoDesigNation'),
                  'CoFMobile' => $request->input('CoFMobile'),
                  'CoFEmail' => $request->input('CoFEmail'),
                  'CfAdd1' => $request->input('CfAdd1'),
                  'CfAdd2' => $request->input('CfAdd2'),
                  'CfAdd3' => $request->input('CfAdd3'),
                  'CfAdd4' => $request->input('CfAdd4'),
                  'CfAdd5' => $request->input('CfAdd5'),
                  'Taluka' => $request->input('Taluka'),
                  'TnName' => $request->input('TnName'),
                  'CfPinCod'=> $request->input('CfPinCod'),
                  'CfCoId' =>  $request->input('CfCoId'),
                  'BankName' => $request->input('BankName'),
                  'BankAcNo' => $request->input('BankAcNo'),
                  'BankIFSCNo' => $request->input('BankIFSCNo'),
                  'BankBranchName' => $request->input('BankBranchName')
                  ]); //$implodecfcoid="|".implode("|",$request->input('CfCoId'))."|"
                $PasswordInst=$this->randomPassword();
                if($affected==1)
                {
                    $LognUser=Login::create([
                        'username' => $request->CgCode,
                        'password' => $PasswordInst,
                        'role' => 'C',
                        'firsttime' => 'Y'
                    ]);
                    $msg_details ="https://control.msg91.com/api/sendhttp.php?authkey=115487ASLXYRme3U5757b138&mobiles=".$request->input('CfCgPrinContact')."&message=Dear%20Sir,%20Your%20user%20name:-".$request->input('CgCode')."%20and%20Password%20:-%20".$PasswordInst."%20from%20DOA-Powered%20by%20SMB.&sender=SMBONL&route=4&DLT_TE_ID=1307161518429402176";
                    $send_msg = file_get_contents($msg_details);    
                }

                return Response::json(['success' => '1']);

        }
        return Response::json(['errors' => $validatedData->errors()]);
            
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DOA  $dOA
     * @return \Illuminate\Http\Response
     */
    public function destroy(DOA $dOA)
    {
        //
    }

    public function finddistrict($id)
    {
        $district = DistrictModel::where('StateId',$id)->get();
        return response()->json($district);
    }

    public function findcollege($id)
    {
        $collegeval = CollegeModel::where('CfDsId',$id)->get();
        return response()->json($collegeval);

    }

    public function findcollegecode($id)
    {
        $cgcode = CollegeModel::select("CgCode","CfCgPrinName","CfCgPrinContact","CfCgPrinEmail","CgLandLineNo","CoFName","CoFMobile","CoFEmail","CoDesigNation","BankName","BankAcNo","BankIFSCNo","BankBranchName","CfAdd1","CfCoId","CfAdd2","CfAdd3","CfAdd4","CfAdd5","Taluka","TnName","CfPinCod")
        ->where("CgId", $id)
        ->get();
        //$cgcode = CollegeModel::where('CgId',$id)->get();
        return response()->json($cgcode);
    }

    public function GetCourse($id)
    {
        
        $GetDetailsCourse = CollegeIntakeModel::select("id","CfCoId","InGovt","InAided","InUnAided","AdGovt","AdAided","AdUnAided")->orderBy('CfCoId')
        ->where([
            ['CgId', '=', $id],
            ['EntryStat', '=', '1'],
        ])
        ->get();
        
        // $CoName = Course::select("CoName")
        // ->where([
        //     ['CfCoId', '=', '1']
        // ])
        // ->get();
        // return $CoName;
        return response()->json($GetDetailsCourse);
    }

    public function SendOtp($clgid, $MoNo) // Send Otp to principle mo no
    {
        // $checkmobile = CollegeModel::select("CfCgPrinContact")
        // ->where([
        //     ['CfCgPrinContact', '=', $MoNo],
        // ])
        // ->get();
        // if($checkmobile[0]['CfCgPrinContact']!=null or $checkmobile[0]['CfCgPrinContact']!='')
        // {
        //     return response()->json(['mobileduplicate' => '1']);
        // }
        $PrcId = SendOtpModel::select("id")
        ->where([
            ['CgId', '=', $clgid],
            ['MoEmId', '=', $MoNo],
        ])
        ->get();
        //return response()->json($PrcId);
        //dd($PrcId);
        $fourRandomDigit = mt_rand(1000,9999);
        if(empty($PrcId) || sizeof($PrcId)==0 )
        {
            $PrcInUp= SendOtpModel::create([
                'CgId' => $clgid,
                'MoEmId' => $MoNo,
                'MobileOtp' => $fourRandomDigit
            ]);
        }
        elseif(!empty($PrcId))
        {
            $PrcInUp = SendOtpModel::where([
                ['CgId', '=', $clgid],
                ['MoEmId', '=', $MoNo],
                ])
              ->update([
                  'MobileOtp' => $fourRandomDigit
                  ]);
        }
        if(!empty($PrcInUp)) // sms send
        {
            $msg_details ="https://control.msg91.com/api/sendhttp.php?authkey=115487ASLXYRme3U5757b138&mobiles=".$MoNo."&message=Dear%20User,%20Your%20OTP%20for%20mobile%20verification%20is%20:-%20".$fourRandomDigit."%20from%20DOA-%20Powered%20by%20SMB.&sender=SMBONL&route=4&DLT_TE_ID=1307161518438747032";
            $send_msg = file_get_contents($msg_details); 
        }
        return response()->json(['success' => '1']);
    }

    public function VerifyPrcOtp($MoOtp, $MoNo, $colid) // verify principle Mobile 
    {
        $GetOtp = SendOtpModel::select("MobileOtp")
        ->where([
            ['CgId', '=', $colid],
            ['MoEmId', '=', $MoNo],
        ])
        ->get();
       
        if($GetOtp[0]['MobileOtp']==$MoOtp)
        {
            $PrcVrUp = SendOtpModel::where([
                ['MoEmId', '=', $MoNo],
                ['CgId', '=', $colid]
                ])
              ->update([
                  'MobileVerify' => 'Y'
                  ]);
        }
        return response()->json($GetOtp);   
    }

    public function SendEmailOtp($CollegeId, $Email) // Send otp to principle email id
    {
        // $checkmobile = CollegeModel::select("CfCgPrinEmail")
        // ->where([
        //     ['CfCgPrinEmail', '=', $Email],
        // ])
        // ->get();
        // if($checkmobile[0]['CfCgPrinEmail']!=null or $checkmobile[0]['CfCgPrinEmail']!='')
        // {
        //     return response()->json(['emailduplicate' => '1']);
        // }
        $PrcEmailId = SendOtpModel::select("id")
        ->where([
            ['CgId', '=', $CollegeId],
            ['MoEmId', '=', $Email],
        ])
        ->get();
        
        $fourRandomDigit = mt_rand(1000,9999);
        if(empty($PrcEmailId) || sizeof($PrcEmailId)==0 )
        {
            $PrcEmInUp= SendOtpModel::create([
                'CgId' => $CollegeId,
                'MoEmId' => $Email,
                'EmailOtp' => $fourRandomDigit
            ]);
        }
        elseif(!empty($PrcEmailId))
        {
            $PrcEmInUp = SendOtpModel::where([
                ['CgId', '=', $CollegeId],
                ['MoEmId', '=', $Email],
                ])
              ->update([
                  'EmailOtp' => $fourRandomDigit
                  ]);
        }
        if(!empty($PrcEmInUp))
        {
            $details = [
                'title' => 'Dear user, Greetings from DOA.',
                'body' => '
                Your OTP for email-id verification is '.$fourRandomDigit
            ];
           
            \Mail::to($Email)->send(new \App\Mail\MyTestMail($details));
        }
       
        return response()->json(['success' => '1']);
        return view('registration');
                        //->view('registration');
    }

    public function newMobileemailotp(Request $request) // for new center 
    {
         
        $validatedData = Validator::make($request->all(), [
            'state' => 'required',
            'District' => 'required',
            'College' => 'required',
            'CfCgPrinName' => 'required',
            'CfCgPrinContact' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|unique:colleges',
            'CfCgPrinEmail' => 'required|email|unique:colleges',
            
        ],
        [
            'state.required' => 'Please Select State',
            'District.required' => 'Please Select District',
            'College.required' => 'Please Select College',
            'CfCgPrinName.required' => 'Please Enter Principal Name',
            'CfCgPrinContact.required' => 'Please Enter Mobile Number',
            'CfCgPrinContact.unique' => 'This Mobile Number is already exist',
            'CfCgPrinContact.max' => 'Length Should not be greater than 10',
            'CfCgPrinEmail.required' => 'Please Enter Email Id',
            'CfCgPrinEmail.unique' => 'Email Id already exist',
            
        ]);
            $input = $request->all();
    
            if ($validatedData->passes()) 
            {
                $PrcEmailId = SendOtpModel::select("id")
                ->where([
                    ['CgId', '=', $request->input('College')],
                    ['MoEmId', '=', $request->input('CfCgPrinContact')],
                ])
                ->get();
                
                $fourRandomDigit = mt_rand(1000,9999);
                if(empty($PrcEmailId) || sizeof($PrcEmailId)==0 )
                {
                    $PrcEmInUp= SendOtpModel::create([
                        'CgId' => $request->input('College'),
                        'MoEmId' => $request->input('CfCgPrinContact'),
                        'MobileOtp' => $fourRandomDigit
                    ]);
                }
                elseif(!empty($PrcEmailId))
                {
                    $PrcEmInUp = SendOtpModel::where([
                        ['CgId', '=', $request->input('College')],
                        ['MoEmId', '=', $request->input('CfCgPrinContact')],
                        ])
                      ->update([
                          'MobileOtp' => $fourRandomDigit
                          ]);
                }
                if(!empty($PrcEmInUp))
                {
                    $details = [
                        'title' => 'Dear user, Greetings from DOA.',
                        'body' => '
                        Your OTP for email-id verification is '.$fourRandomDigit
                    ];
                   
                    \Mail::to($request->input('CfCgPrinEmail'))->send(new \App\Mail\MyTestMail($details));

                    $msg_details ="https://control.msg91.com/api/sendhttp.php?authkey=115487ASLXYRme3U5757b138&mobiles=".$request->input('CfCgPrinContact')."&message=Dear%20User,%20Your%20OTP%20for%20mobile%20verification%20is%20:-%20".$fourRandomDigit."%20from%20DOA-%20Powered%20by%20SMB.&sender=SMBONL&route=4&DLT_TE_ID=1307161518438747032";
                    $send_msg = file_get_contents($msg_details);

                    return response()->json(['success' => '1']);
                }
                else
                {
                    return response()->json(['fail' => '1']);
                }
     
            }
        return Response::json(['errors' => $validatedData->errors()]);
    }


    public function NewMobileVerify(Request $request) // verify new principle Mobile 
    {
        $input = $request->all();
        $GetOtp = SendOtpModel::select("MobileOtp")
        ->where([
            ['CgId', '=', $request->input('College')],
            ['MoEmId', '=', $request->input('CfCgPrinContact')],
        ])
        ->get();
       
        if($GetOtp[0]['MobileOtp']==$request->input('MobileOtp'))
        {
            $PrcVrUp = SendOtpModel::where([
                ['MoEmId', '=', $request->input('CfCgPrinContact')],
                ['CgId', '=', $request->input('College')]
                ])
              ->update([
                  'MobileVerify' => 'Y'
                  ]);
        }
        return response()->json($GetOtp);   
    }

    public function newcenterregistration(Request $request)
    {
        $newreg = Validator::make($request->all(), [
                'state' => 'required',
                'District' => 'required',
                'College' => 'required',
                'CfCgPrinName' => 'required',
                'CfCgPrinContact' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|unique:colleges',
                'CfCgPrinEmail' => 'required|email|unique:colleges',
            ]);

        $input = $request->all();  

        if ($newreg->passes()) 
        {
            $MoVerify = SendOtpModel::select("MobileVerify")
            ->where([
                ['CgId', '=', $request->College],
                ['MoEmId', '=', $request->CfCgPrinContact],
            ])->get();
           
            //print_r($MoVerify);
            if($MoVerify->isEmpty() || $MoVerify[0]['MobileVerify']!='Y')
            {
                return response()->json(['Mo_error' => '1']);
            }

            $affected = DOA::where('CgId', $request->input('College'))
              ->update([
                  'CfCgPrinName' => $request->input('CfCgPrinName'),
                  'CfCgPrinContact' => $request->input('CfCgPrinContact'),
                  'CfCgPrinEmail' => $request->input('CfCgPrinEmail'),
                  ]); //$implodecfcoid="|".implode("|",$request->input('CfCoId'))."|"
                $PasswordInst=$this->randomPassword();
                if($affected==1)
                {
                    $LognUser=Login::create([
                        'username' => $request->input('CenterNo'),
                        'password' => $PasswordInst,
                        'role' => 'C',
                        'firsttime' => 'Y'
                    ]);
                    $msg_details ="https://control.msg91.com/api/sendhttp.php?authkey=115487ASLXYRme3U5757b138&mobiles=".$request->input('CfCgPrinContact')."&message=Dear%20Sir,%20Your%20user%20name:-".$request->input('CenterNo')."%20and%20Password%20:-%20".$PasswordInst."%20from%20DOA-Powered%20by%20SMB.&sender=SMBONL&route=4&DLT_TE_ID=1307161518429402176";
                    $send_msg = file_get_contents($msg_details);    
                }

                return Response::json(['success' => '1']);
        }
        else
        {
            return Response::json(['errors' => $newreg->errors()]);
        }

    }

    public function VerifyPrcEmailOtp($mailotp, $email, $CollegeId) // verify principle email
    {
        $GetEmailOtp = SendOtpModel::select("EmailOtp")
        ->where([
            ['CgId', '=', $CollegeId],
            ['MoEmId', '=', $email],
            ])
        ->get();
        if($GetEmailOtp[0]['EmailOtp']==$mailotp)
        {
            $PrcVrEUp = SendOtpModel::where([
                ['MoEmId', '=', $email],
                ['CgId', '=', $CollegeId]
                ])
              ->update([
                  'EmailVerify' => 'Y'
                  ]);
        }
        return response()->json($GetEmailOtp);
    }

    public function CoFSendMoOtp($clgid, $MoNo) // Send Otp to coordinator mo no
    {
        
        // $checkmobile = CollegeModel::select("CoFMobile")
        // ->where([
        //     ['CoFMobile', '=', $MoNo],
        // ])
        // ->get();
        // if($checkmobile[0]['CoFMobile']!=null or $checkmobile[0]['CoFMobile']!='')
        // {
        //     return response()->json(['mobileduplicate' => '1']);
        // }
        // $CofId = SendOtpModel::select("id")
        // ->where([
        //     ['CgId', '=', $clgid],
        //     ['MoEmId', '=', $MoNo],
        // ])
        // ->get();
        //return response()->json($CofId);
        //dd($CofId);
        $fourRandomDigit = mt_rand(1000,9999);
        if(empty($CofId) || sizeof($CofId)==0 )
        {
            $CofInUp= SendOtpModel::create([
                'CgId' => $clgid,
                'MoEmId' => $MoNo,
                'MobileOtp' => $fourRandomDigit
            ]);
        }
        elseif(!empty($CofId))
        {
            $CofInUp = SendOtpModel::where([
                ['CgId', '=', $clgid],
                ['MoEmId', '=', $MoNo],
                ])
              ->update([
                  'MobileOtp' => $fourRandomDigit
                  ]);
        }
        if(!empty($CofInUp)) // sms send
        {
            $msg_details ="https://control.msg91.com/api/sendhttp.php?authkey=115487ASLXYRme3U5757b138&mobiles=".$MoNo."&message=Dear%20User,%20Your%20OTP%20for%20mobile%20verification%20is%20:-%20".$fourRandomDigit."%20from%20DOA-%20Powered%20by%20SMB.&sender=SMBONL&route=4&DLT_TE_ID=1307161518438747032";
            $send_msg = file_get_contents($msg_details);
        }
        return response()->json(['success' => '1']);
        
    }

    public function VerifyCofOtp($MoOtp, $MoNo, $colid) // verify cooridanator Mobile no
    {
        $GetOtp = SendOtpModel::select("MobileOtp")
        ->where([
            ['CgId', '=', $colid],
            ['MoEmId', '=', $MoNo],
        ])
        ->get();
       
        if($GetOtp[0]['MobileOtp']==$MoOtp)
        {
            $PrcVrUp = SendOtpModel::where([
                ['MoEmId', '=', $MoNo],
                ['CgId', '=', $colid]
                ])
              ->update([
                  'MobileVerify' => 'Y'
                  ]);
        }
        return response()->json($GetOtp);   
    }

    public function SendCofEmailOtp($CollegeId, $Email) // Send otp to coordinator email id
    {
        $PrcEmailId = SendOtpModel::select("id")
        ->where([
            ['CgId', '=', $CollegeId],
            ['MoEmId', '=', $Email],
        ])
        ->get();
        
        $fourRandomDigit = mt_rand(1000,9999);
        if(empty($PrcEmailId) || sizeof($PrcEmailId)==0 )
        {
            $PrcEmInUp= SendOtpModel::create([
                'CgId' => $CollegeId,
                'MoEmId' => $Email,
                'EmailOtp' => $fourRandomDigit
            ]);
        }
        elseif(!empty($PrcEmailId))
        {
            $PrcEmInUp = SendOtpModel::where([
                ['CgId', '=', $CollegeId],
                ['MoEmId', '=', $Email],
                ])
              ->update([
                  'EmailOtp' => $fourRandomDigit
                  ]);
        }
        if(!empty($PrcEmInUp))
        {
            $details = [
                'title' => 'Dear user, Greetings from DOA.',
                'body' => '
                Your OTP for email-id verification is '.$fourRandomDigit
            ];
           
            \Mail::to($Email)->send(new \App\Mail\MyTestMail($details));
        }
       
        return response()->json($PrcEmInUp);
        return view('registration');
                        //->view('registration');
    }

    public function Verifycofemailotp($mailotp, $email, $CollegeId) // verify coordinator email
    {
        $GetEmailOtp = SendOtpModel::select("EmailOtp")
        ->where([
            ['CgId', '=', $CollegeId],
            ['MoEmId', '=', $email],
            ])
            ->get();
        if($GetEmailOtp[0]['EmailOtp']==$mailotp)
        {
            $PrcVrEUp = SendOtpModel::where([
                ['MoEmId', '=', $email],
                ['CgId', '=', $CollegeId]
                ])
              ->update([
                  'EmailVerify' => 'Y'
                  ]);
        }
        //print_r($GetEmailOtp);
        return response()->json($GetEmailOtp);
    }

        //student login
        public function slogin(Request $request)
        {
            $svalidator = Validator::make($request->all(), [
                'lemail' => 'required',
                'lpassword' => 'required'
            ]);
    
            $input = $request->all();
    
            if ($svalidator->passes()) {
                $Getuserdetails = StudLogin::select("StuId","StuEmail","StuPass", "StuMob", "StuDob","stname")
                 ->where([
                 ['StuEmail', '=', $request->lemail],
                 ['StuPass', '=', $request->lpassword],
                 ])->get();
                /* echo $Getuserdetails[0]['StuEmail'];
                 echo $Getuserdetails[0]['StuPass'];*/
 
                 $UserCount = $Getuserdetails->count();
                 if($UserCount > 0)
                 {
                     $request->session()->put('studata',$Getuserdetails);
 
                     return Response::json(['success' => '1']);
                     //return redirect('form');
                 }
                 else
                 {
                      return Response::json(['fail' => '1']);
                 }
     
             }
            return Response::json(['errors' => $svalidator->errors()]);
        }
        // student login
    
    
    // student registration validation
     /*public function registers(Request $request)
        {
            $stvalidators = Validator::make($request->all(), [
                'StuEmail' => 'required',
                'sotp1' => 'required',
                'StuMob' => 'required|min:10',
                'sotp2' => 'required',
                'StuPass' => 'required',
                'StuPassC' => 'required',
                'StuDob' => 'required',
                'squestion1' => 'required',
                'squestion2' => 'required',
                'squestion3' => 'required',
                'squestion4' => 'required',
                'squestion5' => 'required',
                'squestion6' => 'required'
            ]);
    
             //$input = $request->all();
    
            if ($stvalidators->passes()) {
    
               
                return Response::json(['success' => '1']);

            }
            return Response::json(['errors' => $stvalidators->errors()]);
            
        }*/
    
    
    //forgot-login
        public function flogin(Request $request)
        {
            $fvalidator = Validator::make($request->all(), [
                'fusername' => 'required',
                'femailid' => 'required',
                'fmobile' => 'required',
                'fdate' => 'required',
                'fmother' => 'required'
            ]);
    
            $input = $request->all();
    
            if ($fvalidator->passes()) {
    
                return Response::json(['success' => '1']);
    
            }
            return Response::json(['errors' => $fvalidator->errors()]);
        }
    //forgot-lopgin
    
    
        // forgot password
        public function fplogin(Request $request)
        {
            $fpvalidator = Validator::make($request->all(), [
                'fpemail' => 'required',
                'fpotp' => 'required',
                'fpdate' => 'required'
            ]);
    
            $input = $request->all();
    
            if ($fpvalidator->passes()) {
    
                return Response::json(['success' => '1']);
    
            }
            return Response::json(['errors' => $fpvalidator->errors()]);
        }
        // forgotpassword
    
                 // Student Profile
                 public function stprofile(Request $request)
                 {
                     $stpvalidator = Validator::make($request->all(), [
                         'stname' => 'required',
                         'stmobile' => 'required|max:10',
                         'stemail' => 'required',
                         'gender' => 'required',
                         'tab' => 'required',
                         'birthdate' => 'required'
                     ]);
             
                     $input = $request->all();
             
                     if ($stpvalidator->passes()) {
             
                         return Response::json(['success' => '1']);
             
                     }
                     return Response::json(['errors' => $stpvalidator->errors()]);
                 }
                 // Student Profile
         
                 // Communication Address
                 public function staddress(Request $request)
                 {
                     $stpvalidator = Validator::make($request->all(), [
                         'staddress' => 'required',
                         'stcityname' => 'required',
                         'stdistrictname' => 'required',
                         'ststate' => 'required',
                         'stcounrty' => 'required',
                         'postalcode' => 'required|max:6'
                     ]);
             
                     $input = $request->all();
             
                     if ($stpvalidator->passes()) {
             
                         return Response::json(['success' => '1']);
             
                     }
                     return Response::json(['errors' => $stpvalidator->errors()]);
                 }
                 // Communication Address
         
         
                     // Communication Address
                 public function qualification1(Request $request)
                 {
                     $qstpvalidator = Validator::make($request->all(), [
                         'state1' => 'required',
                         'yearname1' => 'required',
                         'monthname1' => 'required',
                         'seatno1' => 'required',
                         'centercode1' => 'required',
                     ]);
             
                     $input = $request->all();
             
                     if ($qstpvalidator->passes()) {
             
                         return Response::json(['success' => '1']);
             
                     }
                     return Response::json(['errors' => $qstpvalidator->errors()]);
                 }
         
                 public function qualification2(Request $request)
                 {
                     $qstpvalidator2 = Validator::make($request->all(), [
                         'state2' => 'required',
                         'yearname2' => 'required',
                         'monthname2' => 'required',
                         'seatno2' => 'required',
                         'centercode2' => 'required',
                     ]);
             
                     $input = $request->all();
             
                     if ($qstpvalidator2->passes()) {
             
                         return Response::json(['success' => '1']);
             
                     }
                     return Response::json(['errors' => $qstpvalidator2->errors()]);
                 }
         
                 public function qualification3(Request $request)
                 {
                     $qstpvalidator3 = Validator::make($request->all(), [
                         'state3' => 'required',
                         'yearname3' => 'required',
                         'monthname3' => 'required',
                         'seatno3' => 'required',
                         'centercode3' => 'required',
                     ]);
             
                     $input = $request->all();
             
                     if ($qstpvalidator3->passes()) {
             
                         return Response::json(['success' => '1']);
             
                     }
                     return Response::json(['errors' => $qstpvalidator3->errors()]);
                 }
                  public function qualification4(Request $request)
                 {
                     $qstpvalidator4 = Validator::make($request->all(), [
                         'state4' => 'required',
                         'yearname4' => 'required',
                         'monthname4' => 'required',
                         'seatno4' => 'required',
                         'centercode4' => 'required',
                     ]);
             
                     $input = $request->all();
             
                     if ($qstpvalidator4->passes()) {
             
                         return Response::json(['success' => '1']);
             
                     }
                     return Response::json(['errors' => $qstpvalidator4->errors()]);
                 }
                 // Communication Address
                 //course
                  public function stcourse(Request $request)
                 {
                     $qscrstpvalidator = Validator::make($request->all(), [
                         'centername' => 'required',
                         'stcrstype' => 'required',
                         'sttype' => 'required'
                     ]);
             
                     $input = $request->all();
             
                     if ($qscrstpvalidator->passes()) {
             
                         return Response::json(['success' => '1']);
             
                     }
                     return Response::json(['errors' => $qscrstpvalidator->errors()]);
                 }
                 //photo
                  public function stphoto(Request $request)
                 {
                     $qsphototpvalidator = Validator::make($request->all(), [
                         'photo' => 'required',
                         'sign' => 'required'
                     ]);
             
                     $input = $request->all();
             
                     if ($qsphototpvalidator->passes()) {
             
                         return Response::json(['success' => '1']);
             
                     }
                     return Response::json(['errors' => $qsphototpvalidator->errors()]);
                 }
         
                //  //reset-ps
                //   public function rspassowd(Request $request)
                //  {
                //      $qsphototpvalidator = Validator::make($request->all(), [
                //          'newps' => 'required',
                //          'cnewps' => 'required'
                //      ]);
             
                //      $input = $request->all();
             
                //      if ($qsphototpvalidator->passes()) {
             
                //          return Response::json(['success' => '1']);
             
                //      }
                //      return Response::json(['errors' => $qsphototpvalidator->errors()]);
                //  }

}
