<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\notification_master;

class NotificationPanel extends Controller
{
  public function index()
  {
      $today = date("Y-m-d");
      $notification_master = notification_master::select('NotiTitle','NotifyFilePath')->where('EntryStat', '=','1')->where('NotifyStat', '=','1')->whereRaw( "(FromDate <= ? AND ToDate >= ?)",   [$today." 00:00:00", $today." 23:59:59"])->orderBy('NotifyId', 'desc')->get();
      return view('pages.content-NotificationPanel',['notification_master' => $notification_master]);
  }
    public function classchange()
    {
      //  $CLASSCODES = codes::select('CdCode','CdDesc','CdSeq')->where('CdType', '=','CLASS')->orderBy('CdSeq', 'asc')->get();
      //  $CLASSCODES=json_decode($CLASSCODES,true);
      //  $aa=DB::select("SELECT EfPC,EfGrdPts,EfRgId FROM `ExamForms` WHERE EfResult='P' ");
      
      //  foreach($aa as $val)
      //  {
      //        $EfPC=$val->EfPC;
      //        $EfGrdPts=$val->EfGrdPts;
      //       echo  $EfRgId=$val->EfRgId;
      //        foreach($CLASSCODES as $ArrClass)
      //       {
      //            $CdCode=$ArrClass['CdCode'];
      //            $CdSeq=$ArrClass['CdSeq'];
      //            if($CdSeq<=$EfPC) 
      //            {
      //                 $FinalResultClass=$CdCode;
      //            }
      //       }
      //       echo $FinalResultClass.'=='.$EfGrdPts.'====>'.$EfPC.'<br>';
      //       $users = ExamForms::where('EfRgId', '=',$EfRgId)->where('ResProStat', '=','1')->update(['EfGrdPts'=>$FinalResultClass]);
      //  }
    }
   
}
