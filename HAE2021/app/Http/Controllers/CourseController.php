<?php

namespace App\Http\Controllers;
use App\Course;
use App\Faculty;
use App\codes;
use Illuminate\Http\Request;
use Response;

class CourseController extends Controller
{
    public function index()
    {
        //
    }

   
    public function create()
    {
        $faculty = Faculty::where('EntryStat','1')
        ->pluck('FcDesc','CfFacId')
        ->all();

        $pattern= codes::where('CdType','PAT')
        ->pluck('CdDesc','CdSeq')->all();

        $CFM= codes::where('CdType','OTHER')
        ->pluck('CdDesc','CdSeq')->all();

        $cotype= codes::where('CdType','CO_TY')
        ->pluck('CdDesc','CdSeq')->all();

        $cocat= codes::where('CdType','CO_CATE')
        ->pluck('CdDesc','CdSeq')->all();

        $coseat= codes::where('CdType','CO_SEAT_TY')
        ->pluck('CdDesc','CdSeq')->all();

        $coval= codes::where('CdType','VALU_TYP')
        ->pluck('CdDesc','CdSeq')->all();
       
        return view('CourseMasterAdd',compact('faculty','pattern','CFM','cotype','cocat', 'coseat', 'coval'));
    }

   
    public function store(Request $request)
    {
        //return $request->input();

        $CoCode=$request->input('CoCode');
        $CoName=$request->input('CoName');
        $CfFcId=$request->input('CfFcId');
        $CfCoShName=$request->input('CfCoShName');
        $CoPattern=$request->input('CoPattern');
        $CoYears=$request->input('CoYears');
        $CoMaxYr=$request->input('CoMaxYr');
        $CoMaxSn=$request->input('CoMaxSn');
        $CfCaryFrwrd=$request->input('CfCaryFrwrd');
        $CoRegDist=$request->input('CoRegDist');
        $CoCate=$request->input('CoCate');
        $CoSeatNo=$request->input('CoSeatNo');
        $CoMarkList=$request->input('CoMarkList');
        $IsScaleDown=$request->input('IsScaleDown');
        $IsExamDept=$request->input('IsExamDept');
        
      
        $CoFcCode= Faculty::select('FcCode')->where('CfFacId',$CfFcId)->get(); 
        foreach ($CoFcCode as  $value) {
             $Faccode=$value->FcCode;
        }
   
    
        if($request->has('CoCalEC')){
            $CoCalEC=$request->input('CoCalEC');
        }else{
            $CoCalEC='0';
        }

        if($request->has('CoStat')){
            $CoStat=$request->input('CoStat');
        }else{
            $CoStat='0';
        }

        if($request->has('IsAcadDept')){
          $IsAcadDept=$request->input('IsAcadDept');
        }else{
            $IsAcadDept='0';
        }

        if($request->has('IsElgDept')){
            $IsElgDept=$request->input('IsElgDept');
        }else{
            $IsElgDept='0';
        }
        
        //chk if duplicate 
        $isExist = Course::select("*")
                  ->where('CoCode', $CoCode)
                  ->where('EntryStat','1')
                  ->exists();

        if ($isExist) 
        {
         // dd('found');
            $request->session()->flash('msg','Course Code Already Exist...!');
          return redirect('co-create');
        }
        else
        {
            $insert_obj=new Course();
            $insert_obj->CoCode=$CoCode;
            $insert_obj->CoName=$CoName;
            $insert_obj->CfFcId=$CfFcId;
            $insert_obj->CoFcCode=$Faccode;
            $insert_obj->CfCoShName=$CfCoShName;
            $insert_obj->CoPattern=$CoPattern;
            $insert_obj->CoYears=$CoYears;
            $insert_obj->CoMaxYr=$CoMaxYr;
            $insert_obj->CoMaxSn=$CoMaxSn;
            $insert_obj->CfCaryFrwrd=$CfCaryFrwrd;
            $insert_obj->CoRegDist=$CoRegDist;
            $insert_obj->CoCate=$CoCate;
            $insert_obj->CoSeatNo=$CoSeatNo;
            $insert_obj->CoMarkList=$CoMarkList;
            $insert_obj->IsScaleDown=$IsScaleDown;
            $insert_obj->IsExamDept=$IsExamDept;
            $insert_obj->IsAcadDept=$IsAcadDept;
            $insert_obj->IsElgDept=$IsElgDept;
            $insert_obj->CoCalEC=$CoCalEC;
            $insert_obj->CoStat=$CoStat;
            $insert_obj->EntryStat='1';
            $insert_obj->save();

            $request->session()->flash('msg','Record Added Successfully...!');
            return redirect('Co-list');
        }
    }

  
    public function show(Request $request)
    {
        return view('CourseMaster')->with('ListArr',Course::where('EntryStat','1')->get());
    }

  
    public function edit(Request $request,$id)
    {
        //return view('CourseMasterEdit')->with('ListArr',Course::find($id));

        $ListArr = Course::where('CfCoId',$id)->get(); 
        $faculty = Faculty::where('EntryStat','1')->pluck('FcDesc','CfFacId')->all();

        $pattern= codes::where('CdType','PAT')
        ->pluck('CdDesc','CdSeq')->all();

        $CFM= codes::where('CdType','OTHER')
        ->pluck('CdDesc','CdSeq')->all();

        $cotype= codes::where('CdType','CO_TY')
        ->pluck('CdDesc','CdSeq')->all();

        $cocat= codes::where('CdType','CO_CATE')
        ->pluck('CdDesc','CdSeq')->all();

        $coseat= codes::where('CdType','CO_SEAT_TY')
        ->pluck('CdDesc','CdSeq')->all();

        $coval= codes::where('CdType','VALU_TYP')
        ->pluck('CdDesc','CdSeq')->all();
        
        return view('CourseMasterEdit',compact('ListArr','faculty','pattern','CFM','cotype','cocat','coseat','coval'));
    }

    
    public function update(Request $request, $id)
    {
       // print_r($request->input());

        $CoCode=$request->input('CoCode');
        $CoName=$request->input('CoName');
        $CfFcId=$request->input('CfFcId');
        $CfCoShName=$request->input('CfCoShName');
        $CoPattern=$request->input('CoPattern');
        $CoYears=$request->input('CoYears');
        $CoMaxYr=$request->input('CoMaxYr');
        $CoMaxSn=$request->input('CoMaxSn');
        $CfCaryFrwrd=$request->input('CfCaryFrwrd');
        $CoRegDist=$request->input('CoRegDist');
        $CoCate=$request->input('CoCate');
        $CoSeatNo=$request->input('CoSeatNo');
        $CoMarkList=$request->input('CoMarkList');
        $IsScaleDown=$request->input('IsScaleDown');
        //$IsExamDept=$request->input('IsExamDept');

        $CoFcCode= Faculty::select('FcCode')->where('CfFacId',$CfFcId)->get(); 
         foreach ($CoFcCode as  $value) {
         $Faccode=$value->FcCode;
        }

        if($request->has('CoCalEC')){
            $CoCalEC=$request->input('CoCalEC');
        }else{
            $CoCalEC='0';
        }

        if($request->has('CoStat')){
            $CoStat=$request->input('CoStat');
        }else{
            $CoStat='0';
        }

        if($request->has('IsAcadDept')){
          $IsAcadDept=$request->input('IsAcadDept');
        }else{
            $IsAcadDept='0';
        }

        if($request->has('IsElgDept')){
            $IsElgDept=$request->input('IsElgDept');
        }else{
            $IsElgDept='0';
        }

        if($request->has('IsExamDept')){
            $IsExamDept=$request->input('IsExamDept');
        }else{
            $IsExamDept='0';
        }
        

        $isExist = Course::select("*")
                  ->where('CoCode', $CoCode)
                  ->where('EntryStat','1')
                  ->where('CfCoId','<>', $id)
                  ->exists();

        if ($isExist) 
        {
            $request->session()->flash('msg','Course Code Already Exist...!');
            return redirect('co-edit/'.$id);
        }
        else
        {
            Course::where('CfCoId', $id)->update([
             'CoCode' => $CoCode,
             'CoName' => $CoName,
             'CfFcId' => $CfFcId,
             'CfCoShName' => $CfCoShName,
             'CoPattern' => $CoPattern,
             'CoYears' => $CoYears,
             'CoMaxYr' => $CoMaxYr,
             'CoMaxSn' => $CoMaxSn,
             'CfCaryFrwrd' => $CfCaryFrwrd,
             'CoRegDist' => $CoRegDist,
             'CoCate' => $CoCate,
             'CoSeatNo' => $CoSeatNo,
             'CoMarkList' => $CoMarkList,
             'IsScaleDown' => $IsScaleDown,
             'IsExamDept' => $IsExamDept,
             'CoCalEC' => $CoCalEC,
             'CoStat' => $CoStat,
             'IsAcadDept' => $IsAcadDept,
             'IsElgDept' => $IsElgDept,
             'CoFcCode' => $Faccode,
             'EntryStat' => '1',
            ]);

            $request->session()->flash('msg','Record Updated Successfully...!');
            return redirect('Co-list');
        }

        
    }

    public function destroy(Request $request,$id)
    {
        Course::where('CfCoId', $id)->update([
            'EntryStat' => '0',
        ]);
        $request->session()->flash('msg','Record Deleted Successfully...!');
        return redirect('Co-list');
    }
}
