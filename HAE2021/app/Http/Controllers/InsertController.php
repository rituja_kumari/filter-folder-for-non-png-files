<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Response;
use DB;
use Illuminate\Http\Request;
use App\Address;
use App\CourseInfo;
use App\SubjectInfo;
use App\Status;
use App\Personal;
use App\Payment;
use Illuminate\Support\Facades\Storage;

class InsertController extends Controller
{
    public function address(Request $request)
    {
        $stpvalidator = Validator::make($request->all(), [
         'staddress' => 'required',
         'stdistrictname' => 'required',
         'state' => 'required',
         'subdist' => 'required',
         'postalcode' => 'required|max:6'
         ]);

         $input = $request->all();
         if ($stpvalidator->passes()) {
            $address = new Address();
            $StuId=session('studata')[0]['StuId'];
            $address->StuId=$StuId;
            $address->address=$request->input('staddress');
            $address->street=$request->input('street');
            $address->landmark=$request->input('landmark');
            $address->state=$request->input('state');
            $address->district=$request->input('stdistrictname');
            $address->sub_district=$request->input('subdist');
            $address->pincode=$request->input('postalcode');

            // $address->p_address=$request->input('p_address');
            // $address->p_street=$request->input('p_street');
            // $address->p_landmark=$request->input('p_landmark');
            // $address->p_state=$request->input('p_state');
            // $address->p_district=$request->input('p_district');
            // $address->p_sub_district=$request->input('p_subdist');
            // $address->p_postalcode=$request->input('p_postalcode');
            $address->save();

            // $request->session()->flash('msg','Record Add Successfully...!');
            // return redirect('form');
            return Response::json(['success' => '1']);
            // return redirect()->route('form');
            // return back()->with('success','Item created successfully!');

         }
         return Response::json(['errors' => $stpvalidator->errors()]);
    }
    public function course_info(Request $request)
    {

        
        $qstpvalidator = Validator::make($request->all(), [
             'centername' => 'required',
             'stcrstype' => 'required',
             'sttype' => 'required',
             'edu_status' => 'required',
             'ststatus' => 'required',
             'obmarks' => 'required',
             'ofmarks' => 'required',
             'passyear' => 'required',
        ]);

        $input = $request->all();
 
        if ($qstpvalidator->passes()) 
        {
            $StuId=session('studata')[0]['StuId'];
            $ChekDuplicate = CourseInfo::select("id")
            ->where([
                ['StuId', '=', $StuId],
                ['CgId', '=', $request->input('centername')],
                ['course_id', '=', $request->input('stcrstype')],
                ['st_type', '=', $request->input('sttype')],
            ])
            ->get();
            $StuExamCount = $ChekDuplicate->count();
            if($StuExamCount > 0)
            {
                return response()->json(['duplicate' => '1']);
            }
            foreach ($_POST['edu_type'] as $key => $val) 
            {
                $QAutoId=$_POST['HidArr'][$key];// qualification table auto id
                $EduStat=$_POST['edu_status'][$key];
                if ($EduStat=='') 
                {
                    return Response::json(['ErrorsEduStat' => $QAutoId]);
                }
                $EduObt=$_POST['edu_obt'][$key];
                if ($EduObt=='') 
                {
                    return Response::json(['ErrorsObt' => $QAutoId]);
                }
                $OutOfMarks=$_POST['edu_ofm'][$key];
                if ($OutOfMarks=='') 
                {
                    return Response::json(['ErrorsOtOMarks' => $QAutoId]);
                }
                $EduYear=$_POST['edu_year'][$key];
                if ($EduYear=='') 
                {
                    return Response::json(['ErrorsEduYear' => $QAutoId]);   
                }
            }


            $corse_info = new CourseInfo();
            
            $corse_info->StuId=$StuId;
            $corse_info->CgId=$request->input('centername');
            $corse_info->course_id=$request->input('stcrstype');
            $corse_info->st_type=$request->input('sttype');
            $corse_info->amount=$request->input('amount');

            
            $corse_info->save();

            if ($corse_info->id) 
            {
                $subject_info = new SubjectInfo();
                $subject_data=[];
                foreach ($_POST['subject'] as $key => $value) 
                {
                    $subject_data['StuId']=$StuId;
                    $subject_data['course_info_id']=$corse_info->id;

                    $subcheck=$subject_data['subject']=$value;
                    if ($subcheck=='') 
                    {
                        return Response::json(['ErrorSub' => '1']);
                    }
                    $Status_sub=$subject_data['ststatus']=$_POST['ststatus'][$key];
                    if ($Status_sub=='') 
                    {
                        return Response::json(['ErrorStatus' => '1']);
                    }
                    $ObtMarks=$subject_data['obmarks']=$_POST['obmarks'][$key];
                    if ($ObtMarks=='') 
                    {
                        return Response::json(['ErrorObtMks' => '1']);
                    }
                    $OfMarks=$subject_data['ofmarks']=$_POST['ofmarks'][$key];
                    if ($OfMarks=='') 
                    {
                        return Response::json(['ErrorOfMarks' => '1']);
                    }
                    $PassYr=$subject_data['passyear']=$_POST['passyear'][$key];
                    if ($PassYr=='') 
                    {
                        return Response::json(['ErrorPassYr' => '1']);
                    }
                    // $subject_info->save();
                    DB::table('subject_info')
                    ->insert($subject_data);
                }
            }

            $edu_data=[];

            foreach ($_POST['edu_type'] as $key => $val) 
            {
                $edu_data['StuId']=$StuId;
                $edu_data['course_info_id']=$corse_info->id;

                $edu_data['edu_type']=$val;
                $QAutoId=$_POST['HidArr'][$key];// qualification table auto id
                $EduStat=$edu_data['edu_status']=$_POST['edu_status'][$key];
                if ($EduStat=='') 
                {
                    return Response::json(['ErrorsEduStat' => $QAutoId]);
                }
                $EduObt=$edu_data['edu_obt']=$_POST['edu_obt'][$key];
                if ($EduObt=='') 
                {
                    return Response::json(['ErrorsObt' => $QAutoId]);
                }
                $OutOfMarks=$edu_data['edu_ofm']=$_POST['edu_ofm'][$key];
                if ($OutOfMarks=='') 
                {
                    return Response::json(['ErrorsOtOMarks' => $QAutoId]);
                }
                $EduYear=$edu_data['edu_year']=$_POST['edu_year'][$key];
                if ($EduYear=='') 
                {
                    return Response::json(['ErrorsEduYear' => $QAutoId]);   
                }
                // $subject_info->save();
                DB::table('qualification_info')
                ->insert($edu_data);
            }

            return Response::json(['success' => '1']);   
        }

        return Response::json(['errors' => $qstpvalidator->errors()]);
     }

     public function address_update(Request $request)
    {
        $stpvalidator = Validator::make($request->all(), [
         'staddress' => 'required',
         'stdistrictname' => 'required',
         'state' => 'required',
         'subdist' => 'required',
         'postalcode' => 'required|max:6'
         ]);
         $input = $request->all();
         if ($stpvalidator->passes()) {
            $address = Address::find($request->input('address_id'));
            $address->address=$request->input('staddress');
            $address->street=$request->input('street');
            $address->landmark=$request->input('landmark');
            $address->state=$request->input('state');
            $address->district=$request->input('stdistrictname');
            $address->sub_district=$request->input('subdist');
            $address->pincode=$request->input('postalcode');
            $address->save();

            // $request->session()->flash('msg','Record Add Successfully...!');
            // return redirect('form');
            return Response::json(['success' => '1']);
            // return redirect()->route('form');
            // return back()->with('success','Item created successfully!');

         }
         return Response::json(['errors' => $stpvalidator->errors()]);
    }

    public function upload(Request $request)
    {
        
        $request->validate([
            'photo' => 'required|mimes:jpg,jpeg|max:2048',
        ]);
        $input = $request->all();
            $StuId=session('studata')[0]['StuId'];
            $photo=$request->file('photo');

        $photoName =  $StuId.'.'.$photo->extension();  
        if(\File::exists(public_path('uploads/photo'), $photoName))
        {
            \File::delete(public_path('uploads/photo'), $photoName);
        }
        $photo->move(public_path('uploads/photo'), $photoName);

        $sign=$request->file('sign');

        $signName =  $StuId.'.'.$sign->extension();  
        if(\File::exists(public_path('uploads/sign'), $signName))
        {
            \File::delete(public_path('uploads/sign'), $signName);
        }
        $sign->move(public_path('uploads/sign'), $signName);
        // return Response::json(['success' => '1']);
        return redirect()->route('form');
        
    }

    public function getconfirm_status(Request $request)
    {
       
        $status = Status::where('StuId', $request->input('hidden_id'))
          ->update([
              'st_lock_status' => $request->input('st_lock_status'),
              ]);
        return Response::json(['success' => '1']);
    }

    public function app_print($StuId='')
    {
        if(empty($StuId))
        {
            $StuId=session('studata')[0]['StuId'];
        }
       
        $personal=Personal::where('StuId',$StuId)->get();
        $address=Address::where('StuId',$StuId)->get();
        $courses = DB::table("course_info")
        ->leftjoin('colleges', 'colleges.CgId', '=', 'course_info.CgId')
        ->leftjoin('courses', 'courses.CfCoId', '=', 'course_info.course_id')
        ->where("course_info.StuId",$StuId)
        ->get();
        return view('pages.app_print',compact('personal','address','courses'));
    }

    public function st_print()
    {
    $StuId=session('studata')[0]['StuId'];
    $personal=Personal::where('StuId',$StuId)->get();
    $address=Address::where('StuId',$StuId)->get();
    $courses = DB::table("course_info")
    ->leftjoin('colleges', 'colleges.CgId', '=', 'course_info.CgId')
    ->leftjoin('courses', 'courses.CfCoId', '=', 'course_info.course_id')
    ->where("course_info.StuId",$StuId)
    ->get();
    return view('pages.stdetails_print',compact('personal','address','courses'));
    }

    public function Payment(Request $request)
    {
        $input = $request->all();

        foreach ($_POST['CfCoIdArr'] as $key => $value) 
        {
            echo $CheckAmt=$_POST['chkamt_'.$value];echo "<br>";
        }
die();
        $OrderId=mt_rand(10000000, 99999999);
          $FormattedData=$OrderId.'|'.$Ammount;
          $CheckSumKey='7DX2bbMKc9YU';
          $CheckSumValue=hash_hmac('sha256',$FormattedData,$CheckSumKey,false);
          $CheckSumValue=strtoupper($CheckSumValue);
          $FormattedData=$FormattedData."|".$CheckSumValue;
    }

         
}
