<?php 
namespace App\Helpers;

class tools{
	
	//generate option list with key value paired array
	public static function options_select($options,$selected_option=''){
		 foreach($options as $key=>$value){
		 	$selected = ($selected_option==  $key )?'selected="selected"' : '' ;
           echo '<option value="'.$key.'" '.$selected.'  >'.$value.'</option>';
         }
	}
}