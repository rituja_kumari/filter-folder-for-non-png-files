<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtStuMarksMaster extends Model
{
    protected $table="CtStuMarksMaster";
    const CREATED_AT = 'EntryTime';
    const UPDATED_AT = NULL;
    protected $fillable = [];
}