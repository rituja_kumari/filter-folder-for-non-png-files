<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table="Courses";
    public $timestamps = false;
    protected $fillable = [];
}
