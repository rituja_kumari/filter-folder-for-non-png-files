<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CtStuMarksMasterLog extends Model
{
    protected $table="CtStuMarksMasterLog";
    const CREATED_AT = 'EntryTime';
    const UPDATED_AT = NULL;
    protected $fillable = [];
}