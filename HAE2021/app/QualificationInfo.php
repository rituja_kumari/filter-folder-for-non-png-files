<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QualificationInfo extends Model
{
    protected $table="qualification_info";
}
