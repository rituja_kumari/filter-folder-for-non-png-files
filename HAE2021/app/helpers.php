<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Exam;
use App\Branch;
use App\Course;
use App\Faculty;
use App\ExamSub;
use App\CollegeModel;
//function sel_property($selectname,$sql,$optval,$selval,$selectedval,$refresh,$required=0,$size=160,$m_bootstrap_css='form-control input-sm' ,$onclick,$disable='enabled',$disable_option=0)
function sel_property($selectname,$m_bootstrap_css,$refresh=0,$required=0,$onclick=null,$ArrOfData=null,$optval,$selval)
{  
    $RequiredLabel=$OnchangeLabel='';
    if($required=="1"){ $RequiredLabel='required="required"'; }
    if($refresh=="1"){ $OnchangeLabel='onchange="form.submit()"'; }
    elseif($onclick!=""){ $OnchangeLabel='onchange='.$onclick.''; }
    echo '<select class="'.$m_bootstrap_css.'" name="'.$selectname.'" id="'.$selectname.'" '.$RequiredLabel.$OnchangeLabel.'>
            <option value="">Select</option>';
            foreach($ArrOfData as $Data)
            {
                echo '<option value="'.$Data->$optval.'">'.$Data->$selval.'</option>';
            }
          
    echo '</select>';
}
function select_session($condition=NULL,$refresh=0,$required=0,$size,$m_bootstrap_css='form-control input-sm',$disabled='enabled',$onclick=null)
{ 
    echo '<label>Session</label>';
    global $session;
    $condition='';
    $sessions = DB::table('sessions')->select('SnNo', DB::raw("CONCAT(SnYear,' - ',SnName) as SnName"))->where('SnStatus', 'O')->get();
    sel_property("session",$m_bootstrap_css,$refresh,$required,$onclick,$sessions,'SnNo','SnName');
}

function select_faculty($condition=NULL,$refresh=0,$required=0,$size,$m_bootstrap_css='form-control input-sm',$disabled='enabled',$echo_query=0,$onclick=NULL)
{ 
    echo '<label>Faculty</label>';
    global $faculty,$session_FcCode;

    //$faculty = DB::table('facultys')->select('id', DB::raw("CONCAT(FcCode,' - ',FcDesc) as FcDesc"))->orderBy('FcCode','asc')->get();
    $query = Faculty::select('id', DB::raw("CONCAT(FcCode,' - ',FcDesc) as FcDesc"))->orderBy('FcCode','asc')->get();
    if($echo_query=='1'){ echo $faculty;}
    sel_property("faculty",$m_bootstrap_css,$refresh,$required,$onclick,$faculty,'id','FcDesc');
}
function sel_course($condition=NULL,$refresh=0,$required=0,$size=0,$m_bootstrap_css='form-control input-sm',$disabled='enabled',$echo_query=0,$onclick=NULL,$comp=0)
{  
    echo '<label>Courses</label>';if($comp=='1'){ echo '<span style="color:red">*</span>';}
    $ConditionArr=json_decode($condition);
    global $courses,$faculty;
    $query = Course::select('CfCoId', DB::raw("CONCAT(CoCode,' - ',CoName) as CoName"))->where('EntryStat', '=', 1)->orderBy('CoCode','asc');
    foreach($ConditionArr as $Condi)
    {
        $query->where($Condi->ColumnName, $Condi->Operator,$Condi->ColumnValue);
    }
    $courses=$query->orderBy('CfCoId','asc')->get();
    if($echo_query=='1'){ echo $courses;}
    sel_property("Selcourse",$m_bootstrap_css,$refresh,$required,$onclick,$courses,'CfCoId','CoName');  
}

function sel_branch($condition=NULL,$refresh=0,$required=0,$size=0,$m_bootstrap_css='form-control input-sm',$disabled='enabled',$onclick=NULL,$comp=0)
{  
    echo '<label>Faculty</label>';if($comp=='1'){ echo '<span style="color:red">*</span>';}
    global $branch;
    $ConditionArr=json_decode($condition);
    $query = Branch::select('CfBrId', DB::raw("CONCAT(branch_code,' - ',branch_name) as branch_name"))->where('EntryStat', '=', 1);
    foreach($ConditionArr as $Condi)
    {
        $query->where($Condi->ColumnName, $Condi->Operator,$Condi->ColumnValue);
    }
    $branches=$query->orderBy('CfBrId','asc')->get();
    sel_property("Selbranch",$m_bootstrap_css,$refresh,$required,$onclick,$branches,'CfBrId','branch_name'); 
}

function sel_exam($condition=NULL,$refresh=0,$required=0,$size=0,$m_bootstrap_css='form-control input-sm',$disabled='enabled',$onclick=NULL,$comp=0)
{ 
    echo '<label>Examination</label>';if($comp=='1'){ echo '<span style="color:red">*</span>';}
    global $semester,$courses;

    $ConditionArr=json_decode($condition);
    $query = Exam::select('EmId', DB::raw("CONCAT(EmCode,' - ',EmName) as EmName"))->where('EntryStat', '=', 1)->orderBy('EmCode', 'asc');
    foreach($ConditionArr as $Condi)
    {
        $query->where($Condi->ColumnName, $Condi->Operator,$Condi->ColumnValue);
    }
    $exams=$query->get();
   //echo $examsaaaa=$query->toSql();
    sel_property("Selexam",$m_bootstrap_css,$refresh,$required,$onclick,$exams,'EmId','EmName'); 
}

function sel_college($condition=NULL,$refresh=0,$required=0,$size=0,$m_bootstrap_css='form-control input-sm',$disabled='enabled',$onclick=NULL,$comp=0)
{ 
    echo '<label>College</label>';if($comp=='1'){ echo '<span style="color:red">*</span>';}
    global $semester,$courses;

    $ConditionArr=json_decode($condition);
    $query = CollegeModel::select('CgId', DB::raw("CONCAT(CgCode,' - ',CgName) as CgName"));
    foreach($ConditionArr as $Condi)
    {
        $query->where($Condi->ColumnName, $Condi->Operator,$Condi->ColumnValue);
    }
    $college=$query->get();
   //echo $examsaaaa=$query->toSql();
    sel_property("center",$m_bootstrap_css,$refresh,$required,$onclick,$college,'CgId','CgName'); 
}

function sel_subjects($condition=NULL,$refresh=0,$required=0,$size=0,$m_bootstrap_css='form-control input-sm',$disabled='enabled',$onclick=NULL)
{ 
    echo '<label>Subject</label>';
    global $subject,$branch,$semester,$get;
    $ConditionArr=json_decode($condition);
    $query = DB::table('examsub')->select('EsCode', DB::raw("CONCAT(EsCode,' - ',EsName) as EsName"));
    foreach($ConditionArr as $Condi)
    {
        $query->where($Condi->ColumnName, $Condi->Operator,$Condi->ColumnValue);
    }
  
    $subject1=$query->get();
    sel_property("subject",$m_bootstrap_css,$refresh,$required,$onclick,$subject1,'EsCode','EsName'); 
    //$get=get_val("select EsId from ". TABLE_ExamSub . " where EsCode='$subject' and  EsParent='S' and EsStrId='$branch' and  EsEmId='$semester' ","EsId");
} 


Function SelectTime($refresh=1,$required=0,$m_bootstrap_css,$style=0,$disabled,$condition,$onclick,$label,$name,$value,$id)
{
     	global $$value;
    	unset($ArrHrs);
    	unset($ArrMins);
    	unset($ArrAmPm);
    	if(empty($id)){ $id=$name;}
    	//Hours Array...
    	//for($i=1; $i<=12; $i++) { if($i<10){ $ArrHrs[]=''.$i; } else{ $ArrHrs[]=$i; } }
        	$ArrHrs=['12','01','02','03','04','05','06','07','08','09','10','11'];
        //Minutes Array...
        for($i=0; $i<60; $i) { if($i<10){ $ArrMins[]=''.$i; } else{ $ArrMins[]=$i; } $i=$i+15; }
        $ArrAmPm=['AM','PM'];
        
        foreach($ArrAmPm as $Key1 => $AmPm)
        {
            foreach($ArrHrs as $Key2 => $Hrs)
            {
                foreach($ArrMins as $Key3 => $Mins)
                {
                    $ArrTime[]=$Hrs.":".$Mins." ".$AmPm;
                }
            }
        }
        $RequiredLabel=$OnchangeLabel=$StyleLabel='';
    	if($label!=""){ echo '<label>'.$label.'</label>'; }
        if($required=="1"){ $RequiredLabel='required="required"'; }
        if($refresh=="1"){ $OnchangeLabel='onchange="form.submit()"'; }
        elseif($onclick!=""){ $OnchangeLabel='onchange='.$onclick.''; }
        if($style=="1"){ $StyleLabel='style="width:70%; height:20px;"'; }
	    echo '<select name="'.$name.'" id="'.$id.'" class="'.$m_bootstrap_css.'" '.$disabled. $RequiredLabel.$OnchangeLabel.$StyleLabel.' >
		    <option value="">Select</option>';
            foreach($ArrTime as $Time)
            {
                $selectedLabel='';
                if($$value == $Time) { $selectedLabel="selected";}
                echo '<option value="'.$Time.'" '.$selectedLabel.' >'.$Time.'</option>';
            
            }
        echo '</select>';

}
//To Get The Sequence Of Childs EsId In A String...
function GetChildEsId($EndOfSeq, $Seperator)
{
    if($Seperator==''){ $Seperator='|'; }
    $ChildEsId=$Seperator.$EndOfSeq .$Seperator;
    $Inside=0;
    $EsType = ExamSub::where('EsId', '=',$EndOfSeq)->first()->EsType;
    if($EsType!='')
    {
        while($EsType!="S")
        {
            $ParentId = ExamSub::where('EsId', '=',$EndOfSeq)->first()->EsParent;
            $ChildEsId=$Seperator.$ParentId."".$ChildEsId;
            $EndOfSeq=$ParentId;
            $EsType = ExamSub::where('EsId', '=',$EndOfSeq)->first()->EsType;
            $Inside=1;
        }
    }
    return $ChildEsId;
}
function GenrateUniqNumber() {
    $number = mt_rand(1000000000, 9999999999); // better than rand()
    return $number;
}
define('AES_256_CBC', 'aes-256-cbc');
function get_encrypt($input) {
   // define('AES_256_CBC', 'aes-256-cbc');
    // Generate a 256-bit encryption key
    $encryption_key = '43K0Y9RG7TVS4RL875IVNPRLQMQOUE8P';

    // Generate an initialization vector
    // This *MUST* be available for decryption as well
    $iv = '43K0Y9RG7TVS4RL8';

    // Create some data to encrypt
    $endata = $input;

    $encrypted = openssl_encrypt($endata, AES_256_CBC, $encryption_key, 0, $iv);
    $data = $encrypted;
    return $data;
}

function get_pkcs5_pad($text, $blocksize) {
    $pad = $blocksize - (strlen($text) % $blocksize);
    return $text . str_repeat(chr($pad), $pad);
}

function get_decrypt($respData) {
   // define('AES_256_CBC', 'aes-256-cbc');
    $key = '43K0Y9RG7TVS4RL875IVNPRLQMQOUE8P';
    $iv = '43K0Y9RG7TVS4RL8';
    $decrypted = openssl_decrypt($respData, AES_256_CBC, $key, 0, $iv);
    return $decrypted;

}

?>