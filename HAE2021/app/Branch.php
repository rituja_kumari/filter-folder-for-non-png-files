<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table="branch";
    public $timestamps = false;
    protected $fillable = [
        'CfBrId', 'branch_code', 'branch_name', 'CfCoId',
    ];
}
