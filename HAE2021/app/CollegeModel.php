<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;

class CollegeModel extends Model
{
    use Notifiable;
    protected $table="colleges";
    protected $guarded = [];
    protected $primaryKey = 'CgId';
   // protected $id;
    public static function getCollege($id, $CgId)
    {
    	
        $query = DB::table('cointakes as a')->select('a.CgId','b.CgName','b.CgCode','b.DsName','b.CfCgPrinName','b.CfCgPrinContact','b.CfCgPrinEmail',DB::raw("CONCAT(b.CfAdd1,', ',b.CfAdd2,' ',b.CfAdd3,' ',b.CfAdd4,' ',b.CfAdd5,' ',b.CfPinCod) as address"),'b.BankName','b.BankBranchName','b.BankAcNo','b.BankIFSCNo','b.Taluka','b.CfPinCod','b.CoFName','b.CoDesigNation','b.CoFMobile','b.CoFEmail','c.CoName','a.InGovt','a.InAided','a.InUnAided','a.AdGovt','a.AdAided','a.AdUnAided')->join('colleges as b','a.CgId','=','b.CgId')->join('courses as c', 'a.CfCoId','=','c.CfCoId');

       // $query = DB::table('colleges')->select('CgName', 'CgCode', 'DsName','CfCgPrinName','CfCgPrinContact','CfCgPrinEmail',DB::raw("CONCAT(CfAdd1,', ',CfAdd2,' ',CfAdd3,' ',CfAdd4,' ',CfAdd5,' ',CfPinCod) as address"),'BankName','BankBranchName','BankAcNo','BankIFSCNo', 'Taluka','CfPinCod','CoFName','CoDesigNation','CoFMobile','CoFEmail');

        if ($CgId=='' and $id!=111)
        {
            
            $query->where('CfDsId', '=', $id)
            ->whereNotNull('CfCgPrinEmail')
            ->whereNotNull('CfCgPrinContact');
        }
        elseif (isset($CgId))
        { 
            $query->where('b.CgId', '=', $CgId)
                 ->whereNotNull('CfCgPrinEmail')
                ->whereNotNull('CfCgPrinContact');
        }
        elseif($id==111)
        {
            
            $query->whereNotNull('CfCgPrinEmail')
                ->whereNotNull('CfCgPrinContact');
        }

        $result = $query->orderBy('b.CgId')->get();
        
    	return $result;
    	
    }
}
