<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudLogin extends Model
{
    protected $table="studlogin";
    protected $guarded = [];
    protected $primaryKey = 'StuId';
}
