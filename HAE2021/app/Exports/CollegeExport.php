<?php

namespace App\Exports;
use App\CollegeModel;
use App\Appcolleges;
use App\CollegeIntakeModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use DB;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\colleges\Entities\colleges;
use Modules\cointakes\Entities\cointakes;

class CollegeExport implements FromCollection,WithHeadings,ShouldAutoSize, WithEvents
{
    
    protected $CfDsId;
    protected $CgId;

    function __construct($CfDsId, $CgId) {
        $this->CfDsId = $CfDsId;
        $this->CgId = $CgId;
       
    }

    public function collection()
    {
        //return Appcolleges::all();
    
        return collect(CollegeModel::getCollege($this->CfDsId, $this->CgId));
    }

     /*public function array():array
     {
         //$query = DB::table('cointakes as a')->select('a.CgId','b.CgName','b.CgCode','a.CfCoId','c.CoName', 'a.InGovt', 'a.AdGovt')->join('colleges as b','a.CgId','=','b.CgId')->join('courses as c', 'a.CfCoId','=','c.CfCoId');
        $query = DB::table('cointakes as a')->select('b.CgName','b.CgCode','b.DsName','b.CfCgPrinName','b.CfCgPrinContact','b.CfCgPrinEmail',DB::raw("CONCAT(b.CfAdd1,', ',b.CfAdd2,' ',b.CfAdd3,' ',b.CfAdd4,' ',b.CfAdd5,' ',b.CfPinCod) as address"),'b.BankName','b.BankBranchName','b.BankAcNo','b.BankIFSCNo','b.Taluka','b.CfPinCod','b.CoFName','b.CoDesigNation','b.CoFMobile','b.CoFEmail','c.CoName', 'a.InGovt', 'a.AdGovt')->join('colleges as b','a.CgId','=','b.CgId')->join('courses as c', 'a.CfCoId','=','c.CfCoId');
        if(isset($this->CgId))                        //colg wise
        {
            
            $query->where('b.CfDsId', $this->CfDsId)
            ->where('a.CgId', $this->CgId);
           
        }
        elseif(isset($this->CfDsId) and $this->CfDsId!=111)
        {
             $query->where('b.CfDsId', $this->CfDsId)->orderBy('b.CgId');   //distict wise
        }
        else
        {
            $query->whereNotNull('b.CfCgPrinEmail')->whereNotNull('b.CfCgPrinContact')->orderBy('b.CgId'); //all
            
        }

         $results = $query->get()->toArray();

         return $results;
     }

     public function map($results):array
     {
      // print_r($results);
        return [
            [
                $results->CgName,
                $results->CgCode,
                $results->DsName,
                $results->CfCgPrinName,
                $results->CfCgPrinContact,
                $results->CfCgPrinEmail,
                $results->CfCgPrinEmail,
              
            ],         
        ];

     }*/

    

    public function headings(): array
    {

        return [
            [
            'Collage No.',
            'Center Name',
            'Center Number',
            'District',
            'Principle Name',
            'Principle Contact',
            'Principle Email',
            'Address',
            'Bank Name',
            'Branch Name',
            'Account No.',
            'IFSC Code',
            'Taluka',                       
            'Pincode',
            'Coordinator Name',
            'Co-Designation',
            'Co-Contact',
            'Co-Email',
            'Courses',
            'InGovt',
            'InAided',
            'InUnAided',
            'AdGovt',
            'AdAided',
            'AdUnAided',
            ],

        ];
    }

    public function registerEvents(): array
    {
        return [
                AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:Y1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                
            },
        ];
    }
}
