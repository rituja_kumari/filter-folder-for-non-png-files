<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectInfo extends Model
{
    protected $table="subject_info";
}
