<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
	protected $table="Faculty";
    public $timestamps = false;
     protected $fillable = [
        'CfFacId', 'FcCode', 'FcDesc', 
    ];
}
