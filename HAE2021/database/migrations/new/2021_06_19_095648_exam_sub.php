<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExamSub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_sub', function (Blueprint $table)
        {
            $table->bigIncrements('EmId');
            $table->integer('EsEmId');
            $table->integer('EsStrId');
            $table->char('EsType', 5);
            $table->string('EsCode', 50);
            $table->string('EsOmrCode', 50);
            $table->string('EsName', 1500);
            $table->string('EsShName', 20);
            $table->char('EsLang', 1);
            $table->string('EsMediums');
            $table->char('EsExempt', 1);
            $table->string('EsOpCode');
            $table->string('EsCombCode');
            $table->integer('EsParent');
            $table->integer('EsPrint'); 
            $table->smallInteger('EsSeq');
            $table->smallInteger('EsLevel');
            $table->smallInteger('EsOrder');
            $table->string('EsPSubCode');
            $table->integer('EsPSubId');
            $table->char('EsHOM', 1);
            $table->char('EsMarkGrd', 1);
            $table->char('EsCollUni', 1);
            $table->char('EsMarksBy', 1);
            $table->string('EsGrades');
            $table->decimal('EsMaxMarks', 10, 2);
            $table->string('EsQMarks');
            $table->char('EsExclTot', 2);
            $table->char('EsACF', 1);
            $table->decimal('EsPassMarks', 10, 2);
            $table->string('EsMustPassEsId', 500);
            $table->decimal('EsPassPC', 5, 2);
            $table->smallInteger('EsExmMarks');
            $table->decimal('EsExmPC', 5, 2);
            $table->string('EsExmAdd');
            $table->smallInteger('EsDistMarks');
            $table->decimal('EsDistPC', 5, 2);
            $table->string('EsDistRem');
            $table->string('EsGrdPts');
            $table->decimal('EsCredit', 5, 2);
            $table->integer('EsMapLEsId');
            $table->string('EsSpFlag', 10);
            $table->smallInteger('EsPartMarks');
            $table->string('EsDeAct', 5);
            $table->string('EsNoComb', 50);
            $table->string('EsThPr', 5);
            $table->string('CfEsHead', 5);
            $table->mediumInteger('CfGroupId');
            $table->string('EsOldCode', 15);
            $table->char('EsConv', 1);
            $table->string('EsSubTot', 5);
            $table->smallInteger('EsSeSr');
            $table->string('EsMustPass', 5);
            $table->string('EsCsSelType', 5);
            $table->smallInteger('EsCsMin');
            $table->smallInteger('EsCsMax');
            $table->char('CfExmLvl', 1);
            $table->string('CfDontCount', 5)->default('N');
            $table->integer('CfNoChld');
            $table->string('CfEndOfSub', 5)->default('N');
            $table->string('CfCoreSub', 5)->default('Y');
            $table->integer('CfIsExam')->default(0);
            $table->integer('CfIsReval')->default(0);
            $table->integer('CfIsLms')->default(0);
            $table->integer('CfIsAns')->default(0);
            $table->integer('EsAnsLength');
            $table->integer('EntryBy');
            $table->integer('EntryStat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_sub');
    }
}
