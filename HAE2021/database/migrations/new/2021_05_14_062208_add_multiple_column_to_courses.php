<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColumnToCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->decimal('ExmFees', 5, 2)->after('CfFcId')->default(0);
            $table->decimal('MrkShtFees', 5, 2)->after('ExmFees')->default(0);
            $table->decimal('AdmFees', 5, 2)->after('MrkShtFees')->default(0);
            $table->decimal('TotFees', 5, 2)->after('AdmFees')->default(0);
            $table->integer('MinMarks')->after('TotFees')->default(0);
            $table->integer('MaxMarks')->after('MinMarks')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            //
        });
    }
}
