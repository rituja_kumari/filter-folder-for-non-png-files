<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Exams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Exams', function (Blueprint $table)
        {
            $table->bigIncrements('EmId');
            $table->string('EmCoCode', 20);
            $table->smallInteger('EmSrNo');
            $table->string('EmCode', 20);
            $table->string('EmName', 80);
            $table->string('EmShName', 20);
            $table->char('EmStreams', 1);
            $table->smallInteger('EmGrpClg');
            $table->integer('CfBrId');
            $table->smallInteger('EmYrSrNo');
            $table->smallInteger('EmATKTSub');
            $table->string('EmElgblCd', 5);
            $table->string('EmLedger', 5);
            $table->string('EmMarkList', 5);  
            $table->char('EmLatEntry', 1);   
            $table->char('EmRejImpr', 1); 
            $table->string('EmEval', 5);
            $table->smallInteger('EmTotSub');
            $table->smallInteger('EmMaxTot');
            $table->decimal('EmPassPC', 5, 2);
            $table->smallInteger('EmVal');
            $table->smallInteger('EmValDev');
            $table->string('EmDiv', 5);
            $table->char('EmConv', 1);
            $table->char('EmDivFirstAtt', 1);
            $table->string('EmDivFirstMrk', 5);
            $table->string('EmSpFlag', 10);
            $table->char('EmColg', 1);
            $table->smallInteger('EmGrps');
            $table->string('EmSubTot', 25);
            $table->decimal('EmMinCr', 6, 2);
            $table->decimal('EmMaxCr', 6, 2);
            $table->integer('CfCoId');
            $table->integer('CfOptGrpCnt');
            $table->integer('CfCmpGrpCnt')->default(0);
            $table->integer('CfOptCompSub')->nullable();
            $table->integer('CfOptMaxSub')->nullable();
            $table->string('CfIntrnShpExam', 5);
            $table->integer('CfIsReval')->nullable();
            $table->integer('RevalEsCount')->default(0); 
            $table->decimal('RevalThreshold', 10, 2)->nullable();
            $table->integer('CfIsImprov')->nullable();
            $table->string('CfTotLvl', 50)->nullable();
            $table->integer('EntryBy')->nullable();
            $table->string('EntryStat', 5)->nullable();
            $table->integer('EmPrnCounter');
            $table->integer('EmRgNoCnt');   
            $table->string('EfClassVar')->nullable();
            $table->string('EfClassVal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('exams');
    }
}
