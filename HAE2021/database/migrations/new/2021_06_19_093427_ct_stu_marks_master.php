<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CtStuMarksMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CtStuMarksMaster', function (Blueprint $table) {
            $table->increments('MarksId');
            $table->integer('SnNo');
            $table->integer('SnSrNo');
            $table->integer('ExmId');
            $table->integer('ExmSrNo');
            $table->integer('EmId');
            $table->integer('EsId');
            $table->string('ChildEsId');
            $table->string('ChildSeq');
            $table->integer('CgId');
            $table->string('RgNo');
            $table->integer('AfId');
            $table->integer('EfId');
            $table->decimal('PassMarks', $precision = 5, $scale = 2);
            $table->decimal('MaxMarks', $precision = 5, $scale = 2);
            $table->decimal('ObtMarks', $precision = 5, $scale = 2);
            $table->decimal('PObtMarks', $precision = 10, $scale = 2);
            $table->decimal('TObtMarks', $precision = 11, $scale = 2);
            $table->decimal('MarksBeforeSdp', $precision = 10, $scale = 2);
            $table->integer('PassFailStat');
            $table->integer('PPassFailStat');
            $table->integer('TPassFailStat');
            $table->decimal('Grace', $precision = 11, $scale = 2);
            $table->decimal('TGrace', $precision = 11, $scale = 2);
            $table->decimal('SGrace', $precision = 10, $scale = 2);
            $table->decimal('EarnedCredit', $precision = 10, $scale = 2);
            $table->decimal('GradePoints', $precision = 10, $scale = 2);
            $table->string('GradeName');
            $table->string('GradeDesc');
            $table->decimal('ObtCG', $precision = 10, $scale = 2);
            $table->decimal('ThfMarks', $precision = 10, $scale = 2);
            $table->integer('IsReval');
            $table->string('EntryFlag');
            $table->integer('EntryUser');
            $table->dateTime('EntryTime');
            $table->string('IpAddress');
            $table->string('MacAddress');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
