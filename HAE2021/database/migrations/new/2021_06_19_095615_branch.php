<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Branch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch', function (Blueprint $table) {
            $table->increments('CfBrId');
            $table->integer('PBrId');
            $table->string('pattern_code');
            $table->string('branch_code');
            $table->string('branch_name');
            $table->string('course_code');
            $table->string('coname_tmp');
            $table->integer('CfCoId');
            $table->string('IsActive');
            $table->integer('EntryBy');
            $table->string('EntryTime');
            $table->integer('EntryStat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
