<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoursewiseintakeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cointakes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('CfCoId');
            $table->string('CgId');
            $table->string('InGovt');
            $table->string('InAided');
            $table->string('InUnAided');
            $table->string('AdGovt');
            $table->string('AdAided');
            $table->string('AdUnAided');
            $table->integer('EntryStat')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cointakes', function (Blueprint $table) {
            //
        });
    }
}
