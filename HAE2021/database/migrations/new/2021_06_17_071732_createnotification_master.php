<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_master', function (Blueprint $table) {
            $table->increments('NotifyId');
            $table->integer('NotifyFor');
            $table->string('UsrRole');
            $table->string('UsrDeptId');
            $table->integer('SnNo');
            $table->integer('ExmId');
            $table->integer('CfFcId');
            $table->integer('CfCoId');
            $table->integer('BrId');
            $table->integer('EmId');
            $table->date('FromDate');
            $table->date('ToDate');
            $table->integer('NotifyStat');
            $table->string('EntryStat');
            $table->integer('EntryUser');
            $table->dateTime('EntryTime');
            $table->string('NotifyFilePath');
            $table->integer('NotifyLevel');
            $table->string('NotiTitle');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_notification_master');
    }
}
