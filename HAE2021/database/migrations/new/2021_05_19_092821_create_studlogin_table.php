<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudloginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studlogin', function (Blueprint $table) {
            $table->bigIncrements('StuId');
            $table->string('stname');
            $table->string('StuEmail');
            $table->string('StuMob');
            $table->string('StuDob');
            $table->string('StuPass');
            $table->string('EntryStat', 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studlogin');
    }
}
