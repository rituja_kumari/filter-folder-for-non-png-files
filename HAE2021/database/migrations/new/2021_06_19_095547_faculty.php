<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Faculty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
         */
    public function up()
    {
        Schema::create('faculty', function (Blueprint $table)
        {

            $table->bigIncrements('CfFacId');
            $table->string('FcCode', 10);
            $table->string('FcDesc');
            $table->string('FcSpFlag',10);
            $table->integer('SFacId');
            $table->string('SFacCode',10);
            $table->integer('EntryBy')->nullable();
            $table->string('EntryStat',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faculty');
    }
}
