<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExamForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_forms', function (Blueprint $table)
        {

            $table->bigIncrements('EfId');
            $table->integer('SnNo')->nullable();
            $table->integer('SnSrNo')->nullable();
            $table->integer('ExmId')->nullable();
            $table->integer('ExmSrNo')->nullable();
            $table->string('ExamValue', 10)->nullable();
            $table->integer('EfShId');
            $table->integer('EfStrId');
            $table->integer('EfEmId');
            $table->integer('EfStrIdNew')->default(-11);
            $table->integer('EfEmIdNew')->default(-11);
            $table->integer('EfEmSrNo');
            $table->string('EfCgCode', 50);
            $table->integer('EfCgId');
            $table->string('EfRgId', 50);
            $table->string('EfFormNo', 50);
            $table->integer('AfId')->nullable();                    
            $table->string('EfRollNo',20);
            $table->string('EfCate',5);
            $table->integer('EfMaxMark');
            $table->integer('EfObtMarks');
            $table->decimal('EfGraceMarks', 11, 2);
            $table->decimal('EfPC', 14, 2);
            $table->integer('EfMaxMark2')->nullable();
            $table->integer('EfObtMarks2')->nullable();
            $table->decimal('EfPC2', 14, 2)->nullable();
            $table->char('EfResult', 1);
            $table->string('EfStatus',5);
            $table->integer('EfHallTkt')->default(-1);
            $table->string('EfAprEsId')->nullable();
            $table->string('EfPasEsId')->nullable();
            $table->string('EfPrevPasEsId')->nullable();
            $table->integer('EfFailEsCount')->nullable();
            $table->string('EfPayStat',5)->default('N');
            $table->string('EfPrefCent',10);
            $table->integer('EfEcId');
            $table->integer('EfThfFlag');
            $table->decimal('EfThfMarks', 10, 2);
            $table->integer('EfSdpFlag')->default(0);
            $table->string('EfSdpSub')->default('N');
            $table->string('EfSpFlag','10');
            $table->integer('EfCent');
            $table->integer('EfIncMarks');
            $table->string('EfIncCode',5);
            $table->string('EfExcp',50);
            $table->integer('EfAction')->nullable();
            $table->string('EfDecl',5);
            $table->integer('EfIncUsed');
            $table->string('EfRuleRem',50);
            $table->integer('EfAObtMarks');
            $table->integer('EfAMaxMarks');
            $table->string('EfAResult',5)->nullable();
            $table->integer('EfDivGrace');
            $table->integer('EfDivPC');
            $table->string('EfDiv',15);
            $table->string('EfPress',5);
            $table->string('EfRemark',50)->nullable();
            $table->string('EfGrdPts',5)->nullable();
            $table->integer('EfLimit');
            $table->integer('EfNextSr');
            $table->integer('EfDivShort');
            $table->integer('EfSrNoEfSrNoEfSrNoEfSrNo');
            $table->integer('EfCertNo');
            $table->integer('EfStateNoStatus')->default(0);
            $table->string('EfStateNo');
            $table->integer('EfGP_CR');
            $table->integer('EfCredits');
            $table->decimal('EfSGPI', 10, 2)->nullable();
            $table->decimal('EfSGPI2', 10, 2)->nullable();
            $table->decimal('EfCGPA', 10, 2)->nullable();
            $table->decimal('EfSubGrace', 10, 2);
            $table->integer('EfPassMarks');
            $table->integer('EfMaxGp');
            $table->integer('EfOvrGrace');
            $table->integer('EfTotal');
            $table->integer('EfSnNo');
            $table->string('EfRemark2',50);
            $table->string('EfSubOrd',100);
            $table->integer('EfIncCnt');
            $table->string('EfResult2',50);
            $table->string('EfSemeResult',50);
            $table->date('EfResDeclDt')->nullable();
            $table->string('EfExmHeldDt')->nullable();
            $table->decimal('EfGraceReq', 10, 2)->default(0.00);
            $table->integer('ExmProCnt')->default(0);
            $table->string('CFCentTh',5)->default('NA');
            $table->string('CFCentPr',5)->default('NA');
            $table->string('EfBatch',20);
            $table->integer('EfAnsStat')->default(0);
            $table->string('EntryFlag',5)->default('N');
            $table->integer('EntryUser')->nullable();
            $table->date('EntryTime')->nullable();
            $table->date('ResProTime')->nullable();
            $table->integer('ResProStat')->default(0);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_forms');
    }
}
