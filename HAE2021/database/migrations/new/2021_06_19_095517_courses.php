<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Courses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Courses', function (Blueprint $table) {
            $table->increments('CfCoId');
            $table->string('CoCode');
            $table->string('CoName');
            $table->string('CoFcCode');
            $table->string('CoRgPrfx');
            $table->decimal('CoYears', $precision = 5, $scale = 2);
            $table->smallInteger('CoDuYears');
            $table->smallInteger('CoExams');
            $table->smallInteger('CoMaxYr');
            $table->smallInteger('CoMaxSn');
            $table->string('CoMediums');
            $table->integer('CoRegDist');
            $table->integer('CoSeatNo');
            $table->string('CoSpSub');
            $table->string('CoLedger');
            $table->string('CoMarkList');
            $table->string('CoPattern');
            $table->string('CoAdmMode');
            $table->integer('CoCate');
            $table->string('CoRule');
            $table->decimal('CoPassPc', $precision = 5, $scale = 2);
            $table->string('CoProgCGPA');
            $table->string('CoCBCS');
            $table->integer('ExmSrNo');
            $table->integer('EmId');
            $table->integer('EsId');
            $table->decimal('CoPassCr', $precision = 6, $scale = 2);
            $table->integer('CoCalEC');
            $table->char('is_branch', 1);
            $table->integer('CfFcId');
            $table->integer('CfLevelId');
            $table->string('CfCoShName',30);
            $table->string('CfCoMarathi', 255);
            $table->integer('CfCaryFrwrd');
            $table->decimal('IsScaleDown', $precision = 10, $scale = 2);
            $table->decimal('TObtMarks', $precision = 11, $scale = 2);
            $table->decimal('MarksBeforeSdp', $precision = 10, $scale = 2);
            $table->integer('IsExamDept');
            $table->integer('IsAcadDept');
            $table->integer('IsElgDept');
            $table->integer('CoStat');
            $table->integer('EntryBy');
            $table->string('EntryStat',5);
            $table->dateTime('EntryTime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
