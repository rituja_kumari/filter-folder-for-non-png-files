<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcrregnfomstemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icrregnfomstemp', function (Blueprint $table) {
            $table->string('BatchNo')->nullable();
            $table->integer('CfSnSrNo')->nullable();
            $table->string('Batch')->nullable();
            $table->integer('FormNo')->nullable();
            $table->integer('StuId')->nullable();
            $table->string('RgCertSrNo')->nullable();
            $table->string('Cate')->nullable();
            $table->string('CgCode')->nullable();
            $table->integer('CgId')->nullable();
            $table->string('CoCode')->nullable();
            $table->string('Branch')->nullable();
            $table->string('Title')->nullable();
            $table->string('Name')->nullable();
            $table->string('Father')->nullable();
            $table->string('Mother')->nullable();
            $table->char('Sex')->nullable();
            $table->date('BirthDate')->nullable();
            $table->string('Caste')->nullable();
            $table->string('Nation')->nullable();
            $table->integer('Country')->nullable();
            $table->integer('CountryP')->nullable();
            $table->string('Medium')->nullable();
            $table->string('SameAddr')->nullable();
            $table->string('Addr1')->nullable();
            $table->string('Addr2')->nullable();
            $table->string('Addr3')->nullable();
            $table->string('Place')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icrregnfomstemp');
    }
}
