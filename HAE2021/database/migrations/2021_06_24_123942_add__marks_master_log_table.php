<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMarksMasterLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CtStuMarksMasterLog', function (Blueprint $table)
        {
            $table->bigIncrements('MarksId');   
            $table->integer('SnNo')->nullable();     
            $table->integer('SnSrNo')->nullable();     
            $table->integer('ExmId')->nullable();     
            $table->integer('ExmSrNo')->nullable();     
            $table->integer('EmId')->nullable();     
            $table->integer('EsId')->nullable(); 
            $table->string('ChildEsId', 500)->nullable();    
            $table->string('ChildSeq', 50)->nullable();  
            $table->integer('CgId')->nullable();   
            $table->string('RgNo', 50)->nullable();
            $table->integer('AfId')->nullable(); 
            $table->integer('EfId')->nullable(); 
            $table->decimal('PassMarks', 5, 2)->nullable();
            $table->decimal('MaxMarks', 5, 2)->nullable();
            $table->decimal('ObtMarks', 5, 2)->nullable();
            $table->decimal('PObtMarks', 10, 2)->nullable();
            $table->decimal('TObtMarks', 11, 2)->nullable();
            $table->decimal('MarksBeforeSdp', 10, 2)->nullable();
            $table->integer('PassFailStat')->default(0);
            $table->integer('PPassFailStatx')->nullable();
            $table->integer('TPassFailStat')->nullable();
            $table->decimal('Grace', 11, 2)->nullable();
            $table->decimal('TGrace', 11, 2)->nullable();
            $table->decimal('SGrace', 10, 2)->nullable();
            $table->decimal('GradePoints', 10, 2)->nullable();
            $table->string('GradeName', 10)->nullable();
            $table->string('GradeDesc', 100)->nullable();
            $table->decimal('ObtCG', 10, 2)->nullable();
            $table->decimal('ThfMarks', 10, 2)->nullable();
            $table->integer('IsReval')->default(0);
            $table->string('EntryFlag', 5)->nullable();
            $table->integer('EntryUser')->nullable();
            $table->date('EntryTime')->nullable();
            $table->string('IpAddress', 50)->nullable();
            $table->string('MacAddress', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CtStuMarksMasterLog', function (Blueprint $table) {
            //
        });
    }
}
