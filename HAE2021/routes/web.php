<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\StudentCountReport;
use App\StudLogin;

use App\Notifications\OffersNotification;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// center route  

// ...

// $pdo = DB::connection()->getPdo();

// if($pdo)
//    {
//      echo "<h1>Connected successfully to database</h1> ".DB::connection()->getDatabaseName();
//    } else {
//      echo "<h1>You are not connected to database</h1>";
//    }

// die;

Route::group(['middleware' => ['customauth']], function(){

    Route::get('/admin-doa', function () {
        return view('pages.content-admin');
        })->name('admin-doa'); 
        
    Route::get('admin', function () 
    {
        return view('pages.content-home');
    })->name('admin');
    Route::get('/printpage', function () {
        return view('my');
    });

    Route::post('rst-url','DOAController@rspassowd')->name('rspassword');
    Route::get('/reset-password', function () {
    
    return view('pages.reset-password');
    })->name('reset-password');

    Route::get('file-upload', 'CenterFileUploadController@fileUpload')->name('file.upload');
    Route::post('file-upload', 'CenterFileUploadController@fileUploadPost')->name('file.upload.post');

    Route::get('/candidate-information-approval-course-wise', function () {
        return view('pages.candidate-information-approval-course-wise');
        }); 
   
     Route::get('/printstdetails', function () {
        return view('pages.stdetails_print');
    });

     //center Details 
    Route::get('center-details','CenterController@create')->name('center-details');
    Route::post('ShowCollege/{id?}','CenterController@ShowCollege');
    Route::post('ShowCollList/{id?}','CenterController@ShowColData');
    Route::post('ShowCollListSingle/{id?}','CenterController@ShowColDataSingle');
    Route::get('center-details/{CfDsId}','CenterController@show');
    Route::get('findcolg/{id}','CenterController@findcolg');
    Route::get('RegisterClg/{id}','CenterController@RegisterClg');
    Route::get('RegisterClgC/{id}','CenterController@RegisterClgC');
    //Route::post('centersubmit','CenterController@store');
    Route::post('export-excel','CenterController@exportIntoExcel');

        // Faculty Master 
    Route::get('fac-show','FacultyController@show');
    Route::get('fac-delete/{id}','FacultyController@destroy');
    Route::get('fac-create','FacultyController@create');
    Route::post('fac-submit','FacultyController@store');    //submit btn
    Route::get('fac-edit/{id}','FacultyController@edit');
    Route::post('fac-update/{id}','FacultyController@update')->name('fac.update');  //update btn
    //Course Master
    Route::get('Co-list','CourseController@show');
    Route::get('co-delete/{id}','CourseController@destroy');
    Route::get('co-create','CourseController@create');
    Route::post('co-submit','CourseController@store');
    Route::get('co-edit/{id}','CourseController@edit'); 
    Route::post('co-update/{id}','CourseController@update')->name('co.update');


    // Branch Master
    Route::get('br-list','BranchController@show');
    Route::get('br-delete/{id}','BranchController@destroy');
    Route::get('br-create','BranchController@create');
    Route::get('findcoursebr/{id}','BranchController@findcoursebr');
    Route::post('br-submit','BranchController@store');
    Route::get('br-edit/{id}','BranchController@edit'); 
    Route::post('br-update/{id}','BranchController@update')->name('br.update');
    // Semester Master 
    Route::get('em-list','ExamController@show');
    Route::get('em-delete/{id}','ExamController@destroy');
    Route::get('em-create','ExamController@create');
    Route::get('findcourse/{id}','ExamController@findcourse');
    Route::get('findbranch/{id}','ExamController@findbranch');
    Route::post('em-submit','ExamController@store');
    Route::get('em-edit/{id}','ExamController@edit'); 
    Route::post('em-update/{id}','ExamController@update')->name('em.update');

    Route::get('notificationMaster','NotificationMaster@index');
    Route::get('Notification','NotificationMaster@index');
    Route::post('SaveNotiMaster','NotificationMaster@SaveRecords');
    Route::get('Notification-delete/{id}','NotificationMaster@Delete');
    Route::get('aa','NotificationMaster@notification_list');
    Route::get('NotificationPanel','NotificationPanel@index');

    //publish ledger
    Route::get('PublishLedger','PublishLedgerController@index');
    Route::get('ResultLedger','PublishLedgerController@CollegeResultLedger');
    Route::post('LedgerList/{id?}','PublishLedgerController@LedgerList');
    Route::post('SavePublishLedger/{id?}','PublishLedgerController@SavePublishLedger');
    Route::get('/Publish_Ledgerpdf/{id?}/{id1?}/{id2?}/{id3?}', 'PublishLedgerController@Publish_Ledgerpdf');
    Route::get('/Publish_BULK_Ledger/{id?}', 'PublishLedgerController@Publish_BULK_Ledger');//for atd 2 nd year
    
    //Marks Entry MOdule
    Route::get('MarksEntry','StuMarksEntry@index');
    Route::post('ShowBranch/{id?}','StuMarksEntry@ShowBranch');
    Route::post('ShowExam/{id?}','StuMarksEntry@ShowExam');
    Route::post('ShowRollName/{id?}/{ReusedFlag?}','StuMarksEntry@ShowRollName');
    Route::post('AddStudentInfo/{id?}','StuMarksEntry@AddStudentInfo');
    Route::post('AutoCalcMarks/{id?}','StuMarksEntry@AutoCalcObtMarks');
    Route::post('DeleteStudentDetails/{id?}/{RgId?}/{Extra?}','StuMarksEntry@DelStudentDetails');
    Route::post('show/{id?}','StuMarksEntry@show');
    Route::post('OnExamChange/{id?}','StuMarksEntry@OnExamChange');
    Route::post('ShowEdit/{id?}','StuMarksEntry@ShowEdit');
    Route::post('EditStudentInfo/{id?}','StuMarksEntry@EditStudentInfo');
    Route::post('FinalSubmission/{id?}','StuMarksEntry@FinalSubmission');
    Route::get('/dynamic_pdf/{id?}/{id1?}/{id2?}', 'StuMarksEntry@pdf');
    Route::get('/dynamic_Ledgerpdf/{id?}/{id1?}/{id2?}/{id3?}/{id4?}', 'StuMarksEntry@dynamic_Ledgerpdf');
    Route::get('fetchUniversityTable/{id?}','StuMarksEntry@fetchUniversityTable');
    Route::get('uploadLedger', 'uploadLedger@index');   
    Route::post('ledger-upload', 'uploadLedger@ledgerUpload')->name('ledger.upload.post');
    // Route::post('ShowExam/{id?}','uploadLedger@ShowExam');
    Route::get('CountReport','StudentCountReport@index');
    Route::post('ShowReportListExamWise/{id?}','StudentCountReport@ShowReportListExamWise');
    Route::get('export/{id?}','StudentCountReport@export');
    Route::get('FinalSubmitActive/{id?}/{id2?}','StudentCountReport@FinalSubmitActive');
    Route::get('UnpublishResult/{id?}/{id2?}','StudentCountReport@UnpublishResult');
    Route::get('StudentResult','StudentCountReport@StudentResult');
    Route::post('ShowStudList/{id?}','StudentCountReport@ShowStudList');
    Route::get('exportResult/{id?}','StudentCountReport@exportResult');
});

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('nameandmarksentry','DOAController@index')->name('nameandmarksentry');
Route::post('newcenterregistration','DOAController@newcenterregistration')->name('newcenterregistration');
Route::post('NewMobileVerify','DOAController@NewMobileVerify')->name('NewMobileVerify');
Route::get('finddistrict/{id}','DOAController@finddistrict');
Route::get('findcollege/{id}','DOAController@findcollege');
Route::get('findcollegecode/{id}','DOAController@findcollegecode');
Route::get('SendOtp/{clgid}/{id}','DOAController@SendOtp');
Route::get('VerifyPrcOtp/{MoOtp}/{Mono}/{CollId}','DOAController@VerifyPrcOtp');
Route::get('SendEmailOtp/{ClgId}/{Email}','DOAController@SendEmailOtp');
Route::get('VerifyPrcEmailOtp/{EmailOtp}/{PrcEmail}/{CollegeId}','DOAController@VerifyPrcEmailOtp');
Route::post('newmobileemailotp','DOAController@newmobileemailotp');
Route::get('CoFSendMoOtp/{CgId}/{CoMobile}','DOAController@CoFSendMoOtp');
Route::get('GetCourse/{id}','DOAController@GetCourse');
Route::get('VerifyCofOtp/{otp}/{mobile}/{cgid}','DOAController@VerifyCofOtp');
Route::get('SendCofEmailOtp/{cgcode}/{coemail}','DOAController@SendCofEmailOtp');
Route::get('Verifycofemailotp/{otp}/{email}/{cgid}','DOAController@Verifycofemailotp');
Route::get('AddCourse/{cgcode}/{coid}/{ingovt}/{inaided}/{inunaided}/{adgovt}/{adaided}/{adunaided}','DOAController@AddCourse');
Route::get('CourseDel/{id}','DOAController@CourseDel');
Route::post('login','DOAController@login')->name('login');
Route::get('/logout', function()
{
    session()->forget(['data', 'Dist', 'CourseWiseIntake', 'UserDetail', 'CoName','studata','StudentType','StudentCount']);
    return redirect('nameandmarksentry');
});
 
    
Route::post('cregister-fp','DOAController@centerforgotps')->name('cfplogin'); // center forget password

// Subject Master
Route::get('es-list','SubjectController@show');
Route::get('es-create','SubjectController@create');
Route::get('course/{id}','SubjectController@findcourse');
Route::post('es-submit','SubjectController@store');

//route content
Route::get('/qt-paper-upload', function () {
    return view('pages.content-question-paper');
    });
    
Route::post('qt-paper-upload-url','DOAController@qtpaperupload')->name('qtpaperupload');

//end center route

// student route
// Route::get('form', function () 
// {
//     if(!session()->has('studata'))
//     {
//         return redirect('registration');
//     }
//     return view('pages.content-form');
// })->name('form');

Route::get('/student-registration', function () {
    return view('student-registration');
});
Route::get('/student-login', function () {
    return view('pages.content-student-login');
});

Route::post('register-u','DOAController@update')->name('update');
Route::post('register','DOAController@login')->name('login');
//Route::post('register-u','DOAController@update')->name('update');
/*Route::post('register-s','DOAController@registers')->name('registerss');*/
Route::post('register-l','DOAController@slogin')->name('slogin');
Route::post('register-f','DOAController@flogin')->name('flogin');
Route::post('register-fp','DOAController@fplogin')->name('fplogin');

Route::post('register-s','StudLoginController@registers')->name('registerss');
Route::get('SendOtp-s/{id}','StudLoginController@SendOtp');
Route::get('SendEmailOtp-s/{Email}','StudLoginController@SendEmailOtp');
Route::get('VerifyStuOtp/{MoOtp}/{Mono}','StudLoginController@VerifyStuOtp');
Route::get('VerifyStuEmailOtp/{EmailOtp}/{StuEmail}','StudLoginController@VerifyStuEmailOtp');

// stprofile
Route::post('stregister','DOAController@stprofile')->name('personalinfo');
Route::post('stadressurl','DOAController@staddress')->name('staddress');
Route::post('qualification1-url','DOAController@qualification1')->name('qualification1');
Route::post('qualification2-url','DOAController@qualification2')->name('qualification2');
Route::post('qualification3-url','DOAController@qualification3')->name('qualification3');
Route::post('qualification4-url','DOAController@qualification4')->name('qualification4');
Route::post('stcourse-url','DOAController@stcourse')->name('stcourse');
Route::post('stphoto-url','DOAController@stphoto')->name('stphoto');

// stprofile
/////student login route
Route::get('form','PersonalController@index')->name('form');
Route::post('form_store','PersonalController@store')->name('form_store');
Route::post('form_update','PersonalController@update')->name('form_update');
Route::post('address','InsertController@address')->name('address');
Route::post('address_update','InsertController@address_update')->name('address_update');
Route::post('course_info','InsertController@course_info')->name('course_info');
Route::post('qualification','InsertController@qualification')->name('qualification');
Route::post('upload','InsertController@upload')->name('upload');
Route::post('confirm_status','InsertController@getconfirm_status')->name('confirm_status');
Route::post('payment','InsertController@Payment')->name('payment');
//Route::get('app_print','InsertController@app_print')->name('app_print');
Route::get('app_print/{name?}','InsertController@app_print')->name('app_print');
Route::get('printstdetails','InsertController@st_print')->name('printstdetails');
Route::get('subdist','DropdownController@getsubdistrict')->name('subdist');

Route::get('course','DropdownController@getcourse')->name('course');
Route::get('amount','DropdownController@getamount')->name('amount');
Route::get('subject','DropdownController@getsubject')->name('subject');
Route::get('edu','DropdownController@getQualification')->name('edu');
Route::get('sttype','DropdownController@getstType')->name('sttype');

///////end student login 
  

Route::post('ShowStudentType/{id}','CandiApprovalCouWise@ShowStudentType');
Route::post('ShowTbl/{id}','CandiApprovalCouWise@ShowTbl');
Route::get('edit/{id}','CandiApprovalCouWise@edit');
Route::post('SubmitApproval/{id}','CandiApprovalCouWise@SubmitApproval');
Route::post('ShowAction/{id}','CandiApprovalCouWise@ShowAction');
Route::post('indexlistview/{id}','CandiApprovalCouWise@index');
Route::post('CandiApprovalPdf/','CandiApprovalCouWise@CandiApprovalPdf');

//Route::get('/send-notification', [NotificationController::class, 'sendOfferNotification']);
//Route::get('/send-notification', 'NotificationController@sendOfferNotification');  
// Route::get('/note', function () {
//    Notification::send(StudLogin::first(), new OffersNotification());
// });
//Route::get('/note', [NotificationController::class, 'sendOfferNotification']);



