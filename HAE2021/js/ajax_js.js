function ajax_combo3(ajax_php, Id, responseId, FormId, EditId = 0,loader_class='.body_overlay') {

    var del_id = $(Id).val();
    if (del_id) {
        var NewUrl = ajax_php + del_id;
    } else if (EditId) {
        var NewUrl = ajax_php + EditId;
    } else {
        var NewUrl = ajax_php + undefined;
    }
    $.ajax({
        url: NewUrl,
        type: "post",
        // data: { "_token": "{{ csrf_token() }}" },
        data: $('#' + FormId).serialize(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        dataType: "json",
		beforeSend: function() {
			$(loader_class).show();
		},
        success: function(data) {
            console.log(data);
            if (data.html) {
                $(responseId).html(data.html);
            }
            if (data.ValidatorErrors) {
                $.each(data.ValidatorErrors, function(index, jsonObject) {
                    $.each(jsonObject, function(key, val) {
                        // console.log("key : " + key + " ; value : " + val);
						$(loader_class).hide();
                        alert(val);
                        exit();

                    });
                });

            }
            if (data.errors) {
                alert(data.errors);
            } 
			$(loader_class).hide();
        }
    });
}

// function AjaxFuncForUploadile(ajax_php, Id, responseId, FormId) {
//     alert();
//     var del_id = $(Id).val();
//     if (del_id) {
//         var NewUrl = ajax_php + del_id;
//     } else {
//         var NewUrl = ajax_php + undefined;
//     }

//     $.ajax({
//         alert();
//         //url: NewUrl,

//         url: "{{ url('') }}",
//         type: "post",
//         data: new FormData($(FormId)[0]),
//         processData: false,
//         contentType: false,
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         //dataType: "json",
//         success: function(data) {
//             console.log(data);
//             if (data.html) {
//                 $(responseId).html(data.html);
//             }
//             if (data.ValidatorErrors) {
//                 $.each(data.ValidatorErrors, function(index, jsonObject) {
//                     $.each(jsonObject, function(key, val) {
//                         // console.log("key : " + key + " ; value : " + val);
//                         alert(val);
//                         exit();

//                     });
//                 });

//             }
//             if (data.errors) {
//                 alert(data.errors);
//             } //else {
//             //     $(Id).empty();
//             // }
//         }
//     });
// }