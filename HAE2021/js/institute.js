$(document).ready(function() {
   
   $('#state').on('change', function() {
       var menu_id = $(this).val();
       if(menu_id) {
             $.ajax({
                url: 'finddistrict/'+menu_id,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                success:function(data) {
                   //console.log(data);
                   if(data){
                   $('#District').empty();
                   $('#College').empty();
                   $('#District').focus;
                   $('#District').append('<option value="">Select</option>');
                   $.each(data, function(key, value){
                   $('select[name="District"]').append('<option value="'+ value.CfDsId +'">' + value.DsName+ '</option>');
                });
             }else{
                $('#District').empty();
             }
             }
             });
       }else{
          $('#District').empty();
       }
    });
 
 $('#District').on('change', function() {
       var collegeid = $(this).val();
       if(collegeid) {
             $.ajax({
                url: 'findcollege/'+collegeid,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                success:function(data) {
                   
                   if(data){
                   $('#CenterNo').empty();
                   $('#College').empty();
                   $('#College').focus;
                   $('#College').append('<option value="">Select</option>');
                   $.each(data, function(key, value){
                   $('select[name="College"]').append('<option value="'+ value.CgId +'">' +value.CgCode +'-'+ value.CgName+ '</option>');
                });
             }else{
                $('#College').empty();
             }
             }
             });
       }else{
          $('#College').empty();
       }
    });

    $('#College').on('change', function() {
       var cgcode = $(this).val();
      $('#CfCoId').val("");
       if(cgcode) {
             $.ajax({
                url: 'findcollegecode/'+cgcode,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                success:function(data) {
                   
                   if(data){
                      
                      
                     $('#CenterNo').empty();
                     $.each(data, function(key, value){
                     $('#CenterNo').val(value.CgCode);
                     $('#CgCode').val(value.CgCode);
                     $('#CfAdd1').val(value.CfAdd1);
                     $("#CfAdd2").val(value.CfAdd2);
                     $("#CfAdd3").val(value.CfAdd3);
                     $("#CfAdd4").val(value.CfAdd4);
                     $("#CfAdd5").val(value.CfAdd5);
                     $("#Taluka").val(value.Taluka);
                     $('#TnName').val(value.TnName);
                     $('#CfPinCod').val(value.CfPinCod);
                     $('#CfCgPrinName').val(value.CfCgPrinName);
                     $('#CfCgPrinContact').val(value.CfCgPrinContact);  
                     $('#CfCgPrinEmail').val(value.CfCgPrinEmail);
                     $('#CgLandLineNo').val(value.CgLandLineNo);
                     $('#CoFName').val(value.CoFName); 
                     $('#CoFMobile').val(value.CoFMobile);
                     $('#CoFEmail').val(value.CoFEmail);
                     $('#CoDesigNation').val(value.CoDesigNation);
                     $('#holdername').val(value.CfCgPrinName);
                     $('#PrincipleName' ).html(value.CfCgPrinName);
                     $('#reBankAcNo').val(value.BankAcNo);
                     $('#BankName').val(value.BankName);
                     $('#BankAcNo').val(value.BankAcNo);
                     $('#BankIFSCNo').val(value.BankIFSCNo);
                     $('#BankBranchName').val(value.BankBranchName);
                     // var getid=value.CfCoId;
                     // var s2 = getid.substring(1);
                     // var str1 = s2.substring(0, s2.length - 1);
                     // var coidarr = str1.split("|");
                     
                   if(value.CfCgPrinName!=null && value.CfCgPrinContact!=null && value.CfCgPrinEmail!=null && value.CgLandLineNo!=null && value.CoFName!=null && value.CoFMobile!=null && value.CoFEmail!=null && value.CoDesigNation!=null && value.CfAdd1!=null && value.CfAdd2!=null && value.CfAdd3!=null && value.CfAdd4!=null && value.CfAdd5!=null && value.Taluka!=null && value.CfPinCod!=null && value.BankName!=null && value.BankAcNo!=null && value.BankIFSCNo!=null && value.BankIFSCNo!=null)
                   {
                      $("#PrcOtp").attr("disabled",true);
                      $('#PrcMoVerify').attr("disabled",true);
                      $("#PrcEmailbtn").attr("disabled",true);
                      $("#PrcEmailVerifybtn").attr("disabled",true);
                      $("#CofOtpBtn").attr("disabled",true);
                      $("#Cofverfybtn").attr("disabled",true);
                      $("#cofsendotpbtn").attr("disabled",true);
                      $("#cofverfyemailbtn").attr("disabled",true);
                      $("#AddCourse").attr("disabled",true);
                      $("#agree-terms").attr("disabled",true);
                      $('#gree-terms').prop('checked', true);
                      $("#save").attr("disabled",true); 
                      $("#save").css("display", "none");
                      $("#buttondelcourse").val("1");
                   }
                   else{
                      $("#PrcOtp").attr("disabled",false);
                      $('#PrcMoVerify').attr("disabled",false);
                      $("#PrcEmailbtn").attr("disabled",false);
                      $("#PrcEmailVerifybtn").attr("disabled",false);
                      $("#CofOtpBtn").attr("disabled",false);
                      $("#Cofverfybtn").attr("disabled",false);
                      $("#cofsendotpbtn").attr("disabled",false);
                      $("#cofverfyemailbtn").attr("disabled",false);
                      $("#AddCourse").attr("disabled",false);
                      
                      $("#save").attr("disabled",false);
                      $("#save").css("display", "block");
                      $("#agree-terms").attr("disabled",false);
                      $("#agree-terms").attr("checked",false);
                      $("#buttondelcourse").val("");
                   }
                });
             }else{
                $('#CenterNo').empty();
             }
             }
             });
       }else{
          $('#CenterNo').empty();
       }
    });

    $('#CfCoId').on('change', function() {
      var menu_id = $('#CfCoId').val();
      var CgId=$('#College').val();
      var buttondelcourse=$('#buttondelcourse').val();
      if(CgId=='')
       {
         $('#College-error').html('Please Select College..!');
       }
       else{
         $('#College-error').html('');
       }
       if(menu_id=='')
       {
         $('#CfCoId-error').html('Please Select Course..!');  
       }
       else{
         $('#CfCoId-error').html('');
       }
      
      if(menu_id && CgId) {
            $.ajax({
               url: 'GetCourse/'+CgId,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}"},
               dataType: "json",
               success:function(data) {
               var trHTML = '';
               $.each(data, function (i, item) {
                  var delid=item.id;
                  if(buttondelcourse==1)
                  {
                     trHTML += '<tr><td>' + item.CfCoId + '</td><td>' + item.InGovt + '</td><td>' + item.InAided + '</td><td>' + item.InUnAided + '</td><td>'+ item.AdGovt +'</td><td>'+item.AdAided+'</td><td>'+item.AdUnAided+'</td><td></td></tr>';
                  }
                  else{
                     trHTML += '<tr><td>' + item.CfCoId + '</td><td>' + item.InGovt + '</td><td>' + item.InAided + '</td><td>' + item.InUnAided + '</td><td>'+ item.AdGovt +'</td><td>'+item.AdAided+'</td><td>'+item.AdUnAided+'</td><td><input type="button" value="Delete" class="delete btn btn-danger btn-sm" data-id="'+item.id+'" ></td></tr>';
                  }
                    
                     //<a href="CourseDel/'+delid+'" class="btn btn-danger btn-sm delete" >Delete</a>
               });
               $('#mytable').html(trHTML);
            }
            });
      }else{
         //$('#CfCoId').empty();
      }
   });

    $('#PrcOtp').on('click', function() {
       var namestate = $('#state').val();
       var namedistrict=$('#District').val();
       var cgcode = $('#College').val();
       var CfCgPrinContact=$('#CfCgPrinContact').val();
       if(namestate=='')
       {
         $('#state-error').html('Please select State..!');
       }
       else{
         $('#state-error').html('');
       }
       if(namedistrict=='')
       {
         $('#District-error').html('Please select District..!');
       }
       else{
         $('#District-error').html('');
       }
       if(cgcode=='')
       {
         $('#College-error').html('Please Select College..!');
       }
       else{
         $('#College-error').html('');
       }
       if(CfCgPrinContact=='')
       {
         $('#MobileOtp-error').html('Please Enter Mobile Number..!');
       }
       else{
         $('#MobileOtp-error').html('');
       }
      
      if(cgcode && CfCgPrinContact) 
      {
            $.ajax({
               url: 'SendOtp/'+cgcode+'/'+CfCgPrinContact,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}"},
               dataType: "json",
               beforeSend: function() {
                  
                  $('.body_overlay').show();
               },
               success:function(data) {
                  if(data.mobileduplicate)
                  {
                     $('.body_overlay').hide();
                     $( '#CfCgPrinContact-error' ).html('Mobile Number is already exist..!');
                  }
                  else{
                     $( '#CfCgPrinContact-error' ).html('');
                  }
                  if(data.success)
                  {
                     $('.body_overlay').hide();
                     $('#CfCgPrinContact-error').html('OTP Sent Successfully').css("color","green");
                     //alert('Otp Send Successfully');
                     $('#PrcOtp').attr('disabled',true);
                  }
            }
            });
      }
       
    });

    $('#PrcMoVerify').on('click', function() {
      var namestate = $('#state').val();
      var namedistrict=$('#District').val();
      var PrcMoOtp = $('#PrcMoOtp').val();
      var CfCgPrinContact=$('#CfCgPrinContact').val();
      var CollegeId=$('#College').val();
       if(namestate=='')
       {
         $('#state-error').html('Please select State..!');
       }
       else{
         $('#state-error').html('');
       }
       if(namedistrict=='')
       {
         $('#District-error').html('Please select District..!');
       }
       else{
         $('#District-error').html('');
       }
       if(CollegeId=='')
       {
         $('#College-error').html('Please Select College..!');
       }
       else{
         $('#College-error').html('');
       }
       if(CfCgPrinContact=='')
       {
         $('#MobileOtp-error').html('Please Enter Mobile Number..!');
       }
       else{
         $('#MobileOtp-error').html('');
       }
       if(PrcMoOtp=='')
       {
         $('#PrcMoOtp-error').html('Please Enter OTP..!');
       }
       else
       {
         $('#PrcMoOtp-error').html('');  
       }
       if(PrcMoOtp) {
             $.ajax({
                url: 'VerifyPrcOtp/'+PrcMoOtp+'/'+CfCgPrinContact+'/'+CollegeId,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                beforeSend: function() {
                  
                  $('.body_overlay').show();
               },
                success:function(data) {
                   
                   if(data)
                   {
                      $.each(data, function(key, value){
                         if(value.MobileOtp==PrcMoOtp)
                         {
                           $('.body_overlay').hide();
                            $('#PrcMoOtp-error').html('Mobile Verified..!').css("color","green");
                            //alert('Mobile Verified..!');
                            $('#PrcMoVerify').attr('disabled',true);
                         }
                         else
                         {
                           $('.body_overlay').hide();
                           $('#PrcMoOtp-error').html('Incorrect OTP..!');
                           //alert('Incorrect OTP..!');
                         }
                      });
                   }
             }
             });
       }
       
    });

    $('#PrcEmailbtn').on('click', function() {
      var namestate = $('#state').val();
      var namedistrict=$('#District').val();
      var cgcode = $('#College').val();
      var CfCgPrinEmail=$('#CfCgPrinEmail').val();
      if(namestate=='')
      {
      $('#state-error').html('Please select State..!');
      }
      else{
      $('#state-error').html('');
      }
      if(namedistrict=='')
      {
      $('#District-error').html('Please select District..!');
      }
      else{
      $('#District-error').html('');
      }
      if(cgcode=='')
      {
      $('#College-error').html('Please Select College..!');
      }
      else{
      $('#College-error').html('');
      }
      if(CfCgPrinEmail=='')
       {
         $('#CfCgPrinEmail-error').html('Please Enter Email Id..!');
         return false;
       }
       else{
         $('#CfCgPrinEmail-error').html('');
       }
       
       if(cgcode) 
       {
             $.ajax({
                url: 'SendEmailOtp/'+cgcode+'/'+CfCgPrinEmail,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                beforeSend: function() {
                  
                  $('.body_overlay').show();
               },
                success:function(data) {
                  if(data.emailduplicate)
                  {
                    $('#CfCgPrinEmail-error').html('Email Id already exist..!');
                    $('.body_overlay').hide();
                  }
                  else{
                    $('#CfCgPrinEmail-error').html('');
                  }
                   if(data.success)
                   {
                      $('.body_overlay').hide();
                      //alert('Otp Send Successfully');
                      $('#CfCgPrinEmail-error').html('OTP Sent Successfully..!').css("color","green");
                      $('#PrcEmailbtn').attr('disabled',true);
                   }
             }
             });
       }
       
    });

    $('#PrcEmailVerifybtn').on('click', function() 
    {
      var namestate = $('#state').val();
      var namedistrict=$('#District').val();
       var PrcEmailotp = $('#PrcEmailotp').val();
       var CfCgPrinEmail=$('#CfCgPrinEmail').val();
       var CollegeId=$('#College').val();
       if(namestate=='')
       {
       $('#state-error').html('Please select State..!');
       }
       else{
       $('#state-error').html('');
       }
       if(namedistrict=='')
       {
       $('#District-error').html('Please select District..!');
       }
       else{
       $('#District-error').html('');
       }
       if(CollegeId=='')
       {
       $('#College-error').html('Please Select College..!');
       }
       else{
       $('#College-error').html('');
       }
       if(CfCgPrinEmail=='')
       {
         $('#CfCgPrinEmail-error').html('Please Enter Email Id..!');
       }
       else{
         $('#CfCgPrinEmail-error').html('');
       }
       if(PrcEmailotp=='')
       {
         $('#PrcEmailotp-error').html('Please Enter OTP..!');
       }
       else
       {
         $('#PrcEmailotp-error').html('');
       }
       if(PrcEmailotp) 
       {
             $.ajax({
                url: 'VerifyPrcEmailOtp/'+PrcEmailotp+'/'+CfCgPrinEmail+'/'+CollegeId,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                beforeSend: function() {
                  
                  $('.body_overlay').show();
               },
                success:function(data) {
                   
                   if(data)
                   {
                      $.each(data, function(key, value){
                         if(value.EmailOtp==PrcEmailotp)
                         {
                            $('.body_overlay').hide();
                            //alert('Email Id Is Verified..!');
                            $('#PrcEmailotp-error').html('Email Id Is Verified..!').css("color","green");
                            $('#PrcEmailVerifybtn').attr('disabled',true);
                         }
                         else
                         {
                           $('.body_overlay').hide();
                           $('#PrcEmailotp-error').html('Incorrect OTP..!');
                           //alert('Incorrect OTP..!');
                         }
                      });
                   }
             }
             });
       }
       
    });
    

    $('#CofOtpBtn').on('click', function() {
      var namestate = $('#state').val();
      var namedistrict=$('#District').val();
       var cgcode = $('#College').val();
       var CoFMobile=$('#CoFMobile').val();
       var CfCgPrinContact=$('#CfCgPrinContact').val();
       if(CfCgPrinContact==CoFMobile && CfCgPrinContact!='' && CoFMobile!='')
       {
         $('#CoFMobile-error').html('Mobile Number Already In Use..!');
         return false;
       }else{$('#CoFMobile-error').html('');}
       if(CfCgPrinContact=='')
       {
         $('#MobileOtp-error').html('Please Enter Mobile Number..!');
       }
       else{
         $('#MobileOtp-error').html('');
       }
       if(namestate=='')
       {
       $('#state-error').html('Please select State..!');
       }
       else{
       $('#state-error').html('');
       }
       if(namedistrict=='')
       {
       $('#District-error').html('Please select District..!');
       }
       else{
       $('#District-error').html('');
       }
       if(cgcode=='')
       {
       $('#College-error').html('Please Select College..!');
       }
       else{
       $('#College-error').html('');
       }
       if(CoFMobile=='')
       {
         $('#CoFMobile-error').html('Please Enter Mobile Number..!');
       }
       else{
         $('#CoFMobile-error').html('');
       }
       
       if(cgcode && CoFMobile) 
       {
             $.ajax({
                url: 'CoFSendMoOtp/'+cgcode+'/'+CoFMobile,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                beforeSend: function() {
                  
                  $('.body_overlay').show();
               },
                success:function(data) {
                   if(data.mobileduplicate)
                   {
                     $('.body_overlay').hide();
                     $('#CoFMobile-error').html('Mobile Number is Already exist..!');
                   }
                   else{
                     $('#CoFMobile-error').html('');
                   }
                   if(data.success)
                   {
                      $('.body_overlay').hide();
                      $('#CoFMobile-error').html('OTP Sent Successfully..!').css("color","green");
                      //alert('Otp Send Successfully');
                      $('#CofOtpBtn').attr('disabled',true);
                   }
             }
             });
       }
    });

    $('#Cofverfybtn').on('click', function() {
      var namestate = $('#state').val();
      var namedistrict=$('#District').val();
       var CofMoOtp = $('#CofMoOtp').val();
       var CoFMobile=$('#CoFMobile').val();
       var CollegeId=$('#College').val();
       if(namestate=='')
       {
       $('#state-error').html('Please select State..!');
       }
       else{
       $('#state-error').html('');
       }
       if(namedistrict=='')
       {
       $('#District-error').html('Please select District..!');
       }
       else{
       $('#District-error').html('');
       }
       if(CollegeId=='')
       {
       $('#College-error').html('Please Select College..!');
       }
       else{
       $('#College-error').html('');
       }
       if(CofMoOtp=='')
       {
         $('#CoMobileVerify-error').html('Please Enter OTP..!');
       }
       else{
         $('#CoMobileVerify-error').html('');
       }
       if(CoFMobile=='')
       {
         $('#CoFMobile-error').html('Please Mobile Number..!');
       }
       else{
         $('#CoFMobile-error').html('');
       }
       if(CofMoOtp) {
             $.ajax({
                url: 'VerifyCofOtp/'+CofMoOtp+'/'+CoFMobile+'/'+CollegeId,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                beforeSend: function() {
                  
                  $('.body_overlay').show();
               },
                success:function(data) {
                   
                   if(data)
                   {
                      $.each(data, function(key, value){
                         if(value.MobileOtp==CofMoOtp)
                         {
                           $('.body_overlay').hide(); 
                           $('#CoMobileVerify-error').html('Mobile Verified..!').css("color","green");
                           //alert('Mobile Verified..!');
                           $('#Cofverfybtn').attr('disabled',true);
                         }
                         else
                         {
                           $('.body_overlay').hide();
                           $('#CoMobileVerify-error').html('Incorrect OTP..!');
                           //alert('Incorrect OTP..!');
                         }
                      });
                   }
             }
             });
       }
       
    });


    $('#cofsendotpbtn').on('click', function() {
      var namestate = $('#state').val();
      var namedistrict=$('#District').val();
      var cgcode = $('#College').val();
      var CoFEmail=$('#CoFEmail').val();
      var CfCgPrinEmail=$('#CfCgPrinEmail').val();
      if(CfCgPrinEmail==CoFEmail && CfCgPrinEmail!='' && CoFEmail!='')
      {
         $('#CoFEmail-error').html('Email Id Already In Use..!');
         return false;
      }
      else{
         $('#CoFEmail-error').html('');
      }
      if(CfCgPrinEmail=='')
      {
        $('#CfCgPrinEmail-error').html('Please Enter Email Id..!');
      }
      else{
        $('#CfCgPrinEmail-error').html('');
      }
      if(namestate=='')
      {
      $('#state-error').html('Please select State..!');
      }
      else{
      $('#state-error').html('');
      }
      if(namedistrict=='')
      {
      $('#District-error').html('Please select District..!');
      }
      else{
      $('#District-error').html('');
      }
      if(cgcode=='')
      {
      $('#College-error').html('Please Select College..!');
      }
      else{
      $('#College-error').html('');
      }
      if(CoFEmail=='')
      {
      $('#CoFEmail-error').html('Please Enter Email Id..!');
      }
      else{
      $('#CoFEmail-error').html('');
      }
      
      if(cgcode && CoFEmail) 
      {
         $.ajax({
               url: 'SendCofEmailOtp/'+cgcode+'/'+CoFEmail,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}"},
               dataType: "json",
               beforeSend: function() {
                  
                  $('.body_overlay').show();
               },
               success:function(data) {
                  
                  if(data)
                  {
                     $('.body_overlay').hide();
                     $('#CoFEmail-error').html('OTP Sent Successfully.!').css("color","green");
                     //alert('Otp Send Successfully');
                     $('#cofsendotpbtn').attr('disabled',true);
                  }
            }
            });
      }
       
    });

    $('#cofverfyemailbtn').on('click', function() 
    {
      var namestate = $('#state').val();
      var namedistrict=$('#District').val();
       var cofotp = $('#cofotp').val();
       var CoFEmail=$('#CoFEmail').val();
       var CollegeId=$('#College').val();
       if(namestate=='')
       {
       $('#state-error').html('Please select State..!');
       }
       else{
       $('#state-error').html('');
       }
       if(namedistrict=='')
       {
       $('#District-error').html('Please select District..!');
       }
       else{
       $('#District-error').html('');
       }
       if(CollegeId=='')
       {
       $('#College-error').html('Please Select College..!');
       }
       else{
       $('#College-error').html('');
       }
       if(CoFEmail=='')
       {
       $('#CoFEmail-error').html('Please Enter Email Id..!');
       }
       else{
       $('#CoFEmail-error').html('');
       }
       if(cofotp=='')
       {
         $('#cofotp-error').html('Please Enter OTP..!');
       }
       else{
         $('#cofotp-error').html('');
       }
       if(cofotp && CoFEmail) 
       {
             $.ajax({
                url: 'Verifycofemailotp/'+cofotp+'/'+CoFEmail+'/'+CollegeId,
                type: "GET",
                data : {"_token":"{{ csrf_token() }}"},
                dataType: "json",
                beforeSend: function() {
                  
                  $('.body_overlay').show();
               },
                success:function(data) {
                   
                   if(data)
                   {
                      $.each(data, function(key, value){
                         if(value.EmailOtp==cofotp)
                         {
                            $('.body_overlay').hide();
                           $('#cofotp-error').html('Email Id Is Verified..!').css("color","green");
                            //alert('Email Id Is Verified..!');
                            $('#cofverfyemailbtn').attr('disabled',true);
                         }
                         else if(value.EmailOtp==null)
                         {
                           $('.body_overlay').hide();
                           $('#cofotp-error').html('Incorrect OTP..!');
                           //alert('Incorrect OTP..!');
                         }
                         else
                         {
                           $('.body_overlay').hide();
                           $('#cofotp-error').html('Incorrect OTP..!');
                           //alert('Incorrect OTP..!');
                         }
                      });
                   }
             }
             });
       }
    });


    $('#AddCourse').on('click', function() {
      var CfCoId = $('#CfCoId').val();
      var cgcode = $('#College').val();
      var InGovt=$('#InGovt').val();
      var InAided=$('#InAided').val();
      var InUnAided=$('#InUnAided').val();
      var AdGovt=$('#AdGovt').val();
      var AdAided=$('#AdAided').val();
      var AdUnAided=$('#AdUnAided').val();
      
      if(cgcode=='')
      {
      $('#College-error').html('Please Select College..!');
      }
      else{
      $('#College-error').html('');
      }
      if(CfCoId=='')
      {
      $('#CfCoId-error').html('Please select Course..!');
      }
      else{
      $('#CfCoId-error').html('');
      }
      if(InGovt=='')
      {
         $('#InGovt-error').html('Please Enter value..!');

      }
      else{
      $('#InGovt-error').html('');
      }
      
      if(InAided=='')
      {
      $('#InAided-error').html('Please Enter value..!');
      }
      else{
      $('#InAided-error').html('');
      }
      if(InUnAided=='')
      {
         $('#InUnAided-error').html('Please Enter value..!');
      }
      else{
         $('#InUnAided-error').html('');
      }
      if(AdGovt=='')
      {
         $('#AdGovt-error').html('Please Enter value..!');
      }
      else{
         $('#AdGovt-error').html('');
      }
      if(AdAided=='')
      {
         $('#AdAided-error').html('Please Enter value..!');
      }
      else{
         $('#AdAided-error').html('');
      }
      if(AdUnAided=='')
      {
         $('#AdUnAided-error').html('Please Enter value..!');
      }
      else{
         $('#AdUnAided-error').html('');
      }
      if(cgcode && CfCoId) 
      {
         $.ajax({
               url: 'AddCourse/'+cgcode+'/'+CfCoId+'/'+InGovt+'/'+InAided+'/'+InUnAided+'/'+AdGovt+'/'+AdAided+'/'+AdUnAided,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}"},
               dataType: "json",
               success:function(data) {
                  
                  if(data.success)
                  {
                     $('#CfCoId').val('');
                     $('#InGovt').val('');
                     $('#InAided').val('');
                     $('#InUnAided').val('');
                     $('#AdGovt').val('');
                     $('#AdAided').val('');
                     $('#AdUnAided').val('');
                     $('#success-course').removeClass('hide');
                     setInterval(function(){ 
                         $('#success-course').addClass('hide');
                     }, 3000);
                    
                     $.ajax({
                        url: 'GetCourse/'+cgcode,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data) {
                        var trHTML = '';
                        $.each(data, function (i, item) {
                           var delid=item.id;
                              trHTML += '<tr><td>' + item.CfCoId + '</td><td>' + item.InGovt + '</td><td>' + item.InAided + '</td><td>' + item.InUnAided + '</td><td>'+ item.AdGovt +'</td><td>'+item.AdAided+'</td><td>'+item.AdUnAided+'</td><td><input type="button" value="Delete" class="delete btn btn-danger btn-sm" data-id="'+item.id+'" ></td></tr>';
                              //<a href="CourseDel/'+delid+'" class="btn btn-danger btn-sm delete" >Delete</a>
                        });
                        $('#mytable').html(trHTML);
                     }
                     });

                  }
                  else if(data.duplicate)
                  {
                     $('#coursefail-msg').removeClass('hide');
                     setInterval(function(){ 
                         $('#coursefail-msg').addClass('hide');
                     }, 4000);
                  }
            }
            });
      }
       
    });
    

  });