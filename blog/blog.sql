-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2022 at 12:55 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_02_02_061953_create_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'Dr. Keenan Kuhn', 'Delectus aliquam ducimus qui dolores voluptates. Sit et ut architecto. Quidem voluptatem amet quia reprehenderit nobis. Quo minus hic quasi et. Eum accusamus et facere molestias impedit.', '2022-02-02 01:29:18', '2022-02-02 01:29:18'),
(2, 'Madie Bernhard', 'Id voluptates voluptatem incidunt magnam corrupti aut. Qui ab est eligendi eveniet non atque aliquam omnis. Commodi animi quae nobis est laudantium.', '2022-02-02 01:29:18', '2022-02-02 01:29:18'),
(3, 'Mr. Jarrell Russel DDS', 'Est voluptates velit est id. Rerum esse placeat quia veritatis ad. Quod eius est molestias aut amet itaque fuga.', '2022-02-02 01:29:18', '2022-02-02 01:29:18'),
(4, 'Susana King IV', 'Alias consequuntur officia quod tempora quaerat dolorem. Mollitia dolorem sit porro laboriosam. Corrupti tenetur sunt sint iusto omnis.', '2022-02-02 01:29:18', '2022-02-02 01:29:18'),
(5, 'Jedidiah Bradtke Jr.', 'Hic necessitatibus rerum quas quo earum et officia. Magnam incidunt omnis quo vel nostrum praesentium inventore aut. Consequuntur odio praesentium voluptatem sit est.', '2022-02-02 01:29:18', '2022-02-02 01:29:18'),
(6, 'Prof. Jocelyn Pacocha', 'Ea omnis fuga officia omnis quis qui itaque. Nulla quo ea repellat numquam fuga dolores. Ut explicabo voluptas eos officiis fugiat sit et. Consequuntur quod iusto occaecati accusantium.', '2022-02-02 01:29:28', '2022-02-02 01:29:28'),
(7, 'Emilie Gorczany III', 'Earum consequuntur ducimus ad ut. Ut quae sit accusamus ex. Officia ut error sint cumque reiciendis fugiat quasi eveniet. Consectetur ipsam est voluptates commodi illum.', '2022-02-02 01:29:28', '2022-02-02 01:29:28'),
(8, 'Kobe Hill', 'Aut aliquid a labore alias ut sit. Id praesentium nesciunt magni ut. Aliquam est pariatur delectus omnis officia officia.', '2022-02-02 01:29:28', '2022-02-02 01:29:28'),
(9, 'Murphy Bernhard II', 'Quod similique doloribus similique. Qui aspernatur rem nobis non illo velit.', '2022-02-02 01:29:28', '2022-02-02 01:29:28'),
(10, 'Sarai White DDS', 'Consequatur voluptatem fugit voluptatem molestiae nostrum ut. Minus voluptatem porro sed ullam voluptatibus.', '2022-02-02 01:29:28', '2022-02-02 01:29:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `is_admin`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@itsolutionstuff.com', NULL, 1, '$2y$10$74juTDOk.dLUXFxJGZl1VuDPkXifeEcODiatQtqzOjQNpWTEQF.1K', NULL, '2022-02-02 01:50:22', '2022-02-02 01:50:22'),
(2, 'User', 'user@itsolutionstuff.com', NULL, 0, '$2y$10$pBZ8aLs7EjDdKWTOdWE/QeLr0lc0qyFwcmJ.zBMcjx6TxSWoK.BDG', NULL, '2022-02-02 01:50:22', '2022-02-02 01:50:22'),
(3, 'abcd', 'abcd@gmail.com', NULL, NULL, '$2y$10$Jy5lrWkbRkl2J62f53P4UOeftpjjdx7ictD2qrsT6cJ98JDksjKO.', 'oHMEbmh9torIbefwWMvHTAca8j5j5nz6gUhJ0Wk3tMMTlv7cqYl0OqCFadys', '2022-02-02 05:19:35', '2022-02-02 05:19:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
