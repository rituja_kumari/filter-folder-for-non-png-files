<?php

include('database.php');

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<style>

  body {
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 30px;
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}
  </style>
</head>     
<body>

<!--===== dependent dropdown form============-->
<div class="container">
  <div class="dependent-dropdown">
  <form autocomplete="off" action="">
    <div class="input-field">Select Country:
    <select id="country" class="form-control">
      <option value="">Select Country</option>
      
  <?php

          $contryData="SELECT id, name from countries";
          $result=mysqli_query($conn,$contryData);
      
          if(mysqli_num_rows($result)>0)
          {
            while($arr=mysqli_fetch_assoc($result))
            {
              // print_r($arr);
                
  ?>

          <option value="<?php echo $arr['id'];?>"><?php echo $arr['name'];?></option>
              
  <?php
  }} 
  ?>

    </select>
    
    </div>
    <div class="input-field">Select State:
      <select id="state" class="form-control">
      <option value="">State</option>
    </select>
    </div>
    <div class="input-field">Select City:
      <select id="city" class="form-control">
      <option value="">City </option>
    </select>
    </div>
    
  </form>
  </div>
</div>
<!--===== dependent dropdown form============-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="ajax-script.js" type="text/javascript"></script>
</body>
</html>