<?php

use Faker\Generator as Faker;
use App\News;
$factory->define(News::class, function (Faker $faker) {
    return [
        // 'id' => $faker->id,

        'title' => $faker->title,

        'description' => $this->faker->paragraph,

        'author' => $this->faker->name,
        
        'image' => $faker->image

    ];
});
